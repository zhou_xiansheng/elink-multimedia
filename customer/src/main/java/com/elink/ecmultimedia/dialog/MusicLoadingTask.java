package com.elink.ecmultimedia.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;

import com.elink.ecmultimedia.thread.ScanHandler;
import com.elink.ecmultimedia.ui.main.myview.CustomDialog;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MediaConstants;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.ecmultimedia.utils.Worker;
import com.elink.elinkmultimediaservices.constants.common.EUsbStatus;

/**
 * 弹出加载框，及加载框消失后的任务
 */
public class MusicLoadingTask extends AsyncTask<Void, Void, Void> {
    private CustomDialog alertDialog;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private String msg;
    @SuppressLint("StaticFieldLeak")
    private MultimediaMainActivity mainActivity;

    public MusicLoadingTask(Context context, String msg) {
        this.context = context;
        this.msg = msg;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        alertDialog = new CustomDialog(context, msg);
        mainActivity = MyApplication.getMainActivity();
        alertDialog.show();
        LogUtils.d("加载框显示");
    }

    @Override
    protected Void doInBackground(Void... params) {
        ScanHandler scanHandler = ScanHandler.get(MediaConstants.path);
        scanHandler.lock();
        scanHandler.unlock();
        // 后台加载完了
        if (mainActivity.musicPlayer.mUsbStatus== EUsbStatus.INSERT) {
            // 需要判断一下 ，可能是 u 盘拔出导致的
            Handler handler = mainActivity.worker.handler;
            Message message = handler.obtainMessage(Worker.USB_PLAY_ALL);
            handler.sendMessage(message);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
            LogUtils.d("加载框消失");
        }
    }
}