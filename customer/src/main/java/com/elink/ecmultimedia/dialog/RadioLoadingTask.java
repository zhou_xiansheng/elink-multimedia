package com.elink.ecmultimedia.dialog;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.elink.ecmultimedia.thread.ScanHandler;
import com.elink.ecmultimedia.ui.main.myview.CustomDialog;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.utils.LogUtils;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class RadioLoadingTask extends AsyncTask<Void, Void, Void> {

    private Lock lock = new ReentrantLock();
    //private AlertDialog alertDialog;
    private CustomDialog alertDialog;
    private final Context context;
    private String msg;


    public RadioLoadingTask(Context context, String msg) {
        this.context = context;
        this.msg = msg;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        alertDialog = new CustomDialog(context, msg);
        alertDialog.show();
        LogUtils.d("加载框显示");
    }

    @Override
    protected Void doInBackground(Void... params) {
        // 这里会在后台线程中执行，并非主线程，这里等待搜索结束后释放锁
        lock();
        unlock();
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
            LogUtils.d("加载框消失");
        }
    }

    public void lock() {
        LogUtils.d("等待锁");
        lock.lock();
        LogUtils.d("持有锁");
    }

    public void unlock() {
        try {
            lock.unlock();
            LogUtils.d("释放锁");
        } catch (Exception e) {
            LogUtils.w("未持有锁!!!");
        }
    }
}