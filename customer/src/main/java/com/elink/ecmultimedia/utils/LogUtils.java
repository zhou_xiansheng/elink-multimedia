package com.elink.ecmultimedia.utils;

import android.util.Log;

import java.util.HashMap;
import java.util.Locale;

public class LogUtils {
    public static final String TAG = "MEC_";

    //log输出等级
    private static final int LOG_LEVEL_DEBUG = 1;
    private static final int LOG_LEVEL_INFO = 2;
    private static final int LOG_LEVEL_WARN = 3;
    private static final int LOG_LEVEL_ERROR = 4;
    private static int logLevel = LOG_LEVEL_ERROR;
    //是否只要输出某一种等级log
    private static boolean IS_SINGLE_LOG = false;
    private static int SINGLE_LOG = LOG_LEVEL_DEBUG;
    //log详细程度
    private static final int GRADE1 = 1;
    private static final int GRADE2 = 2;
    private static final int GRADE3 = 3;
    private static int LOG_DETAILED_GRADE = GRADE3;
    private static final HashMap<String, String> sCachedTag = new HashMap<>();

    public static void d(String message) {
        if (IS_SINGLE_LOG && SINGLE_LOG == LOG_LEVEL_DEBUG) {
            Log.d(buildTag(), buildMessage(message, LOG_DETAILED_GRADE));
        } else if (logLevel >= LOG_LEVEL_DEBUG) {
            Log.d(buildTag(), buildMessage(message, LOG_DETAILED_GRADE));
        }
    }

    public static void i(String message) {
        if (IS_SINGLE_LOG && SINGLE_LOG == LOG_LEVEL_INFO) {
            Log.i(buildTag(), buildMessage(message, LOG_DETAILED_GRADE));
        } else if (logLevel >= LOG_LEVEL_INFO) {
            Log.i(buildTag(), buildMessage(message, LOG_DETAILED_GRADE));
        }
    }

    public static void w(String message) {
        if (IS_SINGLE_LOG && SINGLE_LOG == LOG_LEVEL_WARN) {
            Log.w(buildTag(), buildMessage(message, LOG_DETAILED_GRADE));
        } else if (logLevel >= LOG_LEVEL_WARN) {
            Log.w(buildTag(), buildMessage(message, LOG_DETAILED_GRADE));
        }
    }

    public static void e(String message, Exception e) {
        if (IS_SINGLE_LOG && SINGLE_LOG == LOG_LEVEL_ERROR) {
            Log.e(buildTag(), buildMessage(message, LOG_DETAILED_GRADE), e);
        } else if (logLevel >= LOG_LEVEL_ERROR) {
            Log.e(buildTag(), buildMessage(message, LOG_DETAILED_GRADE), e);
        }
    }

    private static String buildTag() {
        String key = String.format(Locale.US, "%s@%s", TAG, Thread.currentThread().getName());
        if (!sCachedTag.containsKey(key))
            sCachedTag.put(key, String.format(Locale.US, "[%s(%s)]", TAG, Thread.currentThread().getName()));
        return sCachedTag.get(key);
    }

    private static String buildMessage(String message, int logDetailedGrade) {
        StackTraceElement[] traceElements = Thread.currentThread().getStackTrace();

        if (traceElements.length < 4) {
            return message;
        }
        StackTraceElement traceElement = traceElements[4];
        String res = "";
        if (logDetailedGrade == GRADE1) {
            res = String.format(Locale.US, "[%s]",
                    message
            );
        } else if (logDetailedGrade == GRADE2) {
            res = String.format(Locale.US, "[%s][%s]",
                    traceElement.getMethodName(),
                    message
            );
        } else if (logDetailedGrade == GRADE3) {
            res = String.format(Locale.US, "[%s.%s][%s]",
                    traceElement.getClassName().substring(traceElement.getClassName().lastIndexOf(".") + 1),
                    traceElement.getMethodName(),
                    message
            );
        } else {
            res = String.format(Locale.US, "[%s.%s(%s:%d) %s]",
                    traceElement.getClassName().substring(traceElement.getClassName().lastIndexOf(".") + 1),
                    traceElement.getMethodName(),
                    traceElement.getFileName(),
                    traceElement.getLineNumber(),
                    message
            );
        }
        return res;
    }

}