package com.elink.ecmultimedia.utils;

import android.annotation.SuppressLint;

public class TimeUtil {

    @SuppressLint("DefaultLocale")
    public static String formatTime(long millis, long all) {
        int totalSeconds = (int) millis / 1000;
        int s = totalSeconds % 60;
        int totalMinutes = totalSeconds / 60;
        int m = totalMinutes % 60;
        int h = totalMinutes / 60;

        // 判断总时长决定格式
        if (((int) all / 1000) / 60 / 60 > 0) {
            return String.format("%02d:%02d:%02d", h, m, s);
        } else {
            return String.format("%02d:%02d", totalMinutes, s);
        }
    }

}
