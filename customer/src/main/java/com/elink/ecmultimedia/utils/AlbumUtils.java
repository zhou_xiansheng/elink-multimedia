package com.elink.ecmultimedia.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;

import com.bumptech.glide.Glide;
import com.elink.ecmultimedia.R;
import com.elink.ecmultimedia.cache.MusicCache;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.common.EUsbStatus;

import java.io.File;
import java.io.IOException;

public class AlbumUtils {

    public static void setAlbumCover(MusicFile music, ImageView musicAlbum) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            loadAlbumUpQByRetriever(music, musicAlbum);
        } else {
            loadAlbumUnderQByRetriever(music, musicAlbum);
        }
    }

    public static void setUsbListAlbumCover(MusicFile music, ImageView musicAlbum) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            loadAlbumUpQByRetriever(music, musicAlbum);
        } else {
            loadAlbumUnderQByRetriever(music, musicAlbum);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.Q)
    public static void loadAlbumUpQByRetriever(MusicFile music, ImageView musicAlbum) {
        byte[] albumCover = null;
        EUsbStatus usbStatus = MyApplication.getMainActivity().musicPlayer.mUsbStatus;
        if (music != null && EUsbStatus.isStateUseFull(usbStatus)) {
            albumCover = MusicCache.getAlbumFromCache(music.path);
            File file = new File(music.path);
            if (albumCover == null && file.exists() && file.isFile()) {
                MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                try {
                    retriever.setDataSource(music.path);
                    albumCover = retriever.getEmbeddedPicture();
                    MusicCache.addAlbumCache(music.path, albumCover);
                } catch (Exception e) {
                    LogUtils.e("文件加载错误", e);
                }
                try {
                    retriever.close();
                } catch (IOException e) {
                    LogUtils.e("专辑图片加载错误", e);
                }
            }
        } else if (usbStatus != EUsbStatus.DISCONNECT) {
            LogUtils.d("usb 拔出 ！");
        }
        if (albumCover != null && albumCover.length > 0) {
            setAlbumByBitmap(albumCover, musicAlbum);
        } else {
            setAlbumDefault(musicAlbum);
        }
    }


    public static void loadAlbumUnderQByRetriever(MusicFile music, ImageView musicAlbum) {
        byte[] albumCover = null;
        if (music != null) {
            albumCover = MusicCache.getAlbumFromCache(music.path);
            File file = new File(music.path);
            if (albumCover == null && file.exists() && file.isFile()) {
                try {
                    MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                    retriever.setDataSource(music.path);
                    albumCover = retriever.getEmbeddedPicture();
                    MusicCache.addAlbumCache(music.path, albumCover);
                    retriever.release();
                } catch (Exception e) {
                    LogUtils.e(e.getMessage(), e);
                }
            }
        }
        if (albumCover != null && albumCover.length > 0) {
            setAlbumByGlide(albumCover, musicAlbum);
        } else {
            setAlbumDefault(musicAlbum);
        }
    }

    public static void setAlbumByGlide(byte[] albumCover, ImageView musicAlbum) {
        MyApplication.getMainActivity().runOnUiThread(() -> Glide.with(musicAlbum)
                .asBitmap()
                .load(albumCover)
                .error(R.drawable.music_album_default)
                .into(musicAlbum));
    }

    public static void setAlbumByBitmap(byte[] albumCover, ImageView musicAlbum) {
        final Bitmap bitmap = BitmapFactory.decodeByteArray(albumCover, 0, albumCover.length);
        MyApplication.getMainActivity().runOnUiThread(() -> musicAlbum.setImageBitmap(bitmap));
    }

    public static void setAlbumDefault(ImageView musicAlbum) {
        MyApplication.getMainActivity().runOnUiThread(() -> musicAlbum.setImageResource(R.drawable.music_album_default));
    }
}
