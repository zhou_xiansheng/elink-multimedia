package com.elink.ecmultimedia.utils;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.view.View;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

public class Utils {

    private static final String[] PERMISSIONS = {
            "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.WRITE_EXTERNAL_STORAGE"};


    /**
     * 权限申请
     *
     * @param activity
     */
    public void verifyStoragePermissions(Activity activity) {
        int permission;
        try {
            ArrayList<String> arrayList = new ArrayList<>();
            for (int i = 0; i < PERMISSIONS.length; i++) {
                permission = ActivityCompat.checkSelfPermission(activity, PERMISSIONS[i]);
                if (permission != PackageManager.PERMISSION_GRANTED) {
                    arrayList.add(PERMISSIONS[i]);
                }
            }
            if (arrayList.size() > 0) {
                arrayList.toArray();
                String[] strings = new String[arrayList.size()];
                for (int i = 0; i < arrayList.size(); i++) {
                    strings[i] = arrayList.get(i);
                }
                ActivityCompat.requestPermissions(activity, strings, 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 将活动和状态栏相融合
     */
    public static void mergeWithStatusBar(Activity activity) {
        View decorView = activity.getWindow().getDecorView();
        //设置活动布局将显示在状态栏上
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        //设置状态栏颜色为透明
        activity.getWindow().setStatusBarColor(Color.TRANSPARENT);
    }

    public boolean isAddAndNotStateSaved(Fragment fragment) {
        return fragment.isAdded() && !fragment.isStateSaved();
    }
}
