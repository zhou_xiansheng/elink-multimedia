package com.elink.ecmultimedia.utils;

import android.util.Log;

public class UnitUtils {
    public static class Time {
        public static int ms2s(long time) {
            return (int) ((time / 1000) + 0.5);
        }
    }

    public static class Channel {
        /**
         * 浮点频点转int频点
         *
         * @param freq_f 浮点数频点
         * @return
         */
        public static int fm_f2I(float freq_f) {
            int result = (int) ((freq_f + 0.0000001) * 10 + 0.5);
//            LogUtils.d(TAG,"fm_f2I: original=" + freq_f + ", result=" + result);
            return result;
        }

        /**
         * int频点转float频点
         *
         * @param freq_i int频点
         * @return
         */
        public static float fm_i2F(int freq_i) {
            float result = freq_i / 10.0f;
            LogUtils.d("result:" + result);
            return result;
        }
    }
}
