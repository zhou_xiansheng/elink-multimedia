package com.elink.ecmultimedia.utils;

import android.util.DisplayMetrics;
import android.util.Log;

/**
 * 分辨率适配
 */
public class ResolutionAdaptation {
    //歌词相关
    public static int relativePositionToTop;//当前歌词滚动相对于顶部的位置
    //FM绘制
    public static Float mThickness;//刻度粗细
    public static Float mMainThickness;
    public static int mHeight1;//刻度线高度
    public static int mHeight2;
    public static int mHeight3;
    public static int mHeight4;
    public static int mHeight5;
    public static Float mFontSize;//刻度字体大小
    public static int mFontOffsetY;


    public static void doAdaptation() {
        DisplayMetrics displayMetrics = MyApplication.getContext().getResources().getDisplayMetrics();
        int widthPixels = displayMetrics.widthPixels;
        int heightPixels = displayMetrics.heightPixels;
        LogUtils.d("widthPixels:" + widthPixels + " heightPixels:" + heightPixels);
        if (widthPixels >= 2000 && heightPixels >= 1200) {
            relativePositionToTop = 320;
            setThickness(4f, 8f);
            setHeight(80, 70, 60, 50, 40);
            setFontSize(30f);
            setFontOffsetY(60);
        } else if (widthPixels >= 1560 && heightPixels >= 720) {
            relativePositionToTop = 120;
            setThickness(2f, 4f);
            setHeight(30, 25, 20, 15, 10);
            setFontSize(20f);
            setFontOffsetY(40);
        } else if (widthPixels >= 1024 && heightPixels >= 600) {
            relativePositionToTop = 100;
            setThickness(2f, 4f);
            setHeight(30, 25, 20, 15, 10);
            setFontSize(20f);
            setFontOffsetY(40);
        } else {
            relativePositionToTop = 100;
            setThickness(2f, 4f);
            setHeight(50, 40, 30, 20, 10);
            setFontSize(20f);
            setFontOffsetY(40);
        }
    }

    private static void setThickness(Float thickness, Float mainThickness) {
        mThickness = thickness;
        mMainThickness = mainThickness;
    }

    private static void setHeight(int height1, int height2, int height3, int height4, int height5) {
        mHeight1 = height1;
        mHeight2 = height2;
        mHeight3 = height3;
        mHeight4 = height4;
        mHeight5 = height5;
    }

    private static void setFontSize(Float fontSize) {
        mFontSize = fontSize;
    }

    private static void setFontOffsetY(int fontOffsetY) {
        mFontOffsetY = fontOffsetY;
    }
}
