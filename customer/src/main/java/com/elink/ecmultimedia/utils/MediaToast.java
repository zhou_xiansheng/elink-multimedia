package com.elink.ecmultimedia.utils;

import android.widget.Toast;

import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;

public class MediaToast {
    private static final Toast toast = Toast.makeText(MyApplication.getContext(), "", Toast.LENGTH_SHORT);

    private static MultimediaMainActivity mainActivity;

    public static void show(String msg) {
        if (mainActivity == null)
            mainActivity = MyApplication.getMainActivity();
        if (mainActivity != null) {
            toast.setText(msg);
            toast.show();
        } else {
            LogUtils.w("mainActivity null !");
        }

    }

    public static void show(int resId) {
        if (mainActivity == null)
            mainActivity = MyApplication.getMainActivity();
        if (mainActivity != null) {
            toast.setText(resId);
            toast.show();
        } else {
            LogUtils.w("mainActivity null !");
        }
    }
}
