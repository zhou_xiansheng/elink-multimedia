package com.elink.ecmultimedia.utils;


import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import androidx.annotation.NonNull;

import com.elink.ecmultimedia.ui.main.fragment.music.MusicLeftPlayingFragment;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;

import java.util.ArrayList;
import java.util.List;

public class Worker {
    public final static int USB_PLAY_ALL = 1;//usb全部播放 处理

    private final HandlerThread thread;
    public Handler handler;

    public Worker() {
        // 在调用父类构造函数之前初始化 thread
        thread = new HandlerThread("Handlers");
        thread.start();
        handler = new Handler(thread.getLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                int what = msg.what;
                switch (what) {
                    case USB_PLAY_ALL:
                        LogUtils.d("usb 全部播放：" + MediaConstants.path);
                        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
                        boolean isCant = false;
                        if (!mainActivity.canUsbPlay())
                            return;
                        try {
                            List<MusicFile> mediaFileList = UsbDirectoryUtils.getMusicFile(MediaConstants.path);
                            //筛选支持的音频格式
                            ArrayList<MusicFile> musicFiles = new ArrayList<>();
                            for (MusicFile music : mediaFileList
                            ) {
                                String[] split = music.fileName.split("\\.");
                                if (MusicUtils.typeAdaptation(split[split.length - 1])) {
                                    musicFiles.add(music);
                                } else {
                                    isCant = true;
                                }
                            }
                            if (musicFiles.size() > 0) {
                                //修改服务端播放列表的数据
                                mainActivity.serviceConnection.setMusicCollection(musicFiles);
                                if (isCant) {
                                    MediaToast.show("已经过滤调不支持格式的音频");
                                }
                                //播放第一首歌曲
                                MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
                                if (musicLeftPlayingFragment != null) {
                                    musicLeftPlayingFragment.usbPlaying(musicFiles.get(0));
                                    musicLeftPlayingFragment.updateDataAndNotify();
                                }
                                //修改服务端下标信息
                                mainActivity.serviceConnection.setPlayIndex(0);
                            } else {
                                MediaToast.show("当前目录下无音频文件或无支持的格式");
                            }
                        } catch (Exception e) {
                            LogUtils.e(e.getMessage(), e);
                        }
                        break;
                    default:
                        LogUtils.d("未知请求码！");
                }
            }
        };
    }
}

