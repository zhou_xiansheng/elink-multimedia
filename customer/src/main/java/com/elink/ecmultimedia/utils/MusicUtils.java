package com.elink.ecmultimedia.utils;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.ObjectKey;
import com.elink.ecmultimedia.R;
import com.elink.ecmultimedia.cache.MusicCache;
import com.elink.ecmultimedia.pojo.Lyric;
import com.elink.elinkmultimediaservices.beans.MusicFile;

import org.mozilla.universalchardet.UniversalDetector;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MusicUtils {

    @SuppressLint("StaticFieldLeak")
    public static Context context = MyApplication.getContext();
    private static final ArrayList<String> types = new ArrayList<>();

    static {
        //默认支持
        types.add("mp3");
        types.add("aac");
        types.add("amr");
        types.add("flac");
        types.add("midi");
        types.add("vorbis");
        types.add("opus");
        types.add("wav");
        types.add("alac");
        types.add("ogg");
    }

    /**
     * 检测文本编码
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static String detectCharset(File file) throws IOException {
        String encoding = null;
        FileInputStream fis = null;
        try {
            byte[] buf = new byte[4096];
            fis = new FileInputStream(file);
            UniversalDetector detector = new UniversalDetector(null);
            int nread;
            while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
                detector.handleData(buf, 0, nread);
            }
            detector.dataEnd();
            encoding = detector.getDetectedCharset();
        } catch (Exception e) {
            LogUtils.e(e.getMessage(), e);
        } finally {
            if (fis != null) {
                fis.close();
            }
        }
        return encoding;
    }

    /**
     * 歌词路径获取
     * 需要的时候再获取
     *
     * @param music
     * @return
     */
    private static String getLyricPath(MusicFile music) {
        String res = "";
        if (music.lyricPath == null) {
            // 从音乐文件路径中获取文件名，不包含扩展名
            String fileNameWithoutExtension = new File(music.path).getName().replaceFirst("[.][^.]+$", "");
            // 获取文件所在的目录
            File musicDirectory = new File(music.path).getParentFile();
            if (musicDirectory != null && musicDirectory.exists()) {
                File[] filesInDir = musicDirectory.listFiles();
                if (filesInDir != null) {
                    for (File file : filesInDir) {
                        music.lyricPath = "";
                        // 如果文件是一个歌词文件，且包含音乐名，可以不相同包含就行
                        String filename = file.getName().replaceFirst("[.][^.]+$", "");
                        if (filename.equals(fileNameWithoutExtension) && file.getName().endsWith(".lrc")) {
                            res = file.getAbsolutePath();
                            music.lyricPath = res;
                            break;
                        }
                    }
                }
            }
        } else {
            res = music.lyricPath;
        }
        return res;
    }

    /**
     * 读取歌词文件
     *
     * @param lyricPath
     * @return
     * @throws IOException
     */
    private static String getMusicLyric(String lyricPath) {
        if (lyricPath != null) {
            BufferedReader br = null;
            try {
                File file = new File(lyricPath);
                if (file.exists()) {
                    String charset = detectCharset(file);
                    if (charset == null) {
                        charset = "UTF-8";  // 默认使用 UTF-8，如果检测不到编码
                    }
                    br = new BufferedReader(new InputStreamReader(Files.newInputStream(file.toPath()), charset));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line).append("\n");
                    }
                    return sb.toString();
                }
            } catch (Exception e) {
                LogUtils.e(e.getMessage(), e);
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        LogUtils.e(e.getMessage(), e);
                    }
                }
            }
        }
        return null;
    }


    /**
     * 解析歌词文本成数组
     *
     * @param lrcContent
     * @return
     */
    @SuppressLint("SuspiciousIndentation")
    private static ArrayList<Lyric> parseLyricsByLyricContent(String lrcContent) {
        ArrayList<Lyric> lyrics = new ArrayList<>();
        if (lrcContent != null) {
            String[] lines = lrcContent.split("\n");

            Pattern timePattern = Pattern.compile("\\[(\\d+):(\\d+(?:\\.\\d+)?)\\]");
            for (String line : lines) {
                Matcher timeMatcher = timePattern.matcher(line);
                ArrayList<Long> times = new ArrayList<>();

                while (timeMatcher.find()) {
                    long minute = Long.parseLong(Objects.requireNonNull(timeMatcher.group(1)));
                    float second = Float.parseFloat(Objects.requireNonNull(timeMatcher.group(2)));
                    long time = (long) (minute * 60 * 1000 + second * 1000);
                    times.add(time);
                }
                String content = line.replaceAll("\\[\\d+:\\d+(?:\\.\\d+)?\\]", "").trim();
                if (!content.trim().equals(""))
                    for (Long time : times) {
                        lyrics.add(new Lyric(time, content));
                    }
            }

        }
        Collections.sort(lyrics);
        return lyrics;
    }

    /**
     * 读取并解析歌词
     * 实际对外提供的类
     *
     * @param musicFile
     * @return
     */
    public static ArrayList<Lyric> parseLyricsByLyricPath(MusicFile musicFile) {
        LogUtils.d(musicFile.path);
        ArrayList<Lyric> res;
        String lyricPath = getLyricPath(musicFile);
        String musicLyric = getMusicLyric(lyricPath);
        res = parseLyricsByLyricContent(musicLyric);
        return res;
    }

    /**
     * 找到下一个滚动的歌词下标
     *
     * @param time
     * @return
     */
    public static int findPositionForTime(List<Lyric> lyricList, long time) {
        for (int i = 0; i < lyricList.size(); i++) {
            if (lyricList.get(i).time > time) {
                return i - 1;
            }
        }
        return lyricList.size() - 1;
    }

    public static boolean typeAdaptation(String type) {
        //这里适配其他格式的音频，适配后添加到列表中
        return types.contains(type.trim().toLowerCase());
    }
}
