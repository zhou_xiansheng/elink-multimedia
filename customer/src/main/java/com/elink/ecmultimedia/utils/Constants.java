package com.elink.ecmultimedia.utils;

public class Constants {
    public static final int table1 = 1;
    // 广播 类型
    public static final int SCAN_FF = -3;
    public static final int SCAN_F = -1;
    public static final int DETACHED = -4;
    public static final int ATTACHED = 4;
    public static final int SCAN_FINISH = 3;
    // usb 挂载
    public static final int UNMOUNTED = 0;
    public static final int MOUNTED = 2;
    public static final int EJECTING = 5;
}
