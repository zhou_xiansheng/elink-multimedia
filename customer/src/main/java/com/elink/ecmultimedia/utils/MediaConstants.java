package com.elink.ecmultimedia.utils;

public class MediaConstants {

    /**
     * usb音乐目录相关
     */
    public static String path = "/mnt/media_rw";//当前目录

    public static final String rootPath = "/mnt/media_rw";//根，可以用来作为返回的条件

    /**
     * 音乐类型
     */
    public static final String BT_MUSIC = "蓝牙音乐";
    public static final String USB_MUSIC = "USB音乐";
    public static final String HISTORY_MUSIC = "最近播放";

    public static final String PLAY_ALL = "全部播放";
}
