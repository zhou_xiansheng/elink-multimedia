package com.elink.ecmultimedia.utils;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.net.Uri;

import com.elink.ecmultimedia.cache.MusicCache;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.common.EUsbStatus;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;

public class UsbDirectoryUtils {

    private static final String COLUMN_ID = "id";
    private static final String COLUMN_FILE_NAME = "fileName";
    private static final String COLUMN_FILE_PATH = "filePath";
    private static final String COLUMN_FILE_TYPE = "fileType";
    private static final String COLUMN_CLASSIFY = "classify";
    private static final String COLUMN_ARTIST = "artist";
    private static final String COLUMN_ALBUM_COVER = "albumCover";
    private static final String COLUMN_DURATION = "duration";


    public static List<MusicFile> getContentFile(String path) {
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        EUsbStatus mUsbStatus = mainActivity.musicPlayer.mUsbStatus;
        ArrayList<MusicFile> musicFiles = new ArrayList<>();
        ArrayList<MusicFile> contentByPathFromCache = MusicCache.getContentByPathFromCache(path);
        if (mUsbStatus == EUsbStatus.INSERT) {
            musicFiles = (ArrayList<MusicFile>) UsbDirectoryUtils.getContentByPath(path);
            MusicCache.addContentByPathToCache(path, musicFiles);
        } else if (mUsbStatus == EUsbStatus.SCANNED) {
            if (contentByPathFromCache.size() > 0) {
                musicFiles = contentByPathFromCache;
            } else {
                musicFiles = (ArrayList<MusicFile>) UsbDirectoryUtils.getContentByPath(path);
                MusicCache.addContentByPathToCache(path, musicFiles);
            }
        }
        LogUtils.d("size:" + musicFiles.size());
        return musicFiles;
    }

    public static List<MusicFile> getMusicFile(String path) {
        List<MusicFile> musicFiles = MusicCache.getMusicByPathFromCache(path);
        if (musicFiles.size() == 0) {
            List<MusicFile> files = getContentFile(path);
            for (int i = 0; i < files.size(); i++) {
                if (!files.get(i).isDir) {
                    musicFiles.add(files.get(i));
                }
            }
        }
        LogUtils.d("size:" + musicFiles.size());
        return musicFiles;
    }

    @SuppressLint({"Range", "Recycle"})
    public static List<MusicFile> getContentByPath(String directoryPath) {
        LogUtils.d("directoryPath:" + directoryPath);
        LinkedHashSet<MusicFile> directories = new LinkedHashSet<>();
        ArrayList<MusicFile> musics = new ArrayList<>();
        for (int i = 0; i < MusicCache.musicFiles.size(); i++) {
            MusicFile musicFile = MusicCache.musicFiles.get(i);
            String path = musicFile.path;
            if (path.contains(directoryPath)) {
                MusicFile music = new MusicFile();
                String relativePath = path.substring(directoryPath.length() + 1);
                if (relativePath.contains("/")) {
                    String name = relativePath.split("/")[0];
                    music.isDir = true;
                    music.fileName = name;
                    music.path = directoryPath + "/" + name;
                    directories.add(music);
                } else {
                    music.isDir = false;
                    music.path = musicFile.path;
                    music.fileName = musicFile.fileName;
                    music.duration = musicFile.duration;
                    music.artist = musicFile.artist;
                    music.playSource = musicFile.playSource;
                    musics.add(music);
                }
            }
        }
        ArrayList<MusicFile> directoryAndMusic = new ArrayList<>(directories);
        //将音乐排序
        musics.sort(Comparator.comparing(o -> o.fileName));
        directoryAndMusic.addAll(musics);
        LogUtils.d("directories:" + directories.size() + " musics:" + musics.size());
        return directoryAndMusic;
    }

    /**
     * 获取新增的数据，从上次数据下表开始
     *
     * @param index
     * @return
     */
    @SuppressLint("Range")
    public static List<MusicFile> getNewlyAddedMusic(int index) {
        ArrayList<MusicFile> musics = new ArrayList<>();
        Uri uri = Uri.parse("content://com.example.udiskprovider/music/1");
        Uri queryUri = uri.buildUpon()
                .appendQueryParameter("offset", String.valueOf(index))
                .appendQueryParameter("limit", String.valueOf(1000))//大于一次增加的数量
                .build();
        try (Cursor cursor = MyApplication.getContext().getContentResolver().query(queryUri, null, null,
                null, null)) {
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToPosition(index);
                while (cursor.moveToNext()) {
                    MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
                    if (mainActivity.musicPlayer.mUsbStatus == EUsbStatus.DISCONNECT) {
                        LogUtils.d("usb 拔出 ");
                        return new ArrayList<>();
                    }
                    //时长不为 -1 ,且后缀进行过滤
                    MusicFile music = new MusicFile();
                    music.path = cursor.getString(cursor.getColumnIndex(COLUMN_FILE_PATH));
                    if (music.artist == null
                            || music.artist.trim().equals("<unknown>")
                            || music.artist.trim().equals("UNKNOWN")
                            || music.artist.trim().equals("")) {
                        music.artist = "未知歌手";
                    }
                    music.isDir = false;
                    music.fileName = cursor.getString(cursor.getColumnIndex(COLUMN_FILE_NAME));
                    music.duration = cursor.getInt(cursor.getColumnIndex(COLUMN_DURATION));
                    music.artist = cursor.getString(cursor.getColumnIndex(COLUMN_ARTIST));
                    music.playSource = IPlaySource.USB;

                    musics.add(music);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return musics;
    }

    private static final ArrayList<String> types = new ArrayList<>();

    static {
        //默认支持
        types.add("mp3");
        types.add("aac");
        types.add("amr");
        types.add("flac");
        types.add("midi");
        types.add("vorbis");
        types.add("opus");
        types.add("wav");
        types.add("alac");

        //扩展
        types.add("ogg");
    }

    public static boolean typeAdaptation(String type) {
        return types.contains(type.trim().toLowerCase());
    }

    /**
     * 获得 usb 的根目录
     *
     * @return
     */
    @SuppressLint("Range")
    public static String getRootPath() {
        Uri uri = Uri.parse("content://com.example.udiskprovider/music/1");
        String root = null;
        try (Cursor cursor = MyApplication.getContext()
                .getContentResolver()
                .query(uri, null, null, null, null)) {
            if (cursor != null && cursor.moveToNext()) {
                String path = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_FILE_PATH));
                String[] split = path.split("/");
                if (split.length >= 3)
                    root = "/" + split[1] + "/" + split[2];
            } else {
                LogUtils.d("cursor null");
            }
        } catch (Exception e) {
            LogUtils.e(e.getMessage(), e);
        }
        return root;
    }


}
