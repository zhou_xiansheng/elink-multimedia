package com.elink.ecmultimedia.utils;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

import com.elink.ecmultimedia.adapter.radio.CollectionListAdapter;
import com.elink.ecmultimedia.adapter.music.MusicRightUSBListAdapter;
import com.elink.ecmultimedia.connection.MediaConnection;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicBigPlayCollectListFragment;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicFragment;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicLeftPlayingFragment;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicRightMenuListFragment;
import com.elink.ecmultimedia.ui.main.fragment.radio.RadioSearchListFragment;
import com.elink.ecmultimedia.ui.main.myview.CustomerSeekBar;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.adapter.radio.SearchListAdapter;
import com.elink.ecmultimedia.ui.main.fragment.radio.RadioFragment;
import com.elink.ecmultimedia.listener.RadioSeekBarListener;
import com.elink.ecmultimedia.ui.main.view.MusicBigPlayingActivity;

import org.litepal.LitePal;


public class MyApplication extends Application {


    private static RadioSearchListFragment radioSearchListFragment;
    private static MusicFragment musicFragment;
    @SuppressLint("StaticFieldLeak")
    private static Context context;
    private static MediaConnection mediaConnection;

    private static MusicBigPlayCollectListFragment musicBigPlayCollectListFragment;
    private static SearchListAdapter searchListAdapter;

    private static CollectionListAdapter collectionListAdapter;
    private static MusicRightUSBListAdapter musicRightUSBListAdapter;

    private static MusicLeftPlayingFragment musicLeftPlayingFragment;
    private static MusicBigPlayingActivity musicBigPlayingActivity;

    //MusicRightMenuListFragment
    private static MusicRightMenuListFragment musicRightMenuListFragment;

    @SuppressLint("StaticFieldLeak")
    private static RadioFragment radioFragment;

    private static CustomerSeekBar customerSeekBar;

    public static MultimediaMainActivity multimediaMainActivity;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        LitePal.initialize(this);//初始化 LitePal
    }

    public static MusicRightMenuListFragment getMusicRightMenuListFragment() {
        return musicRightMenuListFragment;
    }

    public static void setMusicRightMenuListFragment(MusicRightMenuListFragment musicRightMenuListFragment) {
        MyApplication.musicRightMenuListFragment = musicRightMenuListFragment;
    }


    public static MediaConnection getMusicServiceConnection() {
        return mediaConnection;
    }

    public static void setMusicServiceConnection(MediaConnection mediaConnection) {
        MyApplication.mediaConnection = mediaConnection;
    }


    public static RadioSearchListFragment getRadioSearchListFragment() {
        return radioSearchListFragment;
    }


    public static void setRadioSearchListFragment(RadioSearchListFragment radioSearchListFragment) {
        MyApplication.radioSearchListFragment = radioSearchListFragment;
    }

    public static MusicBigPlayCollectListFragment getMusicBigPlayListFragment() {
        return musicBigPlayCollectListFragment;
    }

    public static void setMusicBigPlayListFragment(MusicBigPlayCollectListFragment musicBigPlayCollectListFragment) {
        MyApplication.musicBigPlayCollectListFragment = musicBigPlayCollectListFragment;
    }

    public static MusicBigPlayingActivity getMusicBigPlayingActivity() {
        return musicBigPlayingActivity;
    }

    public static void setMusicBigPlayingActivity(MusicBigPlayingActivity musicBigPlayingActivity) {
        MyApplication.musicBigPlayingActivity = musicBigPlayingActivity;
    }


    public static MusicLeftPlayingFragment getMusicLeftPlayingFragment() {
        return musicLeftPlayingFragment;
    }

    public static void setMusicLeftPlayingFragment(MusicLeftPlayingFragment musicLeftPlayingFragment) {
        MyApplication.musicLeftPlayingFragment = musicLeftPlayingFragment;
    }

    public static MusicFragment getMusicFragment() {
        return musicFragment;
    }

    public static void setMusicFragment(MusicFragment musicFragment) {
        MyApplication.musicFragment = musicFragment;
    }

    public static MusicRightUSBListAdapter getMusicRightListAdapter() {
        return musicRightUSBListAdapter;
    }

    public static void setMusicRightListAdapter(MusicRightUSBListAdapter musicRightUSBListAdapter) {
        MyApplication.musicRightUSBListAdapter = musicRightUSBListAdapter;
    }

    public static void setContext(Context context) {
        MyApplication.context = context;
    }

    public static SearchListAdapter getRadioListAdapter() {
        return searchListAdapter;
    }

    public static void setRadioListAdapter(SearchListAdapter searchListAdapter) {
        MyApplication.searchListAdapter = searchListAdapter;
    }

    public static RadioSeekBarListener getSeekBarChangeListener() {
        return seekBarChangeListener;
    }

    public static void setSeekBarChangeListener(RadioSeekBarListener seekBarChangeListener) {
        MyApplication.seekBarChangeListener = seekBarChangeListener;
    }

    public static CollectionListAdapter getCollectionRadioAdapter() {
        return collectionListAdapter;
    }

    public static void setCollectionRadioAdapter(CollectionListAdapter collectionListAdapter) {
        MyApplication.collectionListAdapter = collectionListAdapter;
    }

    public static RadioSeekBarListener seekBarChangeListener;

    public static MultimediaMainActivity getMainActivity() {
        return multimediaMainActivity;
    }

    public static void setMainActivity(MultimediaMainActivity multimediaMainActivity) {
        MyApplication.multimediaMainActivity = multimediaMainActivity;
    }

    private static String TAG = "MyApplication";

    public static CustomerSeekBar getCustomerSeekBar() {
        return customerSeekBar;
    }

    public static void setCustomerSeekBar(CustomerSeekBar customerSeekBar) {
        MyApplication.customerSeekBar = customerSeekBar;
    }


    public static Context getContext() {
        return context;
    }

    public static RadioFragment getRadioFragment() {
        return radioFragment;
    }

    public static void setRadioFragment(RadioFragment w) {
        radioFragment = w;
    }
}
