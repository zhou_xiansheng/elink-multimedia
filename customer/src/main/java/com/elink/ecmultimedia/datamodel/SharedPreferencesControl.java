package com.elink.ecmultimedia.datamodel;

import android.content.Context;
import android.content.SharedPreferences;

import com.elink.ecmultimedia.R;
import com.elink.ecmultimedia.pojo.Radio;

public class SharedPreferencesControl {

    /*-------------------------------当前 fm 操作----------------------------------*/
    public static Radio getRadioNow(Context context) {
        Radio radio = new Radio();
        SharedPreferences sharedPreferences = context.getSharedPreferences("radioNow", Context.MODE_PRIVATE);
        radio.id = sharedPreferences.getInt("id", 0);
        radio.i = sharedPreferences.getInt("i", 3);
        radio.type = sharedPreferences.getString("type", "FM");
        radio.typeName = sharedPreferences.getString("typeName", "电台");
        radio.radioId = sharedPreferences.getFloat("radioId", 87.5f);
        radio.thumbPos = sharedPreferences.getInt("thumbPos", 0);
        radio.isCollect = sharedPreferences.getInt("isCollect", 0);
        return radio;
    }

    public static void updateRadioNow(Context context, Radio radio) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("radioNow", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt("id", radio.id);
        edit.putInt("i", radio.i);
        edit.putString("type", radio.type);
        edit.putString("typeName", radio.typeName);
        edit.putFloat("radioId", radio.radioId);
        edit.putInt("thumbPos", radio.thumbPos);
        edit.putInt("isCollect", radio.isCollect);
        edit.apply();//最后需要进行提交
    }
}
