package com.elink.ecmultimedia.datamodel;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.elink.ecmultimedia.pojo.Radio;
import com.elink.ecmultimedia.utils.LogUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class RadioOperations {
    // 根据 id 获取数据
    @SuppressLint("Range")
    public static Radio getSearchRadioById(Context context, int id) {
        LogUtils.d("");
        SQLiteDatabase db = new RadioDatabaseHelper(context).getReadableDatabase();

        Cursor cursor = db.query(RadioDatabaseHelper.RADIO_SEARCH,
                null,
                "id=?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null);
        if (cursor != null)
            cursor.moveToFirst();
        else
            return null;
        Radio radio = new Radio();
        radio.id = cursor.getInt(cursor.getColumnIndex("id"));
        radio.i = cursor.getInt(cursor.getColumnIndex("i"));
        radio.thumbPos = cursor.getInt(cursor.getColumnIndex("thumbPos"));
        radio.radioId = cursor.getFloat(cursor.getColumnIndex("radioId"));
        radio.type = cursor.getString(cursor.getColumnIndex("type"));
        radio.typeName = cursor.getString(cursor.getColumnIndex("typeName"));
        radio.isCollect = cursor.getInt(cursor.getColumnIndex("isCollect"));

        cursor.close();
        db.close();

        return radio;
    }


    /*------------------------------收藏列表相关--------------------------------------*/
    @SuppressLint("Range")
    public static void insertRadioAtPenultimatePosition(Context context, Radio newRadio) {
        LogUtils.d("");
        SQLiteDatabase db = new RadioDatabaseHelper(context).getWritableDatabase();
        db.beginTransaction();

        try {
            // 1. 将最后一个查询并保存
            String selectLastRadioQuery = "SELECT * FROM " + RadioDatabaseHelper.RADIO_SEARCH + " ORDER BY id DESC LIMIT 1";
            Cursor cursor = db.rawQuery(selectLastRadioQuery, null);
            Radio lastRadio = null;
            if (cursor.moveToFirst()) {
                lastRadio = new Radio();
                lastRadio.id = cursor.getInt(cursor.getColumnIndex("id"));
                lastRadio.i = cursor.getInt(cursor.getColumnIndex("i"));
                lastRadio.thumbPos = cursor.getInt(cursor.getColumnIndex("thumbPos"));
                lastRadio.radioId = cursor.getFloat(cursor.getColumnIndex("radioId"));
                lastRadio.type = cursor.getString(cursor.getColumnIndex("type"));
                lastRadio.typeName = cursor.getString(cursor.getColumnIndex("typeName"));
                lastRadio.isCollect = cursor.getInt(cursor.getColumnIndex("isCollect"));
            }
            cursor.close();

            if (lastRadio != null) {
                // 2. 删除最后一个
                db.delete(RadioDatabaseHelper.RADIO_COLLECT, "id=?", new String[]{String.valueOf(lastRadio.id)});

                // 3. 插入新的
                ContentValues values = new ContentValues();
                values.put("i", newRadio.i);
                values.put("thumbPos", newRadio.thumbPos);
                values.put("radioId", newRadio.radioId);
                values.put("type", newRadio.type);
                values.put("typeName", newRadio.typeName);
                values.put("isCollect", newRadio.isCollect);
                db.insert(RadioDatabaseHelper.RADIO_COLLECT, null, values);

                // 4. 插入之前保存的
                ContentValues lastValues = new ContentValues();
                lastValues.put("i", lastRadio.i);
                lastValues.put("thumbPos", lastRadio.thumbPos);
                lastValues.put("radioId", lastRadio.radioId);
                lastValues.put("type", lastRadio.type);
                lastValues.put("typeName", lastRadio.typeName);
                lastValues.put("isCollect", lastRadio.isCollect);
                db.insert(RadioDatabaseHelper.RADIO_COLLECT, null, lastValues);
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public static void deleteCollectRadioByThumbPos(Context context, int thumbPos) {
        LogUtils.d("");
        SQLiteDatabase db = new RadioDatabaseHelper(context).getWritableDatabase();
        int delete = db.delete(RadioDatabaseHelper.RADIO_COLLECT,
                "thumbPos=?",
                new String[]{String.valueOf(thumbPos)});
        LogUtils.d("delete:"+delete);
        db.close();
    }

    /**
     * 删除表中最后一条数据
     *
     * @param context
     */
    @SuppressLint("Range")
    public static void deleteLastRadio(Context context) {
        LogUtils.d("");
        SQLiteDatabase db = new RadioDatabaseHelper(context).getWritableDatabase();

        String selectMaxIdQuery = "SELECT MAX(id) AS max_id FROM " + RadioDatabaseHelper.RADIO_COLLECT;
        Cursor cursor = db.rawQuery(selectMaxIdQuery, null);

        if (cursor.moveToFirst()) {
            int maxId = cursor.getInt(cursor.getColumnIndex("max_id"));
            db.delete(RadioDatabaseHelper.RADIO_COLLECT,
                    "id=?",
                    new String[]{String.valueOf(maxId)});
        }

        cursor.close();
        db.close();
    }

    public static long insertCollectRadio(Context context, Radio radio) {
        LogUtils.d("");
        SQLiteDatabase db = new RadioDatabaseHelper(context).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("i", radio.i);
        values.put("thumbPos", radio.thumbPos);
        values.put("radioId", radio.radioId);
        values.put("type", radio.type);
        values.put("typeName", radio.typeName);
        values.put("isCollect", radio.isCollect);
        long id = db.insert(RadioDatabaseHelper.RADIO_COLLECT,
                null,
                values);
        db.close();
        return id;
    }

    // 更新信息
    public static int updateCollectRadio(Context context, Radio radio) {
        LogUtils.d("radio:"+radio);
        SQLiteDatabase db = new RadioDatabaseHelper(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("i", radio.i);
        values.put("thumbPos", radio.thumbPos);
        values.put("radioId", radio.radioId);
        values.put("type", radio.type);
        values.put("typeName", radio.typeName);
        values.put("isCollect", radio.isCollect);

        int rowsUpdated = db.update(RadioDatabaseHelper.RADIO_COLLECT,
                values,
                "thumbPos=?",
                new String[]{String.valueOf(radio.thumbPos)});
        db.close();

        return rowsUpdated;
    }

    /**
     * 更新最后一条信息
     *
     * @param context
     * @param radio
     * @return
     */
    @SuppressLint("Range")
    public static int updateLastCollectRadio(Context context, Radio radio) {
        LogUtils.d("radio:"+radio);
        SQLiteDatabase db = new RadioDatabaseHelper(context).getWritableDatabase();

        String selectMaxIdQuery = "SELECT MAX(id) AS max_id FROM " + RadioDatabaseHelper.RADIO_COLLECT;
        Cursor cursor = db.rawQuery(selectMaxIdQuery, null);
        int update = 0;
        if (cursor.moveToFirst()) {
            int maxId = cursor.getInt(cursor.getColumnIndex("max_id"));

            ContentValues values = new ContentValues();
            values.put("i", radio.i);
            values.put("thumbPos", radio.thumbPos);
            values.put("radioId", radio.radioId);
            values.put("type", radio.type);
            values.put("typeName", radio.typeName);
            values.put("isCollect", radio.isCollect);

            update = db.update(RadioDatabaseHelper.RADIO_COLLECT, values, "id=?", new String[]{String.valueOf(maxId)});
        }

        cursor.close();
        db.close();
        return update;
    }

    // 获取搜索列表
    @SuppressLint("Range")
    public static LinkedList<Radio> getAllCollectRadios(Context context) {
        LogUtils.d("");
        LinkedList<Radio> radios = new LinkedList<>();
        SQLiteDatabase db = new RadioDatabaseHelper(context).getReadableDatabase();

        String selectQuery = "SELECT * FROM " + RadioDatabaseHelper.RADIO_COLLECT;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Radio radio = new Radio();
                radio.id = cursor.getInt(cursor.getColumnIndex("id"));
                radio.i = cursor.getInt(cursor.getColumnIndex("i"));
                radio.thumbPos = cursor.getInt(cursor.getColumnIndex("thumbPos"));
                radio.radioId = cursor.getFloat(cursor.getColumnIndex("radioId"));
                radio.type = cursor.getString(cursor.getColumnIndex("type"));
                radio.typeName = cursor.getString(cursor.getColumnIndex("typeName"));
                radio.isCollect = cursor.getInt(cursor.getColumnIndex("isCollect"));

                radios.add(radio);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return radios;
    }

    // 清空搜索列表
    public static void clearCollectRadios(Context context) {
        LogUtils.d("");
        SQLiteDatabase db = new RadioDatabaseHelper(context).getWritableDatabase();
        db.delete(RadioDatabaseHelper.RADIO_COLLECT, null, null);
        db.close();
    }

    /*------------------------------搜索列表相关--------------------------------------*/

    public static long insertSearchRadio(Context context, Radio radio) {
        LogUtils.d("");
        SQLiteDatabase db = new RadioDatabaseHelper(context).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("i", radio.i);
        values.put("thumbPos", radio.thumbPos);
        values.put("radioId", radio.radioId);
        values.put("type", radio.type);
        values.put("typeName", radio.typeName);
        values.put("isCollect", radio.isCollect);
        long id = db.insert(RadioDatabaseHelper.RADIO_SEARCH,
                null,
                values);
        db.close();
        return id;
    }

    // 清空搜索列表
    public static void clearSearchRadios(Context context) {
        LogUtils.d("");
        try (RadioDatabaseHelper radioDatabaseHelper = new RadioDatabaseHelper(context)) {
            SQLiteDatabase writableDatabase = radioDatabaseHelper.getWritableDatabase();
            writableDatabase.delete(RadioDatabaseHelper.RADIO_SEARCH, null, null);
            writableDatabase.close();
        }
    }

    // 将所有搜索数据插入
    public static void insertAllSearchRadios(Context context, List<Radio> radios) {
        LogUtils.d("");
        SQLiteDatabase db = new RadioDatabaseHelper(context).getWritableDatabase();
        db.beginTransaction();
        try {
            for (Radio radio : radios) {
                ContentValues values = new ContentValues();
                values.put("i", radio.i);
                values.put("thumbPos", radio.thumbPos);
                values.put("radioId", radio.radioId);
                values.put("type", radio.type);
                values.put("typeName", radio.typeName);
                values.put("isCollect", radio.isCollect);
                db.insert(RadioDatabaseHelper.RADIO_SEARCH, null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    // 获取搜索列表
    @SuppressLint("Range")
    public static ArrayList<Radio> getAllSearchRadios(Context context) {
        LogUtils.d("");
        ArrayList<Radio> radios = new ArrayList<>();
        SQLiteDatabase db = new RadioDatabaseHelper(context).getReadableDatabase();

        String selectQuery = "SELECT * FROM " + RadioDatabaseHelper.RADIO_SEARCH;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Radio radio = new Radio();
                radio.id = cursor.getInt(cursor.getColumnIndex("id"));
                radio.i = cursor.getInt(cursor.getColumnIndex("i"));
                radio.thumbPos = cursor.getInt(cursor.getColumnIndex("thumbPos"));
                radio.radioId = cursor.getFloat(cursor.getColumnIndex("radioId"));
                radio.type = cursor.getString(cursor.getColumnIndex("type"));
                radio.typeName = cursor.getString(cursor.getColumnIndex("typeName"));
                radio.isCollect = cursor.getInt(cursor.getColumnIndex("isCollect"));

                radios.add(radio);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return radios;
    }

    /**
     * 替换表中的所有数据
     *
     * @param context
     * @param radios
     */
    public static void updateAllSearchRadio(Context context, List<Radio> radios) {
        LogUtils.d("");
        clearSearchRadios(context);
        insertAllSearchRadios(context, radios);
    }

    /**
     * @param context
     * @param radio
     * @return
     */
    @SuppressLint("Range")
    public static int updateSearchRadio(Context context, Radio radio) {
        LogUtils.d("");
        SQLiteDatabase db = new RadioDatabaseHelper(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("i", radio.i);
        values.put("thumbPos", radio.thumbPos);
        values.put("radioId", radio.radioId);
        values.put("type", radio.type);
        values.put("typeName", radio.typeName);
        values.put("isCollect", radio.isCollect);
        int update = db.update(RadioDatabaseHelper.RADIO_SEARCH, values, "thumbPos=?", new String[]{String.valueOf(radio.thumbPos)});

        db.close();
        return update;
    }


}
