package com.elink.ecmultimedia.datamodel;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class RadioDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "radiodatabase.db";
    private static final int DATABASE_VERSION = 2;
    public static final String RADIO_COLLECT = "radiocollect";//收藏列表
    public static final String RADIO_SEARCH = "radiosearch";//搜索列表


    private static final String KEY_ID = "id";
    private static final String KEY_I = "i";
    private static final String KEY_THUMB_POS = "thumbPos";
    private static final String KEY_RADIO_ID = "radioId";
    private static final String KEY_TYPE = "type";
    private static final String KEY_TYPE_NAME = "typeName";
    private static final String KEY_IS_COLLECT = "isCollect";
    private static final String CREATE_TABLE_RADIO_SEARCH = "CREATE TABLE " + RADIO_SEARCH + "("
            + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_I + " INTEGER,"
            + KEY_THUMB_POS + " INTEGER,"
            + KEY_RADIO_ID + " REAL,"
            + KEY_TYPE + " TEXT,"
            + KEY_TYPE_NAME + " TEXT,"
            + KEY_IS_COLLECT + " INTEGER" + ")";
    private static final String CREATE_TABLE_RADIO_COLLECT = "CREATE TABLE " + RADIO_COLLECT + "("
            + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_I + " INTEGER,"
            + KEY_THUMB_POS + " INTEGER,"
            + KEY_RADIO_ID + " REAL,"
            + KEY_TYPE + " TEXT,"
            + KEY_TYPE_NAME + " TEXT,"
            + KEY_IS_COLLECT + " INTEGER" + ")";


    public RadioDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_RADIO_SEARCH);
        db.execSQL(CREATE_TABLE_RADIO_COLLECT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP TABLE IF EXISTS " + RADIO_SEARCH);
        db.execSQL(" DROP TABLE IF EXISTS " + RADIO_COLLECT);
        onCreate(db);
    }
}

