package com.elink.ecmultimedia.ui.main.myview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * 自定义 textview
 * 解决当歌曲名滚动时，点击当前页面其他碎片失去焦点，滚动重新开始的问题
 */
@SuppressLint("AppCompatCustomView")
public class MusicNameView extends TextView {

    public MusicNameView(Context context) {
        super(context);
    }

    public MusicNameView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MusicNameView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public boolean isFocused() {
        return true;
    }
}
