package com.elink.ecmultimedia.ui.main.myview;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.elink.ecmultimedia.R;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MyApplication;

public class CustomDialog extends Dialog {
    private final String msg;
    private boolean isShow = false;

    public CustomDialog(@NonNull Context context, String msg) {
        super(context, R.style.TransparentAlertDialogTheme);
        this.msg = msg;
        init();
    }

    private void init() {
        // 设置对话框窗口属性
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager.LayoutParams params = window.getAttributes();
            params.dimAmount = 0.0f; // 设置背景模糊程度为0，即不变暗
            params.gravity = Gravity.CENTER;
            window.setAttributes(params);
        }
        setCancelable(false);
        // 设置对话框布局
        setContentView(R.layout.custom_loading_dialog);
        // 在布局中找到控件，设置其他属性...
        TextView textView = findViewById(R.id.textView);
        textView.setText(msg);
    }

    @Override
    public void onBackPressed() {
        LogUtils.d("");
        super.onBackPressed();
        MyApplication.getMainActivity().moveTaskToBack(false);
    }

    public void show() {
        if (!isShow){
            super.show();
            isShow = true;
        }
    }

    public void disMiss() {
        if (isShow){
            super.dismiss();
            isShow = false;
        }
    }
}

