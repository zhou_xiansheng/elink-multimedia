package com.elink.ecmultimedia.ui.main.fragment.music;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.elink.ecmultimedia.adapter.music.MusicRightHistoryListAdapter;
import com.elink.ecmultimedia.adapter.music.MusicRightUSBListAdapter;
import com.elink.ecmultimedia.callbacks.DiffUtilCallBack;
import com.elink.ecmultimedia.connection.MediaConnection;
import com.elink.ecmultimedia.pojo.Switch;
import com.elink.ecmultimedia.thread.ScanHandler;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MediaConstants;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.ecmultimedia.R;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.common.EAudioSource;
import com.elink.elinkmultimediaservices.constants.common.EUsbStatus;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.locks.ReentrantLock;

public class MusicRightMenuListFragment extends Fragment implements View.OnClickListener {
    private MultimediaMainActivity mainActivity;
    private MusicFragment musicFragment;
    private LinearLayout layout_bluetooth;
    private RelativeLayout layout_usb;
    private RelativeLayout layout_history;
    private LinearLayout layout_menu;
    private View now;


    /*---------------------------- 菜单 ------------------------------*/

    private LinearLayout musicFromUSB;
    private LinearLayout musicFromHistory;
    private LinearLayout musicFromBluetooth;

    /*---------------------------- 蓝牙 ------------------------------*/
    private LinearLayout layoutBtConnect;
    private Button btConnect;
    private TextView btName;
    /*---------------------------- usb ------------------------------*/

    private RecyclerView usbRecyclerView;
    public ArrayList<MusicFile> mUsbDataList;
    public MusicRightUSBListAdapter musicRightUSBListAdapter;
    public LinearLayout layoutReadPrompt;
    public LinearLayout layoutLodePrompt;
    public LinearLayout layoutNoUsbPrompt;

    /*---------------------------- 历史记录 ------------------------------*/
    private RecyclerView historyRecyclerView;
    public final LinkedList<MusicFile> mHistoryDataList = new LinkedList<>();

    public MusicRightHistoryListAdapter rightHistoryListAdapter;

    private LinearLayout layoutNoHistoryPrompt;

    public MusicRightMenuListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_music_right_menu_list, container, false);
        LogUtils.d("");
        MyApplication.setMusicRightMenuListFragment(this);
        musicFragment = MyApplication.getMusicFragment();
        mainActivity = MyApplication.getMainActivity();
        layout_bluetooth = view.findViewById(R.id.layout_bt);
        layout_usb = view.findViewById(R.id.layout_usb);
        layout_history = view.findViewById(R.id.layout_history);
        layout_menu = view.findViewById(R.id.layout_menu);
        now = layout_menu;
        createMenu(view);
        createBt(view);
        createUsb(view);
        createHistory(view);
        return view;
    }

    @Override
    public void onResume() {
        if (mainActivity.aSwitch.musicOrRadio == Switch.PATH_MUSIC) {
            LogUtils.d("path:" + mainActivity.musicPath[0]);
            if (mainActivity.musicPlayer.resumePage != IPlaySource.UNKNOWN
                    || mainActivity.musicPlayer.resumePlay != IPlaySource.UNKNOWN) {// 延迟后跳转，等服务链接上
                if (MultimediaMainActivity.firstResume) {
                    //刚开机还没连接上服务，需要延迟一会
                    MultimediaMainActivity.firstResume = false;
                    new Handler(Looper.getMainLooper()).postDelayed(this::pageResume, 500);
                } else {
                    pageResume();
                }
            } else if (layout_menu.getVisibility() == View.VISIBLE) {
                initMenu();
            } else if (layout_bluetooth.getVisibility() == View.VISIBLE) {
                initBt();
            } else if (layout_usb.getVisibility() == View.VISIBLE) {
                initUsb();
            } else if (layout_history.getVisibility() == View.VISIBLE) {
                initHistory();
            }
        }
        super.onResume();
    }

    public void pageResume() {
        LogUtils.d("");
        //先回到菜单界面，然后进入对应界面，然后播放

        MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
        if (mainActivity.musicPlayer.resumePage == IPlaySource.BT) {
            toMenu();
            forceToBt();
        } else if (mainActivity.musicPlayer.resumePage == IPlaySource.USB) {
            toMenu();
            forceToUsb();
        }

        if (mainActivity.musicPlayer.resumePlay == IPlaySource.BT)
            musicLeftPlayingFragment.btPlaying();
        else if (mainActivity.musicPlayer.resumePlay == IPlaySource.USB)
            musicLeftPlayingFragment.usbPlaying();

        mainActivity.musicPlayer.resumePlay = IPlaySource.UNKNOWN;
        mainActivity.musicPlayer.resumePage = IPlaySource.UNKNOWN;
    }


    private void createMenu(View view) {
        musicFromUSB = view.findViewById(R.id.music_from_usb);
        musicFromBluetooth = view.findViewById(R.id.music_from_bluetooth);
        musicFromHistory = view.findViewById(R.id.music_from_history);
        musicFromUSB.setOnClickListener(this);
        musicFromHistory.setOnClickListener(this);
        musicFromBluetooth.setOnClickListener(this);
    }

    private void createBt(View view) {
        //获取组件
        layoutBtConnect = view.findViewById(R.id.layout_btconnect);
        btConnect = view.findViewById(R.id.btn_connect);
        btName = view.findViewById(R.id.bt_name);
        mainActivity = MyApplication.getMainActivity();
        btConnect.setOnClickListener(this);
    }

    private void createUsb(View view) {
        //获取组件实例
        usbRecyclerView = view.findViewById(R.id.music_right_recycle_view);
        layoutReadPrompt = view.findViewById(R.id.layout_read_prompt);
        layoutLodePrompt = view.findViewById(R.id.layout_lode_prompt);
        layoutNoUsbPrompt = view.findViewById(R.id.layout_no_usb_prompt);
        //指定 recycleView 的布局方式。这里采用网格布局，设置两列
        mUsbDataList = new ArrayList<>();
        usbRecyclerView.setLayoutManager(new GridLayoutManager(requireContext(), 2));
        musicRightUSBListAdapter = new MusicRightUSBListAdapter(mUsbDataList);
        ((SimpleItemAnimator) Objects.requireNonNull(usbRecyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        usbRecyclerView.setAdapter(musicRightUSBListAdapter);
        //将本碎片设置到全局环境中
        MyApplication.setMusicRightListAdapter(musicRightUSBListAdapter);
    }

    private void createHistory(View view) {
        //组件获取
        historyRecyclerView = view.findViewById(R.id.music_history);
        layoutNoHistoryPrompt = view.findViewById(R.id.layout_no_history_prompt);
        mainActivity = MyApplication.getMainActivity();
        musicFragment = MyApplication.getMusicFragment();

        /*列表相关*/
        //设置布局方式
        historyRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        rightHistoryListAdapter = new MusicRightHistoryListAdapter(mHistoryDataList);
        ((SimpleItemAnimator) Objects.requireNonNull(historyRecyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        historyRecyclerView.setAdapter(rightHistoryListAdapter);
    }

    private void initMenu() {
        LogUtils.d("");
        mainActivity.aSwitch.musicRight = Switch.PATH_MENU;
        mainActivity.musicPlayer.isEditor = false;
        mainActivity.musicPath[0] = "";
        mainActivity.musicPath[1] = "";
        MediaConstants.path = MediaConstants.rootPath;
        musicFragment.invisibleViews();
    }

    public void initBt() {
        mainActivity.musicPath[0] = MediaConstants.BT_MUSIC;
        mainActivity.aSwitch.musicRight = Switch.PATH_BT;
        boolean isConnect = mainActivity.serviceConnection.checkBtConnect();
        MusicFragment musicFragment = MyApplication.getMusicFragment();
        if (isConnect) {
            MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
            String deviceName = mainActivity.serviceConnection.getBtName();
            layoutBtConnect.setVisibility(View.GONE);
            btName.setVisibility(View.VISIBLE);
            btName.setText(deviceName);
            LogUtils.d("蓝牙已经连接" + deviceName);
            if (mainActivity.aSwitch.musicLeft == 1) {// 如果是 usb 播放列表 ，将其反转过来
                musicLeftPlayingFragment.toPlay();
            }
            if (!musicFragment.isHidden()) {
                if (!mainActivity.canBtPlay())
                    return;
                mainActivity.setAudioSources(EAudioSource.AUDIO_SOURCE_BT); //修改服务端状态，切换播放器
                if (!musicFragment.isHidden()) {
                    musicLeftPlayingFragment.btPlaying();
                }
            }
        } else {
            LogUtils.d("蓝牙未连接");
            layoutBtConnect.setVisibility(View.VISIBLE);
            btName.setVisibility(View.GONE);
        }
    }


    public void initUsb() {
        LogUtils.d("");
        mainActivity.aSwitch.musicRight = 2;
        mainActivity.musicPath[0] = MediaConstants.USB_MUSIC;
        musicFragment.musicPlayAllDisable(true);

        MusicRightUSBListAdapter musicRightListAdapter = MyApplication.getMusicRightListAdapter();
        MediaConnection musicServiceConnection = MyApplication.getMusicServiceConnection();
        if (musicRightListAdapter != null)
            musicRightListAdapter.startDataLoadThread();
        if (EUsbStatus.isStateUseFull(mainActivity.musicPlayer.mUsbStatus)) {
            if (musicServiceConnection.isServicesBinder) {
                ScanHandler scanHandler = new ScanHandler(MediaConstants.path, MyApplication.getMusicRightListAdapter().backgroundHandler);
                Message message = scanHandler.handler.obtainMessage(ScanHandler.SCAN);
                message.arg1 = MusicRightUSBListAdapter.reFresh_scan;
                message.sendToTarget();
            } else {
                LogUtils.d("music 服务未连接");
                setLayoutNoUsbPrompt();
                Toast.makeText(MyApplication.getContext(), "music 服务未连接", Toast.LENGTH_SHORT).show();
            }
        } else {
            LogUtils.d("usb未插入");
            notifyUsbData(new ArrayList<>());
            setLayoutNoUsbPrompt();
        }
        //修改当前高亮的标签
        updateUsbPlaying();
    }

    public void initHistory() {
        LogUtils.d("");
        musicFragment.historyButtonView();
        mainActivity.musicPath[0] = MediaConstants.HISTORY_MUSIC;
        mainActivity.aSwitch.musicRight = 3;
        if (mainActivity.musicHistory != null) {
            if (mainActivity.musicHistory.size() == 0) {
                setVisibleOfNoHistoryPrompt(View.VISIBLE);
                musicFragment.musicPlayAllDisable(true);
                musicFragment.musicEditDisableOrNot(true);
            } else {
                LogUtils.d("size:" + mainActivity.musicHistory.size());
                updateHistoryAndNotify();
            }
        }
        if (!mainActivity.musicPlayer.isEditor) {
            musicFragment.musicPlayAll.setText("全部播放");
        } else {
            if (mainActivity.musicPlayer.isHistorySelectAll) {
                musicFragment.musicPlayAll.setText("全部取消");
            } else {
                musicFragment.musicPlayAll.setText("全部选择");
            }
        }
    }


    @Override
    public void onClick(View v) {
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        if (v.getId() == R.id.music_from_usb) {
            toUsb();
        } else if (v.getId() == R.id.music_from_bluetooth) {
            if (!mainActivity.canBtPlay())
                return;
            toBt();
        } else if (v.getId() == R.id.music_from_history) {
            toHistory();
        } else if (v.getId() == R.id.btn_connect) {
            blueToothJump();
        }
    }

    /*----------------------菜单 相关-----------------------------*/
    public void initPage() {
        // 获取 Intent 对象
        mainActivity = (MultimediaMainActivity) requireActivity();
        Intent intent = mainActivity.getIntent();
        // 获取传递的参数
        Bundle extras = intent.getExtras();
        if (extras != null) {
            String page = extras.getString("page");
            if ("usb_music".equals(page)) {
                intent.putExtra("page", "");
                if (mainActivity.musicPath[0].equals(MediaConstants.BT_MUSIC)
                        || mainActivity.musicPath[0].equals(MediaConstants.HISTORY_MUSIC)) {
                    mainActivity.onBackPressed();
                }
                //打开 usb 模块
                toUsb();
            }
        }
    }



    /*----------------------蓝牙 相关-----------------------------*/

    public void upDateBtInfo(MusicFile musicFile) {
        mainActivity.runOnUiThread(() -> {
            layoutBtConnect.setVisibility(View.GONE);
            btName.setVisibility(View.VISIBLE);
            btName.setText(musicFile.btName);
        });
    }

    //跳转到蓝牙连接界面
    private void blueToothJump() {

        Intent intent = new Intent();
        intent.setComponent(new ComponentName("com.elink.settings", "com.elink.settings.SettingsActivity"));
        intent.setAction("com.elink.settings.bluetooth");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        try {
            startActivity(intent);
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }

//        Intent intent = new Intent();
//        intent.setComponent(new ComponentName("com.elink.ecsettings",
//                "com.elink.ecsettings.SettingsActivity"));
//        intent.setAction("com.elink.elinksettings.bluetooth");
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        try {
//            startActivity(intent);
//        } catch (Exception e) {
//            Log.e(TAG, "blueToothJump: " + e.getMessage());
//        }
    }

    public void btDisconnectCallback() {
        LogUtils.d("");
        mainActivity.runOnUiThread(() -> {
            layoutBtConnect.setVisibility(View.VISIBLE);
            btName.setVisibility(View.GONE);
        });

    }

    public void btConnectCallback(MusicFile musicFile) {
        if (musicFile != null)
            mainActivity.runOnUiThread(() -> {
                layoutBtConnect.setVisibility(View.GONE);
                btName.setVisibility(View.VISIBLE);
                if (!musicFile.albumCover.equals(""))
                    btName.setText(musicFile.albumCover);
            });
    }
    /*----------------------usb 相关-----------------------------*/

    public void stopUsbScroll() {
        usbRecyclerView.stopScroll();
    }

    public LinearLayoutManager getLayoutManager() {
        return (LinearLayoutManager) usbRecyclerView.getLayoutManager();
    }

    public static boolean isDefaultAlbumCover(ImageView musicAlbum) {
        Drawable currentDrawable = musicAlbum.getDrawable();
        List<Drawable> defaultDrawables = Arrays.asList(
                Objects.requireNonNull(ContextCompat.getDrawable(musicAlbum.getContext(), R.drawable.music_album_default)),
                Objects.requireNonNull(ContextCompat.getDrawable(musicAlbum.getContext(), R.drawable.song_folder2))
        );
        for (Drawable defaultDrawable : defaultDrawables) {
            if (Objects.equals(currentDrawable != null ? currentDrawable.getConstantState() : null, defaultDrawable.getConstantState())) {
                return true;
            }
        }
        return false;
    }


    public void usbScrollToPosition(int position) {
        LogUtils.d("");
        usbRecyclerView.post(() -> {
            LinearLayoutManager layoutManager = (LinearLayoutManager) usbRecyclerView.getLayoutManager();
            if (layoutManager != null) {
                layoutManager.scrollToPositionWithOffset(position, 0);
            }
        });
    }


    /**
     * 去查询数据并根新UI
     */
    @SuppressLint("NotifyDataSetChanged")
    public void usbScanCallBack() {
        LogUtils.d("");
        if (!mainActivity.isForegroundActivities())
            return;
        mainActivity = MyApplication.getMainActivity();
        ScanHandler scanHandler = new ScanHandler(MediaConstants.path, MyApplication.getMusicRightListAdapter().backgroundHandler);
        Message message = scanHandler.handler.obtainMessage(ScanHandler.SCAN);
        message.arg1 = MusicRightUSBListAdapter.reFresh_scan;
        message.sendToTarget();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void usbDetachCallback() {
        LogUtils.d("");
        usbListNotifyDataSetChanged();
        usbPromptAfterNotifyDataSetChanged();
    }

    /**
     * 列表刷新后 提示的状态
     */
    public void usbPromptAfterNotifyDataSetChanged() {
        LogUtils.d("");
        setLayoutLodeEnd();
        if (mainActivity == null)
            mainActivity = MyApplication.getMainActivity();
        if (musicFragment == null) {
            musicFragment = MyApplication.getMusicFragment();
        }
        if (mainActivity.isForegroundActivities()) {
            //提示
            if (mUsbDataList.size() > 0) {
                setLayoutHaveUsbPrompt();
            } else {
                setLayoutNoUsbPrompt();
            }
            //按钮
            if (mUsbDataList.size() > 0) {
                musicFragment.musicPlayAllDisable(mUsbDataList.get((mUsbDataList.size() - 1)).isDir);
            }
        } else {
            LogUtils.d("后台不更新");
        }
    }

    /**
     * 扫描中
     */
    public void setLayoutReadPrompt() {
        LogUtils.d("");
        mainActivity.runOnUiThread(() -> {
            layoutReadPrompt.setVisibility(View.VISIBLE);
            layoutNoUsbPrompt.setVisibility(View.GONE);
        });
    }

    /**
     * 扫描中
     */
    public void setLayoutLodeStart() {
        LogUtils.d("");
        mainActivity.runOnUiThread(() -> {
            layoutNoUsbPrompt.setVisibility(View.GONE);
            layoutLodePrompt.setVisibility(View.VISIBLE);
        });
    }

    /**
     * 扫描中
     */
    public void setLayoutLodeEnd() {
        LogUtils.d("");
        mainActivity.runOnUiThread(() -> {
            layoutLodePrompt.setVisibility(View.GONE);
        });
    }

    /**
     * 没有音乐
     */
    public void setLayoutNoUsbPrompt() {
        LogUtils.d("");
        mainActivity.runOnUiThread(() -> {
            layoutReadPrompt.setVisibility(View.GONE);
            layoutNoUsbPrompt.setVisibility(View.VISIBLE);
        });

    }

    /**
     * 扫描结束，有音乐
     */
    public void setLayoutHaveUsbPrompt() {
        LogUtils.d("");
        mainActivity.runOnUiThread(() -> {
            layoutReadPrompt.setVisibility(View.GONE);
            layoutNoUsbPrompt.setVisibility(View.GONE);
        });

    }

    public void replaceUsbData(List<MusicFile> newData) {
        LogUtils.d("newDate size:" + newData.size());
        mUsbDataList.clear();
        mUsbDataList.addAll(newData);
    }

    /**
     * 完全刷新列表的时候
     *
     * @param newData
     */
    @SuppressLint("NotifyDataSetChanged")
    public void notifyUsbData(List<MusicFile> newData) {
        LogUtils.d("");
        replaceUsbData(newData);
        if (mainActivity.isForegroundActivities()) {
            mainActivity.runOnUiThread(() -> {
                usbListNotifyDataSetChanged();
                usbPromptAfterNotifyDataSetChanged();
                updateUsbPlaying();
            });
        } else {
            LogUtils.d("后台不进行UI更新");
        }
    }

    /**
     * 完全刷新列表的时候
     *
     * @param newData
     */
    @SuppressLint("NotifyDataSetChanged")
    public void notifyUsbData1(List<MusicFile> newData) {
        LogUtils.d("");
        MyApplication.getMusicRightListAdapter().removeCallbacksAndMessages();//将异步加载图片任务全部取消
        stopUsbScroll();//  停止滑动，可能之前在快速滑动的
        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            replaceUsbData(newData);
            mainActivity.runOnUiThread(() -> {
                usbListNotifyDataSetChanged();
                usbScrollToPosition(mainActivity.popStack());
                usbPromptAfterNotifyDataSetChanged();
                updateUsbPlaying();
            });
        }, 15);
    }

    /**
     * 请求后刷新
     *
     * @param newData
     */
    public void notifyUsbData2(List<MusicFile> newData) {
        LogUtils.d(newData.size() + "/" + mUsbDataList.size());
        mainActivity.runOnUiThread(() -> musicFragment.setMusicPath());
        if (mainActivity.isForegroundActivities()) {
            mainActivity.runOnUiThread(() -> {
                DiffUtilCallBack diffCallback = new DiffUtilCallBack(mUsbDataList, newData);
                DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
                replaceUsbData(newData);
                diffResult.dispatchUpdatesTo(musicRightUSBListAdapter);
                usbPromptAfterNotifyDataSetChanged();
                updateUsbPlaying();
            });
        } else {
            LogUtils.d("后台不进行UI更新");
        }
    }


    /**
     * 当切歌的时候
     */
    public void updateUsbPlaying() {
        if (!mainActivity.isForegroundActivities())
            return;
        MusicFile musicNow = MyApplication.getMainActivity().musicNow;
        int prePlaying = -1;
        int nowPlaying = -1;
        for (int i = 0; i < mUsbDataList.size(); i++) {
            if (mUsbDataList.get(i).isPlaying) {
                LogUtils.d("pre:" + mUsbDataList.get(i).path);
                prePlaying = i;
                mUsbDataList.get(i).isPlaying = false;
                break;
            }
        }
        for (int i = 0; i < mUsbDataList.size(); i++) {
            if (musicNow != null
                    && musicNow.path != null
                    && musicNow.path.equals(mUsbDataList.get(i).path)) {
                LogUtils.d("now:" + mUsbDataList.get(i).path);
                mUsbDataList.get(i).isPlaying = true;
                nowPlaying = i;
                break;
            }
        }
        if (prePlaying != -1) {
            Bundle payload = new Bundle();
            payload.putString("playing", "playNow");
            usbListNotifyItemChanged(prePlaying, payload);
        }

        if (nowPlaying != -1) {
            Bundle payload = new Bundle();
            payload.putString("playing", "playNow");
            usbListNotifyItemChanged(nowPlaying, payload);
        }
    }


    public void nextUsbMusicCallback() {
        LogUtils.d("");
        updateUsbPlaying();
    }

    /*----------------------历史记录 相关-----------------------------*/
    public void unSelectAll() {
        LogUtils.d("");
        //如果取消了编辑就将勾选的取消
        ArrayList<MusicFile> musicDeleteList = MyApplication.getMusicFragment().musicDeleteList;
        if (musicDeleteList != null && musicDeleteList.size() > 0) {
            musicDeleteList.forEach(musicFile -> musicFile.isSelected = false);
            musicDeleteList.clear();
        }
    }

    public void notifyHistoryByDiff(List<MusicFile> newData) {
        LogUtils.d("");
        historyListItemChanged(0);//将新前第一条数据高亮清除
        DiffUtilCallBack diffCallback = new DiffUtilCallBack(mHistoryDataList, newData);
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
        mHistoryDataList.clear();
        mHistoryDataList.addAll(newData);
        diffResult.dispatchUpdatesTo(rightHistoryListAdapter);
        mainActivity.musicPlayer.isHistoryModify = false;
    }

    /**
     * 更新历史记录，勾选的数据
     */
    public void updateHistoryList() {
        MyApplication.getMusicFragment().musicDeleteList.forEach(musicFile -> {
            for (int i = 0; i < mainActivity.musicHistory.size(); i++) {
                MusicFile file = mainActivity.musicHistory.get(i);
                if (musicFile.path.equals(file.path)) {
                    file.isSelected = true;
                }
            }
        });
        mHistoryDataList.clear();
        mHistoryDataList.addAll(mainActivity.musicHistory);
    }

    @SuppressLint("NotifyDataSetChanged")
    public void notifyHistoryDataSetChanged() {
        LogUtils.d("");
        if (mainActivity == null)
            mainActivity = MyApplication.getMainActivity();
        if (mainActivity != null) {
            mainActivity.runOnUiThread(() -> historyRecyclerView.post(() -> {
                historyListDataSetChanged();
                mainActivity.musicPlayer.isHistoryModify = false;
            }));
        }
    }

    /**
     * 更新历史记录
     * 刷新列表
     * 更新提示
     * 按钮的启用和禁用  todo
     * 按钮文字的更新   todo
     */
    @SuppressLint("NotifyDataSetChanged")
    public void updateHistoryAndNotify() {
        LogUtils.d("");
        updateHistoryList();
        notifyHistoryDataSetChanged();
        updateVisibleOfNoHistoryPrompt();
    }

    public void updateVisibleOfNoHistoryPrompt() {
        if (mainActivity.musicHistory.size() == 0) {
            setVisibleOfNoHistoryPrompt(View.VISIBLE);
        } else {
            setVisibleOfNoHistoryPrompt(View.GONE);
        }
    }


    public void setVisibleOfNoHistoryPrompt(int visibility) {
        if (mainActivity == null)
            mainActivity = MyApplication.getMainActivity();
        if (mainActivity != null)
            layoutNoHistoryPrompt.setVisibility(visibility);
    }

    public void historyNextMusicCallback(MusicFile musicFile) {
        LogUtils.d("");
        musicFragment = MyApplication.getMusicFragment();
        if (mainActivity.musicHistory.size() == 1) {
            musicFragment.musicPlayAllDisable(false);
            musicFragment.musicEditDisableOrNot(false);
        }
    }

    public void usbDetachHistoryCallback() {
        LogUtils.d("");
        notifyHistoryDataSetChanged();
    }

    public void usbInsertHistoryCallback() {
        LogUtils.d("");
        updateHistoryAndNotify();
        //按钮的启动和禁用
        if (mainActivity.musicHistory.size() > 0) {
            mainActivity.runOnUiThread(() -> {
                musicFragment.musicPlayAllDisable(false);
            });
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    public void nextPlayHistoryCallback() {
        LogUtils.d("");
        //列表刷新
        updateHistoryAndNotify();
        //状态栏刷新
        if (musicFragment == null) {
            musicFragment = MyApplication.getMusicFragment();
        }
        if (musicFragment != null) {
            musicFragment.musicPlayAllDisable(false);
            musicFragment.musicEditDisableOrNot(false);
            if (mainActivity.musicPlayer.isEditor) {
                // 更新全选状态
                musicFragment.updateTextOfAllPlay();
            }
        }
    }
    /*---------------------------UI统一更新-------------------------------*/

    @SuppressLint("NotifyDataSetChanged")
    public void usbListNotifyDataSetChanged() {
        LogUtils.d("");
        if (mainActivity.isForegroundActivities()) {
            musicRightUSBListAdapter.notifyDataSetChanged();
        } else {
            LogUtils.d("后台不进行UI更新");
        }

    }

    @SuppressLint("NotifyDataSetChanged")
    public void usbListNotifyItemChanged(Integer nowPlaying, Bundle payload) {
        if (mainActivity.isForegroundActivities()) {
            musicRightUSBListAdapter.notifyItemChanged(nowPlaying, payload);
        } else {
            LogUtils.d("后台不进行UI更新");
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    public void historyListDataSetChanged() {
        if (mainActivity.isForegroundActivities()) {
            rightHistoryListAdapter.notifyDataSetChanged();
        } else {
            LogUtils.d("后台不进行UI更新");
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    public void historyListItemChanged(Integer position) {
        if (mainActivity.isForegroundActivities()) {
            rightHistoryListAdapter.notifyItemChanged(position);//将新前第一条数据高亮清除
        } else {
            LogUtils.d("后台不进行UI更新");
        }
    }
    //rightHistoryListAdapter.notifyItemChanged(0);//将新前第一条数据高亮清除

    /*------------------------动画--------------------------------*/
    private final ReentrantLock lock = new ReentrantLock();

    public void toMenu() {
        LogUtils.d("");
        initMenu();
        if (now == layout_bluetooth) {
            now = layout_menu;
            slideMenu(layout_bluetooth, now, true);
        } else if (now == layout_usb) {
            mainActivity.clearStack();
            now = layout_menu;
            slideMenu(layout_usb, now, true);
        } else if (now == layout_history) {
            now = layout_menu;
            slideMenu(layout_history, now, true);
        }
    }

    public void toBt() {
        LogUtils.d("");
        musicFragment.btButtonView();
        initBt();
        now = layout_bluetooth;
        slideMenu(layout_menu, now, false);
    }

    public void forceToBt() {
        LogUtils.d("");
        initMenu();
        musicFragment.btButtonView();
        initBt();
        now = layout_bluetooth;
        layout_menu.setVisibility(View.GONE);
        layout_bluetooth.setVisibility(View.GONE);
        now.setVisibility(View.VISIBLE);
    }

    public void toUsb() {
        LogUtils.d("");
        initUsb();
        musicFragment.usbButtonView();
        musicFragment.setTextOfAllPlayBtn("全部播放");
        now = layout_usb;
        slideMenu(layout_menu, now, false);
    }

    public void forceToUsb() {
        LogUtils.d("");
        initMenu();
        initUsb();
        now = layout_usb;
        layout_menu.setVisibility(View.GONE);
        layout_usb.setVisibility(View.GONE);
        now.setVisibility(View.VISIBLE);
        musicFragment.usbButtonView();
        musicFragment.setTextOfAllPlayBtn("全部播放");
    }

    public void toHistory() {
        LogUtils.d("");
        musicFragment.historyButtonView();
        initHistory();
        now = layout_history;
        slideMenu(layout_menu, now, false);
    }


    /**
     * @param font 滑动前的视图
     * @param back 滑动后的视图
     * @param in   是否菜单滑入
     */
    private void slideMenu(View font, View back, boolean in) {
        lock.lock();
        back.setVisibility(View.VISIBLE);
        ObjectAnimator animator;
        if (in) {
            animator = ObjectAnimator.ofFloat(back, "translationX", font.getWidth(), 0);
        } else {
            animator = ObjectAnimator.ofFloat(font, "translationX", 0, font.getWidth());
        }
        animator.setDuration(100);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                font.setVisibility(View.GONE);
                lock.unlock();
            }
        });
        animator.start();
    }

}