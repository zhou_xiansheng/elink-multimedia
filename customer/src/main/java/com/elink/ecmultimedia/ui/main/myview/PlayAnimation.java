package com.elink.ecmultimedia.ui.main.myview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.elink.ecmultimedia.utils.LogUtils;

/**
 * 播放动画绘制
 */
public class PlayAnimation extends View {
    private final Paint paint = new Paint();
    private final int BAR_COUNT = 4;//矩形数量
    private final float[] barHeights = new float[BAR_COUNT];
    private final boolean[] isUp = new boolean[BAR_COUNT];
    private final float BAR_GAP = 5; // 矩形之间的间隙


    public PlayAnimation(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint.setColor(Color.parseColor("#ADD8E6"));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float totalBarWidth = getWidth() / (float) BAR_COUNT;
        float actualBarWidth = totalBarWidth - BAR_GAP; // 减去间隙

        for (int i = 0; i < BAR_COUNT; i++) {
            float left = i * totalBarWidth;
            float right = left + actualBarWidth; // 考虑间隙
            float bottom = getHeight();
            float top = bottom - barHeights[i];
            canvas.drawRect(left, top, right, bottom, paint);
        }
    }

    private void initData() {
        barHeights[0] = 3;
        barHeights[1] = 15;
        barHeights[2] = 30;
        barHeights[3] = 46;

        isUp[0] = true;
        isUp[1] = true;
        isUp[2] = true;
        isUp[3] = false;
    }

    private void generateData() {

        for (int i = 0; i < BAR_COUNT; i++) {
            if (barHeights[i] >= getHeight())
                isUp[i] = false;
            else if (barHeights[i] == 2) {
                isUp[i] = true;
            }
            if (isUp[i]) {
                barHeights[i] += 1;
            } else {
                barHeights[i] -= 1;
            }
        }
    }

    private Handler handler;
    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            generateData();
            invalidate();
            handler.postDelayed(this, 20);
        }
    };

    public void startHandler(boolean isContinue) {
        LogUtils.d("");
        stopHandler();
        handler = new Handler();
        initData();
        if (isContinue)
            handler.post(runnable);
        else {
            generateData();
            invalidate();
        }
    }

    public void pauseHandler() {
        if (handler != null) {
            LogUtils.d("");
            handler.removeCallbacks(runnable);
        }
    }

    public void continueHandler() {
        if (handler != null){
            LogUtils.d("");
            handler.removeCallbacks(runnable);
            handler.post(runnable);
        }
    }

    public void stopHandler() {
        if (handler != null) {
            LogUtils.d("");
            handler.removeCallbacks(runnable);
            handler = null;
        }
    }
}


