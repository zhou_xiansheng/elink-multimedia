package com.elink.ecmultimedia.ui.main.fragment.music;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.elink.ecmultimedia.R;
import com.elink.ecmultimedia.adapter.music.MusicLeftListAdapter;
import com.elink.ecmultimedia.listener.MusicSeekBarListener;
import com.elink.ecmultimedia.pojo.Switch;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.ui.main.view.MusicBigPlayingActivity;
import com.elink.ecmultimedia.utils.AlbumUtils;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.common.EAudioSource;
import com.elink.elinkmultimediaservices.constants.common.EUsbStatus;
import com.elink.elinkmultimediaservices.constants.common.IPlaybackStatus;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;
import com.elink.elinkmultimediaservices.constants.music.IRepeatMode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class MusicLeftPlayingFragment extends Fragment implements View.OnClickListener, View.OnTouchListener {

    public MultimediaMainActivity mainActivity;
    private LinearLayout musicPlay;
    private LinearLayout musicList;

    /*-------------------播放界面----------------------*/
    private ImageView startStop;
    private ImageView musicBefore;
    private ImageView musicNext;
    private ImageView toMusicLeftList;
    private ImageView musicAlbum;
    private ImageView musicSongSource;
    private TextView musicName;
    private TextView musicSinger;


    private SeekBar musicSeekbar;

    /*-------------------播放列表---------------------*/
    private ImageView toMusicPlaying;
    private ImageView playModer;//音乐播放模式
    private RecyclerView recyclerView;
    private final ArrayList<MusicFile> mDataList = new ArrayList<>();
    public MusicLeftListAdapter musicLeftListAdapter;

    public MusicLeftPlayingFragment() {
        LogUtils.d("");
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LogUtils.d("");
        View view = inflater.inflate(R.layout.fragment_music_left_playing, container, false);
        /*-------------------------公共------------------------------*/
        MyApplication.setMusicLeftPlayingFragment(this);
        mainActivity = MyApplication.getMainActivity();
        musicPlay = view.findViewById(R.id.layout_play);
        musicList = view.findViewById(R.id.layout_play_list);

        /*-------------------------播放界面------------------------------*/
        //获取组件
        toMusicLeftList = view.findViewById(R.id.to_music_left_list);
        musicAlbum = view.findViewById(R.id.music_album);
        musicSongSource = view.findViewById(R.id.music_song_source);
        startStop = view.findViewById(R.id.music_start_stop);
        musicBefore = view.findViewById(R.id.music_before);
        musicNext = view.findViewById(R.id.music_next);
        musicName = view.findViewById(R.id.music_name);
        musicName.setSelected(true);
        musicSinger = view.findViewById(R.id.music_singer);
        musicSinger.setSelected(true);
        musicSeekbar = view.findViewById(R.id.seekbar_music);

        //事件监听绑定
        toMusicLeftList.setOnClickListener(this);
        musicAlbum.setOnClickListener(this);
        startStop.setOnClickListener(this);
        musicNext.setOnClickListener(this);
        musicBefore.setOnClickListener(this);
        startStop.setOnTouchListener(this);
        musicNext.setOnTouchListener(this);
        musicBefore.setOnTouchListener(this);
        musicSeekbar.setOnSeekBarChangeListener(new MusicSeekBarListener(this));


        /*-------------------------播放列表------------------------------*/
        //获得组件
        recyclerView = view.findViewById(R.id.music_left_recycle_view);
        toMusicPlaying = view.findViewById(R.id.to_music_playing);
        playModer = view.findViewById(R.id.music_play_mode);
        //设置布局方式
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        updateDataList();
        musicLeftListAdapter = new MusicLeftListAdapter(mDataList, this);
        recyclerView.setAdapter(musicLeftListAdapter);
        //绑定点击监听事件
        toMusicPlaying.setOnClickListener(this);
        playModer.setOnClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        LogUtils.d("");
        if (MyApplication.getMusicServiceConnection().isServicesBinder) {
            if (musicPlay.getVisibility() == View.VISIBLE) {
                if (mainActivity.serviceConnection.isServicesBinder) {
                    initPlay();
                }
            } else if (musicList.getVisibility() == View.VISIBLE) {
                initList();
            }
        }
        super.onResume();
    }


    /**
     * 当从其他页面跳转过来时
     * 根据之前是否在播放进行页面初始化
     */

    public void initPlay() {
        LogUtils.d("");
        mainActivity.aSwitch.bigOrSmall = Switch.PATH_MUSIC_SMALL;
        mainActivity.aSwitch.musicLeft = Switch.PATH_MUSIC_PLAY;
        updateViews();
        updateMusicInfo(mainActivity.musicNow);//歌名 歌手
        if (mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PAUSE) {
            updateUiOfPause();
        } else {
            updateUiOfPlay();
        }
    }

    /*----------------------------播放列表 start-------------------------------------*/
    @SuppressLint("NotifyDataSetChanged")
    public void initList() {
        LogUtils.d("");
        mainActivity.aSwitch.bigOrSmall = Switch.PATH_MUSIC_SMALL;
        mainActivity.aSwitch.musicLeft = Switch.PATH_MUSIC_COLLECT;
        //刷新列表
        updateDataAndNotify();
        //将当前播放显示到可见范围内
        scrollWork();
        //音乐播放模式切换 修改图标，修改模式
        int playMode = mainActivity.serviceConnection.getMusicPlayMode();
        if (playMode == IRepeatMode.REPEAT_MODE_ORDER) {
            //列表循环
            playModer.setImageResource(R.drawable.music_play_mode_order);
        } else if (playMode == IRepeatMode.REPEAT_MODE_RANDOM) {
            //随机播放
            playModer.setImageResource(R.drawable.music_play_mode_shuffle);
        } else if (playMode == IRepeatMode.REPEAT_MODE_SINGLE) {
            //单曲循环
            playModer.setImageResource(R.drawable.music_play_mode_repeat);
        } else {
            LogUtils.d("播放模式切换出错，不再范围内");
        }
    }

    public void updatePlayListData() {
        LogUtils.d("");
        List<MusicFile> musicCollectList = mainActivity.serviceConnection.getMusicCollectList();
        musicCollectList.forEach(musicFile -> {
            if (musicFile.path.equals(mainActivity.musicNow.path)) {
                musicFile.isPlaying = true;
            }
        });
        mDataList.clear();
        mDataList.addAll(musicCollectList);//将收藏列表中的数据添加进去
    }

    public void addToDataList(MusicFile musicFile) {
        mDataList.add(musicFile);
    }

    /**
     * 刷新 usb 播放列表
     */
    @SuppressLint("NotifyDataSetChanged")
    public void notifyDataSetChanged() {
        if (musicLeftListAdapter != null) {
            if (mainActivity == null) {
                mainActivity = MyApplication.getMainActivity();
            }
            if (mainActivity != null) {
                mainActivity.runOnUiThread(() -> {
                    musicLeftListAdapter.notifyDataSetChanged();
                });
            }
        }
    }

    public void updateDataAndNotify() {
        updateDataList();
        notifyDataSetChanged();
    }

    /**
     * 当切歌的时候
     */
    public void updatePlaying() {
        LogUtils.d("");
        MusicFile musicNow = MyApplication.getMainActivity().musicNow;
        int prePlaying;
        int nowPlaying;
        for (int i = 0; i < mDataList.size(); i++) {
            if (mDataList.get(i).isPlaying) {
                prePlaying = i;
                mDataList.get(i).isPlaying = false;
                musicLeftListAdapter.notifyItemChanged(prePlaying);
            }
        }
        for (int i = 0; i < mDataList.size(); i++) {
            if (musicNow != null
                    && musicNow.path != null
                    && musicNow.path.equals(mDataList.get(i).path)) {
                mDataList.get(i).isPlaying = true;
                nowPlaying = i;
                musicLeftListAdapter.notifyItemChanged(nowPlaying);
                break;
            }
        }
    }


    private final static int DO_SCROLL_CODE = 1;

    private final Handler scrollHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            int what = msg.what;
            switch (what) {
                case DO_SCROLL_CODE:
                    scrollWork();
                    break;
                default:
                    LogUtils.d("no default");
            }
        }
    };

    /**
     * 将当前播放 的 music 滚动至可见区域
     */
    public void scrollToVisible() {
        scrollHandler.removeCallbacksAndMessages(null);
        Message message = scrollHandler.obtainMessage(DO_SCROLL_CODE);
        scrollHandler.sendMessageDelayed(message, 500);
    }

    /**
     * 将当前播放 的 music 滚动至可见区域
     */
    public void scrollWork() {
        if (mainActivity.musicNow == null)
            return;
        int index = -1;
        for (int i = 0; i < mDataList.size(); i++) {
            if (mDataList.get(i).path.equals(mainActivity.musicNow.path)) {
                index = i;
                break;
            }
        }
        if (index != -1) {
            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            if (layoutManager != null) {
                if (mainActivity.isForegroundActivities()) {
                    layoutManager.scrollToPositionWithOffset(index, 130);
                }
            }
        }
    }


    /**
     * 服务端信息修改回调
     * 修改当前播放标志
     * 这个直接在项的点击事件中就写好了，这里处理的话反而更加麻烦
     *
     * @param musicFile
     */
    public void playListNextMusicCallback(MusicFile musicFile) {
        updatePlaying();
        scrollToVisible();
    }

    public void playListUsbLostCallback() {
        //直接跳转到播放页面，前往播放列表的按钮也进行隐藏
        mainActivity.runOnUiThread(this::toPlay);
    }


    /*----------------------------播放列表 end-------------------------------------*/

    public void updateDataList() {
        LogUtils.d("");
        List<MusicFile> musicCollectList = mainActivity.serviceConnection.getMusicCollectList();
        musicCollectList.forEach(musicFile -> {
            if (musicFile.path.equals(mainActivity.musicNow.path)) {
                musicFile.isPlaying = true;
            }
        });
        mDataList.clear();
        mDataList.addAll(musicCollectList);//将收藏列表中的数据添加进去
    }

    public void updateMusicSeekbar() {
        int currentPosition = mainActivity.serviceConnection.getCurrentPosition();
        musicSeekbar.setMax(mainActivity.musicNow.duration);//设置滑动条的最大值
        musicSeekbar.setProgress(currentPosition);//滑条到指定位置
        LogUtils.d(currentPosition + "/" + mainActivity.musicNow.duration);
    }

    public void clearMusicSeekbar() {
        LogUtils.d("");
        musicSeekbar.setMax(0);//设置滑动条的最大值
        musicSeekbar.setProgress(0);//滑条到指定位置
    }

    public void initNextMusicSeekbar() {
        LogUtils.d("");
        musicSeekbar.setMax(mainActivity.musicNow.duration);//设置滑动条的最大值
        musicSeekbar.setProgress(0);//滑条到指定位置
    }

    public void setSeekBarProgress(int progress) {
        mainActivity.runOnUiThread(() -> musicSeekbar.setProgress(progress));
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        ImageView imageView = (ImageView) v;
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                imageView.setColorFilter(Color.argb(150, 0, 0, 0), PorterDuff.Mode.SRC_ATOP);
                break;
            case MotionEvent.ACTION_UP:
                v.performClick();
            case MotionEvent.ACTION_CANCEL:
                imageView.clearColorFilter();
                break;
        }
        return true;  // 这里返回 true，否则只会捕获到 DOWN 事件
    }

    /**
     * 按钮点击事件
     *
     * @param v The view that was clicked.
     */

    @SuppressLint("CommitTransaction")
    @Override
    public void onClick(View v) {
        int musicSources = mainActivity.serviceConnection.getMusicSources();
        if (v.getId() == R.id.to_music_left_list) {
            LogUtils.d("到收藏列表");
            toPlayList();
        } else if (v.getId() == R.id.music_album) {
            if (mainActivity.musicNow != null) {
                if ((musicSources == IPlaySource.USB && EUsbStatus.isStateUseFull(mainActivity.musicPlayer.mUsbStatus))) {
                    File file = new File(mainActivity.musicNow.path);
                    if (file.exists()) {
                        Intent intent = new Intent(requireActivity(), MusicBigPlayingActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                    }
                } else if (musicSources == IPlaySource.BT) {
                    startActivity(new Intent(requireActivity(), MusicBigPlayingActivity.class));
                }
            }
        } else if (v.getId() == R.id.music_start_stop) {
            if (musicSources == IPlaySource.BT) {
                if (mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PAUSE) {
                    btPlaying();
                } else if (mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PLAYING) {
                    btPause();
                }
            } else if (musicSources == IPlaySource.USB) {
                if (mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PAUSE) {
                    usbPlaying();
                } else if (mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PLAYING) {
                    usbPause();
                }
            } else {
                if (mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PAUSE) {
                    mainActivity.serviceConnection.customerPlay();
                } else if (mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PLAYING) {
                    mainActivity.serviceConnection.pause();
                }
            }
        } else if (v.getId() == R.id.music_before) {
            if (musicSources == IPlaySource.BT) {
                btPre();
            } else if ((musicSources == IPlaySource.USB))
                if (mainActivity.musicPlayer.mUsbStatus != EUsbStatus.DISCONNECT)
                    if (mainActivity.musicNow != null && mainActivity.musicNow.path != null) {
                        if (mainActivity.canUsbPlay()) {
                            LogUtils.d("上一首");
                            mainActivity.setAudioSources(EAudioSource.AUDIO_SOURCE_USB);
                            mainActivity.serviceConnection.prev();
                        }
                    } else {
                        Toast.makeText(getContext(), "音乐文件不存在！", Toast.LENGTH_SHORT).show();
                    }
        } else if (v.getId() == R.id.music_next) {
            if (musicSources == IPlaySource.BT) {
                btNext();
            } else if (musicSources == IPlaySource.USB) {
                if (mainActivity.musicPlayer.mUsbStatus != EUsbStatus.DISCONNECT)
                    if (mainActivity.musicNow != null && mainActivity.musicNow.path != null) {
                        LogUtils.d("下一首");
                        if (mainActivity.canUsbPlay()) {
                            mainActivity.setAudioSources(EAudioSource.AUDIO_SOURCE_USB);
                            mainActivity.serviceConnection.next();
                        }
                    } else {
                        Toast.makeText(getContext(), "音乐文件不存在！", Toast.LENGTH_SHORT).show();
                    }
            }
        } else if (v.getId() == R.id.to_music_playing) {
            LogUtils.d("跳转到播放界面");
            toPlay();
        } else if (v.getId() == R.id.music_play_mode) {
            //音乐播放模式切换 修改图标，修改模式
            int playMode = mainActivity.serviceConnection.getMusicPlayMode();
            int newMode = (playMode + 1) % 3;
            mainActivity.serviceConnection.setMusicPlayMode(newMode);
            if (newMode == IRepeatMode.REPEAT_MODE_ORDER) {
                //列表循环
                playModer.setImageResource(R.drawable.music_play_mode_order);
            } else if (newMode == IRepeatMode.REPEAT_MODE_RANDOM) {
                //随机播放
                playModer.setImageResource(R.drawable.music_play_mode_shuffle);
            } else if (newMode == IRepeatMode.REPEAT_MODE_SINGLE) {
                //单曲循环
                playModer.setImageResource(R.drawable.music_play_mode_repeat);
            } else {
                LogUtils.d("播放模式切换出错，不再范围内");
            }
        }
    }

    private void btNext() {
        if (!mainActivity.canBtPlay())
            return;
        mainActivity.setAudioSources(EAudioSource.AUDIO_SOURCE_BT);
        mainActivity.serviceConnection.next();
    }

    private void btPre() {
        if (!mainActivity.canBtPlay())
            return;
        mainActivity.setAudioSources(EAudioSource.AUDIO_SOURCE_BT);
        mainActivity.serviceConnection.prev();
    }


    /**
     * 检查播放列表是否需要更新
     *
     * @param music
     */
    public void changeMusicList(MusicFile music) {
        List<MusicFile> musicCollect = mainActivity.serviceConnection.getMusicCollectList();
        if (musicCollect != null) {
            for (int i = 0; i < musicCollect.size(); i++) {
                if (musicCollect.get(i).path.equals(music.path)) {
                    if (mainActivity.musicPlayer.index != i) {
                        mainActivity.musicPlayer.index = i;
                        mainActivity.serviceConnection.setPlayIndex(i);
                    }
                    return;
                }
            }
        } else {
            musicCollect = new ArrayList<>();
        }
        /*不在收藏列表中*/
        LogUtils.d("添加到收藏列表");
        //添加到播放列表
        addToDataList(music);
        notifyDataSetChanged();
        /*
        添加到播放列表并播放。同时修改服务端
         */
        mainActivity.serviceConnection.addToMusicCollect(music);
        mainActivity.musicPlayer.index = musicCollect.size();//修改当前播放音乐的下标
        mainActivity.serviceConnection.setPlayIndex(musicCollect.size());
    }

    /**
     * 右边点击播放使用 歌手信息和播放图标
     *
     * @param music
     */

    public void updateMusicInfo(MusicFile music) {
        LogUtils.d("");
        mainActivity.runOnUiThread(() -> {
            updateUiOfAlbumCover(music, musicAlbum);
            if (music != null) {
                updateUiOfMusicName(music.fileName);
                updateUiOfSingerName(music.fileName, music.artist);
            }
        });
    }

    /**
     * 指定歌曲播放
     */
    public void usbPlaying(MusicFile musicFile) {
        LogUtils.d("");
        //数据修改
        mainActivity.musicNow = musicFile;
        mainActivity.musicPlayer.duration = musicFile.duration;
        /*播放操作*/
        //播放
        mainActivity.serviceConnection.setMusicPlayNow(musicFile);
        mainActivity.serviceConnection.playByMusic();
    }

    /**
     * 简单的播放
     */
    public void usbPlaying() {
        LogUtils.d("");
        mainActivity.setAudioSources(EAudioSource.AUDIO_SOURCE_USB);
        mainActivity.serviceConnection.play();
    }


    public void btPlaying() {
        LogUtils.d("");
        if (!mainActivity.canBtPlay())
            return;
        mainActivity.setAudioSources(EAudioSource.AUDIO_SOURCE_BT);
        mainActivity.serviceConnection.play();
    }

    public void usbPause() {
        LogUtils.d("");
        //修改播放状态
        mainActivity.musicPlayer.status = IPlaybackStatus.STATUS_PAUSE;
        //暂停
        mainActivity.serviceConnection.pause();
    }

    public void btPause() {
        LogUtils.d("");
        mainActivity.serviceConnection.pause();
    }


    public void usbPauseCallBack() {
        LogUtils.d("");
        //修改播放状态
        mainActivity.musicPlayer.status = IPlaybackStatus.STATUS_PAUSE;
        //修改图标
        updateUiOfPause();
    }

    public void usbInsertCallBack() {
        LogUtils.d("");
        //修改播放状态
        mainActivity.musicPlayer.status = IPlaybackStatus.STATUS_PAUSE;
        //修改图标
        updateUiOfPause();
        mainActivity.musicNow = mainActivity.serviceConnection.getMusicNow();
        updateMusicInfo(mainActivity.musicNow);
    }

    public void usbDisconnectCallBack() {
        LogUtils.d("");
        //修改播放状态
        mainActivity.musicPlayer.status = IPlaybackStatus.STATUS_PAUSE;
        //修改图标
        updateUiOfPause();
        MusicFile musicFile = new MusicFile();
        musicFile.playSource = IPlaySource.USB;
        updateMusicInfo(musicFile);
    }

    public void btPauseCallBack() {
        LogUtils.d("");
        //修改播放状态
        mainActivity.musicPlayer.status = IPlaybackStatus.STATUS_PAUSE;
        //修改图标
        updateUiOfPause();
    }

    public void playOrPauseCallBack(int status) {
        if (status == 0) {
            LogUtils.d("pause");
            updateUiOfPause();
        } else if (status == 1) {
            LogUtils.d("play");
            updateSeekbar();
            updateUiOfPlay();
        }
    }

    /**
     * 服务端信息修改回调
     * 歌曲信息、进度条信息
     *
     * @param musicFile
     */
    public void nextMusicCallback(MusicFile musicFile) {

        updateMusicInfo(musicFile);
        int musicSources = mainActivity.serviceConnection.getMusicSources();
        LogUtils.d("musicSources:" + musicSources);
        if (musicSources == IPlaySource.BT) {
            musicSeekbar.setVisibility(View.INVISIBLE);
            mainActivity.lyricNow = new ArrayList<>();
        } else if (musicSources == IPlaySource.USB) {
            initNextMusicSeekbar();
            musicSeekbar.setVisibility(View.VISIBLE);
            updateVisibleBtnOfPlayToList();
        }
        //如果当前是暂停状态就改为播放，候暂停的时候点击下一首这种情况
        updatePlayIcon();
    }

    public void updatePlayIcon() {
        mainActivity.runOnUiThread(() -> {
            if (mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PLAYING) {
                updateUiOfPlay();
            } else if (mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PAUSE) {
                updateUiOfPause();
            }
        });
    }

    public void btDisconnectCallback() {
        LogUtils.d("");
        //修改播放状态
        mainActivity.musicPlayer.status = IPlaybackStatus.STATUS_PAUSE;
        //修改图标
        updateUiOfPause();
        updateMusicInfo(mainActivity.musicNow);
    }


    public void updateViews() {
        mainActivity.runOnUiThread(() -> {
            updateVisibleOfMusicSourceTag();
            updateSeekbar();
            updateVisibleBtnOfPlayToList();
        });
    }

    public void usbLostCallback() {
        LogUtils.d("");
        mainActivity.runOnUiThread(() -> {
            usbPauseCallBack();
            updateVisibleBtnOfPlayToList();
            clearMusicSeekbar();
        });
    }

    public void updateVisibleOfViews() {
        mainActivity.runOnUiThread(() -> {
            updateVisibleOfMusicSourceTag();
            updateVisibleOfSeekBar();
            updateVisibleBtnOfPlayToList();
        });
    }

    public void updateVisibleOfSeekBar() {
        mainActivity.runOnUiThread(() -> {
            int musicSources = mainActivity.serviceConnection.getMusicSources();
            if (musicSources == IPlaySource.USB) {
                if (musicSeekbar.getVisibility() == View.INVISIBLE) {
                    musicSeekbar.setProgress(0);
                    musicSeekbar.setVisibility(View.VISIBLE);
                }
            } else if (musicSources == IPlaySource.BT) {
                if (musicSeekbar.getVisibility() == View.VISIBLE) {
                    musicSeekbar.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    /**
     * 播放源图
     */
    public void updateVisibleOfMusicSourceTag() {
        if (mainActivity.isForegroundActivities()) {
            int musicSources = mainActivity.serviceConnection.getMusicSources();
            mainActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (musicSources == IPlaySource.BT) {
                        LogUtils.d("bt");
                        musicSongSource.setVisibility(View.VISIBLE);
                        musicSongSource.setImageResource(R.drawable.music_song_source_bt);
                    } else if (musicSources == IPlaySource.USB) {
                        LogUtils.d("usb");
                        musicSongSource.setVisibility(View.VISIBLE);
                        musicSongSource.setImageResource(R.drawable.music_song_source_usb);
                    } else {
                        LogUtils.d("no");
                        musicSongSource.setVisibility(View.GONE);
                    }
                }
            });
        }
    }

    public void updateSeekbar() {
        mainActivity.runOnUiThread(() -> {
            int musicSources = mainActivity.serviceConnection.getMusicSources();
            if (musicSources == IPlaySource.USB) {
                if (musicSeekbar.getVisibility() == View.INVISIBLE) {
                    musicSeekbar.setProgress(0);
                    musicSeekbar.setVisibility(View.VISIBLE);
                }
                updateMusicSeekbar();
            } else if (musicSources == IPlaySource.BT) {
                if (musicSeekbar.getVisibility() == View.VISIBLE) {
                    musicSeekbar.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    public void updateVisibleBtnOfPlayToList() {
        LogUtils.d("");
        mainActivity.runOnUiThread(() -> {
            int musicSources = mainActivity.serviceConnection.getMusicSources();
            if (musicSources == IPlaySource.USB) {
                List<MusicFile> musicCollect = mainActivity.serviceConnection.getMusicCollectList();
                if (musicCollect != null && musicCollect.size() > 0) {
                    if (toMusicLeftList.getVisibility() == View.INVISIBLE) {
                        toMusicLeftList.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (toMusicLeftList.getVisibility() == View.VISIBLE) {
                        toMusicLeftList.setVisibility(View.INVISIBLE);
                    }
                }
            } else if (musicSources == IPlaySource.BT) {
                if (toMusicLeftList.getVisibility() == View.VISIBLE) {
                    toMusicLeftList.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    public void usbInsertCallback() {
        LogUtils.d("");
        mainActivity.runOnUiThread(() -> {
            if (mainActivity == null)
                mainActivity = MyApplication.getMainActivity();
            updateMusicInfo(mainActivity.musicNow);
            updateVisibleBtnOfPlayToList();
        });
    }
    /*---------------------------UI统一更新-------------------------------*/

    public void updateUiOfPlay() {
        LogUtils.d("");
        if (mainActivity.isForegroundActivities()) {
            startStop.setImageResource(R.drawable.music_player_btn_pause);
        } else {
            LogUtils.d("后台不更新UI");
        }
    }

    public void updateUiOfPause() {
        LogUtils.d("");
        if (mainActivity.isForegroundActivities()) {
            startStop.setImageResource(R.drawable.music_player_btn_play);
        } else {
            LogUtils.d("后台不更新UI");
        }
    }

    public void updateUiOfAlbumCover(MusicFile musicFile, ImageView imageView) {
        if (mainActivity.isForegroundActivities()) {
            if (musicFile.playSource == IPlaySource.USB) {
                AlbumUtils.setAlbumCover(musicFile, imageView);
            } else if (musicFile.playSource == IPlaySource.BT) {
                AlbumUtils.setAlbumDefault(imageView);
            } else {
                LogUtils.w("playSource:" + musicFile.playSource + "!!!");
            }
        } else {
            LogUtils.d("后台不更新UI");
        }
    }

    public void updateUiOfMusicName(String fileName) {
        if (mainActivity.isForegroundActivities()) {
            if ("未知曲目".equals(fileName)) {
                musicName.setText("暂无曲目");
            } else {
                musicName.setText(fileName);
            }
        } else {
            LogUtils.d("后台不更新UI");
        }
    }

    public void updateUiOfSingerName(String fileName, String singerName) {
        if (mainActivity.isForegroundActivities()) {
            if ("未知曲目".equals(fileName)) {
                musicSinger.setText("敬请期待");
            } else {
                musicSinger.setText(singerName);
            }
        } else {
            LogUtils.d("后台不更新UI");
        }
    }


    /*---------------------------页面跳转动画-------------------------------*/
    public void toPlay() {
        LogUtils.d("");
        initPlay();
        mainActivity.runOnUiThread(() -> flipCard(musicList, musicPlay));
    }

    public void toPlayList() {
        LogUtils.d("");
        initList();
        mainActivity.runOnUiThread(() -> flipCard(musicPlay, musicList));
    }

    private void flipCard(View font, View back) {
        ObjectAnimator fontRotationAnimator = ObjectAnimator.ofFloat(font, "rotationY", 0f, 90f);
        fontRotationAnimator.setDuration(500);
        fontRotationAnimator.setInterpolator(new AccelerateDecelerateInterpolator());

        ObjectAnimator fontAlphaAnimator = ObjectAnimator.ofFloat(font, "alpha", 1f, 0f);
        fontAlphaAnimator.setDuration(500);
        fontAlphaAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        fontRotationAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                font.setVisibility(View.GONE);
                back.setVisibility(View.VISIBLE);
                ObjectAnimator backRotationAnimator = ObjectAnimator.ofFloat(back, "rotationY", 90f, 0f);
                backRotationAnimator.setDuration(500);
                backRotationAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
                ObjectAnimator backAlphaAnimator = ObjectAnimator.ofFloat(back, "alpha", 0f, 1f);
                backAlphaAnimator.setDuration(500);
                backAlphaAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
                backRotationAnimator.start();
                backAlphaAnimator.start();
            }
        });
        fontRotationAnimator.start();
        fontAlphaAnimator.start();
    }

    private ObjectAnimator getObjectAnimator(View view, String propertyName, float... values) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "rotationY", 0f, 90f);
        animator.setDuration(500);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        return animator;
    }
}