package com.elink.ecmultimedia.ui.main.myview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.SeekBar;

import androidx.core.content.ContextCompat;

import com.elink.ecmultimedia.R;
import com.elink.ecmultimedia.listener.RadioSeekBarListener;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.pojo.Radio;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.ecmultimedia.utils.ResolutionAdaptation;

import java.util.Locale;

/**
 * 程序中维护两个队列
 * 队列1，对应数据库。将当前 radio 放到下标为1的位置。
 * 从收藏电台跳到未收藏电台，新建一个radio插入到数组头部。
 * 从收藏电台跳到收藏电台，将该收藏电台移动到数组头部。
 * 从未收藏电台跳转到未收藏电台，直接修改该电台数据。
 * 从未收藏电台跳转到收藏电台，删除未收藏电台并将收藏电台移动到数组头部
 * 写数据库
 * 队列2，对应收藏列表
 * <p>
 * 收藏和未收藏之间切换
 * 收藏到为收藏，修改在1中的标识，删除队列2中的数据
 * 为收藏到收藏，四修改1中的标识，增加到队列2
 */
@SuppressLint("AppCompatCustomView")
public class CustomerSeekBar extends SeekBar {

    public static final int MIN_FM = 87;
    public static final int MAX_FM = 108;

    public MultimediaMainActivity mainActivity;
    private Paint paint;//绘制刻度
    private Paint mainPaint;//绘制刻度
    private Paint textPaint;//绘制刻度值
    public CustomerSeekBar(Context context) {
        super(context);
        init();
    }

    public CustomerSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomerSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LogUtils.d("");
        MyApplication.setCustomerSeekBar(this);
        mainActivity = MyApplication.getMainActivity();//活动对象

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.WHITE); // 刻度的颜色
        paint.setStrokeWidth(ResolutionAdaptation.mThickness); // 设置刻度的粗细为4px

        mainPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mainPaint.setColor(ContextCompat.getColor(getContext(), R.color.light_blue)); // 刻度的颜色
        mainPaint.setStrokeWidth(ResolutionAdaptation.mMainThickness); // 设置刻度的粗细为4px
        mainPaint.setAlpha(255);
        mainPaint.setXfermode(null);

        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setColor(Color.WHITE); // 文本的颜色
        textPaint.setTextSize(ResolutionAdaptation.mFontSize); // 设置文本的大小
        textPaint.setTextAlign(Paint.Align.CENTER); // 设置文本居中对齐
        setProgress(mainActivity.radio.thumbPos);//设置进度条初始值
    }

    @Override
    protected synchronized void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Drawable thumb = getThumb();//滑块的长度
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        // 先绘制原始的SeekBar
        super.onDraw(canvas);

        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        int padding = 33;

        float width = getWidth() - padding; // 计算刻度线的长度
        float interval = width / 103;
        float middleInterval = interval / 2 - 0.11f;
        int height = getHeight();

        int thumbPos = RadioSeekBarListener.progress;//计算滑块的位置 0~205，一共26个可移动项


        // 计算滑块的位置（以刻度为单位）
        int startX;//第一个刻度线要往前格
        int stopX;
        int startY = height / 2; // 起始点在SeekBar的中心
        int stopY;
        for (int i = 0; i <= 103 + 103; i++) { // 需要绘制103条线，滑块在每条新中间也是可以选中的
            float fm = 87.4f + i / 20f * 2f; // 计算当前刻度对应的FM值
            if ((i - 1) == thumbPos) {
                startX = stopX = (int) ((i) * middleInterval + 3 * middleInterval);
                stopY = startY - ResolutionAdaptation.mHeight1;
                canvas.drawLine(startX, startY, stopX, stopY, mainPaint);
            } else if (thumbPos % 2 == 0 && Math.abs(i - 1 - thumbPos) == 1) {
                startX = stopX = (int) ((i) * middleInterval + 3 * middleInterval);
                stopY = startY - ResolutionAdaptation.mHeight2;
                canvas.drawLine(startX, startY, stopX, stopY, paint);
            } else if (thumbPos % 2 == 0 && Math.abs(i - 1 - thumbPos) == 3) {
                startX = stopX = (int) ((i) * middleInterval + 3 * middleInterval);
                stopY = startY - ResolutionAdaptation.mHeight3;
                canvas.drawLine(startX, startY, stopX, stopY, paint);
            } else if (thumbPos % 2 == 0 && Math.abs(i - 1 - thumbPos) == 5) {
                startX = stopX = (int) ((i) * middleInterval + 3 * middleInterval);
                stopY = startY - ResolutionAdaptation.mHeight4;
                canvas.drawLine(startX, startY, stopX, stopY, paint);
            } else if (thumbPos % 2 == 1 && Math.abs(i - 1 - thumbPos) == 2) {
                startX = stopX = (int) ((i) * middleInterval + 3 * middleInterval);
                stopY = startY - ResolutionAdaptation.mHeight2;
                canvas.drawLine(startX, startY, stopX, stopY, paint);
            } else if (thumbPos % 2 == 1 && Math.abs(i - 1 - thumbPos) == 4) {
                startX = stopX = (int) ((i) * middleInterval + 3 * middleInterval);
                stopY = startY - ResolutionAdaptation.mHeight3;
                canvas.drawLine(startX, startY, stopX, stopY, paint);
            } else if (thumbPos % 2 == 1 && Math.abs(i - 1 - thumbPos) == 6) {
                startX = stopX = (int) ((i) * middleInterval + 3 * middleInterval);
                stopY = startY - ResolutionAdaptation.mHeight4;
                canvas.drawLine(startX, startY, stopX, stopY, paint);
            } else if (i % 2 == 0) {
                //每隔两个画个刻度
                startX = stopX = (int) ((i) * middleInterval + 3 * middleInterval);
                if ((i + 14) % 20 == 0) {
                    // 每10个小刻度，设置一个主刻度
                    if (fm >= 88) {
                        // 从88开始，将主刻度突出显示，并在下方显示这个数值
                        stopY = startY - ResolutionAdaptation.mHeight3; // 主刻度长度为60px，往上绘制
                    } else {
                        stopY = startY - ResolutionAdaptation.mHeight4; // 如果FM值小于88，主刻度长度为40px，和普通刻度一样
                    }
                } else {
                    stopY = startY - ResolutionAdaptation.mHeight5; // 普通刻度长度为40px，往上绘制
                }
                canvas.drawLine(startX, startY, stopX, stopY, paint);
            }
            /*绘制下面的的刻度*/
            if (i % 2 == 0) {//每隔两个画个刻度
                startX = (int) ((i) * middleInterval + 3 * middleInterval);
                if ((i + 14) % 20 == 0) {
                    if (fm >= 88) { // 从88开始，将主刻度突出显示，并在下方显示这个数值
                        String format = String.format(Locale.ENGLISH, "%d", (int) fm);
                        float textWidth = textPaint.measureText(format);
                        float textStart = startX;
                        if ((textWidth + startX) > width) {
                            textStart = width - textWidth / 10;
                        }
                        canvas.drawText(format, textStart, startY + ResolutionAdaptation.mFontOffsetY, textPaint); // 在主刻度下方60px处绘制数值
                    }
                }
            }
        }
    }
}