package com.elink.ecmultimedia.ui.main.view;

import static com.elink.ecmultimedia.utils.MusicUtils.findPositionForTime;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.elink.ecmultimedia.R;
import com.elink.ecmultimedia.adapter.music.MusicBigPlayLyricListAdapter;
import com.elink.ecmultimedia.adapter.music.MusicRightUSBListAdapter;
import com.elink.ecmultimedia.listener.MusicSeekBarListener;
import com.elink.ecmultimedia.pojo.Lyric;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicLeftPlayingFragment;
import com.elink.ecmultimedia.ui.main.myview.PlayAnimation;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.ecmultimedia.utils.ResolutionAdaptation;
import com.elink.ecmultimedia.utils.TimeUtil;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.common.EAudioSource;
import com.elink.elinkmultimediaservices.constants.common.IPlaybackStatus;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;

import java.util.ArrayList;

public class MusicBigPlayingActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView back;
    private MultimediaMainActivity mainActivity;
    private MusicRightUSBListAdapter musicRightUSBListAdapter;
    private SeekBar musicSeekbar;
    public MusicLeftPlayingFragment musicLeftPlayingFragment;

    private LinearLayout layoutNoLyricPrompt;
    private LinearLayout layoutUsbMusic;
    private LinearLayout layoutUsbCollect;
    private ImageView usbSwitch;
    private ImageView musicBefore;
    private ImageView musicNext;
    private TextView musicDurationTime;
    public TextView musicNowTime;
    private TextView musicName;
    private TextView musicSinger;
    //歌词相关
    private RecyclerView recyclerViewMusicBigPlay;
    private final ArrayList<Lyric> lyricList = new ArrayList<>();
    private MusicBigPlayLyricListAdapter musicBigPlayLyricListAdapter;
    private boolean isUserScrolling = false;//处理手动滚动列表和handle定时滚动之间的冲突
    public final Handler handler = new Handler(Looper.getMainLooper());

    public PlayAnimation playAnimation = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MyApplication.setMusicBigPlayingActivity(this);
        super.onCreate(savedInstanceState);
        LogUtils.d("");
        mainActivity = MyApplication.getMainActivity();
        musicRightUSBListAdapter = MyApplication.getMusicRightListAdapter();
        musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
        setContentView(R.layout.activity_music_big_playing);
        mergeWithStatusBar();

        //获得组件对象
        back = findViewById(R.id.back);
        createUsb();
        createBt();
    }

    @Override
    protected void onStart() {
        super.onStart();
        LogUtils.d("");
        mainActivity.foregroundActivitiesCountAdd();
    }

    @Override
    protected void onResume() {
        super.onResume();
        int musicSources = mainActivity.serviceConnection.getMusicSources();
        LogUtils.d("musicSources:" + musicSources);
        init();
        if (musicSources == IPlaySource.BT) {
            initBt();
        } else if (musicSources == IPlaySource.USB) {
            initUsb();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        LogUtils.d("");
        mainActivity.foregroundActivitiesCountMinus();
    }


    @Override
    protected void onPause() {
        super.onPause();
        LogUtils.d("");
    }

    @Override
    protected void onDestroy() {
        LogUtils.d("");
//        stopGetCurrentPosition();
        handler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }

    public void createUsb() {
        layoutUsbMusic = findViewById(R.id.layout_usb_music);
        layoutUsbCollect = findViewById(R.id.layout_usb_collect);
        usbSwitch = findViewById(R.id.music_start_stop);
        musicBefore = findViewById(R.id.music_before);
        musicNext = findViewById(R.id.music_next);
        musicSeekbar = findViewById(R.id.big_play_seekbar);
        musicDurationTime = findViewById(R.id.music_duration_time);
        musicNowTime = findViewById(R.id.music_now_time);
        musicName = findViewById(R.id.music_name);
        musicName.setSelected(true);
        musicSinger = findViewById(R.id.music_singer);
        musicName.setSelected(true);


        recyclerViewMusicBigPlay = findViewById(R.id.rv_music_big_play);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setInitialPrefetchItemCount(5); // 设置预取项的数量，尝试影响可见项的数量
        recyclerViewMusicBigPlay.setLayoutManager(layoutManager);
        if (mainActivity.lyricNow != null)
            lyricList.addAll(mainActivity.lyricNow);
        musicBigPlayLyricListAdapter = new MusicBigPlayLyricListAdapter(lyricList);
        recyclerViewMusicBigPlay.setAdapter(musicBigPlayLyricListAdapter);
        layoutNoLyricPrompt = findViewById(R.id.layout_no_lyric_prompt);
        //设置案件监听事件
        back.setOnClickListener(this);
        usbSwitch.setOnClickListener(this);
        musicNext.setOnClickListener(this);
        musicBefore.setOnClickListener(this);
        musicSeekbar.setOnSeekBarChangeListener(new MusicSeekBarListener(this));
        recyclerViewMusicBigPlay.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                //用户滚动
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    isUserScrolling = true;
                } else if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    isUserScrolling = false;
                }
            }
        });
    }


    private RelativeLayout btLayout;
    private ImageView btPre;
    private ImageView btNext;
    private ImageView btSwitch;
    private ImageView musicSourceBt;

    public void createBt() {
        btLayout = findViewById(R.id.layout_bt_music);
        btPre = findViewById(R.id.music_before_bt);
        btNext = findViewById(R.id.music_next_bt);
        btSwitch = findViewById(R.id.music_start_stop_bt);
        musicSourceBt = findViewById(R.id.music_source_bt);

        btSwitch.setOnClickListener(this);
        btPre.setOnClickListener(this);
        btNext.setOnClickListener(this);
    }

    public void init() {
        LogUtils.d("");
        mainActivity.aSwitch.bigOrSmall = 1;
        updateMusicInfo(mainActivity.musicNow);//歌名 歌手
    }

    public void initUsb() {
        LogUtils.d("");
        mainActivity.aSwitch.bigMusic = IPlaySource.USB;
        setVisibleOfBt(false);
        setVisibleOfUsb(true);
        updateMusicLyric();
        initMusicSeekbar();//seekbar
        updatePlaybackIcon();
    }

    public void initBt() {
        LogUtils.d("");
        mainActivity.aSwitch.bigMusic = IPlaySource.BT;
        setVisibleOfUsb(false);
        setVisibleOfBt(true);
        updateBtPlaybackIcon();
    }

    public void toBt() {
        pauseCallBack();
        initBt();
    }

    public void setVisibleOfBt(boolean b) {
        runOnUiThread(() -> {
            if (b) {
                btLayout.setVisibility(View.VISIBLE);
                musicSourceBt.setVisibility(View.VISIBLE);
            } else {
                btLayout.setVisibility(View.GONE);
                musicSourceBt.setVisibility(View.GONE);
            }
        });
    }

    public void setVisibleOfUsb(boolean b) {
        runOnUiThread(() -> {
            if (b) {
                layoutNoLyricPrompt.setVisibility(View.VISIBLE);
                recyclerViewMusicBigPlay.setVisibility(View.VISIBLE);
                layoutUsbMusic.setVisibility(View.VISIBLE);
                layoutUsbCollect.setVisibility(View.VISIBLE);
            } else {
                layoutNoLyricPrompt.setVisibility(View.GONE);
                recyclerViewMusicBigPlay.setVisibility(View.GONE);
                layoutUsbMusic.setVisibility(View.GONE);
                layoutUsbCollect.setVisibility(View.GONE);
            }
        });
    }

    @SuppressLint("NotifyDataSetChanged")
    public void updateMusicLyric() {
        lyricList.clear();
        if (mainActivity.lyricNow != null) {
            lyricList.addAll(mainActivity.lyricNow);
        }
        mainActivity.runOnUiThread(() -> musicBigPlayLyricListAdapter.notifyDataSetChanged());

        if (lyricList.size() == 0) {
            LogUtils.d("暂时无歌词！");
            layoutNoLyricPrompt.setVisibility(View.VISIBLE);
        } else {
            layoutNoLyricPrompt.setVisibility(View.GONE);
            handler.removeCallbacksAndMessages(null);
            lyricsScrolling(true);
        }
    }

    /**
     * isContinue 是否持续滚动
     */
    public void lyricsScrolling(boolean isContinue) {
        int nowTime = mainActivity.serviceConnection.getCurrentPosition();
        int nowPosition = findPositionForTime(lyricList, nowTime);
        musicBigPlayLyricListAdapter.setCurrentlyPlayingPosition(nowPosition);
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerViewMusicBigPlay.getLayoutManager();

        if (layoutManager != null) {
            if (!isUserScrolling) {
                //优化点： 滚动太快，上面还在修改歌词颜色会出现残影，需要延迟一点平滑滚动
                layoutManager.scrollToPositionWithOffset(nowPosition, ResolutionAdaptation.relativePositionToTop);
            }
        }
        if (isContinue) {
            int nextOPosition = nowPosition + 1;
            if (nextOPosition < lyricList.size()) {
                long nextTime = lyricList.get(nextOPosition).time;
                long delayTime = (nextTime - (long) nowTime);
                handler.postDelayed(() -> lyricsScrolling(true), delayTime);
            }
        }
    }


    @SuppressLint("SetTextI18n")
    public void initMusicSeekbar() {
        LogUtils.d("");
        if (mainActivity.musicNow != null) {
            int currentPosition = mainActivity.serviceConnection.getCurrentPosition();
            musicDurationTime.setText(TimeUtil.formatTime(mainActivity.musicNow.duration, mainActivity.musicNow.duration));
            musicNowTime.setText(TimeUtil.formatTime(currentPosition, mainActivity.musicNow.duration));
            musicSeekbar.setMax(mainActivity.musicNow.duration);//设置滑动条的最大值
            musicSeekbar.setProgress(currentPosition);//滑条到指定位置
        }
    }

    private void initNextMusicSeekbar() {
        LogUtils.d("");
        if (mainActivity.musicNow != null) {
            int currentPosition = mainActivity.serviceConnection.getCurrentPosition();
            musicDurationTime.setText(TimeUtil.formatTime(mainActivity.musicNow.duration, mainActivity.musicNow.duration));
            musicNowTime.setText(TimeUtil.formatTime(currentPosition, mainActivity.musicNow.duration));
            musicSeekbar.setMax(mainActivity.musicNow.duration);//设置滑动条的最大值
            musicSeekbar.setProgress(0);//滑条到指定位置
        }
    }

    /**
     * 将活动和状态栏相融合
     */
    public void mergeWithStatusBar() {
        View decorView = getWindow().getDecorView();
        //设置活动布局将显示在状态栏上
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        //设置状态栏颜色为透明
        getWindow().setStatusBarColor(Color.TRANSPARENT);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.back) {
            LogUtils.d("返回");
            onBackPressed();
        } else if (v.getId() == R.id.music_start_stop) {
            if (mainActivity.canUsbPlay()) {
                if (mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PAUSE && mainActivity.musicNow != null) {
                    usbPlay();
                } else if (mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PLAYING) {
                    usbPause();
                }
            }
        } else if (v.getId() == R.id.music_start_stop_bt) {
            if (mainActivity.canBtPlay()) {
                if (mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PAUSE) {
                    btPlay();
                } else if (mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PLAYING) {
                    btPause();
                }
            }
        } else if (v.getId() == R.id.music_next) {
            if (mainActivity.canUsbPlay())
                mainActivity.serviceConnection.next();
        } else if (v.getId() == R.id.music_before) {
            if (mainActivity.canUsbPlay())
                mainActivity.serviceConnection.prev();
        } else if (v.getId() == R.id.music_next_bt) {
            if (mainActivity.canBtPlay())
                mainActivity.serviceConnection.next();
        } else if (v.getId() == R.id.music_before_bt) {
            if (mainActivity.canBtPlay())
                mainActivity.serviceConnection.prev();
        }
    }

    /**
     * 进度条和时间修改
     *
     * @param progress
     */
    public void setSeekBarProgress(int progress) {
        mainActivity.runOnUiThread(() -> {
            musicNowTime.setText(TimeUtil.formatTime(progress, mainActivity.musicNow.duration));
            musicSeekbar.setProgress(progress);
        });
    }

    /**
     * 指定歌曲播放
     */
    public void usbPlay(MusicFile musicFile) {
        LogUtils.d("");
        //数据修改
        mainActivity.musicNow = musicFile;
        mainActivity.musicPlayer.duration = musicFile.duration;
        //播放

        mainActivity.setAudioSources(EAudioSource.AUDIO_SOURCE_USB);
        mainActivity.serviceConnection.setMusicPlayNow(musicFile);
        mainActivity.serviceConnection.playByMusic();
    }

    public void usbPlay() {
        LogUtils.d("");
        mainActivity.setAudioSources(EAudioSource.AUDIO_SOURCE_USB);
        mainActivity.serviceConnection.play();
    }

    public void btPlay() {
        LogUtils.d("");
        mainActivity.setAudioSources(EAudioSource.AUDIO_SOURCE_BT);
        mainActivity.serviceConnection.play();
    }

    /**
     * 暂停操作 包括以下操作
     * 修改状态
     * 修改图标
     * 记录播放位置
     * 取消定时任务
     */
    public void usbPause() {
        LogUtils.d("暂停");
        handler.removeCallbacksAndMessages(null);//将队列中的任务全部清除
        //修改播放状态
        mainActivity.musicPlayer.status = IPlaybackStatus.STATUS_PAUSE;
        //修改图标
        updatePlaybackIcon();
        //暂停
        mainActivity.serviceConnection.pause();
    }

    public void btPause() {
        LogUtils.d("暂停");
        //修改播放状态
        mainActivity.musicPlayer.status = IPlaybackStatus.STATUS_PAUSE;
        //修改图标
        updateBtPlaybackIcon();
        //暂停
        mainActivity.serviceConnection.pause();
    }

    /**
     * 回调暂停
     */
    public void pauseCallBack() {
        LogUtils.d("暂停");
        //修改播放状态
        mainActivity.musicPlayer.status = IPlaybackStatus.STATUS_PAUSE;
        int musicSources = mainActivity.serviceConnection.getMusicSources();
        updateMusicInfo(mainActivity.musicNow);
        if (musicSources == IPlaySource.USB) {
            updateMusicLyric();
            handler.removeCallbacksAndMessages(null);//将队列中的任务全部清除
            //修改图标
            updatePlaybackIcon();
            initMusicSeekbar();
            // 动画暂停
            if (playAnimation != null) {
                playAnimation.pauseHandler();
            }
        } else if (musicSources == IPlaySource.BT) {
            updateBtPlaybackIcon();
        }
    }


    /**
     * 服务端信息修改回调
     * 歌曲信息、进度条信息
     *
     * @param musicFile
     */
    public void nextMusicCallback(MusicFile musicFile) {
        LogUtils.d("下一首 UI 修改");
        mainActivity.musicPlayer.status = IPlaybackStatus.STATUS_PLAYING;
        updateMusicInfo(musicFile);
        int musicSources = mainActivity.serviceConnection.getMusicSources();
        if (musicSources == IPlaySource.USB) {
            MyApplication.getMusicBigPlayListFragment().nextMusicCallback();
            updateMusicLyric();
            initNextMusicSeekbar();
            handler.removeCallbacksAndMessages(null);
            lyricsScrolling(true);

            //如果当前是暂停状态就改为播放，候暂停的时候点击下一首这种情况
            updatePlaybackIcon();
        } else if (musicSources == IPlaySource.BT) {
            updateBtPlaybackIcon();
        }

    }

    /*------------------------------------ ui 更新 --------------------------------------------*/

    /**
     * 歌曲信息
     *
     * @param music
     */
    public void updateMusicInfo(MusicFile music) {
        runOnUiThread(() -> {
            if (music != null) {
                updateUiOfMusicName(music.fileName);
                updateUiOfSingerName(music.artist);
                musicSeekbar.setMax(music.duration);
            }
        });
    }


    public void updatePlaybackIcon() {
        LogUtils.d("");
        runOnUiThread(() -> {
            if (mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PLAYING) {
                updateUiOfPlay();
            } else if (mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PAUSE) {
                updateUiOfPause();
            }
        });
    }

    public void updateBtPlaybackIcon() {
        runOnUiThread(() -> {
            if (mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PLAYING) {
                updateUiOfBtPlay();
            } else if (mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PAUSE) {
                updateUiOfBtPause();
            }
        });
    }

    /*------------------------------------ ui统一更新 --------------------------------------------*/
    public void updateUiOfPause() {
        LogUtils.d("");
        if (mainActivity.isForegroundActivities()) {
            usbSwitch.setImageResource(R.drawable.music_player_btn_play);
        } else {
            LogUtils.d("后台不更新UI");
        }
    }

    public void updateUiOfBtPause() {
        LogUtils.d("");
        if (mainActivity.isForegroundActivities()) {
            btSwitch.setImageResource(R.drawable.music_player_btn_play);
        } else {
            LogUtils.d("后台不更新UI");
        }
    }

    public void updateUiOfPlay() {
        LogUtils.d("");
        if (mainActivity.isForegroundActivities()) {
            usbSwitch.setImageResource(R.drawable.music_player_btn_pause);
        } else {
            LogUtils.d("后台不更新UI");
        }
    }

    public void updateUiOfBtPlay() {
        LogUtils.d("");
        if (mainActivity.isForegroundActivities()) {
            btSwitch.setImageResource(R.drawable.music_player_btn_pause);
        } else {
            LogUtils.d("后台不更新UI");
        }
    }

    public void updateUiOfMusicName(String fileName) {
        LogUtils.d("");
        if (mainActivity.isForegroundActivities()) {
            musicName.setText(fileName);
        } else {
            LogUtils.d("后台不更新UI");
        }
    }

    public void updateUiOfSingerName(String singerName) {
        LogUtils.d("");
        if (mainActivity.isForegroundActivities()) {
            musicSinger.setText(singerName);
        } else {
            LogUtils.d("后台不更新UI");
        }
    }


    /*------------------------------------ 回调 --------------------------------------------*/
    public void playNewCallback(int isNew) {
        LogUtils.d("");
        mainActivity.musicPlayer.status = IPlaybackStatus.STATUS_PLAYING;
        int musicSources = mainActivity.serviceConnection.getMusicSources();
        if (isNew == 0) {
            if (musicSources == IPlaySource.USB) {
                initMusicSeekbar();
                musicSeekbar.setVisibility(View.VISIBLE);
                updatePlaybackIcon();
                handler.removeCallbacksAndMessages(null);
                lyricsScrolling(true);
                if (playAnimation != null) {
                    playAnimation.continueHandler();
                }
            } else if (musicSources == IPlaySource.BT) {
                updateBtPlaybackIcon();
            }
        }
    }

    public void usbLostCallback() {
        onBackPressed();
    }
}