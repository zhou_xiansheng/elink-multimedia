package com.elink.ecmultimedia.ui.main.fragment.radio;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.elink.ecmultimedia.R;
import com.elink.ecmultimedia.adapter.radio.CollectionListAdapter;
import com.elink.ecmultimedia.datamodel.RadioOperations;
import com.elink.ecmultimedia.listener.RadioSeekBarListener;
import com.elink.ecmultimedia.pojo.Radio;
import com.elink.ecmultimedia.ui.main.myview.CustomDialog;
import com.elink.ecmultimedia.ui.main.myview.CustomerSeekBar;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.elinkmultimediaservices.beans.RadioChannel;
import com.elink.elinkmultimediaservices.constants.common.EAudioSource;
import com.elink.elinkmultimediaservices.constants.common.IPlaybackStatus;

import java.util.ArrayList;
import java.util.LinkedList;


public class RadioFragment extends Fragment implements View.OnClickListener {

    private MultimediaMainActivity mainActivity;
    private Button mFM;
    private Button mAM;

    public TextView radioNowView;
    public ImageView isCollectImage;
    private LinearLayout layoutNoAvailablePrompt;

    //收藏列表相关
    public RecyclerView collectionRadioRecycler;
    private CollectionListAdapter mCollectionListAdapter;
    private final ArrayList<Radio> mDataList = new ArrayList<>();

    private CustomerSeekBar mCustomerSeekBar;

    public ImageView radioSwitch;
    private ImageView radioPreBtn;
    private ImageView radioNextBtn;
    private ImageView toRadioList;
    private MultimediaMainActivity mMainActivity;
    private RadioSeekBarListener seekBarChangeListener;

    private CustomerSeekBar customerSeekBar;
    public CustomDialog mCustomDialog;

    public RadioFragment() {
        LogUtils.d("");
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //设置全局context,注意需要在碎片初始化前面
        LogUtils.d("");
        MyApplication.setRadioFragment(this);
        View view = inflater.inflate(R.layout.fragment_radio, container, false);
        mainActivity = MyApplication.getMainActivity();
        seekBarChangeListener = new RadioSeekBarListener();
        MyApplication.setSeekBarChangeListener(seekBarChangeListener);

        //初始化控件
        mFM = view.findViewById(R.id.btn_fm);
        radioSwitch = view.findViewById(R.id.radio_switch);
        toRadioList = view.findViewById(R.id.to_radio_list);
        radioNowView = view.findViewById(R.id.tv_radio_now);
        isCollectImage = view.findViewById(R.id.iv_collect);
        collectionRadioRecycler = view.findViewById(R.id.rv_collection_radio);
        mCustomerSeekBar = view.findViewById(R.id.customerSeekBar);
        mMainActivity = MyApplication.getMainActivity();
        customerSeekBar = MyApplication.getCustomerSeekBar();
        layoutNoAvailablePrompt = view.findViewById(R.id.layout_no_available_prompt);
        radioPreBtn = view.findViewById(R.id.radio_pre_btn);
        radioNextBtn = view.findViewById(R.id.radio_next_btn);

        //指定 recycleView 的布局方式。这里线性布局，默认垂直
        collectionRadioRecycler.setLayoutManager(new LinearLayoutManager(requireContext()));
        /*设置适配器*/
        mCollectionListAdapter = new CollectionListAdapter(mainActivity.radioCollect);
        collectionRadioRecycler.setAdapter(mCollectionListAdapter);
        MyApplication.setCollectionRadioAdapter(mCollectionListAdapter);
        //刷新列表
        mCollectionListAdapter.notifyDataSetChanged();
        //添加监听器
        mFM.setOnClickListener(this);
        isCollectImage.setOnClickListener(this);
        radioSwitch.setOnClickListener(this);
        toRadioList.setOnClickListener(this);

        mCustomerSeekBar.setOnSeekBarChangeListener(seekBarChangeListener);
        radioPreBtn.setOnClickListener(this);
        radioNextBtn.setOnClickListener(this);
        view.findViewById(R.id.circulate_play).setOnClickListener(this);

        init();

        mCustomDialog = new CustomDialog(requireContext(), "电台搜索中");
        return view;
    }

    @Override
    public void onResume() {
        LogUtils.d("");
        super.onResume();
        if (mainActivity.radioPlayer.isSeek) {
            mCustomDialog.show();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mainActivity.radioPlayer.isSeek) {
            mCustomDialog.disMiss();
        }
    }

    /**
     * 将当前播放 的 FM 滚动至可见区域
     */
    public void scrollToCollect() {
        LogUtils.d("");
        LinkedList<Radio> radioCollect = mainActivity.radioCollect;
        int index = -1;
        for (int i = 0; i < radioCollect.size(); i++) {
            if (radioCollect.get(i).thumbPos == mainActivity.radio.thumbPos) {
                index = i;
                break;
            }
        }
        if (index != -1) {
            LinearLayoutManager layoutManager = (LinearLayoutManager) collectionRadioRecycler.getLayoutManager();
            if (layoutManager != null)
                layoutManager.scrollToPositionWithOffset(index, 130);
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    public void init() {
        //初始化部分控件内容
        updateTextView();
        if (mainActivity.radioPlayer.status == IPlaybackStatus.STATUS_PLAYING) {
            radioSwitch.setImageResource(R.drawable.radio_pause); //修改图标
        } else if (mainActivity.radioPlayer.status == IPlaybackStatus.STATUS_PAUSE) {
            radioSwitch.setImageResource(R.drawable.radio_play); //修改图标
        }
        lightOrGreyView();
        enableOrNotSeekBar();
        mainActivity.aSwitch.radioPage = 0;
        setVisibilityOfLayoutNoAvailablePrompt();
        scrollToCollect();

        //
        CollectionListAdapter collectionRadioAdapter = MyApplication.getCollectionRadioAdapter();
        if (collectionRadioAdapter != null) {
            collectionRadioAdapter.notifyDataSetChanged();
        }
    }


    public void lightOrGreyView() {
        LogUtils.d("");
        if (mainActivity.radioPlayer.status == IPlaybackStatus.STATUS_PLAYING) {
            radioNextBtn.clearColorFilter();
            radioPreBtn.clearColorFilter();
        } else if (mainActivity.radioPlayer.status == IPlaybackStatus.STATUS_PAUSE) {
            radioPreBtn.setColorFilter(Color.argb(150, 0, 0, 0), PorterDuff.Mode.SRC_ATOP);
            radioNextBtn.setColorFilter(Color.argb(150, 0, 0, 0), PorterDuff.Mode.SRC_ATOP);
        }
    }

    public void enableOrNotSeekBar() {
        LogUtils.d("");
        mainActivity = MyApplication.getMainActivity();
        mainActivity.runOnUiThread(() -> {
            if (mainActivity.radioPlayer.isScan || mainActivity.radioPlayer.isSeek || mainActivity.isBtPhoning()) {
                mCustomerSeekBar.setEnabled(false);
            } else {
                mCustomerSeekBar.setEnabled(true);
            }
        });

    }


    public void setVisibilityOfLayoutNoAvailablePrompt() {
        int size = mainActivity.radioCollect.size();
        LogUtils.d("size:" + size);
        if (size > 0) {
            layoutNoAvailablePrompt.setVisibility(View.GONE);
        } else {
            layoutNoAvailablePrompt.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 应用初始化时
     * 更新频率显示，是否收藏图标
     */
    @SuppressLint("DefaultLocale")
    private void updateTextView() {
        FragmentActivity fragmentActivity = null;
        //电台数值显示
        if (isAdded()) {
            fragmentActivity = requireActivity();
        }
        if (fragmentActivity == null && mainActivity != null) {
            fragmentActivity = mainActivity;
        }
        if (fragmentActivity != null) {
            //电台频率设置
            updateRadioId();
            //收藏图标设置
            updateCollectImage();
        } else {
            LogUtils.d("没有找到合适的 activity");
        }
    }

    @SuppressLint({"ResourceAsColor", "NotifyDataSetChanged"})
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_collect) {
            //电台的收藏和取消收藏
            int isCollect = mainActivity.radio.isCollect;
            //反着来，收藏变未收藏，未收藏变收藏
            if (isCollect == 0) {
                LogUtils.d("收藏");
                mainActivity.radio.isCollect = 1;
                mainActivity.radioCollect.add(mainActivity.radio); //添加到收藏列表中,放到队首部
                RadioOperations.insertCollectRadio(MyApplication.getContext(), mainActivity.radio);
                updateCollectImage();
                //同时更新搜索列表的状态，如果存在就修改
                for (Radio radio : mainActivity.searchList
                ) {
                    if (radio.thumbPos == mainActivity.radio.thumbPos) {
                        radio.isCollect = 1;
                        break;
                    }
                }
            } else if (isCollect == 1) {
                LogUtils.d("取消收藏");
                mainActivity.radio.isCollect = 0;
                //逆序删除避免漏掉删除
                for (int i = mainActivity.radioCollect.size() - 1; i >= 0; i--) {
                    if (mainActivity.radio.thumbPos == mainActivity.radioCollect.get(i).thumbPos
                    ) {
                        mainActivity.radioCollect.remove(i);
                    }
                }
                updateCollectImage();
                RadioOperations.deleteCollectRadioByThumbPos(MyApplication.getContext(), mainActivity.radio.thumbPos);
                //同时更新搜索列表的状态
                for (Radio radio : mainActivity.searchList
                ) {
                    if (radio.thumbPos == mainActivity.radio.thumbPos) {
                        radio.isCollect = 0;
                        break;
                    }
                }
            }
            //刷新收藏列表
            notifyDataSetChanged();
        } else if (v.getId() == R.id.radio_switch) {
            //电台的播放和暂停
            if (mainActivity.radioPlayer.status == IPlaybackStatus.STATUS_PLAYING) {
                radioSwitchToPause();
            } else if (mainActivity.radioPlayer.status == IPlaybackStatus.STATUS_PAUSE) {
                if (mainActivity.canFmPlay()) {
                    play();
                }
            }
        } else if (v.getId() == R.id.to_radio_list) {
            mainActivity.pageChange(1);
        } else if (v.getId() == R.id.radio_pre_btn) {
            if (!mainActivity.radioPlayer.isScan) {
                if (mainActivity.radioPlayer.status == IPlaybackStatus.STATUS_PLAYING) {
                    if (mainActivity.canFmPlay()) {
                        LogUtils.d("上一台");
                        pre();
                    }
                }
            } else
                Toast.makeText(MyApplication.getContext(), "请先停止搜索!", Toast.LENGTH_SHORT).show();
        } else if (v.getId() == R.id.radio_next_btn) {
            if (!mainActivity.radioPlayer.isScan) {
                if (mainActivity.radioPlayer.status == IPlaybackStatus.STATUS_PLAYING) {
                    if (mainActivity.canFmPlay()) {
                        LogUtils.d("下一台");
                        next();
                    }
                }
            } else
                Toast.makeText(MyApplication.getContext(), "请先停止搜索!", Toast.LENGTH_SHORT).show();
        } else if (v.getId() == R.id.circulate_play) {
            Toast.makeText(MyApplication.getContext(), "敬请期待!", Toast.LENGTH_SHORT).show();
        }
    }

    public void updateSeekBar(int thumbPos) {
        if (mainActivity == null) {
            mainActivity = MyApplication.getMainActivity();
        }
        if (mainActivity != null) {
            LogUtils.d("");
            customerSeekBar.setProgress(thumbPos);
        }
    }

    private void play() {
        LogUtils.d("");
        mainActivity.setAudioSources(EAudioSource.AUDIO_SOURCE_FM);
        mainActivity.serviceConnection.play();
    }

    public void radioSwitchToStart(float radioId) {
        LogUtils.d("");
        RadioChannel radioChannel = new RadioChannel();
        radioChannel.setChannelNum((int) (radioId * 10));
        if (mainActivity.serviceConnection.playSpecialChannel(radioChannel)) {
            mainActivity.setAudioSources(EAudioSource.AUDIO_SOURCE_FM);
        }
    }

    /**
     * 回调使用
     */
    public void playCallBack() {
        LogUtils.d("");
        mainActivity.radioPlayer.status = IPlaybackStatus.STATUS_PLAYING; //改为播放
        radioSwitch.setImageResource(R.drawable.radio_pause); //修改图标
        lightOrGreyView();
    }

    public void searchListToStart(int radioId) {
        LogUtils.d("");
        if (!mainActivity.radioPlayer.isSeek) {
            RadioChannel radioChannel = new RadioChannel();
            radioChannel.setChannelNum(radioId);
            if (mainActivity.serviceConnection.playSpecialChannel(radioChannel)) {
                mainActivity.radioPlayer.status = IPlaybackStatus.STATUS_PLAYING; //改为播放
                radioSwitch.setImageResource(R.drawable.radio_pause); //修改图标
                lightOrGreyView();
            }
        }
    }


    public void next() {
        LogUtils.d("");
        mainActivity.serviceConnection.next();
        scrollToCollect();
    }

    public void pre() {
        LogUtils.d("");
        mainActivity.serviceConnection.prev();
        scrollToCollect();
    }


    public void radioSwitchToPause() {
        LogUtils.d("");
        //电台暂停
        if (mainActivity.serviceConnection.stop()) {
            lightOrGreyView();
        }
    }

    public void pauseCallback() {
        LogUtils.d("");
        //电台暂停
        mainActivity.radioPlayer.status = IPlaybackStatus.STATUS_PAUSE;
        radioSwitch.setImageResource(R.drawable.radio_play);
        lightOrGreyView();
    }


    public void onFocusLostCallback() {
    }

    /* ------------------------------- UI 修改-------------------------------------*/

    @SuppressLint("DefaultLocale")
    public void updateRadioId() {
        //电台频率设置
        if (mainActivity == null)
            mainActivity = MyApplication.getMainActivity();
        if (mainActivity != null) {
            mainActivity.runOnUiThread(() -> radioNowView.setText(String.format("%.1f", mainActivity.radio.radioId)));
        }
    }

    @SuppressLint("DefaultLocale")
    public void updateRadioId(int progress) {
        //电台频率设置
        if (mainActivity == null)
            mainActivity = MyApplication.getMainActivity();
        if (mainActivity != null) {
            mainActivity.runOnUiThread(() -> radioNowView.setText(String.format("%.1f", (progress + 1) / 10f + 87.4f)));
        }
    }

    public void updateCollectImage() {
        if (mainActivity == null) {
            mainActivity = MyApplication.getMainActivity();
        }
        if (mainActivity != null) {
            mainActivity.runOnUiThread(() -> {
                if (mainActivity.radio.isCollect == 1) {
                    isCollectImage.setImageResource(R.drawable.ic_player_icon_fav_s);
                } else {
                    isCollectImage.setImageResource(R.drawable.ic_player_icon_fav_n);
                }
            });
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    public void notifyDataSetChanged() {
        LogUtils.d("");
        //修改收藏列表中的背景图片
        if (mainActivity == null) {
            mainActivity = MyApplication.getMainActivity();
        }
        if (mainActivity != null) {
            mainActivity.runOnUiThread(() -> {
                mCollectionListAdapter.notifyDataSetChanged();
                scrollToCollect();
                setVisibilityOfLayoutNoAvailablePrompt();
            });
        }
    }
}