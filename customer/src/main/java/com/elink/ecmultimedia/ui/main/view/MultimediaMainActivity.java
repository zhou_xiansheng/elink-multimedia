package com.elink.ecmultimedia.ui.main.view;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemProperties;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.elink.ecmultimedia.R;
import com.elink.ecmultimedia.adapter.music.MusicRightUSBListAdapter;
import com.elink.ecmultimedia.cache.MusicCache;
import com.elink.ecmultimedia.datamodel.RadioOperations;
import com.elink.ecmultimedia.datamodel.SharedPreferencesControl;
import com.elink.ecmultimedia.pojo.Lyric;
import com.elink.ecmultimedia.pojo.MusicPlayer;
import com.elink.ecmultimedia.pojo.Radio;
import com.elink.ecmultimedia.pojo.RadioPlayer;
import com.elink.ecmultimedia.pojo.Switch;
import com.elink.ecmultimedia.receivers.UsbProviderReceiver;
import com.elink.ecmultimedia.connection.MediaConnection;
import com.elink.ecmultimedia.thread.LoadMusicFileResourcesThread;
import com.elink.ecmultimedia.thread.ScanHandler;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicFragment;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicLeftPlayingFragment;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicRightMenuListFragment;
import com.elink.ecmultimedia.ui.main.fragment.radio.RadioFragment;
import com.elink.ecmultimedia.ui.main.fragment.radio.RadioSearchListFragment;
import com.elink.ecmultimedia.utils.Constants;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MediaConstants;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.ecmultimedia.utils.MediaToast;
import com.elink.ecmultimedia.utils.ResolutionAdaptation;
import com.elink.ecmultimedia.utils.Utils;
import com.elink.ecmultimedia.utils.Worker;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.common.EAudioSource;
import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.constants.common.EUsbStatus;
import com.elink.elinkmultimediaservices.constants.common.ICustomerPage;
import com.elink.elinkmultimediaservices.constants.common.IPlaybackStatus;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;

import org.litepal.crud.DataSupport;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class MultimediaMainActivity extends AppCompatActivity implements View.OnClickListener {
    public View view;
    public Switch aSwitch;
    private LinearLayout mLayoutBgMyMusic;
    private TextView mMyMusic;
    private LinearLayout mLayoutBgRadio;
    private TextView mRadio;

    private RelativeLayout imageViewSplash;

    public Worker worker;

    /*------------------------------------------收音机相关数据-------------------------------------------------*/
    public RadioPlayer radioPlayer;//收音机播放器

    public static LinkedList<Radio> radioAll; // 数据库列表

    public LinkedList<Radio> radioCollect = new LinkedList<>(); // 收藏列表
    public List<Radio> searchList = new ArrayList<>(); // 搜索列表


    public Radio radio;//当前电台


    /*------------------------------------------音乐相关数据-------------------------------------------------*/
    public static final String STOP_PLAYBACK_ACTION = "android.hardware.usb.action.USB_DEVICE_DETACHED";
    public static final String START_PLAYBACK_ACTION = "android.hardware.usb.action.USB_DEVICE_ATTACHED";
    private final UsbProviderReceiver usbProviderReceiver = new UsbProviderReceiver();
    public MusicPlayer musicPlayer = new MusicPlayer();//音乐播放控制器
    public LinkedList<MusicFile> musicHistory = new LinkedList<>();//收藏列表
    public boolean isSeekBarChanging = false;//收音机进度条和定时器的焦点事件，要么手动滑动，要么通过定时器滑动
    public MusicFile musicNow;//当前播放的音乐
    public ArrayList<Lyric> lyricNow;//当前播放的音乐
    public String[] musicPath = new String[]{"", ""};//进入右边菜单时当前的路径
    private final Stack<Integer> stack = new Stack<>();


    /*------------------------------------------远程服务的绑定-------------------------------------------------*/
    public void doUsbDisconnect() {
        LogUtils.d("");
        MusicCache.clearCache();
        musicPlayer.mUsbStatus = EUsbStatus.DISCONNECT;
        if (isForegroundActivities()) {
            //当应用在前台的时候 才进行清空，否则在后台拔掉u盘再插进去，扫描完成后再打开进入u盘模块可能会造成数据不一致闪退
            MusicRightMenuListFragment musicRightMenuListFragment = MyApplication.getMusicRightMenuListFragment();
            if (musicRightMenuListFragment != null) {
                //将右边usb列表数据清空
                musicRightMenuListFragment.replaceUsbData(new ArrayList<>());
                MyApplication.getMainActivity().runOnUiThread(musicRightMenuListFragment::usbDetachCallback);
            }
        }
        if (serviceConnection.getMusicSources() == IPlaySource.USB) {
            musicPlayer.status = IPlaybackStatus.STATUS_PAUSE;
            musicNow = new MusicFile();
            musicNow.playSource = IPlaySource.USB;
        }
    }


    /**
     * 活动与音乐服务建立连接
     */
    public MediaConnection serviceConnection;

    public MediaToast mediaToast;
    /**
     * 前台判断
     */
    private int foregroundActivities;//当前在前台页面的数量

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LogUtils.d("");
        //启动和绑定服务
        startMusicService();
        //数据初始化
        dataInit();
        usbStatusInit();
        radioDataInit();
        ResolutionAdaptation.doAdaptation();
        super.onCreate(savedInstanceState);
        MyApplication.setMainActivity(this); //将自己设置到全局环境当中去
        setContentView(R.layout.activity_main);
        Utils.mergeWithStatusBar(this);//状态栏融合
        //组件初始化
        imageViewSplash = findViewById(R.id.startSplash);
        mLayoutBgMyMusic = findViewById(R.id.layout_bg_my_music);
        mLayoutBgRadio = findViewById(R.id.layout_bg_radio);
        mMyMusic = findViewById(R.id.btn_my_music);
        mRadio = findViewById(R.id.btn_radio);
        //绑定单击事件
        mLayoutBgMyMusic.setOnClickListener(this);
        mLayoutBgRadio.setOnClickListener(this);
        //注册广播接受器
        registerUsbReceiver();
        registerUsbProviderReceiver();
        addFragment(); // 页面添加
        //
        MusicCache.updateMusicFiles();
        //
        worker = new Worker();
        // 开机动画
        new Handler().postDelayed(() -> imageViewSplash.setVisibility(View.GONE), 1500); //停止动画

        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            imageViewSplash.setVisibility(View.GONE);
        }, 1500); //停止动画
    }


    @Override
    protected void onStart() {
        super.onStart();
        foregroundActivities++;
    }

    public static boolean firstResume = true;

    @Override
    protected void onResume() {
        LogUtils.d("");
        super.onResume();
        if (firstResume) {
            //开机历史记录恢复
            Intent intent = getIntent();
            pageResume(intent, false);
            intent.putExtra("page", ICustomerPage.PAGE_NO);
            intent.putExtra("isPlay", EPlayStatus.STATUS_PAUSE.toString());
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        //mode键切换
        pageResume(intent, true);
    }

    @Override
    protected void onStop() {
        super.onStop();
        foregroundActivities--;
    }


    @SuppressLint({"CommitTransaction", "NotifyDataSetChanged"})
    @Override
    public void onBackPressed() {
        if (aSwitch.musicOrRadio == Switch.PATH_MUSIC) {
            LogUtils.d("mUsbStatus:" + musicPlayer.mUsbStatus
                    + " musicPath[0]:" + musicPath[0]
                    + " isRoot:" + MediaConstants.path.equals(MediaConstants.rootPath));
            //如果是音乐播放界面
            MusicRightMenuListFragment musicRightMenuListFragment = MyApplication.getMusicRightMenuListFragment();
            if (musicPath[0].equals(MediaConstants.USB_MUSIC)) {
                if (musicPlayer.mUsbStatus != EUsbStatus.DISCONNECT
                        && !MediaConstants.path.equals(MediaConstants.rootPath)) {
                    LogUtils.d("返回上级目录");
                    LoadMusicFileResourcesThread.setLoading(MediaConstants.path, false);
                    MyApplication.getMusicRightListAdapter().removeCallbacksAndMessages();
                    MusicFragment musicFragment = MyApplication.getMusicFragment();
                    musicRightMenuListFragment.setLayoutHaveUsbPrompt(); //隐藏提示
                    MediaConstants.path = Paths.get(MediaConstants.path).getParent().toString();

                    ScanHandler scanHandler = new ScanHandler(MediaConstants.path, MyApplication.getMusicRightListAdapter().backgroundHandler);
                    Message message = scanHandler.handler.obtainMessage(ScanHandler.SCAN);
                    message.arg1 = MusicRightUSBListAdapter.reFresh_back;
                    message.sendToTarget();
                    try {
                        int lastIndex = musicPath[1].lastIndexOf("/");
                        if (lastIndex != -1) {
                            musicPath[1] = musicPath[1].substring(0, lastIndex);
                            musicFragment.setMusicPath();
                        }
                    } catch (Exception e) {
                        LogUtils.e(e.getMessage(), e);
                    }
                } else {
                    musicRightMenuListFragment.toMenu();
                }
                return;
            } else if (musicPath[0].equals(MediaConstants.BT_MUSIC)) {
                musicRightMenuListFragment.toMenu();
                return;
            } else if (musicPath[0].equals(MediaConstants.HISTORY_MUSIC)) {
                if (musicPlayer.isEditor) {
                    musicRightMenuListFragment.unSelectAll();
                    musicRightMenuListFragment.notifyHistoryDataSetChanged();
                }
                musicRightMenuListFragment.toMenu();
                return;
            }
        } else if (aSwitch.musicOrRadio == Switch.PATH_RADIO) {
            //收音机页面
            if (aSwitch.radioPage == 1) {
                pageChange(R.id.btn_radio); //搜索页面
                return;
            }
        }
        LogUtils.d("退出多媒体");
        moveTaskToBack(false);
    }

    /**
     * 单击事件处理
     *
     * @param v The view that was clicked.
     */
    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onClick(View v) {
        LinearLayout backgroundNor = null;
        LinearLayout backgroundPre = null;
        TextView textNor = null;
        TextView textPre = null;
        if (v.getId() == R.id.layout_bg_my_music) {
            LogUtils.d("跳转到我的音乐");
            aSwitch.musicOrRadio = 0;
            Drawable drawableSel = getResources().getDrawable(R.drawable.icon_music_sel);
            Drawable drawableNor = getResources().getDrawable(R.drawable.icon_radio_nor);
            mMyMusic.setCompoundDrawablesWithIntrinsicBounds(null, drawableSel, null, null);
            mRadio.setCompoundDrawablesWithIntrinsicBounds(null, drawableNor, null, null);
            backgroundNor = mLayoutBgRadio;
            backgroundPre = mLayoutBgMyMusic;
            textNor = mRadio;
            textPre = mMyMusic;
        } else if (v.getId() == R.id.layout_bg_radio) {
            LogUtils.d("跳转到收音机");
            aSwitch.musicOrRadio = 1;
            Drawable drawableSel = getResources().getDrawable(R.drawable.icon_radio_sel);
            Drawable drawableNor = getResources().getDrawable(R.drawable.icon_music_nor);
            mMyMusic.setCompoundDrawablesWithIntrinsicBounds(null, drawableNor, null, null);
            mRadio.setCompoundDrawablesWithIntrinsicBounds(null, drawableSel, null, null);
            backgroundNor = mLayoutBgMyMusic;
            backgroundPre = mLayoutBgRadio;
            textNor = mMyMusic;
            textPre = mRadio;
            //有音乐历史记录勾选的情况，需要进行清除勾选
            MusicFragment musicFragment = MyApplication.getMusicFragment();
            if (musicFragment != null) {
                ArrayList<MusicFile> musicDeleteList = musicFragment.musicDeleteList;
                if (musicDeleteList != null) {
                    musicDeleteList.forEach(musicFile -> {
                        for (int i = 0; i < musicHistory.size(); i++) {
                            MusicFile file = musicHistory.get(i);
                            if (file.fileName.equals(musicFile.fileName))
                                file.isSelected = false;
                        }
                    });
                    musicDeleteList.clear();
                }
            }
        }

        //音乐还是收音机
        if (backgroundNor != null && backgroundPre != null) {
            //切换碎片
            if (aSwitch.musicOrRadio == 0) {
                pageChange(R.id.btn_my_music);
            } else if (aSwitch.musicOrRadio == 1) {
                if (aSwitch.radioPage == 0)
                    pageChange(R.id.btn_radio);
                else if (aSwitch.radioPage == 1)
                    pageChange(1);
            }
            //切换按钮和文字的背景图
            backgroundNor.setBackground(null);
            backgroundPre.setBackgroundResource(R.drawable.button_selected);
            textPre.setTextColor(ContextCompat.getColor(textNor.getContext(), R.color.light_blue0));
            textNor.setTextColor(ContextCompat.getColor(textNor.getContext(), R.color.white));
        }
    }

    /**
     * 开机历史记录恢复、mode键切换
     */
    public void pageResume(Intent intent, boolean onNewIntent) {
        String page = intent.getStringExtra("page");
        String isPlay = intent.getStringExtra("isPlay");
        LogUtils.d("page:" + page + " isPlay:" + isPlay);
        if (ICustomerPage.PAGE_FM.equals(page)) {
            // 延迟半秒后跳转到 FM播放
            new Handler(Looper.getMainLooper()).postDelayed(() -> {
                if (aSwitch.musicOrRadio != Switch.PATH_RADIO) {
                    mLayoutBgMyMusic.performClick();
                }
                mLayoutBgRadio.performClick();
                if (EPlayStatus.STATUS_PLAYING.toString().equals(isPlay)) {
                    RadioFragment radioFragment = MyApplication.getRadioFragment();
                    if (radioFragment != null) {
                        radioFragment.radioSwitchToStart(radio.radioId);
                    }
                }
            }, 500);
        } else if (ICustomerPage.PAGE_BT.equals(page) || ICustomerPage.PAGE_USB.equals(page)) {
            if (aSwitch.musicOrRadio != Switch.PATH_MUSIC)
                mLayoutBgMyMusic.performClick();
            if (ICustomerPage.PAGE_BT.equals(page)) {
                if (EPlayStatus.STATUS_PLAYING.toString().equals(isPlay))
                    musicPlayer.resumePlay = IPlaySource.BT;
                if (!onNewIntent || !musicPath[0].equals("蓝牙音乐"))
                    musicPlayer.resumePage = IPlaySource.BT;
            } else {
                if (EPlayStatus.STATUS_PLAYING.toString().equals(isPlay))
                    musicPlayer.resumePlay = IPlaySource.USB;
                ;
                if (!onNewIntent || !musicPath[0].equals("USB音乐"))
                    musicPlayer.resumePage = IPlaySource.USB;
            }
        }

    }


    /*----------------------------------------- 碎片切换 --------------------------------------------------*/


    private MusicFragment musicFragment;
    private RadioFragment radioFragment;
    private RadioSearchListFragment radioSearchListFragment;

    public Fragment now = null;

    /**
     * 一开始 将需要跳转的页面 add
     */
    public void addFragment() {
        LogUtils.d("");
        musicFragment = new MusicFragment();
        radioFragment = new RadioFragment();
        radioSearchListFragment = new RadioSearchListFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.layout_fragment_home, musicFragment, "music");
        transaction.commit();
        now = musicFragment;
    }

    public void hideAndShowFragment(Fragment nextFragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.hide(now);
        transaction.show(nextFragment);
        transaction.commit();
        now = nextFragment;
    }

    @SuppressLint("CommitTransaction")
    public void pageChange(int id) {
        if (isFinishing()) {
            LogUtils.d("isFinishing true");
            return;
        }
        if (id == R.id.btn_my_music) {
            //显示音乐碎片
            LogUtils.d("music");
            if (now == musicFragment) {
                LogUtils.d("当前就在 musicFragment 无需跳转");
                return;
            }
            Fragment musicFrame = getSupportFragmentManager().findFragmentByTag("music");
            if (musicFrame != null && now != null) {
                hideAndShowFragment(musicFrame);
                if (musicPlayer.mUsbStatus == EUsbStatus.DISCONNECT
                        && aSwitch.musicLeft == Switch.PATH_MUSIC_COLLECT) {
                    LogUtils.d("翻转到播放页面");
                    MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
                    musicLeftPlayingFragment.toPlay();
                }
                if (aSwitch.musicLeft == 0) {
                    //恢复左边播放信息
                    MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
                    if (musicLeftPlayingFragment != null) {
                        musicLeftPlayingFragment.initPlay();
                    }
                }
                MusicRightMenuListFragment musicRightMenuListFragment = MyApplication.getMusicRightMenuListFragment();
                if (aSwitch.musicRight == Switch.PATH_USB) {
                    musicRightMenuListFragment.initUsb();
                } else if (aSwitch.musicRight == Switch.PATH_BT) {
                    musicRightMenuListFragment.initBt();
                } else if (aSwitch.musicRight == Switch.PATH_HISTORY) {
                    musicRightMenuListFragment.initHistory();
                }
                // 如果 FM 在搜索就停止
                if (radioSearchListFragment != null && radioPlayer.isScan) {
                    radioSearchListFragment.stopSearch();
                }
            } else {
                LogUtils.d("跳转失败");
            }

        } else if (id == R.id.btn_radio) {
            LogUtils.d("radio");
            if (now == radioFragment) {
                LogUtils.d("当前就在 radioFragment 无需跳转");
                return;
            }
            Fragment radioFrame = getSupportFragmentManager().findFragmentByTag("radio");
            //显示 radio
            if (radioFrame == null) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.layout_fragment_home, radioFragment, "radio");
                transaction.commitNow();
                radioFrame = getSupportFragmentManager().findFragmentByTag("radio");
            }
            if (radioFrame != null && now != null) {
                hideAndShowFragment(radioFrame);
                radioFragment.init();
            } else {
                LogUtils.d("跳转失败" + radioFrame + "/" + now);
            }
        } else if (id == 1) {
            if (now == radioSearchListFragment) {
                LogUtils.d("当前就在 radioSearchListFragment 无需跳转");
                return;
            }
            Fragment radioSearchListFrame = getSupportFragmentManager().findFragmentByTag("radioList");
            if (radioSearchListFrame == null) {
                LogUtils.d("第一次初始化 radioList");
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.layout_fragment_home, radioSearchListFragment, "radioList");
                transaction.commitNow();
                radioSearchListFrame = getSupportFragmentManager().findFragmentByTag("radioList");
            }
            if (radioSearchListFrame != null && now != null) {
                hideAndShowFragment(radioSearchListFrame);
                radioSearchListFragment.init();
            } else {
                LogUtils.d("跳转失败 ！");
            }
        }
    }


    /**
     * 将本音乐提前到历史记录的第一位 使用linkList方便
     *
     * @param musicFile
     */
    @SuppressLint("NotifyDataSetChanged")
    public void modifyToHistoryFirst(MusicFile musicFile) {
        MusicFragment musicFragment = MyApplication.getMusicFragment();
        //是否在删除列表内中，在的话就修改标识
        if (musicFragment != null) {
            ArrayList<MusicFile> musicDeleteList = musicFragment.musicDeleteList;
            for (int i = 0; i < musicDeleteList.size(); i++) {
                if (musicDeleteList.get(i).path.equals(musicFile.path)) {
                    musicFile.isSelected = true;
                    break;
                }
            }
        }
        //如果在历史记录中就删除当前位置的
        for (int i = musicHistory.size() - 1; i >= 0; i--) {
            if (musicFile.path.equals(musicHistory.get(i).path)) {
                LogUtils.d("在原来历史记录中先删除，然后添加到顶部");
                musicHistory.remove(i);
                break;
            }
        }
        //放到列表第一个
        int size = musicHistory.size();
        musicHistory.addFirst(musicFile);
        musicPlayer.isHistoryModify = true;
        LogUtils.d("size:" + size + " -> " + musicHistory.size());
    }

    /**
     * 数据库写入
     * 在这里的话当应用关闭的时候写数据库
     * 即使是被切后台也能执行到
     */
    @Override
    protected void onPause() {
        LogUtils.d("");
        super.onPause();
    }

    /**
     *
     */
    public void dataInit() {
        //页面状态
        List<Switch> switches = DataSupport.where().find(Switch.class);
        if (switches != null && switches.size() > 0) {
            aSwitch = switches.get(0);
        } else
            aSwitch = new Switch();
        aSwitch.musicOrRadio = Switch.PATH_MUSIC;
    }

    private void usbStatusInit() {
        // usb 状态
        try {
            int status = Integer.parseInt(SystemProperties.get("sys.bd.otg_state1"));
            LogUtils.d("status:" + status);
            if (status == Constants.MOUNTED) {
                LogUtils.d("扫描中");
                musicPlayer.mUsbStatus = EUsbStatus.INSERT;
            } else if (status == Constants.SCAN_FINISH) {
                LogUtils.d("已插入");
                musicPlayer.mUsbStatus = EUsbStatus.SCANNED;
            } else {
                LogUtils.d("未插入");
                musicPlayer.mUsbStatus = EUsbStatus.DISCONNECT;
            }
        } catch (Exception e) {
            musicPlayer.mUsbStatus = EUsbStatus.DISCONNECT;
        }
    }

    /**
     * 电台数据初始化:全部数据，收藏列表，临时节点
     * 最后一个当成初始电台
     */
    @SuppressLint("DefaultLocale")
    public void radioDataInit() {
        LogUtils.d("");
        isSeekBarChanging = false;
        //收藏列表
        radioAll = RadioOperations.getAllCollectRadios(getApplicationContext());
        radioCollect.addAll(radioAll);
        //搜索列表
        searchList = RadioOperations.getAllSearchRadios(getApplicationContext());
        searchList.forEach(radio -> {
            for (int i = 0; i < radioCollect.size(); i++) {
                if (radio.thumbPos == radioCollect.get(i).thumbPos) {
                    radio.isCollect = 1;
                }
            }
        });
        //获取当前节点
        radio = SharedPreferencesControl.getRadioNow(this);
        radioCollect.forEach(radio1 -> {
            if (radio.thumbPos == radio1.thumbPos) {
                radio.isCollect = 1;
            }
        });
        //播放控制
        radioPlayer = new RadioPlayer();
    }

    /**
     * 播放条件
     * 当 usb 插入且 扫描结束才能播放
     *
     * @return
     */
    public boolean canUsbPlay() {
        LogUtils.d("mUsbStatus:" + musicPlayer.mUsbStatus + " mBtCallStatus:" + musicPlayer.mBtCallStatus);
        if (musicPlayer.mUsbStatus != EUsbStatus.DISCONNECT && !isBtPhoning()) {
            return true;
        } else if (musicPlayer.mUsbStatus == EUsbStatus.DISCONNECT) {
            LogUtils.d("usb 未插入");
        } else if (isBtPhoning()) {
            LogUtils.d("蓝牙通话中！");
            android.widget.Toast.makeText(getApplicationContext(), "蓝牙通话中！", android.widget.Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    public boolean canBtPlay() {
        if (!isBtPhoning()) {
            return true;
        } else if (isBtPhoning()) {
            android.widget.Toast.makeText(getApplicationContext(), "蓝牙通话中！", android.widget.Toast.LENGTH_SHORT).show();
            LogUtils.d("蓝牙通话中！");
        }
        return false;
    }

    public boolean canFmPlay() {
        if (musicPlayer.mBtCallStatus.isPause() && !radioPlayer.isScan && !radioPlayer.isSeek) {
            return true;
        } else if (musicPlayer.mBtCallStatus.isPlaying()) {
            android.widget.Toast.makeText(getApplicationContext(), "蓝牙通话中！", android.widget.Toast.LENGTH_SHORT).show();
            LogUtils.d("蓝牙通话中 ！");
        } else if (radioPlayer.isScan) {
            android.widget.Toast.makeText(MyApplication.getContext(), "请先停止搜索!", android.widget.Toast.LENGTH_SHORT).show();
            LogUtils.d("列表搜索中 ！");
        } else if (radioPlayer.isSeek) {
            android.widget.Toast.makeText(MyApplication.getContext(), "请先停止搜索!", android.widget.Toast.LENGTH_SHORT).show();
            LogUtils.d("搜台中 ！");
        }
        return false;
    }

    public boolean isBtPhoning() {
        return musicPlayer.mBtCallStatus.isPlaying();
    }

    public void setAudioSources(EAudioSource source) {
        LogUtils.d("source:" + source + " value:" + source.getValue());
        serviceConnection.setAudioSources(source.getValue());
    }

    //音乐相关 结束
    @Override
    protected void onDestroy() {
        LogUtils.d("");
        unregisterUsbProviderReceiver();
        unregisterUsbReceiver();
        stopMusicService();
        super.onDestroy();
    }

    public void startMusicService() {
        LogUtils.d("");
        try {
            serviceConnection = new MediaConnection(this);
            Intent intent = new Intent("com.elink.multimedia.MEDIA");
            intent.setPackage("com.elink.elinkmultimediaservices");
            intent.putExtra("caller_package", getPackageName());
            startForegroundService(intent);
            bindService(intent, serviceConnection, BIND_AUTO_CREATE);
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }

    private void stopMusicService() {
        LogUtils.d("");
        try {
            serviceConnection.unregisterMediaCallback();
            Intent intent = new Intent("com.elink.multimedia.MEDIA");
            intent.setPackage("com.elink.elinkmultimediaservices");
            intent.putExtra("caller_package", getPackageName());
            unbindService(serviceConnection);
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }

    public void registerUsbReceiver() {
        //注册广播接受器
        IntentFilter filter = new IntentFilter();
        filter.addAction(STOP_PLAYBACK_ACTION);
        filter.addAction(START_PLAYBACK_ACTION);
//        registerReceiver(usbReceiver, filter);
    }

    public void unregisterUsbReceiver() {
//        unregisterReceiver(usbReceiver);
    }

    public void registerUsbProviderReceiver() {
        //接受上游USB插入发出的广播
        IntentFilter filter_usb = new IntentFilter();
        filter_usb.addAction("action.elink.udisk.provider");
        registerReceiver(usbProviderReceiver, filter_usb);
    }

    public void unregisterUsbProviderReceiver() {
        unregisterReceiver(usbProviderReceiver);
    }


    /*-------------------------usb 音乐位置---------------------------*/
    public int popStack() {
        Integer pop = stack.pop();
        LogUtils.d("pop:" + pop);
        return pop;
    }

    public void pushStack(int position) {
        stack.push(position);
    }

    public void clearStack() {
        stack.clear();
    }

    /*------------------------- 应用是否在前台 ---------------------------*/
    public boolean isForegroundActivities() {
        return foregroundActivities > 0;
    }

    public void foregroundActivitiesCountAdd() {
        foregroundActivities++;
    }

    public void foregroundActivitiesCountMinus() {
        foregroundActivities--;
    }
}