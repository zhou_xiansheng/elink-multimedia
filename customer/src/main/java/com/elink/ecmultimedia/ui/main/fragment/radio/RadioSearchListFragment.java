package com.elink.ecmultimedia.ui.main.fragment.radio;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.elink.ecmultimedia.adapter.radio.SearchListAdapter;
import com.elink.ecmultimedia.datamodel.RadioOperations;
import com.elink.ecmultimedia.pojo.Radio;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.ecmultimedia.R;
import com.elink.ecmultimedia.utils.MediaToast;
import com.elink.elinkmultimediaservices.constants.common.IPlaybackStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RadioSearchListFragment extends Fragment implements View.OnClickListener {
    private MultimediaMainActivity mainActivity;
    private TextView back;
    private Button radioSearch;

    private LinearLayout layoutScanPrompt;
    private LinearLayout layoutNoAvailablePrompt;
    //列表相关
    public RecyclerView radioListRecycler;
    public SearchListAdapter searchListAdapter;
    private final ArrayList<Radio> mDataList = new ArrayList<>();
    public RadioSearchListFragment() {
        // Required empty public constructor
        LogUtils.d("");
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LogUtils.d("");
        View view = inflater.inflate(R.layout.fragment_radio_search_list, container, false);
        MyApplication.setRadioSearchListFragment(this);

        //控件初始化
        radioListRecycler = view.findViewById(R.id.radio_search_list);
        back = view.findViewById(R.id.back);
        radioSearch = view.findViewById(R.id.radio_search);
        layoutScanPrompt = view.findViewById(R.id.layout_search_prompt);
        layoutScanPrompt.bringToFront();//始终置于顶部
        layoutNoAvailablePrompt = view.findViewById(R.id.layout_no_available_prompt);
        mainActivity = MyApplication.getMainActivity();


        //指定 recycleView 的布局方式。这里线性布局，默认垂直
        radioListRecycler.setLayoutManager(new GridLayoutManager(requireContext(), 3));
        /*设置适配器*/
        //给 collectionRadioRecycler 添加数据
        updateDataList();
        searchListAdapter = new SearchListAdapter(mDataList);
        radioListRecycler.setAdapter(searchListAdapter);
        ((SimpleItemAnimator) Objects.requireNonNull(radioListRecycler.getItemAnimator())).setSupportsChangeAnimations(false);
        MyApplication.setRadioListAdapter(searchListAdapter);
        //刷新列表
        searchListAdapter.notifyDataSetChanged();

        //添加监听器
        back.setOnClickListener(this);
        radioSearch.setOnClickListener(this);
        //
        init();
        return view;
    }


    @SuppressLint("NotifyDataSetChanged")
    public void init() {
        LogUtils.d("");
        mainActivity.aSwitch.radioPage = 1;
        updateAndNotifyData();
        promptAfterNotifyDataSetChanged();
        setEnableOrNotOfButton();
    }

    @Override
    public void onResume() {
        if (!isHidden()) {
            setEnableOrNotOfButton();
            LogUtils.d("");
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        LogUtils.d("");
        super.onPause();
    }

    @Override
    public void onDestroy() {
        LogUtils.d("");
        super.onDestroy();
    }

    private final Handler timeoutHandler = new Handler();
    private final Runnable timeoutRunnable = new Runnable() {
        @Override
        public void run() {
            // 停止搜索过程
            if (mainActivity.radioPlayer.isScan)
                mainActivity.serviceConnection.stopScanAndSeek();
            // 当搜索超时时要执行的操作
            LogUtils.d("搜索超时");
            MediaToast.show("搜索超时");
            radioSearch.setText("搜索");
            mainActivity.radioPlayer.status = IPlaybackStatus.STATUS_PLAYING;//播放
            mainActivity.radioPlayer.isScan = false;
            layoutScanPrompt.setVisibility(View.GONE);
            if (mainActivity.searchList.size() == 0)
                layoutNoAvailablePrompt.setVisibility(View.VISIBLE);
            // 显示超时提示

            RadioFragment radioFragment = MyApplication.getRadioFragment();
            if (radioFragment != null)
                radioFragment.enableOrNotSeekBar();

        }
    };

    /**
     * 搜索结束
     */
    public void onSearchCompleted() {
        LogUtils.d("");
        mainActivity.runOnUiThread(() -> {
            radioSearch.setText("搜索");
            timeoutHandler.removeCallbacks(timeoutRunnable);
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.back) {
            //返回电台页面
            requireActivity().onBackPressed();
        } else if (v.getId() == R.id.radio_search) {
            if (mainActivity.radioPlayer.isScan) {
                //停止搜索
                radioSearch.setText("搜索");
                mainActivity.radioPlayer.status = IPlaybackStatus.STATUS_PLAYING;//播放
                mainActivity.radioPlayer.isScan = false;
                layoutScanPrompt.setVisibility(View.GONE);
                if (mainActivity.searchList.size() == 0) {
                    //判断列表如果为空就把空提示加回去
                    layoutNoAvailablePrompt.setVisibility(View.VISIBLE);
                }
                // 停止搜索过程
                stopSearch();

            } else {
                if (mainActivity.serviceConnection.isServicesBinder) {
                    //开始搜索
                    LogUtils.d("搜索");
                    radioSearch.setText("取消");
                    mainActivity.radioPlayer.status = IPlaybackStatus.STATUS_PAUSE;//停止播放
                    mainActivity.radioPlayer.isScan = true;
                    clearDataAndNotifyData();
                    promptSearch();
                    mainActivity.serviceConnection.getSearchedStableChannelList();
                    //设置搜索超时 10秒钟
                    timeoutHandler.postDelayed(timeoutRunnable, 1000 * 10);
                } else {
                    MediaToast.show("radio 服务未连接");
                }
            }
        }
    }

    public void stopSearch() {
        LogUtils.d("");
        //停止搜索
        radioSearch.setText("搜索");
        mainActivity.radioPlayer.status = IPlaybackStatus.STATUS_PLAYING;//播放
        mainActivity.radioPlayer.isScan = false;

        layoutScanPrompt.setVisibility(View.GONE);
        if (mainActivity.searchList.size() == 0) {
            layoutNoAvailablePrompt.setVisibility(View.VISIBLE);
        }

        // 停止搜索过程
        mainActivity.serviceConnection.stopScanAndSeek();
        timeoutHandler.removeCallbacks(timeoutRunnable);//取消超时任务
        RadioFragment radioFragment = MyApplication.getRadioFragment();
        if (radioFragment != null)
            radioFragment.enableOrNotSeekBar();
    }

    /**
     * 搜索
     */
    public void promptSearch() {
        LogUtils.d("");
        if (mainActivity.radioPlayer.isScan) {
            layoutScanPrompt.setVisibility(View.VISIBLE);
            layoutNoAvailablePrompt.setVisibility(View.GONE);
        }
    }


    /**
     * 列表刷新后 提示的状态
     */
    public void promptAfterNotifyDataSetChanged() {
        LogUtils.d("");
        if (mainActivity == null)
            mainActivity = MyApplication.getMainActivity();
        List<Radio> searchList = mainActivity.searchList;
        if (searchList.size() > 0) {
            layoutScanPrompt.setVisibility(View.GONE);
            layoutNoAvailablePrompt.setVisibility(View.GONE);
        } else {
            layoutScanPrompt.setVisibility(View.GONE);
            layoutNoAvailablePrompt.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 滑动列表数据更新
     */

    public void updateDataList() {
        LogUtils.d("");
        mDataList.clear();
        mDataList.addAll(mainActivity.searchList);
    }

    public void clearDataList() {
        LogUtils.d("");
        if (mainActivity == null)
            mainActivity = MyApplication.getMainActivity();
        if (mainActivity != null) {
            mainActivity.searchList.clear();
            mDataList.clear();
            RadioOperations.clearSearchRadios(MyApplication.getContext());
        }
    }


    @SuppressLint("NotifyDataSetChanged")
    public void notifyDataSetChanged() {
        LogUtils.d("");
        if (mainActivity == null)
            mainActivity = MyApplication.getMainActivity();
        if (mainActivity != null) {
            mainActivity.runOnUiThread(() -> searchListAdapter.notifyDataSetChanged());
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    public void updateAndNotifyData() {
        LogUtils.d("");
        updateDataList();
        notifyDataSetChanged();
    }

    /**
     * 清空列表
     */
    @SuppressLint("NotifyDataSetChanged")
    public void clearDataAndNotifyData() {
        LogUtils.d("");
        clearDataList();
        notifyDataSetChanged();
    }


    public void onFocusLostCallback() {
        setEnableOrNotOfButton();//禁用搜索按钮
    }

    @SuppressLint("ResourceAsColor")
    public void setEnableOrNotOfButton() {
        LogUtils.d("搜索按钮设置："+ mainActivity.radioPlayer.status);
        mainActivity.runOnUiThread(() -> {
            if (mainActivity.radioPlayer.isScan) {
                //先判断是否在搜索
                layoutScanPrompt.setVisibility(View.VISIBLE);
                layoutNoAvailablePrompt.setVisibility(View.GONE);
                radioSearch.setText("取消");
            } else {
                if (mainActivity.radioPlayer.status == IPlaybackStatus.STATUS_PLAYING) {
                    radioSearch.setEnabled(true);
                    radioSearch.setTextColor(ContextCompat.getColor(radioSearch.getContext(), R.color.white));
                } else if (mainActivity.radioPlayer.status == IPlaybackStatus.STATUS_PAUSE) {
                    radioSearch.setEnabled(false);
                    radioSearch.setTextColor(ContextCompat.getColor(radioSearch.getContext(), R.color.gray));
                }
                if (mainActivity.searchList.size() == 0) {
                    layoutNoAvailablePrompt.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}