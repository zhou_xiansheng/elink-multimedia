package com.elink.ecmultimedia.ui.main.fragment.music;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.elink.ecmultimedia.R;
import com.elink.ecmultimedia.adapter.music.MusicBigPlayCollectListAdapter;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.music.IRepeatMode;

import java.util.ArrayList;
import java.util.List;


public class MusicBigPlayCollectListFragment extends Fragment implements View.OnClickListener {
    private ImageView playModer;//音乐播放模式
    private RecyclerView recyclerView;
    private ArrayList<MusicFile> mDataList = new ArrayList<>();

    public MusicBigPlayCollectListAdapter musicLeftListAdapter;
    private MultimediaMainActivity mainActivity;
    public MusicBigPlayCollectListFragment() {
        // Required empty public constructor
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //将自己放到环境中
        MyApplication.setMusicBigPlayListFragment(this);
        View view = inflater.inflate(R.layout.fragment_music_big_play_list, container, false);
        //获得组件
        recyclerView = view.findViewById(R.id.music_left_recycle_view);
        playModer = view.findViewById(R.id.music_play_mode);
        mainActivity = MyApplication.getMainActivity();
        /*列表相关*/
        //设置布局方式
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        //配置适配器，数据初始化
        updateDataList();
        musicLeftListAdapter = new MusicBigPlayCollectListAdapter(mDataList, this);
        recyclerView.setAdapter(musicLeftListAdapter);
        //更新列表
        musicLeftListAdapter.notifyDataSetChanged();
        //绑定点击监听事件
        playModer.setOnClickListener(this);
        //初始化数据
        init();
        scrollWork();
        return view;
    }

    @Override
    public void onDestroy() {
        if (musicLeftListAdapter.now != null)
            musicLeftListAdapter.now.playAnimation.stopHandler();
        super.onDestroy();
    }

    private void init() {
        //音乐播放模式切换 修改图标，修改模式
        int playMode = mainActivity.serviceConnection.getMusicPlayMode();
        if (playMode == IRepeatMode.REPEAT_MODE_ORDER) {
            //列表循环
            playModer.setImageResource(R.drawable.music_play_mode_order);
        } else if (playMode == IRepeatMode.REPEAT_MODE_RANDOM) {
            //随机播放
            playModer.setImageResource(R.drawable.music_play_mode_shuffle);
        } else if (playMode == IRepeatMode.REPEAT_MODE_SINGLE) {
            //单曲循环
            playModer.setImageResource(R.drawable.music_play_mode_repeat);
        } else {
            LogUtils.d("播放模式切换出错，不再范围内");
        }
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.music_play_mode) {
            //音乐播放模式切换 修改图标，修改模式
            int playMode = mainActivity.serviceConnection.getMusicPlayMode();
            int newMode = (playMode + 1) % 3;
            mainActivity.serviceConnection.setMusicPlayMode(newMode);
            if (newMode == IRepeatMode.REPEAT_MODE_ORDER) {
                //列表循环
                playModer.setImageResource(R.drawable.music_play_mode_order);
            } else if (newMode == IRepeatMode.REPEAT_MODE_RANDOM) {
                //随机播放
                playModer.setImageResource(R.drawable.music_play_mode_shuffle);
            } else if (newMode == IRepeatMode.REPEAT_MODE_SINGLE) {
                //单曲循环
                playModer.setImageResource(R.drawable.music_play_mode_repeat);
            } else {
                LogUtils.d("播放模式切换出错，不再范围内");
            }
        }
    }

    public void updateDataList() {
        List<MusicFile> musicCollectList = mainActivity.serviceConnection.getMusicCollectList();
        musicCollectList.forEach(musicFile -> {
            if (musicFile.path.equals(mainActivity.musicNow.path))
                musicFile.isPlaying = true;
        });
        mDataList.clear();
        mDataList.addAll(musicCollectList);//将收藏列表中的数据添加进去
    }

    private final static int DO_SCROLL_CODE = 1;

    private Handler scrollHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            int what = msg.what;
            switch (what) {
                case DO_SCROLL_CODE:
                    scrollWork();
                    break;
                default:
                    LogUtils.d("no default");
            }
        }
    };

    /**
     * 将当前播放 的 music 滚动至可见区域
     */
    public void scrollToVisible() {
        scrollHandler.removeCallbacksAndMessages(null);
        Message message = scrollHandler.obtainMessage(DO_SCROLL_CODE);
        scrollHandler.sendMessageDelayed(message, 500);
    }

    public void scrollWork() {
        if (mainActivity.musicNow == null)
            return;
        int index = -1;
        for (int i = 0; i < mDataList.size(); i++) {
            if (mDataList.get(i).path.equals(mainActivity.musicNow.path)) {
                index = i;
                break;
            }
        }
        if (index != -1) {
            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            if (layoutManager != null) {
                layoutManager.scrollToPositionWithOffset(index, 130);
            }
        }
    }


    /**
     * 当切歌的时候
     */
    public void updatePlaying() {
        //将上一首播放的项更新掉
        for (int i = 0; i < mDataList.size(); i++) {
            if (mDataList.get(i).isPlaying) {
                mDataList.get(i).isPlaying = false;
                musicLeftListAdapter.notifyItemChanged(i);
            }
        }
        //更新正在播放的项
        MusicFile musicNow = MyApplication.getMainActivity().musicNow;
        for (int i = 0; i < mDataList.size(); i++) {
            if (musicNow != null
                    && musicNow.path != null
                    && musicNow.path.equals(mDataList.get(i).path)) {
                mDataList.get(i).isPlaying = true;
                musicLeftListAdapter.notifyItemChanged(i);
                break;
            }
        }
    }


    public void nextMusicCallback() {
        updatePlaying();
        scrollToVisible();
    }
}