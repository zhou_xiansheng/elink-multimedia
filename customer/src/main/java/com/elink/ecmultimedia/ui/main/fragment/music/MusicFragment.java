package com.elink.ecmultimedia.ui.main.fragment.music;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.elink.ecmultimedia.R;
import com.elink.ecmultimedia.adapter.music.MusicRightHistoryListAdapter;
import com.elink.ecmultimedia.thread.ScanHandler;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.dialog.MusicLoadingTask;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MediaConstants;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.ecmultimedia.utils.Worker;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.common.EUsbStatus;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class MusicFragment extends Fragment implements View.OnClickListener {
    private MultimediaMainActivity mainActivity;
    private MusicRightMenuListFragment musicRightMenuListFragment;

    private TextView contentFrom;
    public Button musicPlayAll;
    public Button musicEdit;
    public Button musicDelete;
    public ArrayList<MusicFile> musicDeleteList;//需要删除的历史记录

    @SuppressLint("StaticFieldLeak")
    public MusicLoadingTask directoryLoadTask;//控制点进目录后然后点击全部播放，可能后台数据还没加载完成，需要弹框等待


    public MusicFragment() {
        LogUtils.d("");
    }


    @SuppressLint("MissingInflatedId")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LogUtils.d("");
        MyApplication.setMusicFragment(this);
        View view = inflater.inflate(R.layout.fragment_music, container, false);
        mainActivity = MyApplication.getMainActivity();
        musicRightMenuListFragment = MyApplication.getMusicRightMenuListFragment();
        //获取实例对象
        contentFrom = view.findViewById(R.id.content_from);
        musicPlayAll = view.findViewById(R.id.music_play_all);
        musicEdit = view.findViewById(R.id.music_edit);
        musicDelete = view.findViewById(R.id.music_delete);
        musicDeleteList = new ArrayList<>();
        //设置监听器
        contentFrom.setOnClickListener(this);
        musicPlayAll.setOnClickListener(this);
        musicEdit.setOnClickListener(this);
        musicDelete.setOnClickListener(this);
        //
        directoryLoadTask = new MusicLoadingTask(mainActivity, "加载中");
        return view;
    }


    @Override
    public void onStart() {
        LogUtils.d("");
        super.onStart();
    }

    @Override
    public void onResume() {
        LogUtils.d("");
        init();
        super.onResume();
    }

    @Override
    public void onPause() {
        LogUtils.d("");
        super.onPause();
    }

    @Override
    public void onStop() {
        LogUtils.d("");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        LogUtils.d("");
        super.onDestroy();
    }

    private void init() {
        LogUtils.d("");
        //控件隐藏
        invisibleViews();
        if (!mainActivity.musicPath[0].equals("")) {
            initVisibleOfViews(mainActivity.musicPath[0]);
        }
    }

    /**
     * 显示组件
     *
     * @param name
     */
    public void initVisibleOfViews(String name) {
        LogUtils.d("");
        mainActivity.musicPath[0] = name;
        contentFrom.setText(name);
        contentFrom.setVisibility(View.VISIBLE);
        if (mainActivity.musicPath[0].equals("最近播放")) {
            musicPlayAll.setVisibility(View.VISIBLE);
            musicDelete.setVisibility(View.VISIBLE);
            musicEdit.setVisibility(View.VISIBLE);
            musicDeleteDisableOrNot(true);
            if (mainActivity.musicHistory != null)
                if (mainActivity.musicHistory.size() > 0) {
                    musicPlayAllDisable(false);
                    musicEditDisableOrNot(false);
                } else {
                    musicPlayAllDisable(true);
                    musicEditDisableOrNot(true);
                }
            if (mainActivity.musicPlayer.isEditor) {
                musicEdit.setText("完成");
            } else {
                musicEdit.setText("操作");
            }
        } else if (mainActivity.musicPath[0].equals("蓝牙音乐")) {

        } else if (mainActivity.musicPath[0].equals("USB音乐")) {
            musicPlayAll.setVisibility(View.VISIBLE);
            setMusicPath();
        }
    }

    public void usbButtonView() {
        LogUtils.d("");
        mainActivity.musicPath[0] = "USB音乐";
        mainActivity.musicPath[1] = "";
        setMusicPath();
        contentFrom.setVisibility(View.VISIBLE);
        musicPlayAll.setVisibility(View.VISIBLE);
    }

    public void btButtonView() {
        mainActivity.musicPath[0] = "蓝牙音乐";
        contentFrom.setText(mainActivity.musicPath[0]);
        contentFrom.setVisibility(View.VISIBLE);
    }

    public void historyButtonView() {
        LogUtils.d("");
        contentFrom.setVisibility(View.VISIBLE);
        mainActivity.musicPath[0] = "最近播放";
        contentFrom.setText(mainActivity.musicPath[0]);
        musicPlayAll.setVisibility(View.VISIBLE);
        musicDelete.setVisibility(View.VISIBLE);
        musicEdit.setVisibility(View.VISIBLE);

        if (mainActivity.musicHistory != null) {
            if (mainActivity.musicHistory.size() > 0) {
                musicEditDisableOrNot(false);
                if (EUsbStatus.isStateUseFull(mainActivity.musicPlayer.mUsbStatus)) {
                    //可播放的数量大于 0 ，这里先不处理
                    musicPlayAllDisable(false);
                } else {
                    musicPlayAllDisable(!mainActivity.musicPlayer.isEditor);
                }
            } else {
                musicPlayAllDisable(true);
                musicEditDisableOrNot(true);
            }
            if (musicDeleteList.size() > 0) {
                musicDeleteDisableOrNot(false);
            } else {
                musicDeleteDisableOrNot(true);
            }
        } else {
            musicDeleteDisableOrNot(true);
        }
        if (mainActivity.musicPlayer.isEditor) {
            musicEdit.setText("完成");
        } else {
            musicEdit.setText("操作");
        }
    }


    public void musicDeleteDisableOrNot(boolean b) {
        LogUtils.d("b:" + b);
        if (b) {
            musicDelete.setEnabled(false);
            musicDelete.setTextColor(ContextCompat.getColor(musicDelete.getContext(), R.color.gray));
        } else {
            musicDelete.setEnabled(true);
            musicDelete.setTextColor(ContextCompat.getColor(musicDelete.getContext(), R.color.white));
        }
    }

    public void musicPlayAllDisable(boolean b) {
        LogUtils.d("b:" + b);
        if (mainActivity.isForegroundActivities()) {
            mainActivity.runOnUiThread(() -> {
                if (b) {
                    musicPlayAll.setEnabled(false);
                    mainActivity.runOnUiThread(() -> musicPlayAll.setTextColor(ContextCompat.getColor(musicPlayAll.getContext(), R.color.gray)));
                } else {
                    musicPlayAll.setEnabled(true);
                    musicPlayAll.setTextColor(ContextCompat.getColor(musicPlayAll.getContext(), R.color.white));
                }
            });
        }
    }

    public void updateTextOfAllPlay() {
        LogUtils.d("");
        if (mainActivity == null)
            mainActivity = MyApplication.getMainActivity();
        String text;
        LinkedList<MusicFile> musicHistory = mainActivity.musicHistory;
        if (musicDeleteList.size() >= musicHistory.size()) {
            text = "全部取消";
            mainActivity.musicPlayer.isHistorySelectAll = true;
        } else {
            text = "全部选择";
            mainActivity.musicPlayer.isHistorySelectAll = false;
        }
        mainActivity.runOnUiThread(() -> {
            musicPlayAll.setText(text);
        });
    }

    public void musicEditDisableOrNot(boolean b) {
        LogUtils.d("");
        if (b) {
            musicEdit.setEnabled(false);
            musicEdit.setTextColor(ContextCompat.getColor(musicDelete.getContext(), R.color.gray));
        } else {
            musicEdit.setEnabled(true);
            musicEdit.setTextColor(ContextCompat.getColor(musicDelete.getContext(), R.color.white));
        }
    }

    /**
     * 隐藏组件
     */
    public void invisibleViews() {
        contentFrom.setVisibility(View.GONE);
        musicPlayAll.setVisibility(View.GONE);
        musicDelete.setVisibility(View.GONE);
        musicEdit.setVisibility(View.GONE);
    }

    /*--------------------------- 按钮设置文字 -----------------------------------*/
    public void setTextOfAllPlayBtn(String str) {
        musicPlayAll.setText(str);
    }

    public void setTextOfEditBtn(String str) {
        musicEdit.setText(str);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.content_from) {
            LogUtils.d("点击左上角的返回");
            mainActivity.onBackPressed();
            mainActivity.musicPlayer.isEditor = false;
        } else if (v.getId() == R.id.music_play_all) {
            //全部操作
            if (mainActivity.serviceConnection.isServicesBinder) {
                //替换当前播放列表，并播放第一首歌曲
                if (mainActivity.musicPath[0].equals("USB音乐")) {
                    LogUtils.d("usb 全部播放");
                    ScanHandler scanHandler = ScanHandler.get(MediaConstants.path);
                    if (scanHandler != null && (scanHandler.directoryItemCount != scanHandler.directoryItemBackgroundCount)) {
                        //后台数据还没加载完，弹出加载框
                        scanHandler.startLoadingTask();
                    } else {
                        // 扫描完了直接全部播放
                        Handler handler = mainActivity.worker.handler;
                        Message message = handler.obtainMessage(Worker.USB_PLAY_ALL);
                        handler.sendMessage(message);
                    }
                } else if (mainActivity.musicPath[0].equals("最近播放")) {
                    if (mainActivity.musicHistory != null && mainActivity.musicHistory.size() > 0) {
                        if (!mainActivity.musicPlayer.isEditor
                                && mainActivity.musicPlayer.mUsbStatus != EUsbStatus.DISCONNECT) {
                            if (!mainActivity.canUsbPlay())
                                return;
                            LogUtils.d("最近播放全部播放");
                            ArrayList<MusicFile> musicFiles = new ArrayList<>();
                            //过滤出存在的文件
                            for (int i = 0; i < mainActivity.musicHistory.size(); i++) {
                                if (new File(mainActivity.musicHistory.get(i).path).exists()) {
                                    musicFiles.add(mainActivity.musicHistory.get(i));
                                }
                            }
                            if (musicFiles.size() > 0) {
                                mainActivity.serviceConnection.setMusicCollection(musicFiles);
                                //播放第一首歌曲
                                LogUtils.d("播放第一首歌曲");
                                MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
                                musicLeftPlayingFragment.updateDataAndNotify();
                                musicLeftPlayingFragment.usbPlaying(musicFiles.get(0));
                                //修改服务端下标信息
                                mainActivity.serviceConnection.setPlayIndex(0);
                            } else {
                                LogUtils.d("无有效音乐");
                                Toast.makeText(MyApplication.getContext(), "无有效音乐！", Toast.LENGTH_SHORT).show();
                            }
                        } else if (mainActivity.musicPlayer.isEditor) {
                            //勾选全部
                            MusicRightHistoryListAdapter rightHistoryListAdapter = musicRightMenuListFragment.rightHistoryListAdapter;
                            if (musicPlayAll.getText().equals("全部选择")) {
                                LogUtils.d("全部选择");
                                rightHistoryListAdapter.selectAll();
                                mainActivity.musicPlayer.isHistorySelectAll = true;
                                musicPlayAll.setText("全部取消");
                            } else if (musicPlayAll.getText().equals("全部取消")) {
                                LogUtils.d("全部取消");
                                rightHistoryListAdapter.cancelAll();
                                musicDeleteDisableOrNot(true);
                                mainActivity.musicPlayer.isHistorySelectAll = false;
                                musicPlayAll.setText("全部选择");
                            }
                        }
                    }
                } else if (mainActivity.musicPath[0].equals("蓝牙音乐")) {

                }
                MyApplication.getMusicLeftPlayingFragment().updateVisibleBtnOfPlayToList();
            } else
                Toast.makeText(MyApplication.getContext(), "music服务未连接", Toast.LENGTH_SHORT).show();
        } else if (v.getId() == R.id.music_edit) {
            List<MusicFile> musicHistory = mainActivity.musicHistory;
            if (musicHistory.size() == 0) {
                return;
            }
            if (mainActivity.musicPlayer.isEditor) {
                LogUtils.d("编辑");
                mainActivity.musicPlayer.isEditor = false;
                musicEdit.setText("操作");
                musicPlayAll.setText("全部播放");
                musicDeleteDisableOrNot(true);
                if (mainActivity.musicPlayer.mUsbStatus == EUsbStatus.DISCONNECT) {
                    musicPlayAllDisable(true);
                }
                //这里必须直接在历史记录里修改，问题：当切到下一首刚好是勾选的被放到第一位
                musicRightMenuListFragment.rightHistoryListAdapter.cancelAll();
                musicDeleteList.clear();
            } else {
                musicPlayAll.setText("全部选择");
                mainActivity.musicPlayer.isHistorySelectAll = false;
                musicEdit.setText("完成");
                mainActivity.musicPlayer.isEditor = true;//取消所有列表的点击监听事件
                musicRightMenuListFragment.rightHistoryListAdapter.cancelAll();
                musicPlayAllDisable(false);

            }
        } else if (v.getId() == R.id.music_delete) {
            LogUtils.d("删除" + mainActivity.musicHistory.size() + "/" + musicDeleteList.size());
            if (mainActivity.musicHistory.size() == 0 || musicDeleteList.size() == 0) {
                return;
            }
            for (int i = 0; i < musicDeleteList.size(); i++) {
                musicDeleteList.get(i).isSelected = false;
            }

            for (int i = mainActivity.musicHistory.size() - 1; i >= 0; i--) {
                for (int j = 0; j < musicDeleteList.size(); j++) {
                    if (musicDeleteList.get(j).path.equals(mainActivity.musicHistory.get(i).path)) {
                        mainActivity.musicHistory.remove(i);
                        break;
                    }
                }
            }
            MusicRightHistoryListAdapter rightHistoryListAdapter = musicRightMenuListFragment.rightHistoryListAdapter;
            rightHistoryListAdapter.modifyData();
            rightHistoryListAdapter.notifyDataSetChanged();
            musicDeleteList.clear();
            if (mainActivity.musicHistory.size() == 0) {
                //没有数据提示
                if (musicRightMenuListFragment != null) {
                    musicRightMenuListFragment.setVisibleOfNoHistoryPrompt(View.VISIBLE);
                }
                mainActivity.musicPlayer.isHistorySelectAll = false;
                musicEdit.setText("操作");
                musicPlayAll.setText("全部播放");
                musicEditDisableOrNot(true);
                musicPlayAllDisable(true);
                mainActivity.musicPlayer.isEditor = false;//取消编辑
                LogUtils.d("删除到没有历史记录了");
            }
            musicDeleteDisableOrNot(true);
            LogUtils.d("删除 size:" + mainActivity.musicHistory.size());
        }
    }


    /*---------------------------- 页面的显示与隐藏 -------------------------------*/


    public void addToPath(String fileName) {
        if (mainActivity.musicPath[1] != null && !mainActivity.musicPath[1].equals("")) {
            mainActivity.musicPath[1] += ("/" + fileName);
        } else {
            mainActivity.musicPath[1] = "/" + fileName;
        }
    }

    public void setMusicPath() {
        String path = "";
        LogUtils.d(mainActivity.musicPath[0] + "/" + mainActivity.musicPath[1]);
        if (mainActivity.musicPath[0] != null && !mainActivity.musicPath[0].equals("")) {
            path += mainActivity.musicPath[0];
            if (mainActivity.musicPath[1] != null && !mainActivity.musicPath[1].equals("")) {
                path += mainActivity.musicPath[1];
            }
            contentFrom.setText(path);
        }
    }

    public void usbDetachCallback() {
        // 拔出 usb 按钮的禁用
        String text = musicPlayAll.getText().toString();
        if (text.equals(MediaConstants.PLAY_ALL)) {
            musicPlayAllDisable(true);
        }
    }
}