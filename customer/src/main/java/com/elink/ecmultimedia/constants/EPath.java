package com.elink.ecmultimedia.constants;

/**
 * 路径枚举
 */
public enum EPath {
    MUSIC("音乐"),
    FM("收音机"),
    MUSIC_LEFT_PLAY("音乐播放"),
    MUSIC_LEFT_LIST("音乐列表"),
    MUSIC_FULL_SCREEN("全屏播放"),
    MUSIC_RIGHT_MENU("音乐菜单"),
    MUSIC_RIGHT_BT("蓝牙音乐"),
    MUSIC_RIGHT_USB("USB音乐"),
    MUSIC_RIGHT_HISTORY("USB音乐历史记录"),
    FM_PLAY("收音机播放"),
    FM_SEARCH("搜索列表");
    private final String value;

    EPath(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
