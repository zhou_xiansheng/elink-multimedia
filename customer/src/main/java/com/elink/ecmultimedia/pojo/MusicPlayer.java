package com.elink.ecmultimedia.pojo;


import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.constants.common.EUsbStatus;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;

import org.litepal.crud.DataSupport;

/**
 * 音乐播放配置类，包含播放状态，播放列表，播放模式等信息。
 */
public class MusicPlayer extends DataSupport {
    public int index;//播放下标

    public int status = EPlayStatus.STATUS_PAUSE.getValue(); // 播放状态

    public int duration;//当前歌曲时长

    public boolean isEditor = false;//是否在编辑中
    public EUsbStatus mUsbStatus = EUsbStatus.DISCONNECT;
    public boolean isHistorySelectAll = false;//编辑状态是否全部选择
    public EPlayStatus mBtCallStatus = EPlayStatus.STATUS_PAUSE;//蓝牙电话状态

    public boolean isHistoryModify = false;//可能用在，历史记录执行 onResume 中，没有更新就没必要刷新

    public int resumePage = IPlaySource.UNKNOWN;//恢复页面
    public int resumePlay= IPlaySource.UNKNOWN;//恢复播放源



    @Override
    public String toString() {
        return "MusicPlayer{" +
                ", index=" + index +
                ", status=" + status +
                ", duration=" + duration +
                ", mUsbStatus=" + mUsbStatus +
                ", isEditor=" + isEditor +
                '}';
    }
}



