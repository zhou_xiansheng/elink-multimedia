package com.elink.ecmultimedia.pojo;

import com.elink.elinkmultimediaservices.constants.common.IPlaybackStatus;

import org.litepal.crud.DataSupport;

/**
 * 广播播放配置类
 */
public class RadioPlayer extends DataSupport {
    public int status = IPlaybackStatus.STATUS_PAUSE; // 播放状态：1-播放，0-暂停
    public boolean isScan = false;//是否在扫描中，默认停止扫描
    public boolean isSeek = false;//是否在搜台
}



