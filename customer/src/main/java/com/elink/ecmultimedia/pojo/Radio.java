package com.elink.ecmultimedia.pojo;


import com.elink.ecmultimedia.utils.UnitUtils;

import org.litepal.crud.DataSupport;

/**
 * 音乐类，代表一首歌曲的信息。
 */
public class Radio extends DataSupport {

    public int id;
    public int i;
    public int thumbPos;
    public float radioId; // 广播的频率

    public String type; // 广播类型
    public String typeName; // 广播名称

    public int isCollect;//是否被收藏 0-否 1-收藏

    public Radio() {
    }

    public static Radio newFMRadio(int channelNum) {
        float fmI2F = UnitUtils.Channel.fm_i2F(channelNum);
        Radio radio = new Radio();
        radio.radioId = fmI2F;
        radio.thumbPos = Math.round(((fmI2F - 87.4f) * 10 - 1));
        radio.type = "FM";
        radio.typeName = "电台";
        return radio;
    }

    @Override
    public String toString() {
        return "Radio{" +
                "id=" + id +
                ", i=" + i +
                ", thumbPos=" + thumbPos +
                ", radioId=" + radioId +
                ", type='" + type + '\'' +
                ", typeName='" + typeName + '\'' +
                ", isCollect=" + isCollect +
                "}\n";
    }
}
