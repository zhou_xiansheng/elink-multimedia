package com.elink.ecmultimedia.pojo;


/**
 * 歌词
 */
public class Lyric implements Comparable {
    public long time;
    public String lyricSentence;

    public Lyric(long time, String lyricSentence) {
        this.time = time;
        this.lyricSentence = lyricSentence;
    }

    @Override
    public String toString() {
        return "Lyric{" +
                "timeList=" + time +
                ", lyricSentenceList='" + lyricSentence + '\'' +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        return Long.compare(this.time, ((Lyric) o).time);
    }
}

