package com.elink.ecmultimedia.pojo;

import com.elink.elinkmultimediaservices.constants.common.IPlayMode;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;

/**
 * 控制页面显示
 * 分音乐播放和收音机两种情况
 */
public class Switch {
    //路径相关
    public int musicOrRadio;//0-音乐 1-收音机
    public static final int PATH_MUSIC = 0;
    public static final int PATH_RADIO = 1;

    /*音乐*/
    public int bigOrSmall;//0-小窗口播放模式 1-大窗口播放模式
    public static final int PATH_MUSIC_SMALL = 0;
    public static final int PATH_MUSIC_BIG = 1;

    public int musicLeft;//0-播放页面 1-收藏列表
    public static final int PATH_MUSIC_PLAY = 0;
    public static final int PATH_MUSIC_COLLECT = 1;


    public int bigMusic;//大窗口模式是 usb 还是 bt


    public int musicRight;//0-播放源菜单 1-蓝牙数据列表 2-usb数据列表 3-历史记录数据列表
    public static final int PATH_MENU = 0;
    public static final int PATH_BT = 1;
    public static final int PATH_USB = 2;
    public static final int PATH_HISTORY = 3;

    /*收音机*/
    public int radioPage;//0播放页面 1-搜索页面
    public static final int PATH_RADIO_PLAY = 0;//播放菜单
    public static final int PATH_RADIO_SEARCH = 1;//播放菜单

}
