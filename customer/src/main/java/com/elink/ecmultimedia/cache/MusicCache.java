package com.elink.ecmultimedia.cache;

import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.UsbDirectoryUtils;
import com.elink.elinkmultimediaservices.beans.MusicFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 从服务中查询到一些数据进行缓存
 */
public class MusicCache {
    public static List<MusicFile> musicFiles = new ArrayList<>();//所有音乐

    public static void updateMusicFiles() {
        List<MusicFile> newlyAddedMusic = UsbDirectoryUtils.getNewlyAddedMusic(musicFiles.size());
        musicFiles.addAll(newlyAddedMusic);
        LogUtils.d("new:" + newlyAddedMusic.size() + " all:" + musicFiles.size());
    }

    private static HashMap<String, ArrayList<MusicFile>> contentByPath;//路径下的音乐和文件夹
    private static HashMap<String, List<MusicFile>> musicByPath;//路径下的音乐

    private static HashMap<String, byte[]> albumCoverHashMap;

    public static void addAlbumCache(String key, byte[] bytes) {
        if (albumCoverHashMap == null) {
            albumCoverHashMap = new HashMap<>();
        }
        albumCoverHashMap.put(key, bytes);

        if (albumCoverHashMap.size() > 5) {
            for (String s : albumCoverHashMap.keySet()) {
                albumCoverHashMap.remove(s);
                break;
            }
        }
    }

    public static byte[] getAlbumFromCache(String key) {
        if (albumCoverHashMap == null)
            return null;
        for (String s : albumCoverHashMap.keySet()) {
            if (s.equals(key))
                return albumCoverHashMap.get(s);
        }
        return null;
    }

    public static void clearAlbumCache() {
        if (albumCoverHashMap != null)
            albumCoverHashMap.clear();
    }


    /**
     * 向缓存中添加数据
     *
     * @param key
     * @param list
     */
    public static void addContentByPathToCache(String key, ArrayList<MusicFile> list) {
        if (contentByPath == null) {
            contentByPath = new HashMap<>();
        }
        contentByPath.put(key, list);
        LogUtils.d("size:" + contentByPath.size());
    }

    /**
     * 从缓存中获取数据
     *
     * @param key
     * @return
     */
    public static ArrayList<MusicFile> getContentByPathFromCache(String key) {
        if (contentByPath == null)
            return new ArrayList<>();
        for (String s : contentByPath.keySet()) {
            if (s.equals(key)) {
                return contentByPath.get(s);
            }
        }
        return new ArrayList<>();
    }

    /**
     * 清空缓存
     */
    public static void clearCache() {
        if (contentByPath != null) contentByPath.clear();
        if (musicFiles != null) musicFiles.clear();
        if (musicByPath != null) musicByPath.clear();
    }


    /**
     * 全部播放缓存
     *
     * @param key
     * @param list
     */
    public static void addMusicByPathToCache(String key, List<MusicFile> list) {
        if (musicByPath == null) {
            musicByPath = new HashMap<>();
        }
        musicByPath.put(key, list);
    }

    public static List<MusicFile> getMusicByPathFromCache(String key) {
        if (musicByPath == null)
            return new ArrayList<>();
        for (String s : musicByPath.keySet()) {
            if (s.equals(key))
                return musicByPath.get(s);
        }
        return new ArrayList<>();
    }

    public static boolean isContainsPath(String key) {
        if (musicByPath == null)
            return false;
        return musicByPath.containsKey(key);
    }
}
