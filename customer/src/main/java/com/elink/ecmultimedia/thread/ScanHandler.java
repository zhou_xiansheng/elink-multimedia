package com.elink.ecmultimedia.thread;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import androidx.annotation.NonNull;

import com.elink.ecmultimedia.adapter.music.MusicRightUSBListAdapter;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicFragment;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicRightMenuListFragment;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.dialog.MusicLoadingTask;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MediaConstants;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.ecmultimedia.utils.UsbDirectoryUtils;
import com.elink.elinkmultimediaservices.beans.MusicFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 点击某个目录加载该目录下的音乐
 */
public class ScanHandler {
    public static final HashMap<String, ScanHandler> hashMap = new HashMap<>();
    private final ReentrantLock reentrantLock;
    private String path;

    public Handler handler;
    private final HandlerThread handlerThread;
    public static final int LOCK = 1;
    public static final int UNLOCK = 2;
    public static final int SCAN = 3;
    public Handler preHandler;
    public MusicLoadingTask musicLoadingTask;

    private ScanHandler scanHandler;

    public int directoryItemBackgroundCount = 0;//后台处理完的音乐数量
    public int directoryItemCount = 0;//目录下的音乐文件数量
    public boolean scanDirectoryEnd = false;//后台处理完的音乐数量

    public ScanHandler(String path, Handler handler) {
        this.path = path;
        reentrantLock = new ReentrantLock();
        handlerThread = new HandlerThread(path);
        preHandler = handler;
        scanHandler = this;
        initHandler();
    }

    public void initHandler() {
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                int what = msg.what;
                int arg1 = msg.arg1;
                try {
                    switch (what) {
                        case LOCK:
                            //开始扫描的时候进行加锁
                            lock();
                            break;
                        case UNLOCK:
                            //后台任务处理完了后释放锁
                            unlock();
                            break;
                        case SCAN://扫描任务
                            try {
                                MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
                                lock();
                                MusicFragment musicFragment = MyApplication.getMusicFragment();
                                musicFragment.musicPlayAllDisable(true);//全部播放禁用
                                ScanHandler.add(path, scanHandler);
                                ArrayList<MusicFile> files = (ArrayList<MusicFile>) UsbDirectoryUtils.getContentFile(MediaConstants.path);
                                scanHandler.scanDirectoryEnd = true;
                                if (directoryItemCount == directoryItemBackgroundCount)
                                    handler.obtainMessage(ScanHandler.UNLOCK).sendToTarget();
                                if (files.isEmpty() && !Objects.equals(mainActivity.musicPath[1], "")) {
                                    // 换u盘了
                                    mainActivity.musicPath[1] = "";
                                    MediaConstants.path = MediaConstants.rootPath;
                                    //重新扫描
                                    MusicRightMenuListFragment musicRightMenuListFragment = MyApplication.getMusicRightMenuListFragment();
                                    musicRightMenuListFragment.usbScanCallBack();
                                } else {
                                    //刷新列表
                                    Message message = preHandler.obtainMessage(MusicRightUSBListAdapter.NOTIFY_LIST);
                                    message.obj = files;
                                    message.arg1 = arg1;
                                    preHandler.sendMessage(message);
                                }
                            } catch (Exception e) {
                                LogUtils.e(e.getMessage(), e);
                            }
                            break;
                        default:
                    }
                } catch (Exception e) {
                    LogUtils.e(e.getMessage(), e);
                }
            }
        };
    }

    public void lock() {
        LogUtils.d("等待锁");
        reentrantLock.lock();
        LogUtils.d("获得锁");
    }

    public void unlock() {
        try {
            LogUtils.d("释放锁");
            reentrantLock.unlock();
        } catch (Exception e) {
            LogUtils.e(e.getMessage(), e);
        }
    }

    public void quit() {
        try {
            handlerThread.quitSafely();
        } catch (Exception e) {
            LogUtils.e(e.getMessage(), e);
        }
    }

    public void startLoadingTask() {
        if (musicLoadingTask == null) {
            musicLoadingTask = new MusicLoadingTask(MyApplication.getMainActivity(), "加载中");
        }
        musicLoadingTask.execute();
    }

    /*-------------------------------- 集合操作 -------------------------------------*/

    public static ScanHandler get(String path) {
        return hashMap.get(path);
    }

    public static void add(String path, ScanHandler handler) {
        hashMap.put(path, handler);
    }

    public static void remove(String path) {
        hashMap.remove(path);
    }

    public static boolean containsKey(String path) {
        return hashMap.containsKey(path);
    }
}
