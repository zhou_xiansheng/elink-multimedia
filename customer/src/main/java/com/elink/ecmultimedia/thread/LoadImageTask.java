package com.elink.ecmultimedia.thread;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;

import com.elink.ecmultimedia.cache.MusicCache;
import com.elink.ecmultimedia.utils.LogUtils;

import java.io.File;
import java.lang.ref.WeakReference;

/**
 * usb 列表专辑图片异步加载方案
 * 通过比较 imageView 的 Tag 确保 异步加载图片不会错位
 */
public class LoadImageTask extends AsyncTask<String, Void, Bitmap> {
    private final WeakReference<ImageView> imageViewReference;
    private String path;

    public LoadImageTask(ImageView imageView) {
        imageViewReference = new WeakReference<>(imageView);
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected Bitmap doInBackground(String... params) {
        Bitmap res = null;
        String imageUrl = params[0];
        path = imageUrl;
        // 在后台加载图片
        byte[] albumCover = null;
        if (imageUrl != null) {
            albumCover = MusicCache.getAlbumFromCache(imageUrl);
            File file = new File(imageUrl);
            if (albumCover == null && file.exists() && file.isFile()) {
                try (MediaMetadataRetriever retriever = new MediaMetadataRetriever()) {
                    retriever.setDataSource(imageUrl);
                    albumCover = retriever.getEmbeddedPicture();
                    MusicCache.addAlbumCache(imageUrl, albumCover);
                } catch (Exception e) {
                    LogUtils.e(e.getMessage(), e);
                }
            }
        }
        if (albumCover != null && albumCover.length > 0) {
            res = BitmapFactory.decodeByteArray(albumCover, 0, albumCover.length);
        }
        return res;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (imageViewReference != null && bitmap != null) {
            ImageView imageView = imageViewReference.get();
            if (imageView != null && path.equals(imageView.getTag())) {
                imageView.setImageBitmap(bitmap); // 在 UI 线程中设置图片
            }
        }
    }
}
