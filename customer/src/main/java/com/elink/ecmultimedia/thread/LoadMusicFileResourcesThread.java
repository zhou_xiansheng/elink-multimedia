package com.elink.ecmultimedia.thread;

import android.util.Log;

import java.util.HashMap;

public class LoadMusicFileResourcesThread extends Thread {
    public static final HashMap<String, LoadMusicFileResourcesThread> hashMap = new HashMap<>();
    public boolean isLoading;
    public final String path;


    public LoadMusicFileResourcesThread(String path) {
        this.path = path;
        isLoading = true;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public boolean isLoading() {
        synchronized (hashMap) {
            return isLoading;
        }
    }

    /*-------------------------------- 集合操作 -------------------------------------*/


    public static void add(String path, LoadMusicFileResourcesThread thread) {
        hashMap.put(path, thread);
    }

    public static void remove(String path) {
        hashMap.remove(path);
    }

    public static boolean containsKey(String path) {
        return hashMap.containsKey(path);
    }

    public static void setLoading(String path, boolean loading) {
        synchronized (hashMap) {
            LoadMusicFileResourcesThread loadMusicFileResourcesThread = hashMap.get(path);
            if (loadMusicFileResourcesThread != null) {
                loadMusicFileResourcesThread.setLoading(loading);
            }
        }
    }
}


