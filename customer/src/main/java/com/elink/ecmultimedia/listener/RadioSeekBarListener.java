package com.elink.ecmultimedia.listener;

import android.annotation.SuppressLint;
import android.util.Log;
import android.widget.SeekBar;

import com.elink.ecmultimedia.datamodel.RadioOperations;
import com.elink.ecmultimedia.ui.main.myview.CustomerSeekBar;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.ui.main.fragment.radio.RadioFragment;
import com.elink.ecmultimedia.pojo.Radio;

import java.util.LinkedList;

public class RadioSeekBarListener implements SeekBar.OnSeekBarChangeListener {
    private MultimediaMainActivity mainActivity;
    public static int progress;


    public RadioSeekBarListener() {
        mainActivity = MyApplication.getMainActivity();
        progress = mainActivity.radio.thumbPos;
    }

    /**
     * 当滑块的位置改变时，这个方法会被调用
     *
     * @param progress 参数是新的进度值，默认范围从0到100，可以在 layout 文件中指定
     * @param fromUser 参数指示这个改变是否来自用户的交互
     */
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//        Log.d(TAG, "onProgressChanged: " + progress + "/" + fromUser);
        // 当滑块的位置发生改变时，调用SeekBar的invalidate()方法来请求重新绘制
        seekBar.invalidate();//绘制

        RadioSeekBarListener.progress = progress;
        RadioFragment radioFragment = MyApplication.getRadioFragment();
        radioFragment.updateRadioId(progress);
    }

    /**
     * 开始滑动时
     *
     * @param seekBar The SeekBar in which the touch gesture began
     */
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    /**
     * 停止滑动时 播放当前 FM
     *
     * @param seekBar The SeekBar in which the touch gesture began
     */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        RadioFragment radioFragment = MyApplication.getRadioFragment();
        radioFragment.radioSwitchToStart((progress + 1) / 10f + 87.4f);
    }

    @SuppressLint({"DefaultLocale", "NotifyDataSetChanged"})
    public void switchRadioStation(int fm) {
        /*根据FM频率切换电台*/
        RadioFragment radioFragment = MyApplication.getRadioFragment();
        radioFragment.updateRadioId(progress);
    }
}
