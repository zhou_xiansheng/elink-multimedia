package com.elink.ecmultimedia.listener;

import android.util.Log;
import android.widget.SeekBar;

import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.ui.main.view.MusicBigPlayingActivity;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.ecmultimedia.utils.TimeUtil;
import com.elink.elinkmultimediaservices.constants.common.IPlaybackStatus;

public class MusicSeekBarListener implements SeekBar.OnSeekBarChangeListener {

    public MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
    public Object object;
    public MusicSeekBarListener(Object o) {
        object = o;
    }

    public void onProgressChanged(SeekBar seekBar, int progress,
                                  boolean fromUser) {
        if (object instanceof MusicBigPlayingActivity) {
            MusicBigPlayingActivity musicBigPlayingActivity = (MusicBigPlayingActivity) object;
            musicBigPlayingActivity.musicNowTime.setText(TimeUtil.formatTime(progress, mainActivity.musicNow.duration));
        }
    }

    /*滚动时,应当暂停后台定时器*/
    public void onStartTrackingTouch(SeekBar seekBar) {
        mainActivity.isSeekBarChanging = true;
    }

    /**
     * 滑动结束后，重新设置值
     * 如果滑动条到末尾了
     * 暂停播放
     *
     * @param seekBar The SeekBar in which the touch gesture began
     */
    public void onStopTrackingTouch(SeekBar seekBar) {
        mainActivity.isSeekBarChanging = false;
        try {
            mainActivity.serviceConnection.mediaPlayerSeekTo(seekBar.getProgress());
            if (object instanceof MusicBigPlayingActivity) {
                MusicBigPlayingActivity musicBigPlayingActivity = (MusicBigPlayingActivity) object;
                if (mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PAUSE) {
                    // 延时滚动一次
                    musicBigPlayingActivity.handler.postDelayed(() -> musicBigPlayingActivity.lyricsScrolling(false), 500);
                } else {
                    musicBigPlayingActivity.handler.removeCallbacksAndMessages(null);
                    musicBigPlayingActivity.handler.postDelayed(() -> musicBigPlayingActivity.lyricsScrolling(true), 500);
                }
            }
        } catch (Exception e) {
            LogUtils.e(e.getMessage(),e);
        }
    }
}