package com.elink.ecmultimedia.adapter.music;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


import com.elink.ecmultimedia.ui.main.fragment.music.MusicLeftPlayingFragment;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicRightMenuListFragment;
import com.elink.ecmultimedia.ui.main.myview.PlayAnimation;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.ecmultimedia.R;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.common.IPlaybackStatus;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MusicLeftListAdapter extends RecyclerView.Adapter<MusicLeftListAdapter.ViewHold> {

    private final List<MusicFile> musicList;
    private MusicLeftListAdapter.ViewHold now = null;
    private MultimediaMainActivity mainActivity;
    MusicLeftPlayingFragment musicLeftListFragment;

    public MusicLeftListAdapter(ArrayList<MusicFile> musicList, MusicLeftPlayingFragment musicLeftPlayingFragment) {
        this.musicList = musicList;
        this.musicLeftListFragment = musicLeftPlayingFragment;
    }
    @SuppressLint("NotifyDataSetChanged")
    @NonNull
    @Override
    public ViewHold onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_music_left_play_list, parent, false);
        ViewHold viewHold = new ViewHold(view);
        mainActivity = MyApplication.getMainActivity();
        return viewHold;
    }

    /**
     * 给子项进行赋值
     */
    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull ViewHold holder, int position) {
        MusicFile music = musicList.get(position);
        if (music.path != null) {
            holder.path = music.path;
        }
        holder.musicSinger.setText(music.artist);
        holder.musicName.setText(music.fileName);

//        LogUtils.d("fileName:"+music.fileName);

        //播放动画
        holder.playAnimation.stopHandler();
        holder.playAnimation.setVisibility(View.INVISIBLE);
        if (mainActivity.musicNow != null && mainActivity.musicNow.fileName != null) {
            if (holder.path.equals(mainActivity.musicNow.path)) {
                holder.playAnimation.startHandler(mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PLAYING);
                holder.playAnimation.setVisibility(View.VISIBLE);
                now = holder;
            }
        }


        if (music.path != null) {
            File file = new File(music.path);
            if (file.exists()) {
                holder.musicName.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.white));
                holder.musicSinger.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.white));
            } else {
                holder.musicName.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.gray));
                holder.musicSinger.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.gray));
            }
        } else {
            holder.musicName.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.gray));
            holder.musicSinger.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.gray));
        }


        //单击到當前位置播放
        holder.mView.setOnClickListener(v -> {
            LogUtils.d("");
            MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
            MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
            if (!mainActivity.canUsbPlay())
                return;
            if (music.path != null) {
                File file = new File(music.path);
                if (file.exists()) {
                    mainActivity.musicNow = music;//修改当前播放音乐
                    musicLeftPlayingFragment.usbPlaying(music);//播放音乐
                    musicLeftPlayingFragment.updateMusicInfo(music);//更新歌曲显示信息
                    musicLeftListFragment.updatePlaying();
                    MusicRightMenuListFragment musicRightMenuListFragment = MyApplication.getMusicRightMenuListFragment();
                    if (musicRightMenuListFragment != null) {
                        musicRightMenuListFragment.usbInsertHistoryCallback();
                    }
                    /*修改右边高亮的歌曲与当前同步*/
                    if (mainActivity.aSwitch.musicRight == 2) {
                        if (musicRightMenuListFragment != null) {
                            musicRightMenuListFragment.updateUsbPlaying();
                        }
                    }
                } else {
                    Toast.makeText(MyApplication.getContext(), "音乐文件丢失", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return musicList.size();
    }


    static class ViewHold extends RecyclerView.ViewHolder {

        View mView;

        private String path;

        TextView musicName;
        TextView musicSinger;

        PlayAnimation playAnimation;

        public ViewHold(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
            musicName = itemView.findViewById(R.id.music_name);
            musicSinger = itemView.findViewById(R.id.music_singer);
            playAnimation = itemView.findViewById(R.id.music_play_now_flag);
        }
    }
}


