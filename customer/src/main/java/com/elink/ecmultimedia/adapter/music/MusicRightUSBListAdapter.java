package com.elink.ecmultimedia.adapter.music;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.elink.ecmultimedia.thread.LoadImageTask;
import com.elink.ecmultimedia.thread.ScanHandler;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicFragment;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicLeftPlayingFragment;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicRightMenuListFragment;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MediaConstants;
import com.elink.ecmultimedia.utils.MusicUtils;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.ecmultimedia.R;
import com.elink.elinkmultimediaservices.beans.MusicFile;

import java.util.ArrayList;
import java.util.List;

public class MusicRightUSBListAdapter extends RecyclerView.Adapter<MusicRightUSBListAdapter.ViewHold> {


    private MultimediaMainActivity mainActivity;
    private MusicFragment musicFragment;
    public List<MusicFile> musicList;
    private MusicRightUSBListAdapter.ViewHold now = null;//记录当前背景高亮的项


    private static final int MESSAGE_LOAD_MUSIC_LIST = 1;
    public static final int NOTIFY_LIST = 2;
    public static final int reFresh_back = 1;
    public static final int reFresh_scan = 2;
    public static final int reFresh_in = 3;

    public final HandlerThread backgroundThread = new HandlerThread("BackgroundThread");
    public Handler backgroundHandler;


    public MusicRightUSBListAdapter(ArrayList<MusicFile> musicList) {
        this.musicList = musicList;
    }


    /**
     * 1、完成 viewHold 的实例化和赋值
     * 2、给其中的控件设置监听事件
     */
    @SuppressLint("NotifyDataSetChanged")
    @NonNull
    @Override
    public ViewHold onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_music_right, parent, false);
        ViewHold viewHold = new ViewHold(view);
        mainActivity = MyApplication.getMainActivity();
        musicFragment = MyApplication.getMusicFragment();
        return viewHold;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHold holder, int position) {

    }


    /**
     * 给子项进行赋值
     */
    @RequiresApi(api = Build.VERSION_CODES.Q)
    @SuppressLint({"DefaultLocale", "NotifyDataSetChanged"})
    @Override
    public void onBindViewHolder(@NonNull ViewHold holder, int position, @NonNull List<Object> payloads) {
        MusicFile music = musicList.get(position);
        holder.musicAlbum.setTag(music.path);
        holder.path = music.path;
        holder.musicName.setText(music.fileName);
        if (music.isDir) {
            holder.musicSinger.setText("");
            holder.musicAlbum.setImageResource(R.drawable.song_folder2);
            holder.musicName.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.white));
        } else {
            if (payloads.isEmpty()) {
                holder.musicSinger.setText(music.artist);
                holder.musicAlbum.setImageResource(R.drawable.music_album_default);
                // 先默认设置背景，如果存在专辑照明的话就进行替换
                holder.musicName.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.white));
                // 将当前播放的歌曲名高亮显示
                MusicFile musicNow = mainActivity.musicNow;
                if (musicNow != null && musicNow.fileName != null) {
                    if (music.path.equals(musicNow.path)) {
                        // 将点击的项换成高亮图标
                        holder.musicName.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.light_blue));
                        now = holder;
                    }
                }
                LoadImageTask loadImageTask = new LoadImageTask(holder.musicAlbum);
                loadImageTask.execute(music.path);
                super.onBindViewHolder(holder, position, payloads);
            } else {
                Bundle payload = (Bundle) payloads.get(0);
                if (payload.containsKey("playing")) {
                    holder.musicName.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.white));
                    MusicFile musicNow = mainActivity.musicNow;
                    if (musicNow != null && musicNow.fileName != null) {
                        if (music.path.equals(musicNow.path)) {
                            // 将点击的项换成高亮图标
                            holder.musicName.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.light_blue));
                            now = holder;
                        }
                    }
                }
            }
        }
        //单击列表项事件
        holder.mView.setOnClickListener(v -> {
            mainActivity = MyApplication.getMainActivity();
            if (music.isDir) {
                //如果是目录就进入，并展示该目录下的所有音乐
                MusicRightMenuListFragment musicRightMenuListFragment = MyApplication.getMusicRightMenuListFragment();
                mainActivity.pushStack(musicRightMenuListFragment.getLayoutManager().findFirstVisibleItemPosition());
                MediaConstants.path = music.path;
                //上边UI
                musicFragment.addToPath(music.fileName);
                musicFragment.setMusicPath();
                //扫描出该目录下的直接目录和音乐
                backgroundHandler.sendEmptyMessage(MESSAGE_LOAD_MUSIC_LIST);
            } else {
                if (!mainActivity.canUsbPlay())
                    return;
                //如果不是目录就播放
                String[] split = music.fileName.split("\\.");
                if (MusicUtils.typeAdaptation(split[split.length - 1])) {
                    MusicLeftPlayingFragment playingFragment = MyApplication.getMusicLeftPlayingFragment();
                    if (music.path != null) {
                        playingFragment.usbPlaying(music);
                        playingFragment.updatePlayListData();
                        playingFragment.notifyDataSetChanged();
                    }
                    MyApplication.getMusicLeftPlayingFragment().updateVisibleBtnOfPlayToList();
                } else {
                    LogUtils.d("暂不支持该格式");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return musicList.size();
    }

    /**
     * 使用子线程处理耗时任务
     * 这里统一处理点击文件夹加载任务，然后分发给不同的线程去处理
     */
    public void startDataLoadThread() {
        if (!backgroundThread.isAlive()) {
            LogUtils.d("");
            backgroundThread.start();
            backgroundHandler = new Handler(backgroundThread.getLooper()) {
                @Override
                public void handleMessage(@NonNull Message msg) {
                    MusicRightMenuListFragment musicRightMenuListFragment = MyApplication.getMusicRightMenuListFragment();
                    musicFragment = MyApplication.getMusicFragment();
                    switch (msg.what) {
                        case MESSAGE_LOAD_MUSIC_LIST:
                            // 加载音乐列表
                            ScanHandler scanHandler = new ScanHandler(MediaConstants.path, backgroundHandler);
                            scanHandler.handler.obtainMessage(ScanHandler.SCAN).sendToTarget();
                            break;
                        case NOTIFY_LIST:
                            ArrayList<MusicFile> data = (ArrayList<MusicFile>) msg.obj;
                            int arg1 = msg.arg1;
                            musicRightMenuListFragment.stopUsbScroll();//  停止滑动，可能之前在快速滑动的
                            if (arg1 == reFresh_back) {
                                musicRightMenuListFragment.usbScrollToPosition(0);
                                musicRightMenuListFragment.notifyUsbData1(data);
                            } else if (arg1 == reFresh_scan) {
                                musicRightMenuListFragment.notifyUsbData2(data);
                            } else {
                                musicRightMenuListFragment.usbScrollToPosition(0);
                                musicRightMenuListFragment.notifyUsbData(data);
                            }
                            break;
                        default:
                            break;
                    }
                }
            };
        }
    }

    public void removeCallbacksAndMessages() {
        if (backgroundHandler != null) {
            backgroundHandler.removeCallbacksAndMessages(null);
        }
    }


    static public class ViewHold extends RecyclerView.ViewHolder {
        View mView;
        String path;
        TextView musicName;
        TextView musicSinger;
        public ImageView musicAlbum;//专辑图片

        public ViewHold(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
            musicName = itemView.findViewById(R.id.music_name);
            musicSinger = itemView.findViewById(R.id.music_singer);
            musicAlbum = itemView.findViewById(R.id.music_album);
        }
    }
}


