package com.elink.ecmultimedia.adapter.music;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.elink.ecmultimedia.R;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicBigPlayCollectListFragment;
import com.elink.ecmultimedia.ui.main.myview.PlayAnimation;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.ui.main.view.MusicBigPlayingActivity;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.common.IPlaybackStatus;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MusicBigPlayCollectListAdapter extends RecyclerView.Adapter<MusicBigPlayCollectListAdapter.ViewHold> {

    private final List<MusicFile> musicList;

    private MultimediaMainActivity mainActivity;

    public MusicBigPlayCollectListAdapter.ViewHold now = null;
    public MusicBigPlayCollectListFragment musicBigPlayCollectListFragment;


    public MusicBigPlayCollectListAdapter(ArrayList<MusicFile> musicList, MusicBigPlayCollectListFragment musicBigPlayCollectListFragment) {
        this.musicList = musicList;
        this.musicBigPlayCollectListFragment = musicBigPlayCollectListFragment;
    }

    /**
     * 1、完成 viewHold 的实例化和赋值
     * 2、给其中的控件设置监听事件
     */
    @SuppressLint("NotifyDataSetChanged")
    @NonNull
    @Override
    public ViewHold onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_music_big_play_list, parent, false);
        ViewHold viewHold = new ViewHold(view);
        mainActivity = MyApplication.getMainActivity();
        return viewHold;
    }

    /**
     * 给子项进行赋值
     */
    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull ViewHold holder, int position) {

        MusicFile music = musicList.get(position);
        holder.path = music.path;
        holder.musicSinger.setText(music.artist);
        holder.musicName.setText(music.fileName);
//        LogUtils.d("onBindViewHolder: " + music.path);
        //播放动画
        holder.playAnimation.stopHandler();
        holder.playAnimation.setVisibility(View.INVISIBLE);
        if (mainActivity.musicNow != null && mainActivity.musicNow.fileName != null) {
            if (holder.path.equals(mainActivity.musicNow.path)) {
                holder.playAnimation.startHandler(mainActivity.musicPlayer.status == IPlaybackStatus.STATUS_PLAYING);
                holder.playAnimation.setVisibility(View.VISIBLE);
                now = holder;
                MusicBigPlayingActivity musicBigPlayingActivity = MyApplication.getMusicBigPlayingActivity();
                if (musicBigPlayingActivity != null)
                    musicBigPlayingActivity.playAnimation = now.playAnimation;
            }
        }
        //单击到當前位置播放
        holder.mView.setOnClickListener(v -> {
            MusicBigPlayingActivity musicBigPlayingActivity = MyApplication.getMusicBigPlayingActivity();
            MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
            if (music.path != null) {
                if (mainActivity.canUsbPlay()) {
                    File file = new File(music.path);
                    if (file.exists()) {
                        musicBigPlayingActivity.usbPlay(music);//播放音乐
                        /*替换背景图片*/
                        musicBigPlayCollectListFragment.updatePlaying();
                    } else {
                        LogUtils.w("! file.exists()");
                    }
                }
            } else {
                LogUtils.w("music path null");
            }
        });

    }

    @Override
    public int getItemCount() {
        return musicList.size();
    }


    public static class ViewHold extends RecyclerView.ViewHolder {

        View mView;

        private String path;
        TextView musicName;
        TextView musicSinger;

        public PlayAnimation playAnimation;

        public ViewHold(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
            musicName = itemView.findViewById(R.id.music_name);
            musicSinger = itemView.findViewById(R.id.music_singer);
            playAnimation = itemView.findViewById(R.id.music_play_now_flag);
        }
    }
}


