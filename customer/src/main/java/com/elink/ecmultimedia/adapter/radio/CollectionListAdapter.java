package com.elink.ecmultimedia.adapter.radio;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.elink.ecmultimedia.ui.main.fragment.radio.RadioFragment;
import com.elink.ecmultimedia.ui.main.myview.CustomerSeekBar;
import com.elink.ecmultimedia.R;
import com.elink.ecmultimedia.listener.RadioSeekBarListener;
import com.elink.ecmultimedia.pojo.Radio;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.utils.MyApplication;

import java.util.ArrayList;
import java.util.List;

public class CollectionListAdapter extends RecyclerView.Adapter<CollectionListAdapter.ViewHold> {
    private final List<Radio> collectRadios;
    private RadioFragment radioFragment;
    private MultimediaMainActivity mainActivity;
    //记录当前背景高亮的项

    public CollectionListAdapter(List<Radio> countyNames) {
        this.collectRadios = countyNames;
    }

    /**
     * 1、完成 viewHold 的实例化和赋值
     * 2、给其中的控件设置监听事件
     */
    @SuppressLint("NotifyDataSetChanged")
    @NonNull
    @Override
    public ViewHold onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_radio_collect, parent, false);
        ViewHold viewHold = new ViewHold(view);
        radioFragment = MyApplication.getRadioFragment();
        mainActivity = MyApplication.getMainActivity();
        return viewHold;
    }


    /**
     * 给子项进行赋值
     */
    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull ViewHold holder, int position) {
        Radio radio = collectRadios.get(position);
        holder.radioRate.setText(String.format("%.1f", radio.radioId));
        holder.radioType.setText(radio.type);
        holder.radioTypeName.setText(radio.typeName);
        //先將全部取消高亮
        holder.mView.setBackgroundResource(R.drawable.radio_collection_bg_normal);
        if (mainActivity.radio.thumbPos == radio.thumbPos) {
            //将点击的项换成高亮图标
            holder.mView.setBackgroundResource(R.drawable.radio_collection_bg_selected);
        }
        holder.mView.setOnClickListener(v -> {
            if (!mainActivity.canFmPlay())
                return;
            if (!mainActivity.radioPlayer.isScan && !mainActivity.radioPlayer.isSeek) {
                //切换电台
                radioFragment.radioSwitchToStart(Float.parseFloat(holder.radioRate.getText().toString()));
            } else {
                Toast.makeText(MyApplication.getContext(), "请先停止搜索!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return collectRadios.size();
    }


    static class ViewHold extends RecyclerView.ViewHolder {

        View mView;

        TextView radioType;
        TextView radioTypeName;
        TextView radioRate;

        public ViewHold(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
            radioType = itemView.findViewById(R.id.radio_type);
            radioTypeName = itemView.findViewById(R.id.radio_type_name);
            radioRate = itemView.findViewById(R.id.radio_rate);
        }
    }
}


