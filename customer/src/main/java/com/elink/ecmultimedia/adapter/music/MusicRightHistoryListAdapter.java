package com.elink.ecmultimedia.adapter.music;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.AudioManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.elink.ecmultimedia.R;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicFragment;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicLeftPlayingFragment;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.common.EUsbStatus;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MusicRightHistoryListAdapter extends RecyclerView.Adapter<MusicRightHistoryListAdapter.ViewHold> {

    private final LinkedList<MusicFile> musicList;

    private MultimediaMainActivity mainActivity;


    public MusicRightHistoryListAdapter(LinkedList<MusicFile> musicList) {
        this.musicList = musicList;
    }

    /**
     * 1、完成 viewHold 的实例化和赋值
     * 2、给其中的控件设置监听事件
     */
    @SuppressLint("NotifyDataSetChanged")
    @NonNull
    @Override
    public ViewHold onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_music_right_history, parent, false);
        ViewHold viewHold = new ViewHold(view);
        mainActivity = MyApplication.getMainActivity();
        return viewHold;
    }


    /**
     * 更新列表中的数据，删除选中的
     */
    public void modifyData() {
        ArrayList<MusicFile> musicDeleteList = MyApplication.getMusicFragment().musicDeleteList;
        //列表数据
        for (int j = 0; j < musicDeleteList.size(); j++) {
            for (int i = musicList.size() - 1; i >= 0; i--) {
                if (musicDeleteList.get(j).fileName.equals(musicList.get(i).fileName)) {
                    musicList.remove(i);
                    break;
                }
            }
        }

    }


    /**
     * 给子项进行赋值
     */
    @SuppressLint({"DefaultLocale", "NotifyDataSetChanged"})
    @Override
    public void onBindViewHolder(@NonNull ViewHold holder, int position) {
        MusicFile music = musicList.get(position);
        holder.path = music.path;
        holder.musicSinger.setText(music.artist);
        holder.musicName.setText(music.fileName);
        holder.musicAlbum.setImageResource(R.drawable.music_album_default);
        // 复选框的显示与勾选
        if (mainActivity.musicPlayer.isEditor) {
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.checkBox.setChecked(music.isSelected);
        } else {
            holder.checkBox.setVisibility(View.GONE);
        }

        //文字高亮
        File file = new File(music.path);
        if (file.exists()) {
            // 当前歌曲高亮
            if (music.path.equals(mainActivity.musicNow.path)) {
                holder.musicName.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.light_blue));
            } else {
                holder.musicName.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.white));
            }
        } else {
            holder.musicName.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.gray));
        }
        holder.mView.setOnClickListener(v -> {
            MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
            if (mainActivity.musicPlayer.isEditor) {
                boolean isChecked = !holder.checkBox.isChecked();
                holder.checkBox.setChecked(isChecked);
            } else {
                if (!mainActivity.canUsbPlay())
                    return;
                MusicLeftPlayingFragment playingFragment = MyApplication.getMusicLeftPlayingFragment();
                if (music.path != null && EUsbStatus.isStateUseFull(mainActivity.musicPlayer.mUsbStatus)) {
                    if (file.exists()) {
                        List<MusicFile> musicCollectList = mainActivity.serviceConnection.getMusicCollectList();
                        for (int i = 0; i < musicCollectList.size(); i++) {
                            if (musicCollectList.get(i).fileName.equals(music.fileName)) {
                                mainActivity.musicPlayer.index = i;//修改当前播放音乐的下标
                                break;
                            }
                        }
                        playingFragment.usbPlaying(music);//播放音乐
                        playingFragment.updateMusicInfo(music);//更新歌曲显示信息
                        playingFragment.updatePlaying();
                        notifyDataSetChanged();
                    } else {
                        LogUtils.d("文件丢失：" + music.path);
                    }
                } else {
                    LogUtils.d("path为 null");
                }
            }
        });
        holder.checkBox.setOnCheckedChangeListener(null);//首先移除监听器
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MusicFragment musicFragment = MyApplication.getMusicFragment();
                ArrayList<MusicFile> deleteList = musicFragment.musicDeleteList;
                if (isChecked) {
                    music.isSelected = true;
                    deleteList.add(music);
                } else {
                    music.isSelected = false;
                    for (int i = deleteList.size() - 1; i >= 0; i--) {
                        if (music.path.equals(deleteList.get(i).path)) {
                            deleteList.remove(i);
                            break;
                        }
                    }
                }
                if (deleteList.size() == 0) {
                    LogUtils.d("没有选中的了");
                    mainActivity.musicPlayer.isHistorySelectAll = false;
                    musicFragment.musicPlayAll.setText("全部选择");
                    musicFragment.musicDeleteDisableOrNot(true);
                } else if (deleteList.size() == mainActivity.musicHistory.size()) {
                    LogUtils.d("全部选中了");
                    musicFragment.musicPlayAll.setText("全部取消");
                    mainActivity.musicPlayer.isHistorySelectAll = true;
                    musicFragment.musicDeleteDisableOrNot(false);
                } else {
                    LogUtils.d("选中一些");
                    mainActivity.musicPlayer.isHistorySelectAll = false;
                    musicFragment.musicPlayAll.setText("全部选择");
                    musicFragment.musicDeleteDisableOrNot(false);
                }
            }
        });
    }

    @Override
    public void onViewRecycled(@NonNull ViewHold holder) {
        super.onViewRecycled(holder);
        holder.checkBox.setOnCheckedChangeListener(null); // 当ViewHolder被回收时移除监听器
        holder.mView.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return musicList.size();
    }

    /**
     * 全部勾选
     */
    @SuppressLint("NotifyDataSetChanged")
    public void selectAll() {
        LogUtils.d("");
        musicList.forEach(musicFile -> {
            musicFile.isSelected = true;
        });
        notifyDataSetChanged();
        //将所有项添加到删除列表
        if (mainActivity.musicHistory != null)
            mainActivity.musicHistory.forEach(musicFile -> musicFile.isSelected = true);
        MusicFragment musicFragment = MyApplication.getMusicFragment();
        musicFragment.musicDeleteList.clear();
        musicFragment.musicDeleteList.addAll(musicList);
        //将删除按钮显示
        musicFragment.musicDeleteDisableOrNot(false);
    }

    @SuppressLint("NotifyDataSetChanged")
    public void cancelAll() {
        LogUtils.d("");
        if (musicList != null)
            musicList.forEach(musicFile -> {
                musicFile.isSelected = false;
            });
        //保险 将这个也取消删除
        MusicFragment musicFragment = MyApplication.getMusicFragment();
        musicFragment.musicDeleteList.clear();
        if (mainActivity != null && mainActivity.musicHistory != null)
            mainActivity.musicHistory.forEach(musicFile -> musicFile.isSelected = false);
        notifyDataSetChanged();
    }


    static class ViewHold extends RecyclerView.ViewHolder {

        View mView;

        String path;
        TextView musicName;
        TextView musicSinger;
        ImageView musicAlbum;//专辑图片

        public CheckBox checkBox;//复选框

        public ViewHold(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
            musicName = itemView.findViewById(R.id.music_name);
            musicSinger = itemView.findViewById(R.id.music_singer);
            musicAlbum = itemView.findViewById(R.id.music_album);
            checkBox = itemView.findViewById(R.id.checkbox_history);
        }
    }
}


