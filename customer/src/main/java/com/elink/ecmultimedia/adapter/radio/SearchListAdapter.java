package com.elink.ecmultimedia.adapter.radio;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.elink.ecmultimedia.datamodel.RadioOperations;
import com.elink.ecmultimedia.datamodel.SharedPreferencesControl;
import com.elink.ecmultimedia.listener.RadioSeekBarListener;
import com.elink.ecmultimedia.ui.main.fragment.radio.RadioFragment;
import com.elink.ecmultimedia.ui.main.fragment.radio.RadioSearchListFragment;
import com.elink.ecmultimedia.ui.main.myview.CustomerSeekBar;
import com.elink.ecmultimedia.pojo.Radio;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.ecmultimedia.R;

import java.util.List;

public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.ViewHold> {

    private final List<Radio> radios;

    private MultimediaMainActivity mainActivity;
    private ViewHold now = null;//记录当前背景高亮的项

    private RadioFragment radioFragment;


    public SearchListAdapter(List<Radio> radios) {
        this.radios = radios;
    }

    /**
     * 1、完成 viewHold 的实例化和赋值
     * 2、给其中的控件设置监听事件
     */
    @SuppressLint("NotifyDataSetChanged")
    @NonNull
    @Override
    public ViewHold onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_radio_list, parent, false);
        mainActivity = MyApplication.getMainActivity();
        radioFragment = MyApplication.getRadioFragment();

        return new ViewHold(view);
    }

    /**
     * 给子项进行赋值
     */
    @SuppressLint({"DefaultLocale", "NotifyDataSetChanged", "SetTextI18n"})
    @Override
    public void onBindViewHolder(@NonNull ViewHold holder, int position) {

        Radio radio = radios.get(position);

        holder.radioRate.setText(radio.radioId + "");
        holder.radioType.setText(radio.type);
        holder.radioTypeName.setText(radio.typeName);

        if (radio.isCollect == 0) {
            holder.radioIsCollection.setImageResource(R.drawable.radio_collect_n);
        } else if (radio.isCollect == 1) {
            holder.radioIsCollection.setImageResource(R.drawable.radio_collect_d);
        }

        holder.mView.setBackgroundResource(R.drawable.radio_collection_bg_normal);
        if (mainActivity.radio.thumbPos == radio.thumbPos) {
            now = holder;
            //将点击的项换成高亮图标
            holder.mView.setBackgroundResource(R.drawable.radio_collection_bg_selected);
        }


        holder.mView.setOnClickListener(v -> {
            LogUtils.d("");
            if (!mainActivity.canFmPlay())
                return;
            if (!mainActivity.radioPlayer.isScan && !mainActivity.radioPlayer.isSeek) {
                radioFragment.searchListToStart((int) (radio.radioId * 10));
                /*替换背景图片*/
                //先前高亮背景改为默认图标
                if (now != null)
                    now.mView.setBackgroundResource(R.drawable.radio_collection_bg_normal);
                //将点击的项换成高亮图标
                holder.mView.setBackgroundResource(R.drawable.radio_collection_bg_selected);
                now = holder;
                RadioSearchListFragment radioSearchListFragment = MyApplication.getRadioSearchListFragment();
                if (radioSearchListFragment != null) {
                    radioSearchListFragment.setEnableOrNotOfButton();
                }
            }
        });
        //收藏事件
        holder.radioIsCollection.setOnClickListener(v -> {
                    LogUtils.d("onBindViewHolder: 搜索列表收藏事件" + radio.thumbPos + "/" + mainActivity.radio.thumbPos);
                    if (!mainActivity.radioPlayer.isScan) {
                        int isCollect = radio.isCollect;
                        //反着来，收藏变未收藏，未收藏变收藏
                        if (isCollect == 0) {
                            radio.isCollect = 1;
                            mainActivity.radioCollect.add(radio);
                            if (mainActivity.radio.thumbPos == radio.thumbPos) {
                                mainActivity.radio.isCollect = 1;
                                SharedPreferencesControl.updateRadioNow(MyApplication.getContext(), mainActivity.radio);
                            }
                            RadioOperations.updateSearchRadio(MyApplication.getContext(), radio);
                            RadioOperations.insertCollectRadio(MyApplication.getContext(), radio);
                        } else if (isCollect == 1) {
                            radio.isCollect = 0;
                            //逆序删除避免漏掉删除,虽然应该不存在相同的项
                            for (int i = mainActivity.radioCollect.size() - 1; i >= 0; i--) {
                                if (radio.thumbPos == mainActivity.radioCollect.get(i).thumbPos) {
                                    mainActivity.radioCollect.remove(i);
                                }
                            }
                            if (mainActivity.radio.thumbPos == radio.thumbPos) {
                                mainActivity.radio.isCollect = 0;
                                SharedPreferencesControl.updateRadioNow(MyApplication.getContext(), mainActivity.radio);
                            }
                            RadioOperations.updateSearchRadio(MyApplication.getContext(), radio);
                            RadioOperations.deleteCollectRadioByThumbPos(MyApplication.getContext(), radio.thumbPos);
                        }
                        // 通知适配器该项数据已更改
                        notifyItemChanged(position);
                    }
                }
        );

    }


    @Override
    public int getItemCount() {
        return radios.size();
    }


    static class ViewHold extends RecyclerView.ViewHolder {

        View mView;

        TextView radioType;

        TextView radioTypeName;
        TextView radioRate;

        ImageView radioIsCollection;

        public ViewHold(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
            radioType = itemView.findViewById(R.id.radio_type);
            radioTypeName = itemView.findViewById(R.id.radio_type_name);
            radioRate = itemView.findViewById(R.id.radio_rate);
            radioIsCollection = itemView.findViewById(R.id.radio_is_collection);
        }
    }
}


