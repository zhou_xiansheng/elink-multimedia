package com.elink.ecmultimedia.adapter.music;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.elink.ecmultimedia.R;
import com.elink.ecmultimedia.pojo.Lyric;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.utils.MyApplication;

import java.util.ArrayList;
import java.util.List;

public class MusicBigPlayLyricListAdapter extends RecyclerView.Adapter<MusicBigPlayLyricListAdapter.ViewHold> {
    private final List<Lyric> lyricList;
    private MultimediaMainActivity mainActivity;
    private int currentlyPlayingPosition;

    private final int expectedColor = ContextCompat.getColor(MyApplication.getContext(), R.color.orange);


    public MusicBigPlayLyricListAdapter(ArrayList<Lyric> musicList) {
        this.lyricList = musicList;
    }

    @SuppressLint("NotifyDataSetChanged")
    @NonNull
    @Override
    public ViewHold onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_music_lyric_list, parent, false);
        ViewHold viewHold = new ViewHold(view);
        mainActivity = MyApplication.getMainActivity();
        //滑动事件
        return viewHold;
    }

    /**
     * 更新歌词高亮
     *
     * @param position
     */
    public void setCurrentlyPlayingPosition(int position) {
        int previous = currentlyPlayingPosition;
        currentlyPlayingPosition = position;
        if (mainActivity != null)
            mainActivity.runOnUiThread(() -> {
                notifyItemChanged(previous);
                notifyItemChanged(position);
            });
    }

    /**
     * 给子项进行赋值
     */
    @SuppressLint({"DefaultLocale", "ResourceAsColor"})
    @Override
    public void onBindViewHolder(@NonNull ViewHold holder, int position) {
        Lyric lyric = lyricList.get(position);
        holder.lyricContent.setText(lyric.lyricSentence);
        if (position == currentlyPlayingPosition) {
            //如果不是亮的就变亮
            int currentHintTextColor = holder.lyricContent.getCurrentTextColor();
            if (currentHintTextColor != expectedColor)
                holder.lyricContent.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.light_blue));
        } else {
            holder.lyricContent.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return lyricList.size();
    }

    static class ViewHold extends RecyclerView.ViewHolder {

        View mView;

        TextView lyricContent;

        public ViewHold(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
            lyricContent = itemView.findViewById(R.id.music_lyric);
        }
    }
}


