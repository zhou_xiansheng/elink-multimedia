package com.elink.ecmultimedia.callbacks;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.widget.Toast;

import com.elink.ecmultimedia.cache.MusicCache;
import com.elink.ecmultimedia.pojo.Switch;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicFragment;

import com.elink.ecmultimedia.ui.main.fragment.music.MusicLeftPlayingFragment;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicRightMenuListFragment;
import com.elink.ecmultimedia.ui.main.fragment.radio.RadioFragment;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.ui.main.view.MusicBigPlayingActivity;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MusicUtils;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.elinkmultimediaservices.IMusicCallback;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.common.EAudioSource;
import com.elink.elinkmultimediaservices.constants.common.EBtBState;
import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.constants.common.EUsbStatus;
import com.elink.elinkmultimediaservices.constants.common.IErrorType;
import com.elink.elinkmultimediaservices.constants.common.IPlayMode;
import com.elink.elinkmultimediaservices.constants.common.IPlaybackStatus;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;
import com.elink.elinkmultimediaservices.constants.wrapper.PlayStatusWrapper;

public class MusicCallback extends IMusicCallback.Stub {
    @Override
    public void onFocusLost() {
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        mainActivity.runOnUiThread(() -> {
            LogUtils.d("失去音频焦点回调");
            // 更新UI，例如暂停播放器等。
            if (mainActivity.aSwitch.musicOrRadio == Switch.PATH_MUSIC) {
                if (mainActivity.aSwitch.bigOrSmall == Switch.PATH_MUSIC_SMALL) {
                    MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
                    if (musicLeftPlayingFragment != null) {
                        musicLeftPlayingFragment.usbPauseCallBack();
                    }
                } else if (mainActivity.aSwitch.bigOrSmall == Switch.PATH_MUSIC_BIG) {
                    MusicBigPlayingActivity musicBigPlayingActivity = MyApplication.getMusicBigPlayingActivity();
                    if (musicBigPlayingActivity != null) {
                        musicBigPlayingActivity.pauseCallBack();
                    }
                }
            }
        });
    }

    @SuppressLint("CommitTransaction")
    @Override
    public void onUsbDisconnect() {
        LogUtils.d("");
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        MusicRightMenuListFragment musicRightMenuListFragment = MyApplication.getMusicRightMenuListFragment();
        MusicFragment musicFragment = MyApplication.getMusicFragment();
        MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
        mainActivity.doUsbDisconnect();
        if (mainActivity.aSwitch.musicOrRadio == 0) {
            if (mainActivity.aSwitch.bigOrSmall == 0) {
                if (mainActivity.aSwitch.musicLeft == Switch.PATH_MUSIC_PLAY
                        && mainActivity.serviceConnection.getMusicSources() == IPlaySource.USB) {
                    LogUtils.d("左边播放界面");
                    if (musicLeftPlayingFragment != null) {
                        musicLeftPlayingFragment.usbLostCallback();
                    }
                } else if (mainActivity.aSwitch.musicLeft == 1) {
                    LogUtils.d("左边播放列表");
                    if (musicLeftPlayingFragment != null) {
                        musicLeftPlayingFragment.playListUsbLostCallback();
                    }
                }
                if (mainActivity.aSwitch.musicRight == 2) {
                    LogUtils.d("usb 音乐");
                    if (musicFragment != null) {
                        mainActivity.runOnUiThread(musicFragment::usbDetachCallback);
                    }
                } else if (mainActivity.aSwitch.musicRight == 3) {
                    LogUtils.d("右边历史记录");
                    if (musicFragment != null) {
                        new Handler(Looper.getMainLooper()).postDelayed(musicFragment::usbDetachCallback, 3000);
                    }
                    if (musicRightMenuListFragment != null) {
                        musicRightMenuListFragment.usbDetachHistoryCallback();
                    }
                }
            } else if (mainActivity.aSwitch.bigOrSmall == 1) {
                LogUtils.d("大窗口播放模式");
                // 修改UI
                if (mainActivity.serviceConnection.getMusicSources() == IPlaySource.USB) {
                    mainActivity.runOnUiThread(() -> {
                        MusicBigPlayingActivity musicBigPlayingActivity = MyApplication.getMusicBigPlayingActivity();
                        if (musicBigPlayingActivity != null) {
                            musicBigPlayingActivity.usbLostCallback();
                        }
                    });
                }
            }
        }
    }

    /**
     * usb 扫描过程，定时回调
     * 播放列表、usb音乐、历史记录等更新
     *
     * @throws RemoteException
     */
    @Override
    public void usbScanCallBack() throws RemoteException {
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        MusicRightMenuListFragment musicRightMenuListFragment = MyApplication.getMusicRightMenuListFragment();
        MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
        MusicCache.updateMusicFiles();
        //刷新各个列表
        if (mainActivity.aSwitch.musicOrRadio == 0) {
            if (mainActivity.aSwitch.bigOrSmall == 0) {
                if (mainActivity.aSwitch.musicRight == 2) {
                    LogUtils.d("usb 音乐");//刷新
                    musicRightMenuListFragment.usbScanCallBack();
                }
            }
        }
    }

    @Override
    public void onUsbStatusChangeCallBack(int status) throws RemoteException {
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        MusicRightMenuListFragment musicRightMenuListFragment = MyApplication.getMusicRightMenuListFragment();
        MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
        EUsbStatus usbStatus = EUsbStatus.fromValue(status);
        int musicSources = mainActivity.serviceConnection.getMusicSources();
        EAudioSource audioSources = mainActivity.serviceConnection.getAudioSources();
        LogUtils.d("usbStatus:" + usbStatus + " musicSources:" + musicSources);
        if (musicSources == IPlaySource.USB) {
            if (usbStatus == EUsbStatus.INSERT) {
                musicLeftPlayingFragment.usbInsertCallBack();
                musicLeftPlayingFragment.updateVisibleBtnOfPlayToList();
                if (mainActivity.aSwitch.musicRight == 3) {
                    musicRightMenuListFragment.usbInsertHistoryCallback();
                }
                //usb 挂载上立即开机恢复自动播放
                if (audioSources.check(EAudioSource.AUDIO_SOURCE_USB)) {
                    if (mainActivity.musicPlayer.resumePage != IPlaySource.UNKNOWN) {
                        musicLeftPlayingFragment.usbPlaying();
                    }
                }
                mainActivity.musicPlayer.resumePage = IPlaySource.UNKNOWN;
            } else if (usbStatus == EUsbStatus.SCANNED) {
            } else if (usbStatus == EUsbStatus.DISCONNECT) {
                musicLeftPlayingFragment.usbDisconnectCallBack();
            }
        }
    }


    @Override
    public void btMusicDataCallBack(MusicFile musicFile) throws RemoteException {
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        int musicSources = mainActivity.serviceConnection.getMusicSources();
        if (musicFile.playSource == IPlaySource.KW) {
            LogUtils.d("忽略回调");
            return;
        }
        LogUtils.d("fileName:" + musicFile.fileName);
        mainActivity.musicNow = musicFile;
        //刷新列表
        mainActivity.runOnUiThread(() -> {
            if (mainActivity.aSwitch.musicOrRadio == 0) {
                if (mainActivity.aSwitch.bigOrSmall == 0) {
                    if (mainActivity.aSwitch.musicLeft == 0) {
                        //左边播放。歌曲信息、进度条信息
                        MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
                        if (musicLeftPlayingFragment != null) {
                            musicLeftPlayingFragment.nextMusicCallback(musicFile);
                        }
                    }
                }
            }
        });
    }

    /**
     * 蓝牙断开连接回调
     *
     * @param i
     * @param musicFile
     * @throws RemoteException
     */
    @Override
    public void btConnectStatusCallback(int i, MusicFile musicFile) throws RemoteException {
        EBtBState btBState = EBtBState.fromValue(i);
        LogUtils.d("btBState:" + btBState + " fileName:" + musicFile.fileName);
        MusicRightMenuListFragment musicRightMenuListFragment = MyApplication.getMusicRightMenuListFragment();
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        int musicSources = mainActivity.serviceConnection.getMusicSources();
        if (musicSources == IPlaySource.BT)
            mainActivity.musicNow = musicFile;
        if (btBState == EBtBState.STATE_DISCONNECTED) {
            if (mainActivity.serviceConnection.getMusicSources() == IPlaySource.BT) {
                if (mainActivity.aSwitch.bigOrSmall == Switch.PATH_MUSIC_SMALL) {
                    if (mainActivity.aSwitch.musicLeft == 0) {
                        //左边播放。歌曲信息、进度条信息
                        MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
                        if (musicLeftPlayingFragment != null) {
                            mainActivity.musicNow = musicFile;
                            musicLeftPlayingFragment.btDisconnectCallback();
                        }
                    }
                    if (mainActivity.aSwitch.musicRight == 1) {
                        if (musicRightMenuListFragment != null) {
                            musicRightMenuListFragment.btDisconnectCallback();
                        }
                    }
                } else {
                    MusicBigPlayingActivity musicBigPlayingActivity = MyApplication.getMusicBigPlayingActivity();
                    if (musicBigPlayingActivity != null) {
                        mainActivity.musicPlayer.status = IPlaybackStatus.STATUS_PAUSE;
                        musicBigPlayingActivity.updateMusicInfo(musicFile);
                        musicBigPlayingActivity.updateUiOfBtPause();
                    }
                }
            }
        } else if (btBState == EBtBState.STATE_CONNECTING) {//修改UI但是不进行判断播放
            MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
            if (mainActivity.aSwitch.musicOrRadio == 0) {
                if (mainActivity.aSwitch.bigOrSmall == 0) {
                    if (mainActivity.aSwitch.musicRight == 1 || musicSources == IPlaySource.BT) {
                        if (mainActivity.aSwitch.musicRight == 1) {
                            mainActivity.setAudioSources(EAudioSource.AUDIO_SOURCE_BT);
                            mainActivity.musicNow = musicFile;
                        }
                        if (mainActivity.aSwitch.musicLeft == Switch.PATH_MUSIC_COLLECT) {
                            musicLeftPlayingFragment.toPlay();
                        } else {
                            musicRightMenuListFragment.upDateBtInfo(musicFile);
                        }
                        musicLeftPlayingFragment.updateUiOfPause();
                    }
                }
            }
        } else if (btBState == EBtBState.STATE_CONNECTED) {//完全连接上，判断是否需要播放
            LogUtils.d("btBState:" + btBState + " fileName:" + musicFile.fileName);
            MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
            EAudioSource audioSources = mainActivity.serviceConnection.getAudioSources();
            if (mainActivity.aSwitch.musicOrRadio == 0) {
                if (mainActivity.aSwitch.bigOrSmall == 0) {
                    if (mainActivity.aSwitch.musicRight == 1) { //延迟播放，手机上可能还没有切换到车机音频，手机会先放一下，所以延迟等切换到车机音频
                        new Handler(Looper.getMainLooper()).postDelayed(musicLeftPlayingFragment::btPlaying, 4000);
                    }

//                    else if (audioSources.check(EAudioSource.AUDIO_SOURCE_BT)) {
//                        // 1、开机恢复播放，可能离开了蓝牙界面、
//                        // 2、之前播放的是蓝牙，断开连接，离开蓝牙模块
//                        mainActivity.musicPlayer.resumePage = IPlaySource.UNKNOWN;
//                        musicLeftPlayingFragment.btPlaying();
//                    }

                }
            }
        } else {
            LogUtils.d("未处理状态 btBState:" + btBState);
        }
    }

    /**
     * 蓝牙电话状态改变
     *
     * @param
     * @throws RemoteException
     */
    @Override
    public void btPhoneStatusChangeCallback(PlayStatusWrapper playStatusWrapper) throws RemoteException {
        LogUtils.d("status:" + playStatusWrapper.getStatus());
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        mainActivity.musicPlayer.mBtCallStatus = playStatusWrapper.getStatus();
        //fm 滑动条需要回调控制
        RadioFragment radioFragment = MyApplication.getRadioFragment();
        if (radioFragment != null) {
            radioFragment.enableOrNotSeekBar();
        }
    }

    @Override
    public void progressOfUsbCallback(int process) {
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        int musicSources = mainActivity.serviceConnection.getMusicSources();
        int playMode = mainActivity.serviceConnection.getPlayMode();
        Switch aSwitch = mainActivity.aSwitch;
        if (playMode == IPlayMode.PLAY_MODE_MUSIC && musicSources == IPlaySource.USB && !mainActivity.isSeekBarChanging) {
            if (aSwitch.musicOrRadio == Switch.PATH_MUSIC) {
                if (aSwitch.bigOrSmall == Switch.PATH_MUSIC_SMALL
                        && aSwitch.musicLeft == Switch.PATH_MUSIC_PLAY) {
                    MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
                    if (musicLeftPlayingFragment != null) {
                        musicLeftPlayingFragment.setSeekBarProgress(process);
                    }
                } else if (aSwitch.bigOrSmall == Switch.PATH_MUSIC_BIG) {
                    MusicBigPlayingActivity musicBigPlayingActivity = MyApplication.getMusicBigPlayingActivity();
                    if (musicBigPlayingActivity != null) {
                        musicBigPlayingActivity.setSeekBarProgress(process);
                    }
                }
            }
        }
    }

    @Override
    public void musicSourceChangeCallback(int source) throws RemoteException {
        LogUtils.d("");
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        if (mainActivity.aSwitch.musicOrRadio == Switch.PATH_MUSIC) {
            if (mainActivity.aSwitch.bigOrSmall == Switch.PATH_MUSIC_SMALL) {
                if (mainActivity.aSwitch.musicLeft == Switch.PATH_MUSIC_PLAY) {
                    new Handler(Looper.getMainLooper()).postDelayed(() -> {
                        MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
                        musicLeftPlayingFragment.updateVisibleOfViews();
                        musicLeftPlayingFragment.updateMusicInfo(mainActivity.serviceConnection.getMusicNow());
                    }, 300);
                }
            }
        }
    }


    /**
     * @param status    播放状态
     * @param isNew     是否是原来音乐还是下一首
     * @param musicFile 当前播放的音乐
     */
    @Override
    public void onPlaybackStatusChanged(int status, int isNew, MusicFile musicFile) {
        LogUtils.d("status:" + EPlayStatus.fromValue(status)
                + " isNew:" + isNew
                + " path:" + musicFile.path);
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        MusicRightMenuListFragment musicRightMenuListFragment = MyApplication.getMusicRightMenuListFragment();
        int musicSources = mainActivity.serviceConnection.getMusicSources();
        EAudioSource audioSources = mainActivity.serviceConnection.getAudioSources();
        if (musicFile.playSource == IPlaySource.KW || musicSources != musicFile.playSource) {
            LogUtils.d("忽略回调 ");
            return;
        }
        if (musicFile.playSource == IPlaySource.USB
                && mainActivity.musicPlayer.mUsbStatus != EUsbStatus.DISCONNECT) {
            mainActivity.lyricNow = MusicUtils.parseLyricsByLyricPath(musicFile);//歌词修改
            mainActivity.musicPlayer.duration = musicFile.duration;
        } else {
            LogUtils.d(musicFile.playSource + "/" + mainActivity.musicPlayer.mUsbStatus);
        }
        mainActivity.musicNow = musicFile;
        mainActivity.musicPlayer.status = status;

        if (!musicFile.fileName.trim().isEmpty()) {
            mainActivity.musicNow = musicFile;
        }
        //历史记录 播放列表 修改
        if (mainActivity.serviceConnection.getMusicSources() == IPlaySource.USB) {
            mainActivity.modifyToHistoryFirst(musicFile);//历史记录修改
            MyApplication.getMusicLeftPlayingFragment().changeMusicList(musicFile);
        }

        mainActivity.runOnUiThread(() -> {
            MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
            if (mainActivity.aSwitch.bigOrSmall == 0) {
                if (mainActivity.aSwitch.musicLeft == 0) {
                    LogUtils.d("播放界面");
                    if (status == IPlaybackStatus.STATUS_PAUSE) {
                        mainActivity.runOnUiThread(() -> {
                            musicLeftPlayingFragment.playOrPauseCallBack(0);
                        });
                    } else if (status == IPlaybackStatus.STATUS_PLAYING) {
                        if (isNew == 0) {
                            //当前音乐
                            mainActivity.runOnUiThread(() -> musicLeftPlayingFragment.playOrPauseCallBack(1));
                        } else if (isNew == 1) {
                            //切歌
                            musicLeftPlayingFragment.nextMusicCallback(musicFile);
                        } else if (isNew == 2) {
                            musicLeftPlayingFragment.nextMusicCallback(musicFile);
                        }
                    }
                } else if (mainActivity.aSwitch.musicLeft == 1) {
                    LogUtils.d("左边播放列表");
                    if (audioSources.check(EAudioSource.AUDIO_SOURCE_USB)) {
                        //左边收藏列表。播放标志修改
                        if (musicLeftPlayingFragment != null) {
                            musicLeftPlayingFragment.playListNextMusicCallback(musicFile);
                        }
                    } else if (audioSources.check(EAudioSource.AUDIO_SOURCE_BT)) {
                        musicLeftPlayingFragment.toPlay();
                    }
                }
                if (mainActivity.aSwitch.musicRight == 2) {
                    LogUtils.d("右边 usb");
                    if (musicRightMenuListFragment != null) {
                        musicRightMenuListFragment.updateUsbPlaying();
                    }
                } else if (mainActivity.aSwitch.musicRight == 3) {
                    LogUtils.d("右边历史记录");
                    if (musicRightMenuListFragment != null
                            && mainActivity.serviceConnection.getMusicSources() == IPlaySource.USB) {
                        musicRightMenuListFragment.nextPlayHistoryCallback();
                    }
                }
            } else if (mainActivity.aSwitch.bigOrSmall == 1) {
                LogUtils.d("大窗口播放");
                MusicBigPlayingActivity musicBigPlayingActivity = MyApplication.getMusicBigPlayingActivity();
                if (musicBigPlayingActivity != null) {
                    if (status == IPlaybackStatus.STATUS_PAUSE) {
                        musicBigPlayingActivity.pauseCallBack();
                    } else if (status == IPlaybackStatus.STATUS_PLAYING) {
                        if (mainActivity.serviceConnection.getMusicSources() == IPlaySource.BT
                                && mainActivity.aSwitch.bigMusic == IPlaySource.USB) {
                            musicBigPlayingActivity.toBt();
                        }
                        if (isNew == 0) {
                            //当前音乐
                            musicBigPlayingActivity.playNewCallback(0);
                        } else if (isNew == 1) {
                            //切歌
                            musicBigPlayingActivity.nextMusicCallback(musicFile);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void errorCallBack(int type) throws RemoteException {
        LogUtils.d("type:" + type);
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        mainActivity.runOnUiThread(() -> {
            if (type == IErrorType.USB_PLAY)
                Toast.makeText(mainActivity, "音乐无效", Toast.LENGTH_SHORT).show();
            else if (type == IErrorType.BT_PHONE)
                Toast.makeText(mainActivity, "蓝牙通话中", Toast.LENGTH_SHORT).show();
            else if (type == IErrorType.USB_DISCONNECT)
                Toast.makeText(mainActivity, "usb 未插入", Toast.LENGTH_SHORT).show();
        });
    }
}
