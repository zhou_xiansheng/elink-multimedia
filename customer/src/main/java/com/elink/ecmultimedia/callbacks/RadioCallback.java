package com.elink.ecmultimedia.callbacks;

import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;

import com.elink.ecmultimedia.R;
import com.elink.ecmultimedia.datamodel.RadioOperations;
import com.elink.ecmultimedia.datamodel.SharedPreferencesControl;
import com.elink.ecmultimedia.pojo.Radio;
import com.elink.ecmultimedia.pojo.Switch;
import com.elink.ecmultimedia.ui.main.fragment.radio.RadioFragment;
import com.elink.ecmultimedia.ui.main.fragment.radio.RadioSearchListFragment;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.ecmultimedia.utils.RadioConstants;
import com.elink.ecmultimedia.utils.UnitUtils;
import com.elink.elinkmultimediaservices.IRadioCallback;
import com.elink.elinkmultimediaservices.beans.RadioChannel;
import com.elink.elinkmultimediaservices.constants.common.EAudioSource;
import com.elink.elinkmultimediaservices.constants.common.IPlaybackStatus;
import com.elink.elinkmultimediaservices.constants.radio.ERadioStatus;

import java.util.LinkedList;
import java.util.List;

public class RadioCallback extends IRadioCallback.Stub {
    @Override
    public void onSearchedListChanged(int status, List<RadioChannel> list) throws RemoteException {
        LogUtils.d("");
        RadioSearchListFragment radioSearchListFragment = MyApplication.getRadioSearchListFragment();
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        if (radioSearchListFragment != null) {
            mainActivity.radioPlayer.isScan = false;
            radioSearchListFragment.onSearchCompleted();
            if (status == IPlaybackStatus.STATUS_PLAYING) {
                // 继续播放
                mainActivity.runOnUiThread(() -> {
                    mainActivity.radioPlayer.status = IPlaybackStatus.STATUS_PLAYING;//播放
                    MyApplication.getRadioFragment().radioSwitch.setImageResource(R.drawable.radio_pause); //修改图标
                });
            }
            //修改适配器中的数据,并刷新列表
            LogUtils.d("修改适配器中的数据,并刷新列表");
            mainActivity.searchList.clear();
            //数据转换 RadioChannel--》Radio
            Radio radio;
            for (RadioChannel radioChannel : list) {
                radio = Radio.newFMRadio(radioChannel.getChannelNum());
                mainActivity.searchList.add(radio);
            }

            //数据库更新
            new Handler(Looper.getMainLooper()).post(() -> RadioOperations.updateAllSearchRadio(MyApplication.getContext(), mainActivity.searchList));
            //是否在收藏列表中，在的话就改变其标识
            for (Radio r : mainActivity.searchList) {
                for (Radio ra : mainActivity.radioCollect) {
                    if (r.thumbPos == ra.thumbPos) {
                        r.isCollect = 1;
                        break;
                    }
                }
            }
            //列表数据修改
            radioSearchListFragment.updateDataList();
            // UI 修改
            mainActivity.runOnUiThread(() -> {
                radioSearchListFragment.notifyDataSetChanged();
                radioSearchListFragment.promptAfterNotifyDataSetChanged();

                //启用一些功能
                RadioFragment radioFragment = MyApplication.getRadioFragment();
                if (radioFragment != null) {
                    radioFragment.lightOrGreyView();
                    radioFragment.enableOrNotSeekBar();
                }
            });
        }

    }

    @Override
    public void onFocusLost() throws RemoteException {
        LogUtils.d("");
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        mainActivity.radioPlayer.status = IPlaybackStatus.STATUS_PAUSE;
        mainActivity.radioPlayer.isSeek = false;
        mainActivity.radioPlayer.isScan = false;
        //暂停播放 ，修改UI
        if (mainActivity.aSwitch.musicOrRadio == 1) {
            if (mainActivity.aSwitch.radioPage == 0) {
                //播放页面
                RadioFragment radioFragment = MyApplication.getRadioFragment();
                if (radioFragment != null) {
                    radioFragment.pauseCallback();
                    radioFragment.onFocusLostCallback();
                }
            } else if (mainActivity.aSwitch.radioPage == 1) {
                //搜索页面
                RadioSearchListFragment radioSearchListFragment = MyApplication.getRadioSearchListFragment();
                if (radioSearchListFragment != null) {
                    radioSearchListFragment.onFocusLostCallback();
                }
            }
        }
    }

    /**
     * 将这个频率添加到电台列表中
     * 1、在不在搜索列表中
     * 2、在不在收藏列表中
     * 3、在不在电台列表界面
     *
     * @param channelNum
     */
    @Override
    public void searchNext(int channelNum) {
        LogUtils.d("channelNum:" + channelNum);
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        RadioFragment radioFragment = MyApplication.getRadioFragment();
//        if (radioFragment != null) {
//            radioFragment.unlock();
//        }
        if (channelNum == -1) {
            LogUtils.d("被取消搜索 ！");
            mainActivity.radioPlayer.isSeek = false;
            return;
        }

        if (mainActivity != null) {
            float fmI2F = UnitUtils.Channel.fm_i2F(channelNum);
            List<Radio> searchList = mainActivity.searchList;
            boolean inSearchList = false;
            for (int i = 0; i < searchList.size(); i++) {
                Radio radio = searchList.get(i);
                if (radio.radioId == fmI2F) {
                    inSearchList = true;
                    break;
                }
            }
            //不在原列表中
            if (!inSearchList) {
                LogUtils.d("不在电台列表中");
                //构建添加到搜索列表中
                Radio radio = Radio.newFMRadio(channelNum);
                searchList.add(radio);
                //写数据库
                RadioOperations.insertSearchRadio(MyApplication.getContext(), radio);
                //是否收藏在中
                LinkedList<Radio> radioCollect = mainActivity.radioCollect;
                for (int i = 0; i < radioCollect.size(); i++) {
                    if (radioCollect.get(i).radioId == radio.radioId) {
                        radio.isCollect = RadioConstants.collect;
                        RadioOperations.updateSearchRadio(MyApplication.getContext(), radio);
                        break;
                    }
                }
                if (mainActivity.aSwitch.musicOrRadio == Switch.PATH_RADIO) {
                    if (mainActivity.aSwitch.radioPage == Switch.PATH_RADIO_SEARCH) {
                        LogUtils.d("刷新电台列表");
                        RadioSearchListFragment radioSearchListFragment = MyApplication.getRadioSearchListFragment();
                        if (radioSearchListFragment != null) {
                            radioSearchListFragment.updateAndNotifyData();
                            radioSearchListFragment.promptAfterNotifyDataSetChanged();
                        }
                    }
                }
            } else {
                LogUtils.d("在电台列表中");
            }
        }
    }

    /**
     * float v = 87.4f + (fm + 1) / 20f * 2f;
     */
    @Override
    public void onPlaybackStatusChanged(int status, RadioChannel radioChannel) {
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        RadioFragment radioFragment = MyApplication.getRadioFragment();
        RadioSearchListFragment radioSearchListFrame = (RadioSearchListFragment) mainActivity.getSupportFragmentManager().findFragmentByTag("radioList");
        if (status == IPlaybackStatus.STATUS_PAUSE) {
            //暂停
            LogUtils.d("暂停回调");
            if (radioFragment != null) {
                radioFragment.pauseCallback();
            }
        } else if (status == IPlaybackStatus.STATUS_PLAYING) {
            //播放
            LogUtils.d("播放回调");
            if (radioFragment != null) {
                mainActivity.setAudioSources(EAudioSource.AUDIO_SOURCE_FM);
                mainActivity.setAudioSources(EAudioSource.AUDIO_SOURCE_FM);
                Radio radio = Radio.newFMRadio(radioChannel.getChannelNum());
                for (int j = 0; j < mainActivity.radioCollect.size(); j++) {
                    if (mainActivity.radioCollect.get(j).thumbPos == radio.thumbPos) {
                        radio.isCollect = 1;
                    }
                }
                radioFragment.playCallBack();
                // seek 相关
                mainActivity.radioPlayer.isSeek = false;
                mainActivity.radioPlayer.isScan = false;
                if (radioSearchListFrame != null) {
                    radioSearchListFrame.setEnableOrNotOfButton();
                }
                LogUtils.d(mainActivity.radio.radioId + "/" + radio.radioId);
                if (mainActivity.radio.thumbPos != radio.thumbPos) {
                    //更新当前电台及数据库
                    mainActivity.radio = radio;
                    SharedPreferencesControl.updateRadioNow(mainActivity, mainActivity.radio);
                    //更新页面 UI
                    radioFragment.updateCollectImage();
                    radioFragment.updateSeekBar(radio.thumbPos);
                    //更新收藏列表高亮
                    radioFragment.notifyDataSetChanged();
                    // 如果在电台列表界面
                }
                radioFragment.enableOrNotSeekBar();
            }
        }
    }

    @Override
    public void onSeekStatusChanged(int i) throws RemoteException {
        ERadioStatus status = ERadioStatus.fromValue(i);
        LogUtils.d("status:" + status);
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        RadioFragment radioFragment = MyApplication.getRadioFragment();
        if (status == ERadioStatus.SEEK_RUNNING) {
            mainActivity.radioPlayer.isSeek = true;
            mainActivity.runOnUiThread(() -> radioFragment.mCustomDialog.show());
            radioFragment.enableOrNotSeekBar();
        } else if (status == ERadioStatus.SEEK_FINISHED) {
            mainActivity.radioPlayer.isSeek = false;
            mainActivity.runOnUiThread(() -> radioFragment.mCustomDialog.disMiss());
        }
    }
}
