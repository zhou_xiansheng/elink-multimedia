package com.elink.ecmultimedia.callbacks;

import android.util.Log;

import androidx.recyclerview.widget.DiffUtil;

import com.elink.elinkmultimediaservices.beans.MusicFile;

import java.util.List;
import java.util.Objects;


/**
 * 用于 RecycleView，计算两次数据之间的差异，然后更新差异的部分
 */
public class DiffUtilCallBack extends DiffUtil.Callback {
    private final List<MusicFile> oldList;
    private final List<MusicFile> newList;

    public DiffUtilCallBack(List<MusicFile> oldList, List<MusicFile> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return Objects.equals(oldList.get(oldItemPosition).path, newList.get(newItemPosition).path);
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).equals(newList.get(newItemPosition));
    }
}