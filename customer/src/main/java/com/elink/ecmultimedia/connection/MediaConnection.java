package com.elink.ecmultimedia.connection;

import android.content.ComponentName;
import android.os.IBinder;
import android.os.RemoteException;
import android.widget.Toast;

import com.elink.ecmultimedia.callbacks.MusicCallback;
import com.elink.ecmultimedia.callbacks.RadioCallback;
import com.elink.ecmultimedia.ui.main.fragment.music.MusicLeftPlayingFragment;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MusicUtils;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.elinkmultimediaservices.IMusicService;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.beans.RadioChannel;
import com.elink.elinkmultimediaservices.constants.common.EAudioSource;
import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.constants.common.IPlayMode;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;

import java.util.LinkedList;
import java.util.List;

public class MediaConnection implements android.content.ServiceConnection {
    public IMusicService mediaService;
    private MusicCallback musicCallback;
    private RadioCallback radioCallback;
    private final MultimediaMainActivity mainActivity;

    public boolean isServicesBinder = false;

    public MediaConnection(MultimediaMainActivity mainActivity) {
        MyApplication.setMusicServiceConnection(this);
        this.mainActivity = mainActivity;
        musicCallback = new MusicCallback();
        radioCallback = new RadioCallback();
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        if (service != null) {
            LogUtils.d("服务绑定");
            mediaService = IMusicService.Stub.asInterface(service);
            if (mediaService != null) {
                try {
                    isServicesBinder = true;
                    registerMediaCallback();
                    initData();
                    initPlayPage();
                } catch (Exception e) {
                    LogUtils.e(e.getMessage(), e);
                }
            } else {
                LogUtils.w("music 服务绑定失败 ！");
            }
        }
    }

    private void initPlayPage() {
        //数据初始化
        MusicLeftPlayingFragment musicLeftPlayingFragment = MyApplication.getMusicLeftPlayingFragment();
        if (musicLeftPlayingFragment != null) {
            musicLeftPlayingFragment.initPlay();//绑定完成后进行各个页面的初始化
        }
        //页面跳转
        if (getPlayMode() == IPlayMode.PLAY_MODE_MUSIC) {
            if (getMusicSources() == IPlaySource.BT) {
                // 蓝牙音乐
            } else if (getMusicSources() == IPlaySource.USB) {
                // usb 音乐
            } else {
                //菜单
            }
        } else {
            // FM
        }
    }


    @Override
    public void onServiceDisconnected(ComponentName name) {
        LogUtils.d("");
        Toast.makeText(MyApplication.getContext(), "media 服务断开", Toast.LENGTH_SHORT).show();
        isServicesBinder = false;
        mainActivity.startMusicService();
    }

    /**
     * 音乐数据
     */
    public void initData() {
        LogUtils.d("");
        try {
            //播放状态
            mainActivity.musicPlayer.status = mediaService.getMusicStatus();
            mainActivity.musicNow = mediaService.getMusicNow();
            if (getMusicSources() == IPlaySource.USB) {
                mainActivity.lyricNow = MusicUtils.parseLyricsByLyricPath(mainActivity.musicNow);//歌词修改
            }
            LogUtils.d("status:" + EPlayStatus.fromValue(mainActivity.musicPlayer.status)
                    + " path:" + mainActivity.musicNow.path);
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }

    public void mediaPlayerSeekTo(int n) {
        try {
            mediaService.mediaPlayerSeekTo(n);
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }


    public int getCurrentPosition() {
        try {
            return mediaService.getCurrentPosition();
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
        return 0;
    }


    public void prev() {
        try {
            mediaService.prev();
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }

    public void next() {
        try {
            mediaService.next();
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }

    public void playByMusic() {
        try {
            mediaService.playByMusic();
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }

    public void play() {
        try {
            mediaService.play();
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }
    public void customerPlay() {
        try {
            mediaService.customerPlay();
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }

    public void pause() {
        try {
            mediaService.pause();
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }

    public List<MusicFile> getMusicCollectList() {
        try {
            return mediaService.getMusicCollect();
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
        return new LinkedList<>();
    }

    public void setMusicCollection(List<MusicFile> musicFiles) {
        try {
            mediaService.setMusicCollection(musicFiles);
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }

    public void addToMusicCollect(MusicFile music) {
        try {
            mediaService.addToMusicCollect(music);
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }

    public int getPlayMode() {
        try {
            return mediaService.getPlayMode();
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
        return -1;
    }

    public void setAudioSources(int sources) {
        LogUtils.d("sources:" + EAudioSource.fromValue(sources));
        try {
            mediaService.setAudioSources(sources);
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }

    public int getMusicPlayMode() {
        try {
            return mediaService.getMusicPlayMode();
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
        return -1;
    }

    public void setMusicPlayMode(int newMode) {
        try {
            mediaService.setMusicPlayMode(newMode);
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }

    public void setPlayIndex(int index) {
        try {
            LogUtils.d("index:" + index);
            mediaService.setPlayIndex(index);
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }

    public boolean checkBtConnect() {
        try {
            return mediaService.checkBtConnect();
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
        return false;
    }

    public String getBtName() {
        try {
            return mediaService.getBtName();
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
        return "???";
    }

    public void registerMediaCallback() {
        LogUtils.d("");
        try {
            if (mediaService != null) {
                mediaService.registerMusicCallback(musicCallback);
                mediaService.registerRadioCallback(radioCallback);
            }
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }

    public void unregisterMediaCallback() {
        LogUtils.d("");
        try {
            if (mediaService != null) {
                mediaService.unregisterMusicCallback(musicCallback);
                mediaService.unregisterRadioCallback(radioCallback);
            }
            musicCallback = null;
            radioCallback = null;
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }

    public MusicFile getMusicNow() {
        try {
            return mediaService.getMusicNow();
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
        return new MusicFile();
    }

    public void setMusicPlayNow(MusicFile musicFile) {
        try {
            mediaService.setMusicPlayNow(musicFile);
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }

    public int getMusicSources() {
        try {
            return mediaService.getMusicSources();
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
        return IPlaySource.UNKNOWN;
    }

    public EAudioSource getAudioSources() {
        try {
            return EAudioSource.fromValue(mediaService.getAudioSources());
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
        return EAudioSource.AUDIO_SOURCE_UNKNOWN;
    }


    public boolean playSpecialChannel(RadioChannel radioChannel) {
        try {
            mediaService.playSpecialChannel(radioChannel);
            return true;
        } catch (RemoteException e) {
            LogUtils.w(e.getMessage());
            return false;
        }
    }

    public boolean stop() {
        try {
            mediaService.stop();
            return true;
        } catch (RemoteException e) {
            LogUtils.w(e.getMessage());
            return false;
        }
    }

    public void stopScanAndSeek() {
        try {
            mediaService.stopScanAndSeek();
        } catch (RemoteException e) {
            LogUtils.w(e.getMessage());
        }
    }

    public void getSearchedStableChannelList() {
        try {
            mediaService.getSearchedStableChannelList();
        } catch (RemoteException e) {
            LogUtils.w(e.getMessage());
        }
    }
}
