package com.elink.ecmultimedia.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MyApplication;

public class AudioFocusChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String focusState = intent.getStringExtra("focus");
        LogUtils.d("");

        //修改状态和UI
        switch (focusState) {
            case "loss":
                LogUtils.d("音频焦点消失");
                MyApplication.getMusicLeftPlayingFragment().usbPauseCallBack();
                break;
            case "loss_transient":
                break;
        }
    }
}
