package com.elink.ecmultimedia.receivers;

import static com.elink.ecmultimedia.utils.Constants.ATTACHED;
import static com.elink.ecmultimedia.utils.Constants.DETACHED;
import static com.elink.ecmultimedia.utils.Constants.EJECTING;
import static com.elink.ecmultimedia.utils.Constants.MOUNTED;
import static com.elink.ecmultimedia.utils.Constants.SCAN_F;
import static com.elink.ecmultimedia.utils.Constants.SCAN_FF;
import static com.elink.ecmultimedia.utils.Constants.SCAN_FINISH;
import static com.elink.ecmultimedia.utils.Constants.UNMOUNTED;
import static com.elink.ecmultimedia.utils.Constants.table1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.elink.ecmultimedia.ui.main.fragment.music.MusicRightMenuListFragment;
import com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity;
import com.elink.ecmultimedia.utils.LogUtils;
import com.elink.ecmultimedia.utils.MyApplication;
import com.elink.elinkmultimediaservices.constants.common.EUsbStatus;

public class UsbProviderReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        int status = intent.getIntExtra("status", -100);
        int tableId = intent.getIntExtra("tableId", 0);
        if (tableId != table1) {
            LogUtils.d("不是我需要的广播:" + tableId);
            return;
        }
        switch (status) {
            case SCAN_FF:
//                LogUtils.d("[get] 接收失败");
                break;
            case SCAN_F:
//                LogUtils.d("[get] 扫描失败");
                break;
            case EJECTING:
//                LogUtils.d("[get] EJECTING");
                break;
            case DETACHED:
//                LogUtils.d("[get] 断开链接");
                break;
            case ATTACHED:
//                LogUtils.d("[get] u盘插入");
                break;
            case SCAN_FINISH:
//                LogUtils.d("[get] 扫描完成");
                doAfterScanFinish();
                break;
            case UNMOUNTED:
//                LogUtils.d("卸载盘");
                break;
            case MOUNTED:
//                LogUtils.d("挂载上");
                doAfterMOUNTED();
                break;
        }
    }

    private void doAfterScanFinish() {
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        mainActivity.musicPlayer.mUsbStatus = EUsbStatus.SCANNED;
    }

    private void doAfterMOUNTED() {
        MultimediaMainActivity mainActivity = MyApplication.getMainActivity();
        mainActivity.musicPlayer.mUsbStatus = EUsbStatus.INSERT;
        // USB设备已连接，扫描提示
        MusicRightMenuListFragment musicRightMenuListFragment = MyApplication.getMusicRightMenuListFragment();
        if (musicRightMenuListFragment != null) {
            //扫描提示，扫描完成服务那边会进行回调
            musicRightMenuListFragment.setLayoutReadPrompt();
        }
    }
}
