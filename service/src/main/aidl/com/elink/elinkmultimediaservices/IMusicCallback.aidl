package com.elink.elinkmultimediaservices;


import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.wrapper.PlayStatusWrapper;
/**
 * 音乐播放状态回调
 */
interface IMusicCallback {
//失去焦点事件的回调
void onFocusLost();
//usb 拔出
void onUsbDisconnect();
//usb 扫描中
void usbScanCallBack();
//应用蓝牙信息回调
void btMusicDataCallBack(in MusicFile musicFile);
//蓝牙连接断开回调
void btConnectStatusCallback(int status,in MusicFile musicFile);
//电话状态回调
void btPhoneStatusChangeCallback(in PlayStatusWrapper status);
//USB音乐进度回调
void progressOfUsbCallback(int process);
//音乐播放源切换
void musicSourceChangeCallback(int source);
//播放张太改变回调
void onPlaybackStatusChanged(int p0,int p1,in MusicFile musicFile);
//usb状态回调
void onUsbStatusChangeCallBack(int status);
//异常回调
void errorCallBack(int  type);
}