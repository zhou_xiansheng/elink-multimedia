package com.elink.elinkmultimediaservices;

import com.elink.elinkmultimediaservices.beans.RadioChannel;
interface IRadioCallback {
     //搜索列表回调
     void onSearchedListChanged(int status,in List<RadioChannel> list);
     //失去焦点事件的回调
     void onFocusLost();
     //搜索到下一台的回调
     void searchNext(int frequency);
     // 播放暂停回调
     void onPlaybackStatusChanged(int p0,in RadioChannel radioChannel);
     //搜索状态回调
     void onSeekStatusChanged(int stateValue);
}