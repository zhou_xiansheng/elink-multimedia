// IMusicService.aidl
package com.elink.elinkmultimediaservices;

// Declare any non-default types here with import statements
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.beans.RadioChannel;
import com.elink.elinkmultimediaservices.IMusicCallback;
import com.elink.elinkmultimediaservices.IRadioCallback;
interface IMusicService {
    //播放
    void play();
    void customerPlay();
    void playByMusic();
    //暂停
    void pause();
    //停止:用于广播
    void stop();
    //下一个
    void next();
    //上一个
    void prev();

    //-------------------------Music-------------------------
    //获取媒体文件列表
    List<MusicFile> getMediaFileList();
    //获取音乐收藏列表
    List<MusicFile> getMusicCollect();
    //获得当前音乐播放的位置
    int getCurrentPosition();
    //获得音乐播放模式
    int getMusicPlayMode();
    //设置音乐播放模式
    void setMusicPlayMode(int newMode);
    //获得当前播放状态
    int getMusicStatus();
    int getMediaStatus();
    //获得当前播放的音乐
    MusicFile getMusicPlayNow();
    //设置当前播放的音乐，列表指定播放时先调用
    void setMusicPlayNow(in MusicFile musicfile);
    //设置播放下标
    void setPlayIndex(int index);
    //替换播放列表
    void setMusicCollection(in List<MusicFile> musicFiles);
    //将当前音乐田间到播放列表末尾
    void addToMusicCollect(in MusicFile musicFile);
    //设置播放器的播放位置
    void mediaPlayerSeekTo(int progress);
    //音乐播放回调
    void registerMusicCallback(in IMusicCallback callback);
    void unregisterMusicCallback(in IMusicCallback callback);
    //当前播放下标
    int getCollectIndex();
    //当前播放的音乐
    MusicFile getMusicNow();
    //--------------------------蓝牙音乐相关----------------------------
    //检测蓝牙连接状态，并但会连接的设备名称
    boolean checkBtConnect();
    String getBtName();
    //获取当前播放的歌曲信息
    MusicFile getMusicData();
   //--------------------------公共部分----------------------------
    int getAudioSources();
    void setAudioSources(int i);
    int getMusicSources();
    int getPlayMode();
    //-------------------------radio-------------------------
    //播放指定电台
    void playSpecialChannel(in RadioChannel channel);
    //搜索固定频道
    void searchStableChannels();
    //收藏/取消收藏电台
    void setChannelCollection(in RadioChannel channel);
    //获取Radio开关状态
    int getPowerStatus();
    //获取搜索到的固定频道列表
    List<RadioChannel> getSearchedStableChannelList();
    //停止搜索
    void stopScanAndSeek();
    //获取收藏的电台列表
    List<RadioChannel> getCollectedChannelList();
    //回调注册
    void registerRadioCallback(in IRadioCallback callback);
    void unregisterRadioCallback(in IRadioCallback callback);
}