package com.elink.elinkmultimediaservices.service.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.service.MediaServices;
import com.elink.elinkmultimediaservices.utils.LogUtils;

public class VideoBroadcastReceiver extends BroadcastReceiver {
    public static final int VIDEO_PLAY = 1;
    public static final int VIDEO_PAUSE = 0;

    @Override
    public void onReceive(Context context, Intent intent) {
        int status = intent.getIntExtra("isvideoplaying", -1);
        switch (status) {
            case VIDEO_PLAY:
                PlayManage.setVideoPlayStatus(EPlayStatus.STATUS_PLAYING);
                break;
            case VIDEO_PAUSE:
                PlayManage.setVideoPlayStatus(EPlayStatus.STATUS_PAUSE);
                break;
            default:
                LogUtils.d("未处理：" + status);
        }
    }
}
