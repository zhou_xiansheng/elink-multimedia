package com.elink.elinkmultimediaservices.service.receiver;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.elink.elinkmultimediaservices.constants.common.EAudioSource;
import com.elink.elinkmultimediaservices.constants.common.ICustomerPage;
import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.service.MediaServices;
import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.service.manager.listeners.BtCommonStateListener;
import com.elink.elinkmultimediaservices.utils.AppPackageUtils;
import com.elink.elinkmultimediaservices.utils.LogUtils;

/**
 * 先检查下一个模式是否可用
 * 然后切换
 */
public class ModeSwitchBroadcastReceiver extends BroadcastReceiver {
    private final MediaServices mediaServices;
    private long mPreTime;
    private EAudioSource mSourceNext = EAudioSource.AUDIO_SOURCE_FM;

    public ModeSwitchBroadcastReceiver(MediaServices mediaServices) {
        this.mediaServices = mediaServices;
        mPreTime = System.currentTimeMillis();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        long nowTime = System.currentTimeMillis();
        if (nowTime - mPreTime > 500) {
            mPreTime = nowTime;
            mSourceNext = PlayManage.sAudioSource.getNext();
            while (!checkMode(mSourceNext)) {
                LogUtils.d("mModeNext:" + mSourceNext + "不可用！");
                mSourceNext = mSourceNext.getNext();
            }
            if (!PlayManage.checkAudioSource(mSourceNext)) modeSwitch(mSourceNext);
        }
    }

    private boolean checkMode(EAudioSource type) {
        boolean res = false;
        if (type.check(EAudioSource.AUDIO_SOURCE_FM)) {
            res = true;
        } else if (type.check(EAudioSource.AUDIO_SOURCE_USB)) {
            if (PlayManage.checkUsbUseful())
                res = true;
        } else if (type.check(EAudioSource.AUDIO_SOURCE_BT)) {
            if (BtCommonStateListener.isBtConnect())
                res = true;
        } else if (type.check(EAudioSource.AUDIO_SOURCE_KUWO)) {
            Intent intent = new Intent();
            intent.setClassName("cn.kuwo.kwmusiccar", "cn.kuwo.kwmusiccar.WelcomeActivity");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            mediaServices.startActivity(intent);
            if (AppPackageUtils.isContainsApp(intent, mediaServices)) {
                LogUtils.d("kw 可用");
            } else {
                LogUtils.d("kw 不可用");
            }
            res = true;
        }
        return res;
    }

    public void modeSwitch(EAudioSource type) {
        if (type.check(EAudioSource.AUDIO_SOURCE_FM)) {
            doSwitch(ICustomerPage.PAGE_FM);
        } else if (type.check(EAudioSource.AUDIO_SOURCE_USB)) {
            doSwitch(ICustomerPage.PAGE_USB);
        } else if (type.check(EAudioSource.AUDIO_SOURCE_BT)) {
            doSwitch(ICustomerPage.PAGE_BT);
        } else if (type.check(EAudioSource.AUDIO_SOURCE_KUWO)) {
//            mediaServices.setAudioSources(EAudioSource.AUDIO_SOURCE_KUWO);
//            try {
//                Intent intent = new Intent();
//                intent.setClassName("cn.kuwo.kwmusiccar", "cn.kuwo.kwmusiccar.WelcomeActivity");
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                mediaServices.startActivity(intent);
//
//            }
            mediaServices.mKwMusicManager.play();
        }
    }

    public void doSwitch(String page) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("com.elink.ecmultimedia", "com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity"));
        Bundle extras = new Bundle();
        extras.putString("page", page);
        extras.putString("isPlay", EPlayStatus.STATUS_PLAYING.toString());
        intent.putExtras(extras);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mediaServices.startActivity(intent);
    }
}
