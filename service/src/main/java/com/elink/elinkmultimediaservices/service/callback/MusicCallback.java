package com.elink.elinkmultimediaservices.service.callback;

import android.os.RemoteException;

import com.elink.elinkmultimediaservices.IMusicCallback;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.common.EBtBState;
import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;
import com.elink.elinkmultimediaservices.constants.wrapper.PlayStatusWrapper;
import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.service.MediaServices;
import com.elink.elinkmultimediaservices.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 音乐信息回调
 */
public class MusicCallback {
    private MediaServices mediaServices;
    public List<IMusicCallback> callbacks;

    public MusicCallback(MediaServices mediaServices) {
        this.mediaServices = mediaServices;
        callbacks = new ArrayList<>();
    }

    public void clear() {
        LogUtils.d("");
        callbacks.clear();
    }

    /*-------------------------- kw相关 回调 ---------------------------------*/
    public void kwPlayCallBack(MusicFile music) {
        LogUtils.d("size:" + callbacks.size());
        for (int i = callbacks.size() - 1; i >= 0; i--) {
            try {
                callbacks.get(i).onPlaybackStatusChanged(EPlayStatus.STATUS_PLAYING.getValue(), 1, music);
            } catch (Exception e) {
                LogUtils.w(e.getMessage());
                if ("null".equals(e.getMessage()))
                    callbacks.remove(i);
            }
        }
    }

    /*-------------------------- usb相关 回调 ---------------------------------*/
    public void musicSourceChangeCallback(int sources) {
        LogUtils.d("size:" + callbacks.size());
        for (int i = callbacks.size() - 1; i >= 0; i--) {
            try {
                callbacks.get(i).musicSourceChangeCallback(sources);
            } catch (Exception e) {
                LogUtils.w(e.getMessage());
                if ("null".equals(e.getMessage()))
                    callbacks.remove(i);
            }
        }
    }

    public void progressOfUsbCallback(int process) {
        for (int i = callbacks.size() - 1; i >= 0; i--) {
            try {
                callbacks.get(i).progressOfUsbCallback(process);
            } catch (Exception e) {
//                LogUtils.w(e.getMessage());
                if ("null".equals(e.getMessage()))
                    callbacks.remove(i);
            }
        }
    }

    public void usbEjectCallback() {
        LogUtils.d("size:" + callbacks.size());
        for (int i = callbacks.size() - 1; i >= 0; i--) {
            try {
                callbacks.get(i).onUsbDisconnect();
            } catch (Exception e) {
                LogUtils.w(e.getMessage());
                if ("null".equals(e.getMessage()))
                    callbacks.remove(i);
            }
        }
    }

    public void playCallBack(int isNew, MusicFile music) {
        LogUtils.d("size:" + callbacks.size() + " path" + music.path);

        music.lastPlayProgress = mediaServices.mUsbMusicManager.getCurrentPosition();
        for (int i = callbacks.size() - 1; i >= 0; i--) {
            try {
                callbacks.get(i).onPlaybackStatusChanged(EPlayStatus.STATUS_PLAYING.getValue(), isNew, music);
            } catch (Exception e) {
                LogUtils.w(e.getMessage());
                if ("null".equals(e.getMessage()))
                    callbacks.remove(i);
            }
        }

    }
    /*-------------------------- bt 回调 ---------------------------------*/

    /**
     * 蓝牙状态发生改变
     *
     * @param n         状态
     * @param musicData 发生改变后的音乐信息
     */
    public void btConnectStatusCallback(int n, MusicFile musicData) {
        LogUtils.d("size:" + callbacks.size() + " btBState:" + EBtBState.fromValue(n));
        for (int i = callbacks.size() - 1; i >= 0; i--) {
            try {
                callbacks.get(i).btConnectStatusCallback(n, musicData);
            } catch (Exception e) {
                LogUtils.w(e.getMessage());
                if ("null".equals(e.getMessage()))
                    callbacks.remove(i);
            }
        }
    }

    public void btPlayCallBack(MusicFile music) {
        LogUtils.d("size:" + callbacks.size());
        for (int i = callbacks.size() - 1; i >= 0; i--) {
            try {
                callbacks.get(i).onPlaybackStatusChanged(EPlayStatus.STATUS_PLAYING.getValue(), 1, music);
            } catch (Exception e) {
                LogUtils.w(e.getMessage());
                if ("null".equals(e.getMessage()))
                    callbacks.remove(i);
            }
        }
    }

    public void btPauseCallBack(MusicFile musicFile) {
        LogUtils.d("");
        if (PlayManage.isMediaPlay1())
            return;
        for (int i = callbacks.size() - 1; i >= 0; i--) {
            try {
                callbacks.get(i).onPlaybackStatusChanged(EPlayStatus.STATUS_PAUSE.getValue(), 0, musicFile);
            } catch (Exception e) {
                LogUtils.w(e.getMessage());
                if ("null".equals(e.getMessage()))
                    callbacks.remove(i);
            }

        }
    }

    public void btCallStatusChangeCallback(PlayStatusWrapper playStatusWrapper) {
        LogUtils.d("size:" + callbacks.size());
        for (int i = callbacks.size() - 1; i >= 0; i--) {
            try {
                callbacks.get(i).btPhoneStatusChangeCallback(playStatusWrapper);
            } catch (Exception e) {
                LogUtils.w(e.getMessage());
                if ("null".equals(e.getMessage()))
                    callbacks.remove(i);
            }
        }
    }

    public void btLostFocusPauseCallBack(MusicFile musicFile) {
        LogUtils.d("size:" + callbacks.size());
        if (!PlayManage.isMediaPlay2() && PlayManage.sMusicSources == IPlaySource.BT) {
            for (int i = callbacks.size() - 1; i >= 0; i--) {
                try {
                    callbacks.get(i).onPlaybackStatusChanged(EPlayStatus.STATUS_PAUSE.getValue(), 0, musicFile);
                } catch (Exception e) {
                    LogUtils.w(e.getMessage());
                    if ("null".equals(e.getMessage()))
                        callbacks.remove(i);
                }
            }
        }
    }
    /*-------------------------- 公共 回调 ---------------------------------*/

    /**
     * 暂停回调
     * 当蓝牙音乐和 usb 音乐都暂停的时候就回调修改UI
     */
    public void pauseCallBack(MusicFile musicFile) {
//        boolean needPause = false;
//        if (PlayManage.sMusicSources == IPlaySource.USB
//                || PlayManage.sMusicSources == IPlaySource.BT
//                || PlayManage.sMusicSources == IPlaySource.KW
//                || PlayManage.sMusicSources == IPlaySource.VIDEO) {
//            if (!PlayManage.isMediaPlay2()) {
//                needPause = true;
//            }
//        }
//        LogUtils.d("needPause:" + needPause + " size:" + callbacks.size() + " path:" + musicFile.path);
//        if (!PlayManage.isMediaPlay1()) {
//
//        }
        for (int i = callbacks.size() - 1; i >= 0; i--) {
            try {
                callbacks.get(i).onPlaybackStatusChanged(EPlayStatus.STATUS_PAUSE.getValue(), 0, musicFile);
            } catch (Exception e) {
                LogUtils.w(e.getMessage());
                if ("null".equals(e.getMessage()))
                    callbacks.remove(i);
            }
        }

    }

    /**
     * 失去焦点 回调客户端修改UI
     */
    public void onFocusLost() {
        LogUtils.d("size:" + callbacks.size());
        if (!PlayManage.isMediaPlay4()) {
            for (int i = callbacks.size() - 1; i >= 0; i--) {
                try {
                    callbacks.get(i).onFocusLost();
                } catch (Exception e) {
                    LogUtils.w(e.getMessage());
                    if ("null".equals(e.getMessage()))
                        callbacks.remove(i);
                }
            }
        }
    }

    public void onUsbStatusChangeCallBack(int status) {
        LogUtils.d("");
        for (int i = callbacks.size() - 1; i >= 0; i--) {
            try {
                callbacks.get(i).onUsbStatusChangeCallBack(status);
            } catch (Exception e) {
                LogUtils.w(e.getMessage());
                if ("null".equals(e.getMessage()))
                    callbacks.remove(i);
            }
        }
    }

    public void usbScanCallBack() {
        for (int i = callbacks.size() - 1; i >= 0; i--) {
            try {
                callbacks.get(i).usbScanCallBack();
            } catch (Exception e) {
                if ("null".equals(e.getMessage()))
                    callbacks.remove(i);
            }
        }
    }

    public void errorCallBack(int type) {
        //客户端回调
        for (int i = callbacks.size() - 1; i >= 0; i--) {
            try {
                callbacks.get(i).errorCallBack(type);
            } catch (Exception e) {
                LogUtils.w(e.getMessage());
                if ("null".equals(e.getMessage()))
                    callbacks.remove(i);
            }
        }
    }
}
