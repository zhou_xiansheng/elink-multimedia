package com.elink.elinkmultimediaservices.service.handler;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import androidx.annotation.NonNull;

import com.elink.bluetoothapi.BtJar;
import com.elink.bluetoothapi.data.BtType;
import com.elink.bluetoothapi.data.MusicData;
import com.elink.elinkmultimediaservices.constants.common.EAudioSource;
import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.service.MediaServices;
import com.elink.elinkmultimediaservices.service.manager.listeners.BtCommonStateListener;
import com.elink.elinkmultimediaservices.utils.LogUtils;

public class MediaHandler {
    public static final int BT_DELAY_SYNC_STATUS = 1;
    public static final int INIT_BT_STATUS = 2;
    private HandlerThread thread;
    public Handler handler;
    private final MediaServices mMediaServices;

    public MediaHandler(MediaServices mMediaServices) {
        this.mMediaServices = mMediaServices;
        initThreadHandler();
    }

    public void initThreadHandler() {
        if (thread != null && thread.isAlive())
            return;
        thread = new HandlerThread("MusicHandler");
        thread.start();
        handler = new Handler(thread.getLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                int what = msg.what;
                switch (what) {
                    case BT_DELAY_SYNC_STATUS:
                        LogUtils.d("BT_DELAY_SYNC_STATUS");
                        //蓝牙播放状态延时同步
                        //迅速切换到FM，同步在FM播放之后，会出现FM和蓝牙都是播放状态的时候，这是就需要再次判断，播放源是不是蓝牙
                        if (PlayManage.checkAudioSource(EAudioSource.AUDIO_SOURCE_BT)) {
                            MusicData musicData = BtJar.get().getMusicData(BtType.BT_TYPE_GOC);
                            EPlayStatus status = musicData.isPlaying() ? EPlayStatus.STATUS_PLAYING : EPlayStatus.STATUS_PAUSE;
                            if (status != PlayManage.sBtStatus) {
                                LogUtils.d("状态同步:" + status);
                                PlayManage.setBtPlayStatus(status);
                                if (PlayManage.isBtPlay()) {
                                    mMediaServices.btPlayCallBack(mMediaServices.mBtMusicManager.musicNow);
                                } else {
                                    mMediaServices.btPauseCallBack(mMediaServices.mBtMusicManager.musicNow);
                                }
                            }
                        }
                        break;
                    case INIT_BT_STATUS:
                        LogUtils.d("INIT_BT_STATUS");
                        boolean isBtConnect = BtCommonStateListener.checkBtConnect();
                        if (isBtConnect) {
                            MusicData musicData = BtJar.get().getMusicData(BtType.BT_TYPE_GOC);
                            LogUtils.d("btName:" + musicData.getConnectedBtName() + " isBtPlay:" + PlayManage.isBtPlay());
                            if (musicData.isPlaying()) {
                                mMediaServices.setAudioSources(EAudioSource.AUDIO_SOURCE_BT);
                                PlayManage.setBtPlayStatus(EPlayStatus.STATUS_PLAYING);
                                mMediaServices.mBtMusicManager.musicAudioManager.requestAudioFocus();
                            }
                        } else {
                            LogUtils.d("isBtConnected:" + BtCommonStateListener.isBtConnected);
                        }
                        break;
                    default:
                        LogUtils.d("未知 code,不进行处理！what:" + what);
                }
            }
        };
    }

    /**
     * 蓝牙状态延时两秒后进行同步
     */
    public void btStatusSync() {
        //延时一定时间进行状态同步
        Message message = handler.obtainMessage(BT_DELAY_SYNC_STATUS);
        handler.sendMessageDelayed(message, 2000);
    }

    /**
     * 移除回调
     */
    public void reMoveBtStatusSync() {
        handler.removeMessages(BT_DELAY_SYNC_STATUS);
    }
}
