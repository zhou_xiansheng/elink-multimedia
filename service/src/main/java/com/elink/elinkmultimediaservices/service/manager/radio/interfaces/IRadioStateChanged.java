package com.elink.elinkmultimediaservices.service.manager.radio.interfaces;

public interface IRadioStateChanged {
    void onSeekStatusChanged(int status,int station);
    void onScanStatusChanged(int status, int[] stations);
    void onPowerStatusChanged(int status);
    void onTuneStatusChanged(int status);
}
