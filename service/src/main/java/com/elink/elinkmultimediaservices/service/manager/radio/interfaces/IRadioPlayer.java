package com.elink.elinkmultimediaservices.service.manager.radio.interfaces;

import com.elink.elinkmultimediaservices.beans.RadioChannel;
import com.elink.elinkmultimediaservices.service.interfaces.IPlayManager;

import java.util.List;

/**
 * 电台播放器接口
 */
public interface IRadioPlayer extends IPlayManager {
    /**
     * 轮播
     */
//    void rotate();

    /**
     * 搜索固定频道
     */
    void searchStableChannel();

    /**
     * 收藏频道
     * @param radioChannel 频道
     */
    void setChannelCollection(RadioChannel radioChannel);

    /**
     * 获取收藏列表
     */
    List<RadioChannel> getCollectionList();

    /**
     * 获取搜索到的电台列表
     */
    List<RadioChannel> getSearchedList();

    /**
     * 播放指定频道
     *
     * @param channel 频道
     */
    void playSpecialChannel(RadioChannel channel);

    /**
     * 判断是否正在搜索
     * @return 是否正在搜索
     */
    boolean isSearching();

    /**
     * 停止扫描
     */
    void stopScanAndSeek();

    /**
     * 释放资源
     */
    void release();


    /**
     * 获取Radio开关状态
     * @return Radio开关状态
     */
    int getPowerStatus();
}
