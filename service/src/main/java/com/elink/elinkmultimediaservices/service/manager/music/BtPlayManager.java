package com.elink.elinkmultimediaservices.service.manager.music;


import android.media.MediaPlayer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

import com.elink.bluetoothapi.BtJar;
import com.elink.bluetoothapi.data.BtCallState;
import com.elink.bluetoothapi.data.BtType;
import com.elink.bluetoothapi.data.HfpClientCall;
import com.elink.bluetoothapi.data.MusicData;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.common.EAudioSource;
import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;
import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.service.MediaServices;
import com.elink.elinkmultimediaservices.service.audiofocus.BtMusicAudioManager;
import com.elink.elinkmultimediaservices.service.interfaces.IPlayManager;
import com.elink.elinkmultimediaservices.service.manager.listeners.BtCallListener;
import com.elink.elinkmultimediaservices.service.manager.listeners.BtCommonStateListener;
import com.elink.elinkmultimediaservices.service.manager.listeners.BtMusicInfoListener;
import com.elink.elinkmultimediaservices.service.manager.listeners.BtServiceConnectStateListener;
import com.elink.elinkmultimediaservices.utils.LogUtils;

import java.util.List;

public class BtPlayManager implements IPlayManager {
    public MediaPlayer player;//播放器
    public final BtMusicAudioManager musicAudioManager;//音频焦点
    public final MediaServices mediaServices;
    public MusicFile musicNow;//当前兰也音乐信息


    public BtCommonStateListener btCommonStateListener;
    private BtServiceConnectStateListener btServiceConnectStateListener;
    private final BtMusicInfoListener btMusicInfoListener;

    private HandlerThread thread;
    public Handler musicInfoChangeHandler;
    public static final int DO_STATUS_TO_PLAY_CALLBACK = 1;
    public static final int DO_STATUS_TO_PAUSE_CALLBACK = 2;
    public static final int HANDLE_MESSAGE = 3;
    public static final int CHANGE_MUSIC_MESSAGE = 4;

    public BtPlayManager(MediaServices mediaServices) {
        LogUtils.d("");

        this.mediaServices = mediaServices;
        musicNow = new MusicFile();

        btMusicInfoListener = new BtMusicInfoListener(this);
        btServiceConnectStateListener = new BtServiceConnectStateListener();
        btCommonStateListener = new BtCommonStateListener(this, mediaServices);
        //初始化蓝牙设备
        BtJar.get().registerMusicInfoListener(btMusicInfoListener);
        BtJar.get().registerServiceConnectStateListener(btServiceConnectStateListener);
        BtJar.get().registerCommonStateListener(btCommonStateListener);
        musicAudioManager = new BtMusicAudioManager(this);
        initThreadHandler();
    }

    public void initThreadHandler() {
        thread = new HandlerThread("playThread");
        thread.start();
        musicInfoChangeHandler = new Handler(thread.getLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                int what = msg.what;
                int arg2 = msg.arg2;
                switch (what) {
                    case DO_STATUS_TO_PLAY_CALLBACK:
                        if (PlayManage.isBtPlay()) {
                            if (musicAudioManager.requestAudioFocus()) {
                                LogUtils.d("状态改变回调 播放" + arg2);
                                mediaServices.unMute();
                                BtJar.get().muteMusic(BtType.BT_TYPE_GOC, false);
                                mediaServices.setAudioSources(EAudioSource.AUDIO_SOURCE_BT);
                                musicNow = BtMusicInfoListener.getMusicData();//取最新的
                                mediaServices.btPlayCallBack(musicNow);
                            } else {
                                LogUtils.d("焦点请求失败");
                            }
                        } else {
                            LogUtils.d("播放过滤 状态已经改变");
                        }
                        break;
                    case DO_STATUS_TO_PAUSE_CALLBACK:
                        if (!PlayManage.isBtPlay()) {
                            LogUtils.d("状态改变回调 暂停:" + arg2);
                            musicAudioManager.abandonAudioFocus();
                            callBackPause();
                        } else {
                            LogUtils.d("暂停过滤");
                        }
                        break;
                    case HANDLE_MESSAGE:
                        if (PlayManage.sBtStatus == EPlayStatus.STATUS_PLAY_PREPARING) {
                            return;
                        }
                        if (PlayManage.isBtPlay()) {
                            musicInfoChangeHandler.removeCallbacksAndMessages(DO_STATUS_TO_PLAY_CALLBACK);
                            Message message = musicInfoChangeHandler.obtainMessage(DO_STATUS_TO_PLAY_CALLBACK);
                            message.arg2 = arg2;
                            musicInfoChangeHandler.sendMessageDelayed(message, 100);
                        } else if (PlayManage.sBtStatus == EPlayStatus.STATUS_PAUSE) {
                            if (PlayManage.sMusicSources == IPlaySource.BT) {
                                musicInfoChangeHandler.removeCallbacksAndMessages(DO_STATUS_TO_PAUSE_CALLBACK);
                                Message message = musicInfoChangeHandler.obtainMessage(DO_STATUS_TO_PAUSE_CALLBACK);
                                message.arg2 = arg2;
                                musicInfoChangeHandler.sendMessageDelayed(message, 100);
                            }
                        }
                        break;
                    case CHANGE_MUSIC_MESSAGE:
                        if (PlayManage.isBtCallPlay()) {
                            LogUtils.d("蓝牙通话中。。");
                            return;
                        }
                        MusicFile musicFile = BtMusicInfoListener.getMusicData();
                        //不为空，歌名改变了，进行回调
                        if (!"未知曲目".equals(musicFile.fileName) && !musicFile.fileName.equals(musicNow.fileName)) {
                            if (!PlayManage.isMediaPlay6() && musicFile.isPlaying) {
                                //其他播放源没有在播放的，且当前蓝牙正在播放
                                LogUtils.d("蓝牙歌名变化 fileName:" + musicFile.fileName);
                                musicNow = musicFile;
                                // 苹果手机会将歌词当作歌名传递过来，增加当前播放时间作为判断
                                MusicData musicData = btMusicInfoListener.btMusicDataCache.get(arg2);
                                if (musicData != null && (musicData.getSongPos() == 0 || musicData.getSongPos() == 1)) {
                                    LogUtils.d("切换歌曲" + arg2);
                                    if (musicAudioManager.requestAudioFocus()) {
                                        mediaServices.unMute();
                                        BtJar.get().muteMusic(BtType.BT_TYPE_GOC, false);
                                    }
                                } else {
                                    LogUtils.d("缓存已经清除");
                                }
                                mediaServices.btPlayCallBack(musicFile);//回调客户端
                            }
                        }
                        break;
                    default:
                        LogUtils.d("未知 code,不进行处理！");
                }
            }
        };
    }


    public boolean canPause() {
        if (BtCommonStateListener.isBtConnect()) {
            return true;
        }
        return false;
    }


    public boolean canBtPlay() {
        if (!(PlayManage.isBtCallPlay() || BtCallListener.isBtPhoning())
                && !mediaServices.isHibernate
                && BtCommonStateListener.isBtConnect()
                && !PlayManage.isBtPlay()) {
            return true;
        } else if (PlayManage.isBtCallPlay() || BtCallListener.isBtPhoning()) {
            LogUtils.d("通话中禁止播放 ！");
        } else if (!BtCommonStateListener.isBtConnect()) {
            LogUtils.d("蓝牙未连接");
        } else if (mediaServices.isHibernate) {
            LogUtils.d("休眠中禁止播放 ！");
        } else if (PlayManage.isBtPlay()) {
            LogUtils.d("已经在播放了！");
        }
        return false;
    }

    public boolean canBtNextPlay() {
        if (!(PlayManage.isBtCallPlay() || BtCallListener.isBtPhoning())
                && !mediaServices.isHibernate
                && BtCommonStateListener.isBtConnect()) {
            return true;
        } else if (PlayManage.isBtCallPlay() || BtCallListener.isBtPhoning()) {
            LogUtils.d("通话中禁止播放 ！");
        } else if (!BtCommonStateListener.isBtConnect()) {
            LogUtils.d("蓝牙未连接");
        } else if (mediaServices.isHibernate) {
            LogUtils.d("休眠中禁止播放 ！");
        }
        return false;
    }

    @Override
    public void play() {
        if (canBtPlay()) {
            LogUtils.d("播放");
            PlayManage.setBtPlayStatus(EPlayStatus.STATUS_PLAY_PREPARING);
            if (musicAudioManager.requestAudioFocus()) {
                doCompensate();
                BtJar.get().playMusic(BtType.BT_TYPE_GOC);
                BtJar.get().muteMusic(BtType.BT_TYPE_GOC, false);
                BtJar.get().registerMusicInfoListener(btMusicInfoListener);
                mediaServices.setAudioSources(EAudioSource.AUDIO_SOURCE_BT);
                musicNow = BtMusicInfoListener.getMusicData();
                mediaServices.unMute();
                PlayManage.setBtPlayStatus(EPlayStatus.STATUS_PLAYING);
                if (!musicNow.fileName.equals("未知曲目")) {
                    mediaServices.btPlayCallBack(musicNow);//回调客户端
                } else {
                    // 有时候 ，播放然后手机上将音乐后台杀掉，车机上播放，这时候回调播放但是没有信息
                    if (!"".equals(btMusicInfoListener.lastMusicData.getTitle())) {
                        mediaServices.btPlayCallBack(BtMusicInfoListener.getMusicData(btMusicInfoListener.lastMusicData));//回调客户端
                    }
                }
                doDiscard();
                mediaServices.mMediaHandler.btStatusSync();
            } else {
                LogUtils.d("音频焦点获取失败");
                pause();
            }
        }
    }

    /**
     * 暂停
     */
    @Override
    public void pause() {
        if (canPause()) {
            try {
                LogUtils.d("");
                BtJar.get().pauseMusic(BtType.BT_TYPE_GOC);
                BtJar.get().muteMusic(BtType.BT_TYPE_GOC, true);
                PlayManage.setBtPlayStatus(EPlayStatus.STATUS_PAUSE);
                musicInfoChangeHandler.removeCallbacksAndMessages(null);
                musicAudioManager.abandonAudioFocus();
                musicNow = BtMusicInfoListener.getMusicData();
                mediaServices.pauseCallBack(musicNow);
                btMusicInfoListener.firstCallbackAfterPause = true;
                doDiscard();
            } catch (Exception e) {
                LogUtils.e(e.getMessage(), e);
            }
        }
    }

    public void callBackPause() {
        try {
            LogUtils.d("");
            PlayManage.setBtPlayStatus(EPlayStatus.STATUS_PAUSE);
            musicAudioManager.abandonAudioFocus();
            musicNow = BtMusicInfoListener.getMusicData();
            mediaServices.pauseCallBack(musicNow);
            btMusicInfoListener.firstCallbackAfterPause = true;
            doDiscard();
        } catch (Exception e) {
            LogUtils.e(e.getMessage(), e);
        }
    }

    public void btDisconnectPause() {
        LogUtils.d("");
        PlayManage.setBtPlayStatus(EPlayStatus.STATUS_PAUSE);
        musicAudioManager.abandonAudioFocus();
        musicNow = BtMusicInfoListener.getMusicData();
    }

    @Override
    public void prev() {
        if (canBtNextPlay())
            if (musicAudioManager.requestAudioFocus()) {
                LogUtils.d("");
                BtJar.get().prevMusic(BtType.BT_TYPE_GOC);
                BtJar.get().muteMusic(BtType.BT_TYPE_GOC, false);
                mediaServices.unMute();
                BtJar.get().registerMusicInfoListener(btMusicInfoListener);
                doDiscard();
            } else {
                LogUtils.d("音频焦点获取失败");
            }
    }

    @Override
    public void stop() {

    }

    @Override
    public void next() {
        if (canBtNextPlay())
            if (musicAudioManager.requestAudioFocus()) {
                LogUtils.d("");
                BtJar.get().nextMusic(BtType.BT_TYPE_GOC);
                BtJar.get().muteMusic(BtType.BT_TYPE_GOC, false);
                mediaServices.unMute();
                BtJar.get().registerMusicInfoListener(btMusicInfoListener);
                doDiscard();
            } else {
                LogUtils.d("音频焦点获取失败");
            }
    }


    /**
     * 状态改变后丢弃一定时间内的回调
     */
    public void doDiscard() {
        btMusicInfoListener.discard = true; //丢弃相关
        new Handler(Looper.getMainLooper()).postDelayed(() -> btMusicInfoListener.discard = false, 500);
    }

    /*
    做补偿
    后台播放酷我音乐，多媒体点击蓝牙播放
    这时酷我暂停回调的慢，蓝牙回调判定酷我在播放就将蓝牙进行暂停
     */
    public void doCompensate() {
        if (PlayManage.isKwPlay()) {
            PlayManage.setKwPlayStatus(EPlayStatus.STATUS_PAUSE);
        }
    }


    public void pauseRightNow() {
        //如果未播放就播放 立即播放
        LogUtils.d("立即暂停");
        PlayManage.setBtPlayStatus(EPlayStatus.STATUS_PAUSE);
        BtJar.get().pauseMusic(BtType.BT_TYPE_GOC);
        BtJar.get().muteMusic(BtType.BT_TYPE_GOC, true);
        musicAudioManager.abandonAudioFocus();
        mediaServices.btLostFocusPauseCallBack(BtMusicInfoListener.getMusicData());
    }


    public static boolean isBtTerminated(List<HfpClientCall> hfpClientCalls) {
        for (int i = 0; i < hfpClientCalls.size(); i++) {
            HfpClientCall device = hfpClientCalls.get(i);
            if (!(device.getState() == BtCallState.CALL_STATE_TERMINATED)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 设置蓝牙音量
     *
     * @param percent
     */
    public void setPlayVolumeTo(float percent) {
        BtJar btJar = BtJar.get();
        if (btJar != null) {
            btJar.setMusicVolume(BtType.BT_TYPE_GOC, percent);
        }
    }
}
