package com.elink.elinkmultimediaservices.service.receiver;

import static com.elink.elinkmultimediaservices.utils.Constants.ACTION_BOOT_HU;
import static com.elink.elinkmultimediaservices.utils.Constants.ACTION_SHUTDOWN_HU;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.elink.elinkmultimediaservices.constants.common.IPlayMode;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;
import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.service.MediaServices;
import com.elink.elinkmultimediaservices.utils.LogUtils;

public class HibernateBroadcastReceiver extends BroadcastReceiver {
    private int playSources = IPlaySource.UNKNOWN;
    boolean isPlayBeforeHibernate = false;
    private int playMode = IPlayMode.PLAY_MODE_UNKNOWN;
    private MediaServices mediaServices;

    public HibernateBroadcastReceiver(MediaServices mediaServices) {
        this.mediaServices = mediaServices;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (ACTION_SHUTDOWN_HU.equals(action)) {
            LogUtils.d("车机进入休眠");
            mediaServices.isHibernate = true;
            if (PlayManage.isMediaPlay4()) {
                LogUtils.d("休眠前在播放:"+ PlayManage.sMusicSources);
                isPlayBeforeHibernate = true;
                mediaServices.pause();
                playSources = mediaServices.getMusicSources();
                playMode = mediaServices.getPlayMode();
            }
        } else if (ACTION_BOOT_HU.equals(action)) {
            LogUtils.d("车机唤醒");
            mediaServices.isHibernate = false;
            if (isPlayBeforeHibernate
                    && mediaServices.getMusicSources() == playSources
                    && mediaServices.getPlayMode() == playMode) {
                LogUtils.d("继续播放");
                mediaServices.play();
                isPlayBeforeHibernate = false;
            }
        }
    }
}
