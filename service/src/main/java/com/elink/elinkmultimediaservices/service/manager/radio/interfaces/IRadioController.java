package com.elink.elinkmultimediaservices.service.manager.radio.interfaces;

public interface IRadioController {

    /**
     * 异步打开
     *
     * @param frequency 频率
     */
    void powerUpAsync(int frequency);

    /**
     * 异步关闭
     *
     */
    void powerDownAsync();

    /**
     * 获取收音机模块开启状态
     * @return 收音机开关状态
     */
    boolean isRadioEnabled();

    /**
     * 设置收音机静音状态
     * @param isMute 静音状态 true:mute false:loud
     */
    boolean setMuteStatus(boolean isMute);

    /**
     * 向指定方向搜索频道
     *
     * @param freq_start 开始频率
     * @param isUp 搜索方向:true:向频率更大方向 false:向更小方向
     */
    void seekChannelAsync(int freq_start,boolean isUp);

    /**
     * 调谐到指定频率
     * @param freq 频率
     */
    void tuneChannelAsync(int freq);

    /**
     * 开始自动扫描
     */
    void startAutoScanAsync();
    /**
     * 停止自动扫描
     */
    boolean stopAutoScan();


    /**
     * 判断是否处于扫描状态
     * @return 是否处于扫描状态
     */
    boolean isScanning();

    /**
     * 判断是否处于扫描状态
     * @return 是否处于扫描状态
     */
    boolean isSeeking();


    /**
     * 停止扫描
     */
    boolean stopScanAndSeek();
    /**
     * 释放资源
     */
    void release();
}
