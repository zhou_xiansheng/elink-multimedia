package com.elink.elinkmultimediaservices.service.manager.radio.thread;

import static com.elink.elinkmultimediaservices.service.manager.radio.impl.RadioControllerImpl.BUFFER_SIZE_IN_BYTES;

import android.annotation.SuppressLint;
import android.media.AudioRecord;
import android.media.AudioTrack;

import com.elink.elinkmultimediaservices.service.manager.radio.impl.RadioControllerImpl;
import com.elink.elinkmultimediaservices.utils.LogUtils;

public class RenderThread extends Thread {

    private RadioControllerImpl mRadioController;
    // 以避免爆裂噪音。
    private static final int AUDIO_FRAMES_TO_IGNORE_COUNT = 10;
    private int mCurrentFrame = 0;

    public RenderThread(RadioControllerImpl mRadioController) {
        this.mRadioController = mRadioController;
    }

    private boolean isAudioFrameNeedIgnore() {
        return mCurrentFrame < AUDIO_FRAMES_TO_IGNORE_COUNT;
    }

    public boolean isRecordStop() {
        if (mRadioController.mAudioRecord != null &&
                mRadioController.mAudioRecord.getState() == AudioRecord.STATE_INITIALIZED &&
                mRadioController.mAudioRecord.getRecordingState() == AudioRecord.RECORDSTATE_STOPPED) {
            if (mRadioController.mAudioRecord != null) {
                LogUtils.w("state:" + mRadioController.mAudioRecord.getState()
                        + " getRecordingState:" + mRadioController.mAudioRecord.getRecordingState());
            } else {
                LogUtils.d("mAudioRecord:null");
            }
            return true;
        }
        return false;
    }

    public boolean isRecordRunning() {
        if (mRadioController.mAudioRecord != null &&
                mRadioController.mAudioRecord.getState() == AudioRecord.STATE_INITIALIZED &&
                mRadioController.mAudioRecord.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
            return true;
        }
        if (mRadioController.mAudioRecord != null) {
            LogUtils.w("state:" + mRadioController.mAudioRecord.getState()
                    + " recordingState:" + mRadioController.mAudioRecord.getRecordingState());
        } else {
            LogUtils.d("mAudioRecord:null");
        }
        return false;
    }

    public boolean isTrackStopped() {
        if (mRadioController.mAudioTrack != null
                && mRadioController.mAudioTrack.getState() == AudioTrack.STATE_INITIALIZED
                && mRadioController.mAudioTrack.getPlayState() == AudioTrack.PLAYSTATE_STOPPED) {
            if (mRadioController.mAudioTrack != null) {
                LogUtils.w("state:" + mRadioController.mAudioTrack.getState()
                        + " playState:" + mRadioController.mAudioTrack.getPlayState());
            } else {
                LogUtils.d("mAudioTrack:null");
            }
            return true;
        }

        return false;
    }

    public boolean isAllReady() {
        if (mRadioController.mAudioTrack != null
                && mRadioController.mAudioTrack.getState() == AudioTrack.STATE_INITIALIZED
                && mRadioController.mAudioTrack.getPlayState() == AudioTrack.PLAYSTATE_PLAYING
                && mRadioController.mAudioRecord != null
                && mRadioController.mAudioRecord.getState() == AudioRecord.STATE_INITIALIZED
                && mRadioController.mAudioRecord.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
            return true;
        }
        if (mRadioController.mAudioRecord != null) {
            LogUtils.d("state:" + mRadioController.mAudioRecord.getState()
                    + " getRecordingState:" + mRadioController.mAudioRecord.getRecordingState());
        } else {
            LogUtils.d("mAudioRecord:null");
        }
        if (mRadioController.mAudioTrack != null) {
            LogUtils.d("state:" + mRadioController.mAudioTrack.getState()
                    + " playState:" + mRadioController.mAudioTrack.getPlayState());
        } else {
            LogUtils.d("mAudioTrack:null");
        }
        return false;
    }

    /**
     * 数据读写
     * mAudioRecord----------->mAudioTrack
     */
    @SuppressLint("Range")
    public void doAfterAllReady() {
        byte[] buffer = new byte[BUFFER_SIZE_IN_BYTES];
        int size = 0;
        synchronized (mRadioController.mAudioRecord) {
            if (mRadioController.mAudioRecord != null) { //读取广播内容
                size = mRadioController.mAudioRecord.read(buffer, 0, BUFFER_SIZE_IN_BYTES);
            }
        }
        //检查是否需要忽略AudioRecord中的前3帧音频数据，以避免爆裂噪音。
        if (isAudioFrameNeedIgnore()) {
            mCurrentFrame += 1;
            notifyRenderingLock();
        }
        if (size <= 0) notifyRenderingLock();
        byte[] tmpBuf = new byte[size];
        System.arraycopy(buffer, 0, tmpBuf, 0, size);
        if (mRadioController.isRender()) {        //再次检查以避免出现噪音，因为mIsRender可能已更改，AudioRecord正在读取。
            mRadioController.mAudioTrack.write(tmpBuf, 0, tmpBuf.length);
        }
        notifyRenderingLock();
    }

    public void notifyRenderingLock() {
        synchronized (mRadioController.mRenderingLock) {
            mRadioController.mRenderingLock.notify();
        }
    }

    @SuppressLint("Range")
    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                if (mRadioController.isRender()) {
                    if (mRadioController.mAudioRecord == null
                            || mRadioController.mAudioTrack == null) {
                        mRadioController.initAudioRecordSink();
                    }
                    if (isRecordStop()) {
                        mRadioController.mAudioRecord.startRecording();
                    }
                    if (isRecordRunning() && isTrackStopped()) {
                        try {
                            mRadioController.mAudioTrack.play();
                        } catch (Exception e) {
                            LogUtils.w(e.getMessage());
                            mRadioController.initAudioRecordSink();
                        }
                    }
                    if (isAllReady()) doAfterAllReady();
                } else {
                    // 耳机模式将在这里等待。
                    mCurrentFrame = 0;
                    try {
                        if (mRadioController.mAudioTrack != null &&
                                mRadioController.mAudioTrack.getPlayState() == AudioTrack.PLAYSTATE_PLAYING) {
                            mRadioController.mAudioTrack.stop();
                        }
                        if (mRadioController.mAudioRecord != null &&
                                mRadioController.mAudioRecord.getRecordingState() ==
                                        AudioRecord.RECORDSTATE_RECORDING) {
                            mRadioController.mAudioRecord.stop();
                        }
                    } catch (IllegalStateException e) {
                        LogUtils.d("IllegalStateException");
                    } finally {
                        synchronized (mRadioController.mRenderLock) {
                            LogUtils.d("waiting for mRenderLock");
                            mRadioController.mRenderLock.wait();
                        }
                    }
                }
            }
        } catch (InterruptedException e) {
            LogUtils.d("RenderThread.run, thread is interrupted, need exit thread");
        } finally {
            if (mRadioController.mAudioRecord != null &&
                    (mRadioController.mAudioRecord.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING)) {
                mRadioController.mAudioRecord.stop();
            }

            if (mRadioController.mAudioTrack != null &&
                    (mRadioController.mAudioTrack.getPlayState() == AudioTrack.PLAYSTATE_PLAYING)) {
                mRadioController.mAudioTrack.stop();
            }
        }
    }
}
