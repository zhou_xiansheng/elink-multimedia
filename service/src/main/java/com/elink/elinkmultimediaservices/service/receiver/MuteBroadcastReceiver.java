package com.elink.elinkmultimediaservices.service.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.elink.elinkmultimediaservices.constants.common.IPlayMode;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;
import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.service.MediaServices;
import com.elink.elinkmultimediaservices.utils.LogUtils;
import com.elink.xui.adaptapi.audio.AudioStreamType;

/**
 * 静音相关的广播，其他地方点击静音，多媒体需要作出响应的响应
 * 设置点击静音媒体进行暂停播放
 * 设置点击取消静音，媒体恢复播放
 * 媒体点击播放取消静音
 * 注意：可能的操作，媒体播放->设置点击静音->媒体切换播放源 这时会触发媒体恢复播放，需要判断一下当前播放源是否和静音前的相同
 */
public class MuteBroadcastReceiver extends BroadcastReceiver {
    private boolean isMuteFirst = true;
    private boolean isUnMuteFirst = true;
    private int playMode = IPlayMode.PLAY_MODE_UNKNOWN;
    private int playSources = IPlaySource.UNKNOWN;
    boolean isPlayBeforeMute = false;//静音前是否在播放
    private final MediaServices mediaServices;

    public MuteBroadcastReceiver(MediaServices mediaServices) {
        this.mediaServices = mediaServices;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (mediaServices.mAudio.isMute(AudioStreamType.STREAM_MUSIC)) {
            if (isMuteFirst) {
                LogUtils.d("静音");
                mediaServices.isMute = true;
                if (PlayManage.isMediaPlay4()) {
                    isPlayBeforeMute = true;
                    mediaServices.pause();
                    playMode = mediaServices.getPlayMode();
                    playSources = mediaServices.getMusicSources();
                    LogUtils.d("静音前在播放:" + playSources + "/" + playMode);
                }
                isMuteFirst = false;
                new Handler().postDelayed(() -> isMuteFirst = true, 2000);
            }
        } else if (!mediaServices.mAudio.isMute(AudioStreamType.STREAM_MUSIC)) {
            if (isUnMuteFirst) {
                LogUtils.d("解除静音");
                mediaServices.isMute = false;
                if (isPlayBeforeMute
                        && mediaServices.getMusicSources() == playSources
                        && mediaServices.getPlayMode() == playMode) {
                    LogUtils.d("继续播放");
                    mediaServices.play();
                }
                isPlayBeforeMute = false;
                isUnMuteFirst = false;
                new Handler().postDelayed(() -> isUnMuteFirst = true, 2000);
            }
        }
    }
}
