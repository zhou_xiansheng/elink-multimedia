package com.elink.elinkmultimediaservices.service.interfaces;

public interface IPlayManager {

    //播放
    void play();

    //暂停
    void pause();

    //下一个
    void next();

    //上一个
    void prev();

    //停止播放器
    void stop();
}
