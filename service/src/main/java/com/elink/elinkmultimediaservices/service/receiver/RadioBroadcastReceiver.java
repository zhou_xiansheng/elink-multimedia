package com.elink.elinkmultimediaservices.service.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.elink.elinkmultimediaservices.service.manager.radio.impl.RadioControllerImpl;

public class RadioBroadcastReceiver extends BroadcastReceiver {
    private RadioControllerImpl radioPlaybackController;

    public RadioBroadcastReceiver(RadioControllerImpl radioPlaybackController) {
        this.radioPlaybackController = radioPlaybackController;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_SHUTDOWN.equals(intent.getAction())) {
            /**
             * 这里 exitFm，系统将发送广播，系统将关闭，所以fm不需要回叫活动
             */
            radioPlaybackController.release();
        }
    }
}
