package com.elink.elinkmultimediaservices.service;

import static com.elink.elinkmultimediaservices.utils.Constants.ACTION_BOOT_HU;
import static com.elink.elinkmultimediaservices.utils.Constants.ACTION_SHUTDOWN_HU;
import static com.elink.elinkmultimediaservices.utils.Constants.ECGALLERY_VIDEO_STATE;
import static com.elink.elinkmultimediaservices.utils.Constants.MEDIA_MODE_NEXT;
import static com.elink.elinkmultimediaservices.utils.Constants.STREAM_MUTE_CHANGED_ACTION;
import static com.elink.elinkmultimediaservices.utils.Constants.UDISK_PROVIDER;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;

import com.elink.bluetoothapi.BluetoothConfig;
import com.elink.bluetoothapi.BtJar;
import com.elink.bluetoothapi.listener.IBluetoothCallListener;
import com.elink.elinkmultimediaservices.PermissionRequestActivity;
import com.elink.elinkmultimediaservices.R;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.beans.RadioChannel;
import com.elink.elinkmultimediaservices.constants.common.EAudioSource;
import com.elink.elinkmultimediaservices.constants.common.ICustomerPage;
import com.elink.elinkmultimediaservices.constants.common.IPlayMode;
import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;
import com.elink.elinkmultimediaservices.constants.radio.ERadioStatus;
import com.elink.elinkmultimediaservices.constants.wrapper.PlayStatusWrapper;
import com.elink.elinkmultimediaservices.service.receiver.AudioFocusChangeReceiver;
import com.elink.elinkmultimediaservices.service.receiver.ModeSwitchBroadcastReceiver;
import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.service.receiver.HibernateBroadcastReceiver;
import com.elink.elinkmultimediaservices.service.receiver.MuteBroadcastReceiver;
import com.elink.elinkmultimediaservices.service.receiver.UsbProviderReceiver;
import com.elink.elinkmultimediaservices.service.receiver.VideoBroadcastReceiver;
import com.elink.elinkmultimediaservices.service.binders.MediaRemoteBinder;
import com.elink.elinkmultimediaservices.service.callback.FMCallback;
import com.elink.elinkmultimediaservices.service.callback.MusicCallback;
import com.elink.elinkmultimediaservices.service.interfaces.IPlayManager;
import com.elink.elinkmultimediaservices.service.handler.MediaHandler;
import com.elink.elinkmultimediaservices.service.manager.listeners.BtCallListener;
import com.elink.elinkmultimediaservices.service.manager.listeners.BtCommonStateListener;
import com.elink.elinkmultimediaservices.service.manager.listeners.BtMusicInfoListener;
import com.elink.elinkmultimediaservices.service.manager.listeners.IAudioTrackListener;
import com.elink.elinkmultimediaservices.service.manager.music.BtPlayManager;
import com.elink.elinkmultimediaservices.service.manager.music.KWPlayManager;
import com.elink.elinkmultimediaservices.service.manager.music.UsbPlayManager;
import com.elink.elinkmultimediaservices.service.manager.radio.RadioIPlayManager;
import com.elink.elinkmultimediaservices.utils.LogUtils;
import com.elink.elinkmultimediaservices.utils.MusicUtils;
import com.elink.elinkmultimediaservices.utils.MyApplication;
import com.elink.elinkmultimediaservices.utils.SPUtils;
import com.elink.elinkmultimediaservices.utils.SystemPropertiesUtils;
import com.elink.xui.adaptapi.audio.Audio;
import com.elink.xui.adaptapi.audio.AudioStreamType;

import java.util.ArrayList;
import java.util.List;

public class MediaServices extends Service {
    // 权限id
    //前台服务 ID
    private static final int NOTIFICATION_ID = 1;
    //服务绑定 和 回调
    private final MediaRemoteBinder mBinder;
    //回调
    public MusicCallback mMusicCallback;
    public FMCallback mFmCallback;
    //播放器
    public UsbPlayManager mUsbMusicManager;//usb
    public BtPlayManager mBtMusicManager;//蓝牙
    public KWPlayManager mKwMusicManager;//酷我音乐
    public RadioIPlayManager mRadioManager;//FM
    private IPlayManager mPlayManager;//管理接口
    //广播接收
    private MuteBroadcastReceiver muteBroadcastReceiver;
    private VideoBroadcastReceiver videoBroadcastReceiver;
    private HibernateBroadcastReceiver hibernateBroadcastReceiver;
    private BroadcastReceiver usbProviderReceiver;
    private ModeSwitchBroadcastReceiver mModeSwitchBroadcastReceiver;
    private AudioFocusChangeReceiver mAudioFocusChangeReceiver;
    //蓝牙电话监听
    private IBluetoothCallListener bluetoothCallListener;
    //系统音频源切换
    public Audio mAudio;
    private IAudioTrackListener iAudioTrackListener;
    public boolean isMute = false;//静音
    public boolean isHibernate = false; //休眠

    public MediaHandler mMediaHandler;

    public MediaServices() {
        mBinder = new MediaRemoteBinder(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LogUtils.d("media 服务启动");
//        SystemPropertiesUtils.checkUsbState();
        MusicUtils.musicScanner();//触发启动 盘扫描
        SPUtils.PlayManageGet();
        registerReceiver();
        mMediaHandler = new MediaHandler(this);
        mMusicCallback = new MusicCallback(this);
        mFmCallback = new FMCallback();
        initAudio();
        initBt();
        initKwMusicManager();
        initFMManager();
        initUsbMusicManager();
        initBtMusicManager();
        updatePlayManager();
        startMusicForeground();
        resumeCustomer();
    }

    private void initAudio() {
        mAudio = Audio.create(this);
        //静音 导航压低蓝牙声音
        iAudioTrackListener = new IAudioTrackListener(mBtMusicManager);
        mAudio.registerAudioTrackChangeListener(iAudioTrackListener);
    }


    @Override
    public IBinder onBind(Intent intent) {
        String callerPackage = intent.getStringExtra("caller_package");
        LogUtils.d("Service onBind by: " + callerPackage);
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String callerPackage = intent.getStringExtra("caller_package");
        LogUtils.d("Service started by: " + callerPackage);
        if (callerPackage != null && callerPackage.equals("com.elink.ecmultimedia"))
            PlayManage.setClientStatus(true);
//        requestPermission();
        return START_STICKY;
    }

    public void requestPermission() {
        LogUtils.d("");
        Intent permissionIntent = new Intent(this, PermissionRequestActivity.class);
        permissionIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(permissionIntent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        String callerPackage = intent.getStringExtra("caller_package");
        LogUtils.d("Service onUnbind by: " + callerPackage);
        if (callerPackage != null && callerPackage.equals("com.elink.ecmultimedia"))
            PlayManage.setClientStatus(false);
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        LogUtils.d("");
        mKwMusicManager.onRelease();
        mUsbMusicManager.destroyMediaPlay();
        unregisterReceiver();
        stopMusicForeground();
        super.onDestroy();
    }


    private void initBt() {
        bluetoothCallListener = new BtCallListener(this);
        BluetoothConfig config = new BluetoothConfig.Builder()
                .configGocBt(true)
                .build();
        BtJar.get().init((Application) MyApplication.getContext(), config);
        BtJar.get().registerCallListener(bluetoothCallListener);
    }

    private void startMusicForeground() {
        Notification notification = createNotification();
        startForeground(NOTIFICATION_ID, notification);
    }

    private void stopMusicForeground() {
        stopForeground(true); // 移除通知
    }

    private void registerReceiver() {
        LogUtils.d("");
        registerMuteReceiver();
        registerUsbReceiver();
        registerVideoReceiver();
        registerHibernateReceiver();
        registerModeSwitchBroadcastReceiver();
        registerAudioFocusChangeReceiver();
    }

    private void unregisterReceiver() {
        LogUtils.d("");
        unregisterMuteReceiver();
        unregisterUsbReceiver();
        unregisterVideoReceiver();
        unregisterHibernateReceiver();
        unregisterAudioTrackChangeListener();
        unregisterModeSwitchBroadcastReceiver();
        unregisterAudioFocusChangeReceiver();
    }

    private void registerMuteReceiver() {
        muteBroadcastReceiver = new MuteBroadcastReceiver(this);
        IntentFilter muteFilter = new IntentFilter();
        muteFilter.addAction(STREAM_MUTE_CHANGED_ACTION);
        registerReceiver(muteBroadcastReceiver, muteFilter);
    }

    private void registerHibernateReceiver() {
        hibernateBroadcastReceiver = new HibernateBroadcastReceiver(this);
        IntentFilter hibernateFilter = new IntentFilter();
        hibernateFilter.addAction(ACTION_SHUTDOWN_HU);
        hibernateFilter.addAction(ACTION_BOOT_HU);
        registerReceiver(hibernateBroadcastReceiver, hibernateFilter);
    }

    private void registerUsbReceiver() {
        usbProviderReceiver = new UsbProviderReceiver(this);
        IntentFilter usbFilter = new IntentFilter();
        usbFilter.addAction(UDISK_PROVIDER);
        registerReceiver(usbProviderReceiver, usbFilter);
    }

    private void registerVideoReceiver() {
        videoBroadcastReceiver = new VideoBroadcastReceiver();
        IntentFilter videoFilter = new IntentFilter();
        videoFilter.addAction(ECGALLERY_VIDEO_STATE);
        registerReceiver(videoBroadcastReceiver, videoFilter);
    }

    private void registerModeSwitchBroadcastReceiver() {
        mModeSwitchBroadcastReceiver = new ModeSwitchBroadcastReceiver(this);
        IntentFilter mediaModeNext = new IntentFilter();
        mediaModeNext.addAction(MEDIA_MODE_NEXT);
        registerReceiver(mModeSwitchBroadcastReceiver, mediaModeNext);
    }

    private void registerAudioFocusChangeReceiver() {
        mAudioFocusChangeReceiver = new AudioFocusChangeReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(AudioFocusChangeReceiver.ACTION_AUDIO_FOCUS_CHANGE);
        registerReceiver(mAudioFocusChangeReceiver, filter);
    }

    private void unregisterMuteReceiver() {
        unregisterReceiver(muteBroadcastReceiver);
    }

    private void unregisterUsbReceiver() {
        unregisterReceiver(usbProviderReceiver);
    }

    private void unregisterVideoReceiver() {
        unregisterReceiver(videoBroadcastReceiver);
    }

    private void unregisterHibernateReceiver() {
        unregisterReceiver(hibernateBroadcastReceiver);
    }

    private void unregisterAudioTrackChangeListener() {
        mAudio.unregisterAudioTrackChangeListener(iAudioTrackListener);
    }

    private void unregisterModeSwitchBroadcastReceiver() {
        unregisterReceiver(mModeSwitchBroadcastReceiver);
    }

    private void unregisterAudioFocusChangeReceiver() {
        unregisterReceiver(mAudioFocusChangeReceiver);
    }


    public void initKwMusicManager() {
        if (mKwMusicManager == null) {
            mKwMusicManager = new KWPlayManager(this);
        }
    }

    public void initFMManager() {
        if (mRadioManager == null) {
            mRadioManager = new RadioIPlayManager(this);
        }
    }

    public void initUsbMusicManager() {
        if (mUsbMusicManager == null) {
            mUsbMusicManager = new UsbPlayManager(this);
        }
    }

    public void initBtMusicManager() {
        if (mBtMusicManager == null) {
            mBtMusicManager = new BtPlayManager(this);
            //启动判断一下蓝牙的连接状态,需要延时一下，不然一直是未连接状态
            mMediaHandler.handler.sendMessageDelayed(mMediaHandler.handler.obtainMessage(MediaHandler.INIT_BT_STATUS), 500);
        }
    }

    /**
     * 更新播放器
     */
    public void updatePlayManager() {
        if (PlayManage.checkAudioSource(EAudioSource.AUDIO_SOURCE_USB)) {
            mPlayManager = mUsbMusicManager;
        } else if (PlayManage.checkAudioSource(EAudioSource.AUDIO_SOURCE_BT)) {
            mPlayManager = mBtMusicManager;
        } else if (PlayManage.checkAudioSource(EAudioSource.AUDIO_SOURCE_FM)) {
            mPlayManager = mRadioManager;
        } else if (PlayManage.checkAudioSource(EAudioSource.AUDIO_SOURCE_KUWO)) {
            mPlayManager = mKwMusicManager;
        } else {
            mPlayManager = null;
        }
        LogUtils.d("playManager:" + mPlayManager);
    }


    /**
     * 设置前台服务提高服务的优先级
     *
     * @return
     */
    private Notification createNotification() {
        NotificationChannel channel;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channel = new NotificationChannel("channelId", "Channel Name", NotificationManager.IMPORTANCE_LOW);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "channelId")
                .setContentTitle("media Service")
                .setContentText("media is running in foreground")
                .setSmallIcon(R.drawable.ic_launcher_foreground) // 设置您的图标
                .setPriority(NotificationCompat.PRIORITY_LOW);
        return builder.build();
    }

    /**
     * 启动客户端恢复到开机前的页面
     */
    private void resumeCustomer() {
        if (!SystemPropertiesUtils.needCustomerResume())
            return;
        Bundle extras = new Bundle();
        String page = null;
        if (PlayManage.checkAudioSource(EAudioSource.AUDIO_SOURCE_USB)) {
            page = ICustomerPage.PAGE_USB;
        } else if (PlayManage.checkAudioSource(EAudioSource.AUDIO_SOURCE_BT)) {
            page = ICustomerPage.PAGE_BT;
        } else if (PlayManage.checkAudioSource(EAudioSource.AUDIO_SOURCE_FM)) {
            page = ICustomerPage.PAGE_FM;
        }
        if (page != null) {
            extras.putString("page", page);
            if (PlayManage.needMediaResumePlay())
                extras.putString("isPlay", EPlayStatus.STATUS_PLAYING.toString());
            else
                extras.putString("isPlay", EPlayStatus.STATUS_PAUSE.toString());
            Intent intent = new Intent();
            intent.setComponent(new ComponentName("com.elink.ecmultimedia",
                    "com.elink.ecmultimedia.ui.main.view.MultimediaMainActivity"));
            intent.putExtras(extras);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(intent);
            LogUtils.d("客户端恢复");
        }
    }
    /*-------------------------- 回调 相关 ---------------------------------*/
    /*----------------------------- FM 回调-------------------------------*/

    public void radioFMFocusLostCallBack() {
        mFmCallback.radioFocusLostCallBack();
    }

    public void onFMPlaybackStatusChangedCallBack(int status, RadioChannel channel) {
        mFmCallback.onPlaybackStatusChangedCallBack(status, channel);
    }

    public void searchCallBack(int channelNumber) {
        mFmCallback.searchCallBack(channelNumber);
    }

    public void onSearchedListChanged(int status, ArrayList<RadioChannel> radioChannels) {
        mFmCallback.onSearchedListChanged(status, radioChannels);
    }
    public void onSeekStatusChanged(ERadioStatus radioStatus) {
        mFmCallback.onSeekStatusChanged(radioStatus);
    }

    public void errorCallBack(int type) {
        mMusicCallback.errorCallBack(type);
    }


    /*-------------------------- kw 回调 ---------------------------------*/

    public void kwPlayCallBack(MusicFile music) {
        mMusicCallback.kwPlayCallBack(music);
    }

    /*-------------------------- usb 回调 ---------------------------------*/
    public void onUsbStatusChangeCallBack(int status) {
        mMusicCallback.onUsbStatusChangeCallBack(status);
    }

    public void usbScanCallback() {
        mMusicCallback.usbScanCallBack();
    }

    public void usbEjectCallback() {
        mMusicCallback.usbEjectCallback();
    }


    public void playCallBack(int isNew, MusicFile music) {
        mMusicCallback.playCallBack(isNew, music);
    }

    /*-------------------------- bt 回调 ---------------------------------*/
    public void btConnectStatusCallback(int n, MusicFile musicData) {
        mMusicCallback.btConnectStatusCallback(n, musicData);
    }

    public void btPlayCallBack(MusicFile music) {
        mMusicCallback.btPlayCallBack(music);
    }


    public void btCallStatusChangeCallback(PlayStatusWrapper playStatusWrapper) {
        mMusicCallback.btCallStatusChangeCallback(playStatusWrapper);
    }

    public void btPauseCallBack(MusicFile musicFile) {
        mMusicCallback.btPauseCallBack(musicFile);
    }

    public void btLostFocusPauseCallBack(MusicFile musicFile) {
        mMusicCallback.btLostFocusPauseCallBack(musicFile);
    }
    /*-------------------------- 公共 回调 ---------------------------------*/

    /**
     * 暂停回调
     * 当蓝牙音乐和 usb 音乐都暂停的时候就回调修改UI
     */
    public void pauseCallBack(MusicFile musicFile) {
        mMusicCallback.pauseCallBack(musicFile);
    }


    public void play() {
        mPlayManager.play();
    }

    /**
     * 当先播放fm,然后播放音乐必须走这里，不然播放音乐但是会调用fm的播放器
     */
    public void customerPlay() {
        if (PlayManage.checkUsbUseful()) {
            setAudioSources(EAudioSource.AUDIO_SOURCE_USB);
            play();
        } else if (BtCommonStateListener.isBtConnect()) {
            setAudioSources(EAudioSource.AUDIO_SOURCE_BT);
            play();
        } else {
            LogUtils.d("无可用播放源！！");
        }
    }

    public void playByMusic() {
        mUsbMusicManager.playByMusic();
    }

    public void pause() {
        mPlayManager.pause();
    }

    public void next() {
        mPlayManager.next();
    }

    public void pre() {
        mPlayManager.prev();
    }

    public void stop() {
        mRadioManager.stop();
    }

    /**
     * 蓝牙、usb、kw
     *
     * @return
     */
    public MusicFile getMediaMusicPlayNow() {
        MusicFile res;
        if (PlayManage.checkAudioSource(EAudioSource.AUDIO_SOURCE_BT)) {
            res = mBtMusicManager.musicNow;
            LogUtils.d("fileName:" + res.fileName);
        } else if (PlayManage.checkAudioSource(EAudioSource.AUDIO_SOURCE_USB)) {
            res = getUsbFile();
            LogUtils.d("fileName:" + res.fileName);
        } else if (PlayManage.checkAudioSource(EAudioSource.AUDIO_SOURCE_KUWO)) {
            res = mKwMusicManager.musicNow;
            LogUtils.d("fileName:" + res.fileName);
        } else {
            res = new MusicFile();
            res.playSource = IPlaySource.UNKNOWN;
            LogUtils.d("未知播放源:" + PlayManage.sAudioSource);
        }
        return res;
    }

    /**
     * 蓝牙、usb
     *
     * @return
     */
    public MusicFile getCustomerMusicPlayNow() {
        MusicFile res;
        if (PlayManage.sMusicSources == IPlaySource.BT) {
            res = mBtMusicManager.musicNow;
            LogUtils.d("fileName:" + res.fileName);
        } else if (PlayManage.sMusicSources == IPlaySource.USB) {
            res = getUsbFile();
            LogUtils.d("fileName:" + res.fileName);
        } else {
            res = new MusicFile();
            LogUtils.d("未知播放源:" + PlayManage.sAudioSource);
        }
        return res;
    }

    public MusicFile getUsbFile() {
        MusicFile res;
        if (PlayManage.checkUsbUseful()) {
            res = mUsbMusicManager.getMusicPlayNow();
        } else {
            res = new MusicFile();
            res.playSource = IPlaySource.UNKNOWN;
        }
        return res;
    }

    /**
     * 获得多媒体当前总体播放状态
     *
     * @return
     */
    public int getMusicStatus() {
        return PlayManage.getMusicStatus();
    }

    public int getMediaStatus() {
        return PlayManage.getMediaStatus();
    }


    /*--------------------------usb 相关---------------------------------*/
    public void setMusicPlayNow(MusicFile musicfile) {
        setAudioSources(EAudioSource.AUDIO_SOURCE_USB);
        mUsbMusicManager.setMusicPlayNow(musicfile);
    }

    public void play(MusicFile musicFile) {
        mUsbMusicManager.play(musicFile);
    }

    public List<MusicFile> getMediaFileList() {
        return mUsbMusicManager.getMusics();
    }

    public List<MusicFile> getMusicCollect() {
        List<MusicFile> res;
        if (PlayManage.checkUsbUseful()) {
            res = mUsbMusicManager.getMusicCollect();
        } else {
            res = new ArrayList<>();
        }
        return res;
    }

    public int getCurrentPosition() {
        return mUsbMusicManager.getCurrentPosition();
    }

    public void mediaPlayerSeekTo(int progress) {
        mUsbMusicManager.mediaPlayerSeekTo(progress);
    }


    public void setMusicCollection(List<MusicFile> musicFiles) {
        mUsbMusicManager.setMusicCollection(musicFiles);
    }

    public void addToMusicCollect(MusicFile musicFile) {
        mUsbMusicManager.addToMusicCollect(musicFile);
    }

    public int getAudioSources() {
        return PlayManage.getAudioSource().getValue();
    }

    public int getMusicPlayMode() {
        return mUsbMusicManager.getPlayMode();
    }

    public void setMusicPlayMode(int newMode) {
        mUsbMusicManager.setPlayMode(newMode);
    }

    public void setPlayIndex(int index) {
        mUsbMusicManager.setPlayIndex(index);
    }


    /*--------------------------蓝牙播放相关---------------------------------*/
    public boolean checkBtConnect() {
        return BtCommonStateListener.checkBtConnect();
    }

    public String getBtName() {
        return BtCommonStateListener.devicesName;
    }

    public MusicFile getMusicData() {
        return BtMusicInfoListener.getMusicData();
    }
    /*--------------------------FM相关---------------------------------*/

    /**
     * 收音机播放指定频率
     *
     * @param channel
     */
    public void playSpecialChannel(RadioChannel channel) {
        setAudioSources(EAudioSource.AUDIO_SOURCE_FM);
        mRadioManager.playSpecialChannel(channel);
    }

    public int getPowerStatus() {
        return mRadioManager.getPowerStatus();
    }

    public List<RadioChannel> getSearchedStableChannelList() {
        mRadioManager.searchStableChannel();
        return null;
    }

    public void stopScanAndSeek() {
        mRadioManager.stopScan();
    }

    /*--------------------------播放源相关---------------------------------*/


    public int getMusicSources() {
        return PlayManage.getMusicSources();
    }

    public int getPlayMode() {
        return PlayManage.getPlayMode();
    }

    public void setAudioSources(EAudioSource source) {
        if (!PlayManage.checkAudioSource(source)) {
            LogUtils.d(PlayManage.getAudioSource() + "--------->" + source);
            PlayManage.setAudioSource(source);
            if (source.check(EAudioSource.AUDIO_SOURCE_USB)) {
                PlayManage.setPlayMode(IPlayMode.PLAY_MODE_MUSIC);
                PlayManage.setMusicSources(IPlaySource.USB);
                mMusicCallback.musicSourceChangeCallback(IPlaySource.USB);
            } else if (source.check(EAudioSource.AUDIO_SOURCE_BT)) {
                PlayManage.setPlayMode(IPlayMode.PLAY_MODE_MUSIC);
                PlayManage.setMusicSources(IPlaySource.BT);
                mMusicCallback.musicSourceChangeCallback(IPlaySource.USB);
            } else if (source.check(EAudioSource.AUDIO_SOURCE_FM)) {
                PlayManage.setPlayMode(IPlayMode.PLAY_MODE_RADIO);
            } else if (source.check(EAudioSource.AUDIO_SOURCE_KUWO)) {
                PlayManage.setPlayMode(IPlayMode.PLAY_MODE_KW);
            } else {
                LogUtils.w("无效播放源");
            }
            updatePlayManager();
        }
    }



    /*---------------------------静音相关-------------------------------------*/

    /**
     * 取消静音
     */
    public void unMute() {
        if (isMute) {
            LogUtils.d("取消静音 ！");
            // 取消静音
            isMute = false;
            mAudio.adjustToggleMute(AudioStreamType.STREAM_MUSIC, 0);
        }
    }
}