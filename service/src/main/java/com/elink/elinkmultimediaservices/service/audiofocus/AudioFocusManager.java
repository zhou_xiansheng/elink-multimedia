package com.elink.elinkmultimediaservices.service.audiofocus;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Handler;

import com.elink.elinkmultimediaservices.utils.LogUtils;
import com.elink.elinkmultimediaservices.utils.MyApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * 音频焦点管理
 */
public class AudioFocusManager implements AudioManager.OnAudioFocusChangeListener {
    private final AudioManager mAudioManager;
    private final AudioFocusRequest mAudioFocusRequest;
    private List<OnFocusChangedListener> mListenerList;
    private boolean mIsFocusGained = false;

    private static final class SInstanceHolder {
        static final AudioFocusManager sInstance = new AudioFocusManager();
    }

    public static AudioFocusManager getInstance() {
        return SInstanceHolder.sInstance;
    }

    private AudioFocusManager() {
        mListenerList = new ArrayList<>();
        mAudioManager = (AudioManager) MyApplication.getContext().getSystemService(Context.AUDIO_SERVICE);
        AudioAttributes audioAttr = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build();
        mAudioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                .setAudioAttributes(audioAttr)
                .setAcceptsDelayedFocusGain(true)
                .setOnAudioFocusChangeListener(this, new Handler())
                .build();
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        LogUtils.d("focusChange:" + focusChange);
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
            case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT:
            case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK:
            case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE:
                mIsFocusGained = true;
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                mIsFocusGained = false;
                break;
        }
        notifyFocusChanged(focusChange);
    }

    public boolean isFocusGained() {
        return mIsFocusGained;
    }


    /**
     * 请求音频焦点
     *
     * @return
     */
    public boolean requestAudioFocus() {
        if (mIsFocusGained) {
            return true;
        }
        if (mAudioManager.requestAudioFocus(mAudioFocusRequest) == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            LogUtils.d("success");
            mIsFocusGained = true;
            return true;
        } else {
            LogUtils.d("failed!");
            return false;
        }
    }

    /**
     * 释放音频焦点
     */
    public void releaseAudioFocus() {
        if (!mIsFocusGained) {
            return;
        }
        if (mAudioManager.abandonAudioFocusRequest(mAudioFocusRequest) == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            LogUtils.d("release success");
            mIsFocusGained = false;
        } else {
            LogUtils.d("release failed !");
        }
    }

    public void registerAudioFocusChangedListener(OnFocusChangedListener listener) {
        if (listener != null && !mListenerList.contains(listener)) {
            mListenerList.add(listener);
        }
    }

    public void unregisterAudioFocusChangedListener(OnFocusChangedListener listener) {
        mListenerList.remove(listener);
    }

    public interface OnFocusChangedListener {
        void onAudioFocusChanged(int state);
    }

    private void notifyFocusChanged(int state) {
        for (OnFocusChangedListener listener : mListenerList) {
            listener.onAudioFocusChanged(state);
        }
    }
}
