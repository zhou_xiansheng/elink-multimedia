package com.elink.elinkmultimediaservices.service.receiver;

import com.elink.bluetoothapi.BtJar;
import com.elink.bluetoothapi.data.BtType;
import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.constants.common.EAudioSource;
import com.elink.elinkmultimediaservices.utils.LogUtils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class AudioFocusChangeReceiver extends BroadcastReceiver {
    public static final String ACTION_AUDIO_FOCUS_CHANGE = "com.bird.action.AUDIO_FOCUS_CHANGE";
    public static final String EXTRA_PACKAGENAME = "packagename";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (ACTION_AUDIO_FOCUS_CHANGE.equals(action)) {
            String pkgName = intent.getStringExtra(EXTRA_PACKAGENAME);
            if (pkgName != null) {
                if (pkgName.contains("amap")) {
                    if (BtJar.get() != null) {
                        if (PlayManage.checkAudioSource(EAudioSource.AUDIO_SOURCE_BT)){
                            LogUtils.d("audiofocus changed amap, change volume 0.3f");
                            BtJar.get().setMusicVolume(BtType.BT_TYPE_GOC, 0.3f);
                        }
                    } else {
                        LogUtils.d("audiofocus changed amap,but btjar is null");
                    }
                } else if (pkgName.contains("elinkmultimediaservices")) {
                    if (BtJar.get() != null) {
                        if (PlayManage.checkAudioSource(EAudioSource.AUDIO_SOURCE_BT)){
                            LogUtils.d("audiofocus changed this, change volume 1f");
                            BtJar.get().setMusicVolume(BtType.BT_TYPE_GOC, 1f);
                        }
                    } else {
                        LogUtils.d("audiofocus changed this,but btjar is null");
                    }
                }
            }

        }
    }
}
