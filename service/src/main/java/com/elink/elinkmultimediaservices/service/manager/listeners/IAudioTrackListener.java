package com.elink.elinkmultimediaservices.service.manager.listeners;

import com.elink.elinkmultimediaservices.service.manager.music.BtPlayManager;
import com.elink.elinkmultimediaservices.utils.LogUtils;
import com.elink.xui.adaptapi.audio.Audio;

/**
 * 导航时候压低蓝牙音乐
 */
public class IAudioTrackListener implements Audio.IAudioFocusListener, Audio.IAudioTrackChangeListener {

    private static final String naviPackage = "com.autonavi.amapauto";

    private final BtPlayManager btMusicManager;

    public IAudioTrackListener(BtPlayManager btMusicManager) {
        this.btMusicManager = btMusicManager;
    }

    @Override
    public void onAudioTrackChange(String pkg, boolean isPlaying) {
        LogUtils.d("pkg:"+ pkg + " isPlaying:" + isPlaying);
        if (naviPackage.equals(pkg)) {
            if (isPlaying) {
                LogUtils.d("导航中 蓝牙音乐压低声音");
                btMusicManager.setPlayVolumeTo(0.3f);
            } else {
                LogUtils.d("导航结束 蓝牙音乐声音恢复");
                btMusicManager.setPlayVolumeTo(1.0f);
            }
        } else if ("com.elink.ecgallery".equals(pkg)) {
        }
    }

    @Override
    public void onAudioFocusChange(String pkg) {
        LogUtils.d("pkg:"+ pkg);
    }
}
