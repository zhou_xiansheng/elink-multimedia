package com.elink.elinkmultimediaservices.service.audiofocus;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Looper;

import com.elink.elinkmultimediaservices.service.manager.music.BtPlayManager;
import com.elink.elinkmultimediaservices.utils.LogUtils;
import com.elink.elinkmultimediaservices.utils.MyApplication;

public class BtMusicAudioManager implements AudioManager.OnAudioFocusChangeListener {

    private final BtPlayManager musicManager;
    private final AudioFocusRequest audioFocusRequest;
    private final AudioManager audioManager;

    private boolean isGainFocus = false;

    public BtMusicAudioManager(BtPlayManager musicManager) {
        this.musicManager = musicManager;
        audioManager = (AudioManager) MyApplication.getContext().getSystemService(Context.AUDIO_SERVICE);
        AudioAttributes audioAttr = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build();
        audioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                .setAudioAttributes(audioAttr)
                .setAcceptsDelayedFocusGain(true)
                .setOnAudioFocusChangeListener(this, new Handler())
                .build();
    }

    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                LogUtils.d("重新获得焦点");
                musicManager.play();
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                LogUtils.d("失去焦点");
                isGainFocus = false;
                musicManager.pauseRightNow();
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                LogUtils.d("暂时失去音频焦点，很快会重新获得");
                isGainFocus = false;
                musicManager.pauseRightNow();
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                LogUtils.d("暂时失去焦点，但可以“低声播放”");
                if (musicManager.player.isPlaying())
                    new Handler(Looper.getMainLooper()).post(() -> {
                        musicManager.player.setVolume(0.1f, 0.1f);
                    });
                break;
        }
    }


    public boolean requestAudioFocus() {
        boolean res;
        if (isGainFocus) {
            res = true;
        } else {
            LogUtils.d("");
            res = (audioManager.requestAudioFocus(audioFocusRequest) == AudioManager.AUDIOFOCUS_REQUEST_GRANTED);
            isGainFocus = res;
        }
        if (!res) {
            LogUtils.d("焦点请求失败 ！");
        }
        return res;
    }


    /**
     * Android O及以上版本
     */
    public void abandonAudioFocus() {
        LogUtils.d("");
        if (isGainFocus) {
            audioManager.abandonAudioFocus(this);
            audioManager.abandonAudioFocusRequest(audioFocusRequest);
            isGainFocus = false;
        }
    }
}
