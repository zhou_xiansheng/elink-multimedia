package com.elink.elinkmultimediaservices.service.callback;

import android.os.RemoteException;

import com.elink.elinkmultimediaservices.IRadioCallback;
import com.elink.elinkmultimediaservices.beans.RadioChannel;
import com.elink.elinkmultimediaservices.constants.radio.ERadioStatus;
import com.elink.elinkmultimediaservices.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

public class FMCallback {
    public final List<IRadioCallback> callbacks = new ArrayList<>();

    public void radioFocusLostCallBack() {
        LogUtils.d("size:" + callbacks.size());
        // 失去音频焦点 回调客户端
        for (int i = callbacks.size() - 1; i >= 0; i--) {
            try {
                callbacks.get(i).onFocusLost();
            } catch (Exception e) {
                LogUtils.w(e.getMessage());
                callbacks.remove(i);
            }
        }
    }

    /**
     * 播放状态改变回调
     *
     * @param status
     * @param channel
     */
    public void onPlaybackStatusChangedCallBack(int status, RadioChannel channel) {
        LogUtils.d("status:" + status + " size:" + callbacks.size());
        for (int i = callbacks.size() - 1; i >= 0; i--) {
            try {
                callbacks.get(i).onPlaybackStatusChanged(status, channel);
            } catch (Exception e) {
                LogUtils.w(e.getMessage());
                callbacks.remove(i);
            }
        }
    }

    public void searchCallBack(int channelNumber) {
        LogUtils.d("channelNumber:" + channelNumber);
        for (int i = callbacks.size() - 1; i >= 0; i--) {
            try {
                callbacks.get(i).searchNext(channelNumber);
            } catch (Exception e) {
                LogUtils.w(e.getMessage());
                callbacks.remove(i);
            }
        }
    }

    /**
     * @param status        搜索状态
     * @param radioChannels 搜索结果
     */
    public void onSearchedListChanged(int status, ArrayList<RadioChannel> radioChannels) {
        //客户端回调
        for (IRadioCallback callback : callbacks) {
            try {
                callback.onSearchedListChanged(status, radioChannels);
            } catch (RemoteException e) {
                LogUtils.w(e.getMessage());
            }
        }
    }

    public void onSeekStatusChanged(ERadioStatus status) {
        LogUtils.d("status:" + status);
        //客户端回调
        for (IRadioCallback callback : callbacks) {
            try {
                callback.onSeekStatusChanged(status.getValue());
            } catch (RemoteException e) {
                LogUtils.w(e.getMessage());
            }
        }
    }
}
