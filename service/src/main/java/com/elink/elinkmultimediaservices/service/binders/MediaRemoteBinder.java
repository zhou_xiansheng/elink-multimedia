package com.elink.elinkmultimediaservices.service.binders;

import android.os.RemoteException;

import com.elink.elinkmultimediaservices.IMusicCallback;
import com.elink.elinkmultimediaservices.IMusicService;
import com.elink.elinkmultimediaservices.IRadioCallback;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.beans.RadioChannel;
import com.elink.elinkmultimediaservices.constants.common.EAudioSource;
import com.elink.elinkmultimediaservices.constants.common.IPlayMode;
import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.service.MediaServices;
import com.elink.elinkmultimediaservices.utils.LogUtils;

import java.util.List;


public class MediaRemoteBinder extends IMusicService.Stub {

    private final MediaServices mediaServices;


    public MediaRemoteBinder(MediaServices mediaServices) {
        this.mediaServices = mediaServices;
    }


    @Override
    public void play() throws RemoteException {
        if (mediaServices != null) {
            mediaServices.play();
        }
    }

    @Override
    public void customerPlay() throws RemoteException {
        if (mediaServices != null) {
            mediaServices.customerPlay();
        }
    }

    @Override
    public void playByMusic() throws RemoteException {
        if (mediaServices != null) {
            mediaServices.playByMusic();
        }
    }


    @Override
    public void pause() throws RemoteException {
        if (mediaServices != null) {
            mediaServices.pause();
        }
    }


    @Override
    public void stop() throws RemoteException {
        if (mediaServices != null) {
            mediaServices.stop();
        }
    }

    @Override
    public void next() throws RemoteException {
        if (mediaServices != null) {
            mediaServices.next();
        }
    }


    @Override
    public void prev() throws RemoteException {
        if (mediaServices != null) {
            mediaServices.pre();
        }
    }

    @Override
    public List<MusicFile> getMediaFileList() throws RemoteException {
        if (mediaServices != null) {
            return mediaServices.getMediaFileList();
        } else {
            return null;
        }
    }

    @Override
    public List<MusicFile> getMusicCollect() throws RemoteException {
        if (mediaServices != null) {
            return mediaServices.getMusicCollect();
        } else {
            return null;
        }
    }

    @Override
    public int getCurrentPosition() throws RemoteException {
        if (mediaServices != null) {
            return mediaServices.getCurrentPosition();
        } else {
            return -1;
        }
    }

    @Override
    public int getMusicPlayMode() throws RemoteException {
        if (mediaServices != null) {
            return mediaServices.getMusicPlayMode();
        } else {
            return -1;
        }
    }


    @Override
    public MusicFile getMusicPlayNow() throws RemoteException {
        if (mediaServices != null) {
            return mediaServices.getMediaMusicPlayNow();
        } else {
            return null;
        }
    }

    @Override
    public void setMusicPlayNow(MusicFile musicfile) throws RemoteException {
        if (mediaServices != null) {
            mediaServices.setMusicPlayNow(musicfile);
        }
    }

    @Override
    public void setMusicPlayMode(int newMode) throws RemoteException {
        if (mediaServices != null) {
            mediaServices.setMusicPlayMode(newMode);
        }
    }

    @Override
    public int getMusicStatus() throws RemoteException {
        if (mediaServices != null) {
            return mediaServices.getMusicStatus();
        } else {
            return EPlayStatus.STATUS_UNKNOWN.getValue();
        }
    }

    @Override
    public int getMediaStatus() throws RemoteException {
        if (mediaServices != null) {
            return mediaServices.getMediaStatus();
        } else {
            return EPlayStatus.STATUS_UNKNOWN.getValue();
        }
    }

    @Override
    public void setPlayIndex(int index) throws RemoteException {
        if (mediaServices != null) {
            mediaServices.setPlayIndex(index);
        }
    }

    @Override
    public void setMusicCollection(List<MusicFile> musicFiles) throws RemoteException {

        if (mediaServices != null) {
            mediaServices.setMusicCollection(musicFiles);
        }
    }

    @Override
    public void addToMusicCollect(MusicFile musicFile) throws RemoteException {
        if (mediaServices != null) {
            mediaServices.addToMusicCollect(musicFile);
        }
    }

    @Override
    public void mediaPlayerSeekTo(int progress) throws RemoteException {
        if (mediaServices != null) {
            mediaServices.mediaPlayerSeekTo(progress);
        }
    }

    @Override
    public void registerMusicCallback(IMusicCallback callback) throws RemoteException {
        if (mediaServices.mMusicCallback != null) {
            mediaServices.mMusicCallback.callbacks.add(callback);
        }
    }

    @Override
    public void unregisterMusicCallback(IMusicCallback callback) throws RemoteException {
        if (mediaServices.mMusicCallback.callbacks != null) {
            mediaServices.mMusicCallback.callbacks.remove(getActualCallback(callback));
        }
    }

    // 辅助方法：获取实际的回调对象
    private IMusicCallback getActualCallback(IMusicCallback proxyCallback) {
        for (IMusicCallback actualCallback : mediaServices.mMusicCallback.callbacks) {
            if (actualCallback.asBinder() == proxyCallback.asBinder()) {
                return actualCallback;
            }
        }
        return null;
    }

    @Override
    public int getCollectIndex() throws RemoteException {
        return 0;
    }

    @Override
    public MusicFile getMusicNow() throws RemoteException {
        return mediaServices.getCustomerMusicPlayNow();
    }

    @Override
    public boolean checkBtConnect() throws RemoteException {
        if (mediaServices != null) {
            return mediaServices.checkBtConnect();
        } else {
            return false;
        }
    }

    @Override
    public String getBtName() throws RemoteException {
        if (mediaServices != null) {
            return mediaServices.getBtName();
        } else {
            return "";
        }
    }

    @Override
    public MusicFile getMusicData() throws RemoteException {
        if (mediaServices != null) {
            return mediaServices.getMusicData();
        } else {
            return null;
        }
    }

    @Override
    public int getAudioSources() throws RemoteException {
        if (mediaServices != null) {
            return mediaServices.getAudioSources();
        } else {
            return EAudioSource.AUDIO_SOURCE_UNKNOWN.getValue();
        }
    }

    @Override
    public void setAudioSources(int i) throws RemoteException {
        if (mediaServices != null) {
            mediaServices.setAudioSources(EAudioSource.fromValue(i));
        }
    }

    @Override
    public int getMusicSources() throws RemoteException {
        if (mediaServices != null) {
            return mediaServices.getMusicSources();
        } else {
            return -1;
        }
    }


    @Override
    public int getPlayMode() throws RemoteException {
        if (mediaServices != null) {
            return mediaServices.getPlayMode();
        } else {
            return IPlayMode.PLAY_MODE_UNKNOWN;
        }
    }

    /*----------------------------------收音机相关-------------------------------------------*/

    @Override
    public void playSpecialChannel(RadioChannel channel) throws RemoteException {
        mediaServices.playSpecialChannel(channel);
    }

    @Override
    public void searchStableChannels() throws RemoteException {

    }

    @Override
    public void setChannelCollection(RadioChannel channel) throws RemoteException {

    }

    @Override
    public int getPowerStatus() throws RemoteException {
        return mediaServices.getPowerStatus();
    }

    @Override
    public List<RadioChannel> getSearchedStableChannelList() throws RemoteException {
        return mediaServices.getSearchedStableChannelList();
    }

    @Override
    public void stopScanAndSeek() throws RemoteException {
        mediaServices.stopScanAndSeek();
    }

    @Override
    public List<RadioChannel> getCollectedChannelList() throws RemoteException {
        return null;
    }

    @Override
    public void registerRadioCallback(IRadioCallback callback) throws RemoteException {
        mediaServices.mFmCallback.callbacks.add(callback);
    }

    @Override
    public void unregisterRadioCallback(IRadioCallback callback) throws RemoteException {
        mediaServices.mFmCallback.callbacks.remove(getActualCallback(callback));
    }

    // 辅助方法：获取实际的回调对象
    private IRadioCallback getActualCallback(IRadioCallback proxyCallback) {
        for (IRadioCallback actualCallback : mediaServices.mFmCallback.callbacks) {
            if (actualCallback.asBinder() == proxyCallback.asBinder()) {
                return actualCallback;
            }
        }
        return null;
    }
}
