package com.elink.elinkmultimediaservices.service.manager.listeners;

import android.util.Log;

import com.elink.bluetoothapi.listener.IBluetoothServiceConnectStateListener;
import com.elink.elinkmultimediaservices.utils.LogUtils;

public class BtServiceConnectStateListener implements IBluetoothServiceConnectStateListener {
    @Override
    public void onServiceConnectStateChange(boolean connected) {
        if (connected) {
            //连接上 修改 UI 播放
            LogUtils.d("服务链接上");
        } else {
            //断开连接，回调修改 UI
            LogUtils.d("服务断开");
        }
    }
}
