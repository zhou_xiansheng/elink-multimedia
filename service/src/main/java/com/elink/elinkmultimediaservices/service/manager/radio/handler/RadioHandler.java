package com.elink.elinkmultimediaservices.service.manager.radio.handler;

import static com.elink.elinkmultimediaservices.constants.radio.IRadioStatus.POWER_UP;
import static com.elink.elinkmultimediaservices.constants.radio.IRadioStatus.SEEK_FINISHED;
import static com.elink.elinkmultimediaservices.constants.radio.IRadioStatus.SEEK_RUNNING;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

import com.elink.elinkmultimediaservices.beans.RadioChannel;
import com.elink.elinkmultimediaservices.constants.common.IPlaybackStatus;
import com.elink.elinkmultimediaservices.constants.radio.IRadioStatus;
import com.elink.elinkmultimediaservices.service.manager.radio.impl.RadioControllerImpl;
import com.elink.elinkmultimediaservices.utils.FmUtils;
import com.elink.elinkmultimediaservices.utils.LogUtils;
import com.elink.elinkmultimediaservices.utils.UnitUtils;

import java.lang.ref.WeakReference;


public class RadioHandler extends Handler {
    private final WeakReference<RadioControllerImpl> reference;
    public static final int MSG_WHAT_POWER_UP = 0x00;
    public static final int MSG_WHAT_POWER_DOWN = 0x01;
    public static final int MSG_WHAT_SCAN = 0x02;
    public static final int MSG_WHAT_SEEK = 0x03;
    public static final int MSG_WHAT_TUNE = 0x04;
    public static final int MSG_WHAT_FM_EXIT = 0x05;

    public RadioHandler(RadioControllerImpl controller, Looper looper) {
        super(looper);
        this.reference = new WeakReference<>(controller);
    }

    @Override
    public void handleMessage(@NonNull Message msg) {
        super.handleMessage(msg);
        RadioControllerImpl controller = reference.get();
        switch (msg.what) {
            case MSG_WHAT_POWER_UP:
                LogUtils.d("MSG_WHAT_POWER_UP");
                if (controller != null) {
                    controller.handlePowerUp(msg.arg1);
                }
                break;
            case MSG_WHAT_POWER_DOWN:
                LogUtils.d("MSG_WHAT_POWER_DOWN");
                if (controller != null) {
                    controller.powerDown();
                }
                break;
            case MSG_WHAT_SCAN:
                LogUtils.d("MSG_WHAT_SCAN");
                if (controller != null) {
                    int[] stations = null;
                    controller.mIsScanning = true;
                    controller.notifySearchStatusChanged(IRadioStatus.SCAN_RUNNING, null);
                    //添加以处理当前电台重置时的情况
                    //发生在由于权限更改，fm 已关闭&从 FmFavoriteActivity 重新启动
                    //在其他情况下，两个值应该相同
                    if ((controller.mPowerStatus == POWER_UP) ||
                            (controller.powerUp(UnitUtils.Channel.fm_f2I(FmUtils.computeFrequency(controller.mCurrentStation))) &&
                                    controller.playFrequency(FmUtils.computeFrequency(controller.mCurrentStation)))) {
                        controller.wakeLockAcquire();
                        stations = controller.startScan();
                    }
                    // 检查是否取消扫描
                    if (((stations != null) && stations[0] == -100) || !reference.get().mIsScanning) {
                        //cancelled
                        LogUtils.d("取消扫描 回调");
                        controller.notifySearchStatusChanged(IRadioStatus.SCAN_FINISHED, null);
                    } else {
                        LogUtils.d("回调");
                        if (stations == null)
                            stations = new int[]{};
                        controller.notifySearchStatusChanged(IRadioStatus.SCAN_FINISHED, stations);
                    }
                    controller.wakeLockRelease();
                    controller.mIsScanning = false;
                }
                break;
            case MSG_WHAT_SEEK:
                LogUtils.d("MSG_WHAT_SEEK");
                if (controller != null) {
                    if (controller.mIsSeeking) {
                        LogUtils.d("seek Task is doing...");
                        return;
                    }
                    controller.mIsSeeking = true;
                    controller.notifySeekStatusChanged(SEEK_RUNNING, 0);
                    float seekStation = controller.seekStation(UnitUtils.Channel.fm_i2F(msg.arg1), (boolean) msg.obj);
                    boolean tunedState = false;
                    int station = FmUtils.computeStation(seekStation);
                    if (FmUtils.isValidStation(station)) {
                        tunedState = controller.tuneStation(UnitUtils.Channel.fm_f2I(seekStation));
                    }
                    if (tunedState) {
                        LogUtils.d("success:" + station);
                    } else {
                        seekStation = FmUtils.computeFrequency(controller.mCurrentStation);
                    }
                    controller.notifySeekStatusChanged(SEEK_FINISHED, UnitUtils.Channel.fm_f2I(seekStation));
                    if (!controller.mIsSeeking) { // 如果 seek  被取消了直接返回，一般是切换其他播放源头，导致焦点被抢了触发
                        LogUtils.d("失去焦点，不接下去处理");
                        reference.get().radioServices.searchCallBack(-1);
                        return;
                    }
                    RadioChannel radioChannel = new RadioChannel();
                    radioChannel.setChannelNum(station);
                    reference.get().radioServices.onFMPlaybackStatusChangedCallBack(IPlaybackStatus.STATUS_PLAYING, radioChannel);
                    reference.get().radioServices.searchCallBack(station);
                    controller.mIsSeeking = false;
                }
                break;
            case MSG_WHAT_TUNE:
                LogUtils.d("MSG_WHAT_TUNE");
                if (controller != null) {
                    boolean isTune = controller.tuneStation(msg.arg1);
                    // 如果调谐失败，则通过当前电台更新ui
                    // TODO: 2023/6/30
                    if (isTune) {
                        LogUtils.d("MSG_WHAT_TUNE fail");
                    } else {
                        FmUtils.computeFrequency(controller.mCurrentStation);
                    }
                }
                break;
            case MSG_WHAT_FM_EXIT:
                LogUtils.d("MSG_WHAT_FM_EXIT");
                if (controller != null) {
                    controller.powerDown();
                    controller.closeDevice();
                }
                break;
        }
    }
}
