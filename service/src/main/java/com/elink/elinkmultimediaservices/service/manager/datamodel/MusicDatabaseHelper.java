package com.elink.elinkmultimediaservices.service.manager.datamodel;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class MusicDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "music_database";
    private static final int DATABASE_VERSION = 6;
    public static final String TABLE_MUSIC_COLLECT = "MusicPlayList";//播放列表
    // MusicFile 字段
    public static final String KEY_ID = "id";
    public static final String KEY_ARTIST = "artist";
    public static final String KEY_ALBUM_COVER = "albumCover";
    public static final String KEY_DURATION = "duration";
    public static final String KEY_PLAYBACK_PROGRESS = "playbackProgress";
    public static final String KEY_LYRIC_PATH = "lyricPath";
    public static final String KEY_LAST_PLAYBACK_TIMESTAMP = "lastPlaybackTimestamp";
    public static final String KEY_IS_SELECTED = "isSelected";
    public static final String KEY_FILE_NAME = "fileName";
    public static final String KEY_PATH = "path";
    public static final String KEY_IS_DIR = "isDir";
    public static final String KEY_IS_VALID = "isValid";
    public static final String KEY_PLAY_SOURCE = "playSource";
    private static final String CREATE_TABLE_MUSIC_COLLECT = "CREATE TABLE IF NOT EXISTS " + TABLE_MUSIC_COLLECT + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_ARTIST + " TEXT,"
            + KEY_ALBUM_COVER + " TEXT,"
            + KEY_DURATION + " INTEGER,"
            + KEY_PLAYBACK_PROGRESS + " INTEGER,"
            + KEY_LYRIC_PATH + " TEXT,"
            + KEY_LAST_PLAYBACK_TIMESTAMP + " INTEGER,"
            + KEY_IS_SELECTED + " INTEGER,"
            + KEY_FILE_NAME + " TEXT,"
            + KEY_PATH + " TEXT,"
            + KEY_IS_DIR + " INTEGER,"
            + KEY_IS_VALID + " INTEGER,"
            + KEY_PLAY_SOURCE + " INTEGER"
            + ")";



    public MusicDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_MUSIC_COLLECT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP TABLE IF EXISTS " + TABLE_MUSIC_COLLECT);
        onCreate(db);
    }
}

