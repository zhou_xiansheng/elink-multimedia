package com.elink.elinkmultimediaservices.service.receiver;

import static com.elink.elinkmultimediaservices.utils.Constants.ATTACHED;
import static com.elink.elinkmultimediaservices.utils.Constants.DETACHED;
import static com.elink.elinkmultimediaservices.utils.Constants.EJECTING;
import static com.elink.elinkmultimediaservices.utils.Constants.MOUNTED;
import static com.elink.elinkmultimediaservices.utils.Constants.SCAN_F;
import static com.elink.elinkmultimediaservices.utils.Constants.SCAN_FF;
import static com.elink.elinkmultimediaservices.utils.Constants.SCAN_FINISH;
import static com.elink.elinkmultimediaservices.utils.Constants.UNMOUNTED;
import static com.elink.elinkmultimediaservices.utils.Constants.table1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;

import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.constants.common.EUsbStatus;
import com.elink.elinkmultimediaservices.service.MediaServices;
import com.elink.elinkmultimediaservices.utils.LogUtils;
import com.elink.elinkmultimediaservices.utils.MusicUtils;
import com.elink.elinkmultimediaservices.utils.SystemPropertiesUtils;

public class UsbProviderReceiver extends BroadcastReceiver {
    private final MediaServices mediaServices;
    private final Handler handler = new Handler(Looper.getMainLooper());

    /**
     * 出现过U盘扫描后多次发送扫描完成广播，多次回调造成客户端ANR，这里设置一定时间内只接收一条该广播
     */
    private Integer SCAN_FINISH_LIMIT = 0;

    public UsbProviderReceiver(MediaServices mediaServices) {
        this.mediaServices = mediaServices;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        int status = intent.getIntExtra("status", -100);
        int tableId = intent.getIntExtra("tableId", -100);
        if (tableId != table1) {
            LogUtils.d("不是我需要的广播:" + tableId);
            return;
        }
        switch (status) {
            case SCAN_FF:
                LogUtils.d("接收失败");
                break;
            case SCAN_F:
                LogUtils.d("扫描失败");
                break;
            case DETACHED:
                LogUtils.d("断开链接");
                break;
            case ATTACHED:
                LogUtils.d("u盘插入");
                break;
            case MOUNTED:
                LogUtils.d("挂载上");
                doAfterMounted();
                break;
            case SCAN_FINISH:
                doAfterScannedFinish();
                LogUtils.d("扫描完成");
                break;
            case EJECTING:
                LogUtils.d("EJECTING");
                if (SCAN_FINISH_LIMIT == 0) {
                    SCAN_FINISH_LIMIT = 1;
                    new Handler(Looper.getMainLooper()).postDelayed(() -> SCAN_FINISH_LIMIT = 0, 1000);
                    doAfterUsbReject();
                }
                break;
            case UNMOUNTED:
                break;
            default:
                LogUtils.d("未处理:" + status);
        }
    }

    public void doAfterMounted() {
        //检查上次播放的音乐和播放列表
        mediaServices.mUsbMusicManager.checkPlayList();
        mediaServices.mUsbMusicManager.checkMusicNow();
        updateUsbStatus(EUsbStatus.INSERT);
        //保险再清除一下
        mediaServices.mUsbMusicManager.errorMusic.clear();
        mediaServices.mUsbMusicManager.musicNow.isValid = true;
        updateMusicData();
    }

    public void doAfterScannedFinish() {
        handler.removeCallbacksAndMessages(null);
        runnable.run();
        updateUsbStatus(EUsbStatus.SCANNED);
        mediaServices.mUsbMusicManager.mMusics = MusicUtils.musicScanner();
        handler.removeCallbacksAndMessages(null);
    }

    public void doAfterUsbReject() {
        handler.removeCallbacksAndMessages(null);
        mediaServices.mUsbMusicManager.setPlayStatus(EPlayStatus.STATUS_PAUSE);
        updateUsbStatus(EUsbStatus.DISCONNECT);
        mediaServices.mUsbMusicManager.mMusics.clear();
        mediaServices.mUsbMusicManager.stopPlayer();
        mediaServices.usbEjectCallback();//回调客户端
    }

    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            //接收广播有延迟可能。
            //再判断一下，避免恰好在扫描完成时候拔出的，这时候扫描完成的广播已经发送了。
            if (PlayManage.checkUsbUseful(SystemPropertiesUtils.checkUsbState())) {
//                mediaServices.mUsbMusicManager.updateMusic();
//                mediaServices.mUsbMusicManager.updatePlayList();
                mediaServices.usbScanCallback();
                updateMusicData();
            }
        }
    };

    public void updateMusicData() {
        if (PlayManage.checkUsbInsert()) {
            handler.postDelayed(runnable, 2000);
        }
    }

    public void updateUsbStatus(EUsbStatus usbStatus) {
        PlayManage.setUsbStatus(usbStatus);  //本地状态
        mediaServices.onUsbStatusChangeCallBack(usbStatus.value);//回调客户端
    }
}
