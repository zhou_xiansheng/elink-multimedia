package com.elink.elinkmultimediaservices.service.manager.datamodel;


import static com.elink.elinkmultimediaservices.service.manager.datamodel.MusicDatabaseHelper.KEY_ALBUM_COVER;
import static com.elink.elinkmultimediaservices.service.manager.datamodel.MusicDatabaseHelper.KEY_ARTIST;
import static com.elink.elinkmultimediaservices.service.manager.datamodel.MusicDatabaseHelper.KEY_DURATION;
import static com.elink.elinkmultimediaservices.service.manager.datamodel.MusicDatabaseHelper.KEY_FILE_NAME;
import static com.elink.elinkmultimediaservices.service.manager.datamodel.MusicDatabaseHelper.KEY_ID;
import static com.elink.elinkmultimediaservices.service.manager.datamodel.MusicDatabaseHelper.KEY_IS_DIR;
import static com.elink.elinkmultimediaservices.service.manager.datamodel.MusicDatabaseHelper.KEY_IS_SELECTED;
import static com.elink.elinkmultimediaservices.service.manager.datamodel.MusicDatabaseHelper.KEY_IS_VALID;
import static com.elink.elinkmultimediaservices.service.manager.datamodel.MusicDatabaseHelper.KEY_LAST_PLAYBACK_TIMESTAMP;
import static com.elink.elinkmultimediaservices.service.manager.datamodel.MusicDatabaseHelper.KEY_LYRIC_PATH;
import static com.elink.elinkmultimediaservices.service.manager.datamodel.MusicDatabaseHelper.KEY_PATH;
import static com.elink.elinkmultimediaservices.service.manager.datamodel.MusicDatabaseHelper.KEY_PLAYBACK_PROGRESS;
import static com.elink.elinkmultimediaservices.service.manager.datamodel.MusicDatabaseHelper.KEY_PLAY_SOURCE;
import static com.elink.elinkmultimediaservices.service.manager.datamodel.MusicDatabaseHelper.TABLE_MUSIC_COLLECT;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;
import com.elink.elinkmultimediaservices.utils.LogUtils;
import com.elink.elinkmultimediaservices.utils.MyApplication;

import java.util.LinkedList;
import java.util.List;

public class MusicOperations {
    /*--------------------------收藏列表相关放相关--------------------------------*/

    public static void updateCollectMusic(List<MusicFile> musicFiles) {
        LogUtils.d("");
        clearCollectMusic();
        insertAllIntoCollectMusic(musicFiles);
    }

    public static void clearCollectMusic() {
        SQLiteDatabase db = new MusicDatabaseHelper(MyApplication.getContext()).getReadableDatabase();
        db.delete(TABLE_MUSIC_COLLECT, null, null);
        db.close();
    }

    public static long insertIntoCollectMusic(MusicFile musicFile) {
        LogUtils.d("");
        SQLiteDatabase db = new MusicDatabaseHelper(MyApplication.getContext()).getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ARTIST, musicFile.artist);
        values.put(KEY_ALBUM_COVER, musicFile.albumCover);
        values.put(KEY_DURATION, musicFile.duration);
        values.put(KEY_PLAYBACK_PROGRESS, musicFile.playProgress);
        values.put(KEY_LYRIC_PATH, musicFile.lyricPath);
        values.put(KEY_LAST_PLAYBACK_TIMESTAMP, musicFile.lastPlayProgress);
        values.put(KEY_IS_SELECTED, musicFile.isSelected ? 1 : 0);
        values.put(KEY_FILE_NAME, musicFile.fileName);
        values.put(KEY_PATH, musicFile.path);
        values.put(KEY_IS_DIR, musicFile.isDir ? 1 : 0);
        values.put(KEY_IS_VALID, musicFile.isValid ? 1 : 0);
        values.put(KEY_PLAY_SOURCE, musicFile.playSource);
        // 插入行
        long id = db.insert(TABLE_MUSIC_COLLECT, null, values);
        db.close();
        return id;
    }

    // 批量插入 MusicFile 记录的方法
    public static void insertAllIntoCollectMusic(List<MusicFile> musicFiles) {
        SQLiteDatabase db = new MusicDatabaseHelper(MyApplication.getContext()).getReadableDatabase();
        db.beginTransaction(); // 开始事务
        try {
            for (MusicFile musicFile : musicFiles) {
                ContentValues values = new ContentValues();
                values.put(KEY_ARTIST, musicFile.artist);
                values.put(KEY_ALBUM_COVER, musicFile.albumCover);
                values.put(KEY_DURATION, musicFile.duration);
                values.put(KEY_PLAYBACK_PROGRESS, musicFile.playProgress);
                values.put(KEY_LYRIC_PATH, musicFile.lyricPath);
                values.put(KEY_LAST_PLAYBACK_TIMESTAMP, musicFile.lastPlayProgress);
                values.put(KEY_IS_SELECTED, musicFile.isSelected ? 1 : 0);
                values.put(KEY_FILE_NAME, musicFile.fileName);
                values.put(KEY_PATH, musicFile.path);
                values.put(KEY_IS_DIR, musicFile.isDir ? 1 : 0);
                values.put(KEY_IS_VALID, musicFile.isValid ? 1 : 0);
                values.put(KEY_PLAY_SOURCE, musicFile.playSource);
                db.insert(TABLE_MUSIC_COLLECT, null, values);
            }
            db.setTransactionSuccessful(); // 设置事务成功完成
        } finally {
            db.endTransaction(); // 结束事务
            db.close();
        }
    }

    // 获取收藏列表
    @SuppressLint("Range")
    public static LinkedList<MusicFile> getAllCollectMusic() {
        LinkedList<MusicFile> musicFiles = new LinkedList<>();
        SQLiteDatabase db = new MusicDatabaseHelper(MyApplication.getContext()).getReadableDatabase();
        Cursor cursor = db.query(TABLE_MUSIC_COLLECT, null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                MusicFile musicFile = new MusicFile();
                musicFile.id = cursor.getInt(cursor.getColumnIndex(KEY_ID));
                musicFile.artist = cursor.getString(cursor.getColumnIndex(KEY_ARTIST));
                musicFile.albumCover = cursor.getString(cursor.getColumnIndex(KEY_ALBUM_COVER));
                musicFile.duration = cursor.getInt(cursor.getColumnIndex(KEY_DURATION));
                musicFile.playProgress = cursor.getLong(cursor.getColumnIndex(KEY_PLAYBACK_PROGRESS));
                musicFile.lyricPath = cursor.getString(cursor.getColumnIndex(KEY_LYRIC_PATH));
                musicFile.lastPlayProgress = cursor.getLong(cursor.getColumnIndex(KEY_LAST_PLAYBACK_TIMESTAMP));
                musicFile.isSelected = cursor.getInt(cursor.getColumnIndex(KEY_IS_SELECTED)) == 1;
                musicFile.fileName = cursor.getString(cursor.getColumnIndex(KEY_FILE_NAME));
                musicFile.path = cursor.getString(cursor.getColumnIndex(KEY_PATH));
                musicFile.isDir = cursor.getInt(cursor.getColumnIndex(KEY_IS_DIR)) == 1;
                musicFile.isValid = cursor.getInt(cursor.getColumnIndex(KEY_IS_VALID)) == 1;
                musicFile.playSource = IPlaySource.USB;
                musicFiles.add(musicFile);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return musicFiles;
    }
}
