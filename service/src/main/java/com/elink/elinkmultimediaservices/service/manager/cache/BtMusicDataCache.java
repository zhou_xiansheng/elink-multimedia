package com.elink.elinkmultimediaservices.service.manager.cache;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

/**
 * 对蓝牙回调的最后 N 条信息进行缓存，可能有用
 * @param <K>
 * @param <V>
 */
public class BtMusicDataCache<K, V> {
    private final int capacity;
    private final Queue<K> queue;
    private final Map<K, V> map;

    public BtMusicDataCache(int capacity) {
        this.capacity = capacity;
        this.queue = new LinkedList<>();
        this.map = new HashMap<>();
    }

    public void put(K key, V value) {
        if (queue.size() == capacity) {
            K oldestKey = queue.poll();
            map.remove(oldestKey);
        }
        queue.offer(key);
        map.put(key, value);
    }

    public V get(K key) {
        return map.get(key);
    }

    public V getLast() {
        K lastKey = null;
        for (K key : queue) {
            lastKey = key;
        }
        if (lastKey != null) {
            return map.get(lastKey);
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return "KeyValueCache{" +
                "queue=" + queue +
                ", map=" + map +
                '}';
    }
}
