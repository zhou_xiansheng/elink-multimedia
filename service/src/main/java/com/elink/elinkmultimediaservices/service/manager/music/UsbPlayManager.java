package com.elink.elinkmultimediaservices.service.manager.music;

import static com.elink.elinkmultimediaservices.beans.MusicPlayer.FIELD_MUSIC_PLAYER;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.net.Uri;

import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.common.EAudioSource;
import com.elink.elinkmultimediaservices.constants.common.IErrorType;
import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.constants.common.IPlaybackStatus;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;
import com.elink.elinkmultimediaservices.constants.music.IRepeatMode;
import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.beans.MusicPlayer;
import com.elink.elinkmultimediaservices.service.MediaServices;
import com.elink.elinkmultimediaservices.service.audiofocus.UsbMusicAudioManager;
import com.elink.elinkmultimediaservices.service.interfaces.IPlayManager;
import com.elink.elinkmultimediaservices.service.manager.datamodel.MusicOperations;
import com.elink.elinkmultimediaservices.utils.LogUtils;
import com.elink.elinkmultimediaservices.utils.MusicUtils;
import com.elink.elinkmultimediaservices.utils.MyApplication;
import com.elink.elinkmultimediaservices.utils.SPUtils;
import com.elink.elinkmultimediaservices.utils.SystemPropertiesUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class UsbPlayManager implements IPlayManager {
    private static final String FIELD_MUSIC_NOW = "field_music_now";
    //
    public MediaServices mediaServices;
    public MediaPlayer player;//播放器
    private final UsbMusicAudioManager musicAudioManager;//音频焦点
    public MusicPlayer musicPlayer;//音乐播放控制器
    public MusicFile musicNow;//当前播放的数据。
    public List<MusicFile> mMusics;//从存储中读取的数据
    public LinkedList<MusicFile> mPlayList;//收藏列表
    private boolean mIsPlayError = false;//播放发生错误


    /**
     * 初始化
     */
    public UsbPlayManager(MediaServices mediaServices) {
        LogUtils.d("");
        this.mediaServices = mediaServices;
        SystemPropertiesUtils.checkUsbState();
        mMusics = MusicUtils.musicScanner();
        musicPlayer = SPUtils.getMusicPlayer(FIELD_MUSIC_PLAYER);
        musicNow = SPUtils.getMusic(FIELD_MUSIC_NOW);
        mPlayList = MusicOperations.getAllCollectMusic();
        checkPlayList();
        checkMusicNow();
        musicAudioManager = new UsbMusicAudioManager(this);
    }

    /**
     * 检查当前播放的资源是否存在
     */
    public void checkMusicNow() {
        LogUtils.d("");
        File file = new File(musicNow.path);
        if (file.exists() && file.isFile()) {
            boolean isInList = false;
            for (int i = 0; i < mPlayList.size(); i++) {
                MusicFile musicFile = mPlayList.get(i);
                if (musicFile.path.equals(musicNow.path)) {
                    setPlayIndex(i);
                    isInList = true;
                    break;
                }
            }
            if (!isInList) {
                mPlayList.addFirst(musicNow);
                setPlayIndex(0);
            }
        } else {
            setPlayIndex(-1);
            musicNow = new MusicFile();
        }
    }

    /**
     * 检查播放列表是否存在
     */
    public void checkPlayList() {
        LogUtils.d("");
        for (int i = mPlayList.size() - 1; i >= 0; i--) {
            MusicFile musicFile = mPlayList.get(i);
            File file = new File(musicFile.path);
            if (!file.exists()) {
                mPlayList.remove(i);
            }
        }
    }


    @SuppressLint("SdCardPath")
    public boolean initMediaPlay(MusicFile musicFile) {
        if (player != null) {
            player.setOnCompletionListener(null);
            player.setOnErrorListener(null);
            player.setOnPreparedListener(null);
            try {
                if (player.isPlaying()) {
                    player.pause();
                    timer.cancel();
                }
                player.reset();
            } catch (Exception e) {
                player = null;
                initMediaPlay(musicFile);
                LogUtils.e(e.getMessage(), e);
                return false;
            }
        } else {
            player = new MediaPlayer();
        }
        if (new File(musicFile.path).exists()) {
            mediaServices.setAudioSources(EAudioSource.AUDIO_SOURCE_USB);
            musicNow = musicFile;
            musicPlayer.setDuration(musicNow.duration);
            musicPlayer.setCurrentPosition(0);
            SPUtils.put(FIELD_MUSIC_NOW, musicNow);
            SPUtils.put(FIELD_MUSIC_PLAYER, musicPlayer);
            try {
                player.setDataSource(MyApplication.getContext(), Uri.fromFile(new File(musicFile.path)));
                if (player == null) {
                    LogUtils.d("player 初始化失败 ！");
                    musicNow.isValid = false;
                    errorMusic.put(musicNow.path, "player null");
                    errorPause();
                } else {
                    setPlayListener();
                    player.prepareAsync();
                    return true;
                }
            } catch (Exception e) {
                LogUtils.e(e.getMessage(), e);
                errorMusic.put(musicNow.path, "player null");
                errorPause();
            }
        } else {
            LogUtils.d("音乐地址无效 path:" + musicFile.path);
        }
        return false;
    }


    public void setPlayListener() {
        LogUtils.d("");
        player.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                LogUtils.d("what:" + what + " extra" + extra);
                mIsPlayError = true;
                if (extra == -2147483648) {
                    musicNow.isValid = false;
                    errorMusic.put(musicNow.path, String.valueOf(extra));
                }
                return false;
            }
        });
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                LogUtils.d("播放完成");
                PlayManage.setUsbPlayStatus(EPlayStatus.STATUS_PAUSE);
                musicPlayer.setCurrentPosition(0);
                if (mIsPlayError) {
                    mIsPlayError = false;
                    if (!musicNow.isValid || errorMusic.containsKey(musicNow.path)) {
                        errorPause();
                    } else {
                        LogUtils.d("初始化失败，但是还是继续播放");
                        play();
                    }
                } else {
                    modePlay();
                }
            }
        });
        player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                LogUtils.d("开始播放");
                setPlayStatus(EPlayStatus.STATUS_PLAYING);
                player.start();
                timerStart();
                mediaServices.unMute();
                mediaServices.mMediaHandler.reMoveBtStatusSync();
                mediaServices.playCallBack(IPlaybackStatus.STATUS_PLAYING, musicNow);
            }
        });
    }

    public boolean canUsbPlay() {
        if (PlayManage.checkUsbUseful()
                && !mediaServices.isHibernate
                && !PlayManage.isBtCallPlay()
                && !PlayManage.isUsbPlaying()) {
            return true;
        } else if (!PlayManage.checkUsbUseful()) {
            LogUtils.d("usb 未插入 ！");
            mediaServices.errorCallBack(IErrorType.USB_DISCONNECT);
        } else if (PlayManage.isBtCallPlay()) {
            LogUtils.d("蓝牙通话中，不能播放");
            mediaServices.errorCallBack(IErrorType.BT_PHONE);
        } else if (mediaServices.isHibernate) {
            LogUtils.d("休眠中禁止播放 ！");
        } else if (PlayManage.isUsbPlaying()) {
            LogUtils.d("已经是播放状态 ");
        }
        return false;
    }

    public boolean canUsbNextPlay() {
        if (PlayManage.checkUsbUseful()
                && !mediaServices.isHibernate
                && !PlayManage.isBtCallPlay()) {
            return true;
        } else if (!PlayManage.checkUsbUseful()) {
            LogUtils.d("usb 未插入 ！");
            mediaServices.errorCallBack(IErrorType.USB_DISCONNECT);
        } else if (mediaServices.isHibernate) {
            LogUtils.d("休眠中禁止播放 ！");
        } else if (PlayManage.isBtCallPlay()) {
            LogUtils.d("蓝牙通话中，不能播放");
            mediaServices.errorCallBack(IErrorType.BT_PHONE);
        }
        return false;
    }


    /**
     * 播放指定的音乐
     *
     * @param musicFile
     */
    public HashMap<String, String> errorMusic = new HashMap<>();

    public void play(MusicFile musicFile) {
        if (!canUsbNextPlay())
            return;
        if (musicAudioManager.requestAudioFocus()) {
            LogUtils.d("指定资源播放");
            if (!initMediaPlay(musicFile)) {
                LogUtils.d("初始化播放器失败 ！");
            }
        } else {
            LogUtils.d("音频焦点请求失败！");
        }
    }

    /**
     * 简单播放，不需要设置播放源
     */
    @Override
    public void play() {
        if (!canUsbPlay())
            return;
        if (musicAudioManager.requestAudioFocus()) {
            try {
                if (musicNow.path.equals("")) { //刚刷软件、切换u盘等，需要初始化数据
                    LogUtils.d("播放 刷机后第一次播放");
                    initData();
                    initMediaPlay(musicNow);
                } else if (player == null) { //刚启动服务，播放器还没初始化
                    LogUtils.d("播放 切换u盘|开机后 第一次播放");
                    initMediaPlay(musicNow);
                } else {
                    if (errorMusic.containsKey(musicNow.path)) {
                        errorPause();
                        return;
                    }
                    setPlayStatus(EPlayStatus.STATUS_PLAYING);
                    mediaServices.setAudioSources(EAudioSource.AUDIO_SOURCE_USB);
                    player.seekTo(musicPlayer.getCurrentPosition());
                    player.start();
                    mediaServices.unMute();
                    timerStart();
                    mediaServices.mMediaHandler.reMoveBtStatusSync();
                    mediaServices.playCallBack(0, musicNow);//回调客户端
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            LogUtils.d("音频焦点请求失败");
        }
    }

    /**
     * 播放列表等的播放，需要设置播放源
     */
    public void playByMusic() {
        if (!canUsbPlay())
            return;
        if (musicAudioManager.requestAudioFocus()) {
            LogUtils.d("");
//            changeMusicList();
            initMediaPlay(musicNow);
        }
    }


    /**
     * 右边点击播放
     * 不在收藏列表中，添加到播放列表
     */
    public void changeMusicList() {
        for (int i = 0; i < mPlayList.size(); i++) {
            if (mPlayList.get(i).path.equals(musicNow.path)) {
                LogUtils.d("在收藏列表中");
                musicPlayer.setPlayIndex(i);
                return;
            }
        }
        LogUtils.d("添加到播放列表");
        mPlayList.add(musicNow);
        musicPlayer.setPlayIndex(mPlayList.size());
    }


    /**
     * 第一次播放时候指定第一首歌曲
     */
    public void initData() {
        if (musicNow.playSource == IPlaySource.UNKNOWN) {
            // 第一次播放，播放第一个目录下的前 10 首歌曲
            ArrayList<MusicFile> rootMusic = getMusicUnderTheFirstDirectory();
            if (rootMusic.size() > 0) {
                mPlayList.addAll(rootMusic);
                musicNow = rootMusic.get(0);//播放第一首
                setPlayIndex(0);
                MusicOperations.insertIntoCollectMusic(musicNow);
            } else {
                LogUtils.d("3");
            }
        } else {
            LogUtils.d("1");
        }
    }

    public ArrayList<MusicFile> getMusicUnderTheFirstDirectory() {
        String usbTag = android.os.SystemProperties.get("sys.elink.tableid1");
        String path = "/mnt/media_rw/" + usbTag;
        return MusicUtils.getMusicUnderTheFirstDirectory(path);
    }

    /**
     * 暂停
     */
    @Override
    public void pause() {
        try {
            if (PlayManage.checkUsbUseful()) {
                if (player.isPlaying()) {
                    LogUtils.d("暂停");
                    setPlayStatus(EPlayStatus.STATUS_PAUSE);
                    musicPlayer.setCurrentPosition(player.getCurrentPosition());
                    player.pause();
                    timer.cancel();
                    musicAudioManager.abandonAudioFocus();//释放焦点
                    mediaServices.pauseCallBack(musicNow);//回调客户端
                } else {
                    LogUtils.d("暂停失败");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pause2() {
        try {
            setPlayStatus(EPlayStatus.STATUS_PAUSE);
            if (PlayManage.checkUsbUseful()) {
                if (player.isPlaying()) {
                    LogUtils.d("暂停");
                    musicPlayer.setCurrentPosition(player.getCurrentPosition());
                    timer.cancel();
                    player.pause();
                    musicAudioManager.abandonAudioFocus();//释放焦点
                    mediaServices.pauseCallBack(musicNow);//回调客户端
                } else {
                    LogUtils.d("暂停失败");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void errorPause() {
        LogUtils.d("");
        if (PlayManage.checkUsbUseful()) {
            try {
                setPlayStatus(EPlayStatus.STATUS_PAUSE);
                musicAudioManager.abandonAudioFocus();//释放焦点
                musicNow.isValid = false;
                timer.cancel();
                mediaServices.errorCallBack(IErrorType.USB_PLAY);
                mediaServices.pauseCallBack(musicNow);//回调客户端
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void temporarilyLoseFocusPause() {
        try {
            if (PlayManage.checkUsbUseful()) {
                if (player.isPlaying()) {
                    LogUtils.d("");
                    setPlayStatus(EPlayStatus.STATUS_PAUSE);
                    timer.cancel();
                    musicPlayer.setCurrentPosition(player.getCurrentPosition());
                    player.pause();
                    mediaServices.pauseCallBack(musicNow);//回调客户端
                } else {
                    LogUtils.d("暂停失败");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void next() {
        if (!canUsbNextPlay())
            return;
        int size = mPlayList.size();
        int index = musicPlayer.getPlayIndex();
        if (size > 0) {
            index = (index + 1) % size;
            LogUtils.d("index:" + index + " size:" + mPlayList.size());
            nextMusic(index, mPlayList.get(index));
        } else {
            initData();
            if (mPlayList.size() > 0) {
                next();
            }
        }
    }

    @Override
    public void prev() {
        if (!canUsbNextPlay())
            return;
        int size = mPlayList.size();
        int index = musicPlayer.getPlayIndex();
        LogUtils.d("index:" + (index + 1));
        if (size > 0) {
            index--;
            if (index == -1) {
                //回到最后一首
                index = size - 1;
            }
            LogUtils.d("index:" + (index + 1) + " size:" + mPlayList.size());
            nextMusic(index, mPlayList.get(index));
        } else {
            initData();
            if (mPlayList.size() > 0) {
                prev();
            }
        }
    }

    @Override
    public void stop() {

    }


    public void stopPlayer() {
        LogUtils.d("");
        setPlayStatus(EPlayStatus.STATUS_PAUSE);
        if (player != null) {
            try {
                player.pause();
                timer.cancel();
                player.stop();
                player.release();
                player = null;
                musicAudioManager.abandonAudioFocus();
            } catch (Exception e) {
                LogUtils.e(e.getMessage(), e);
            }
        }
    }

    public void modePlay() {
        LogUtils.d("");
        int playMode = musicPlayer.getPlayMode();
        int index = musicPlayer.getPlayIndex();
        int size = mPlayList.size();
        if (size > 0)
            if (playMode == IRepeatMode.REPEAT_MODE_ORDER) {
                LogUtils.d("列表循环");
                index++;
                if (index >= size)
                    index = 0;  //到列表末尾了回到第一首
                LogUtils.d("index:" + (index + 1) + " size" + mPlayList.size());
                nextMusic(index, mPlayList.get(index));
            } else if (playMode == IRepeatMode.REPEAT_MODE_RANDOM) {
                LogUtils.d("随机播放");
                //随机播放
                int min = 0;
                int max = size - 1;
                int range = max - min + 1;
                Random random = new Random(System.currentTimeMillis());
                int i = random.nextInt(range);
                LogUtils.d("index:" + (i + 1) + "/" + mPlayList.size());
                nextMusic(i, mPlayList.get(i));
            } else if (playMode == IRepeatMode.REPEAT_MODE_SINGLE) {
                LogUtils.d("单曲循环");
                File file = new File(musicNow.path);
                if (file.exists()) {
                    musicPlayer.setCurrentPosition(0);
                    PlayManage.setUsbPlayStatus(EPlayStatus.STATUS_PAUSE);
                    play();
                    mediaServices.playCallBack(0, musicNow);
                } else {
                    //当前歌曲被删除后
                    LogUtils.d("当前歌曲不存在！");
                }
            } else {
                LogUtils.d("播放模式切换出错，不再范围内");
            }
    }

    //正在查找的时候切换播放列表的情况，可能会触发问题
    public void nextMusic(int index, MusicFile music) {
        setPlayIndex(index);
        if (musicAudioManager.requestAudioFocus()) {
            if (initMediaPlay(music)) {
                setPlayStatus(EPlayStatus.STATUS_PLAYING);
            }
        }
    }

    /**
     * 获得所有音乐数据
     *
     * @return
     */
    public List<MusicFile> getMusics() {
        return mMusics;
    }

    /**
     * 获得以及数据
     * 1、目录
     * 2、如果音乐在根目录也添加进去
     *
     * @return
     */
    public List<MusicFile> getMusicCollect() {
        return mPlayList;
    }


    /**
     * 定时器开启
     * 设置进度条，当影月播放完成后的播放策略
     */
    private Timer timer;

    public void timerStart() {
        LogUtils.d("");
        if (timer != null)
            timer.cancel();
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void run() {
                mediaServices.mMusicCallback.progressOfUsbCallback(getCurrentPosition());
            }
        }, 0, 1000);
    }

    /**
     * 获得当前的播放位置
     *
     * @return
     */
    public int getCurrentPosition() {
        int currentPosition = 0;
        if (player != null) {
            if (PlayManage.isUsbPlaying()) {
                currentPosition = player.getCurrentPosition();
            } else {
                currentPosition = musicPlayer.getCurrentPosition();
                LogUtils.d("currentPosition:" + currentPosition);
            }
        } else {
            LogUtils.d("player null");
        }
        return currentPosition;
    }

    /**
     * 到指定位置播放
     *
     * @param progress
     */
    public void mediaPlayerSeekTo(int progress) {
        LogUtils.d("修改播放进度：" + progress);
        musicPlayer.setCurrentPosition(progress);
        if (PlayManage.isUsbPlaying()) {
            if (progress >= musicNow.duration)
                progress = musicNow.duration - 1000;
            if (player == null) {
                LogUtils.w("player null !");
            } else {
                player.seekTo(progress);
            }
        } else {
            LogUtils.d("当前为暂停状态");
        }
    }

    /**
     * 设置播放模式
     *
     * @param mode
     */
    public void setPlayMode(int mode) {
        LogUtils.d("mode:" + mode);
        musicPlayer.setPlayMode(mode);
    }

    /**
     * 销毁播放器
     */
    public void destroyMediaPlay() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    /**
     * 替换播放列表
     *
     * @param musicFiles
     */
    public void setMusicCollection(List<MusicFile> musicFiles) {
        mPlayList.clear();
        mPlayList.addAll(musicFiles);
        //做数据存储
        MusicOperations.updateCollectMusic(mPlayList);
    }

    /**
     * 添加到播放列表末尾
     *
     * @param musicFile
     */
    public void addToMusicCollect(MusicFile musicFile) {
        mPlayList.add(musicFile);
        MusicOperations.insertIntoCollectMusic(musicFile);
    }

    public int getPlayMode() {
        return musicPlayer.getPlayMode();
    }

    public void setPlayIndex(int index) {
        musicPlayer.setPlayIndex(index);
    }


    public MusicFile getMusicPlayNow() {
        return musicNow;
    }

    public void setPlayStatus(EPlayStatus status) {
        PlayManage.setUsbPlayStatus(status);
    }

    /**
     * 设置当前音乐，需要修改播放状态
     *
     * @param musicfile
     */
    public void setMusicPlayNow(MusicFile musicfile) {
        setPlayStatus(EPlayStatus.STATUS_PLAY_PREPARING);
        musicNow = musicfile;
    }
}
