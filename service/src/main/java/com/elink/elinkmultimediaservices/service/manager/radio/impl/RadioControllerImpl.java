package com.elink.elinkmultimediaservices.service.manager.radio.impl;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioSystem;
import android.media.AudioTrack;
import android.os.HandlerThread;
import android.os.PowerManager;

import com.elink.elinkmultimediaservices.constants.radio.IRadioStatus;
import com.elink.elinkmultimediaservices.service.MediaServices;
import com.elink.elinkmultimediaservices.service.manager.radio.RadioIPlayManager;
import com.elink.elinkmultimediaservices.service.manager.radio.handler.RadioHandler;
import com.elink.elinkmultimediaservices.service.audiofocus.AudioFocusManager;
import com.elink.elinkmultimediaservices.service.manager.radio.interfaces.IRadioStateChanged;
import com.elink.elinkmultimediaservices.service.manager.radio.interfaces.IRadioController;
import com.elink.elinkmultimediaservices.service.manager.radio.receiver.FmBroadcastReceiver;
import com.elink.elinkmultimediaservices.service.manager.radio.thread.RenderThread;
import com.android.fmradio.FmNative;
import com.elink.elinkmultimediaservices.utils.FmUtils;
import com.elink.elinkmultimediaservices.utils.LogUtils;
import com.elink.elinkmultimediaservices.utils.MyApplication;
import com.elink.elinkmultimediaservices.utils.UnitUtils;


public class RadioControllerImpl implements IRadioController {
    private static final int SAMPLE_RATE = 44100;
    private static final int CHANNEL_CONFIG = AudioFormat.CHANNEL_CONFIGURATION_STEREO;
    private static final int AUDIO_FORMAT = AudioFormat.ENCODING_PCM_16BIT;
    public static final int BUFFER_SIZE_IN_BYTES = AudioRecord.getMinBufferSize(SAMPLE_RATE, CHANNEL_CONFIG, AUDIO_FORMAT);
    private final AudioManager mAudioManager;
    public AudioRecord mAudioRecord;
    public AudioTrack mAudioTrack;
    public PowerManager.WakeLock mWakeLock;
    private final RadioHandler mHandler;
    private final RadioIPlayManager mRadioIPlayManager;
    public MediaServices radioServices;
    public IRadioStateChanged mStateCallback;
    public int mPowerStatus = IRadioStatus.POWER_DOWN;

    @SuppressLint("InvalidWakeLockTag")
    public RadioControllerImpl(RadioIPlayManager radioIPlayManager) {
        mRadioIPlayManager = radioIPlayManager;
        radioServices = radioIPlayManager.mMediaServices;
        mStateCallback = new RadioStateChangeImp(mRadioIPlayManager);
        mAudioManager = (AudioManager) MyApplication.getContext().getSystemService(Context.AUDIO_SERVICE);
        PowerManager powerManager = (PowerManager) MyApplication.getContext().getSystemService(Context.POWER_SERVICE);
        mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, LogUtils.TAG);
//        if (!FmUtils.isFmSuspendSupport()) {
//            mWakeLock.setReferenceCounted(false);
//        }
        HandlerThread handlerThread = new HandlerThread("FmHandlerThread");
        handlerThread.start();
        mHandler = new RadioHandler(this, handlerThread.getLooper());
        startRenderThread();
        registerReceiver();
    }

    @Override
    public void powerUpAsync(int frequency) {
        mHandler.removeMessages(RadioHandler.MSG_WHAT_POWER_UP);
        mHandler.obtainMessage(RadioHandler.MSG_WHAT_POWER_UP, frequency, 0).sendToTarget();
    }

    @Override
    public void powerDownAsync() {
        mHandler.removeMessages(RadioHandler.MSG_WHAT_SCAN);
        mHandler.removeMessages(RadioHandler.MSG_WHAT_SEEK);
        mHandler.removeMessages(RadioHandler.MSG_WHAT_POWER_UP);
        mHandler.removeMessages(RadioHandler.MSG_WHAT_TUNE);
        mHandler.removeMessages(RadioHandler.MSG_WHAT_POWER_DOWN);
        mHandler.obtainMessage(RadioHandler.MSG_WHAT_POWER_DOWN).sendToTarget();
    }

    @Override
    public boolean isRadioEnabled() {
        return false;
    }

    @Override
    public boolean setMuteStatus(boolean isMute) {
        if (mPowerStatus != IRadioStatus.POWER_UP) {
            return false;
        }
        LogUtils.d(String.valueOf(isMute));
        mRadioIPlayManager.mIsRadioMuted = FmNative.setMute(isMute) == 1;
        return mRadioIPlayManager.mIsRadioMuted;
    }


    @Override
    public void seekChannelAsync(int freqStart, boolean isUp) {
        mPowerStatus = IRadioStatus.POWER_UP;
        mHandler.removeMessages(RadioHandler.MSG_WHAT_SEEK);
        mHandler.obtainMessage(RadioHandler.MSG_WHAT_SEEK, freqStart, 0, isUp).sendToTarget();
    }

    @Override
    public void startAutoScanAsync() {
        mHandler.removeMessages(RadioHandler.MSG_WHAT_SCAN);
        mHandler.obtainMessage(RadioHandler.MSG_WHAT_SCAN).sendToTarget();
    }

    @Override
    public void tuneChannelAsync(int freq) {
        mHandler.removeMessages(RadioHandler.MSG_WHAT_TUNE);
        mHandler.obtainMessage(RadioHandler.MSG_WHAT_TUNE, freq, 0).sendToTarget();
    }

    @Override
    public boolean stopScanAndSeek() {
        LogUtils.d("");
        if (!FmNative.stopScan()) {
            LogUtils.d("stop scan or seek failed.");
            return false;
        }
        if (mIsSeeking) {
            radioServices.mFmCallback.searchCallBack(-1);
            mIsSeeking = false;
        } else if (mIsScanning) {
            mIsScanning = false;
        }
        return true;
    }

    @Override
    public boolean stopAutoScan() {
        if (mPowerStatus != IRadioStatus.POWER_UP) {
            return false;
        }
        boolean res = false;
        mHandler.removeMessages(RadioHandler.MSG_WHAT_SCAN);
        mHandler.removeMessages(RadioHandler.MSG_WHAT_SEEK);
        if (mIsNativeScanning || mIsNativeSeeking) {
            mIsStopScanCalled = true;
            res = FmNative.stopScan();
        }
        return res;
    }

    @Override
    public boolean isScanning() {
        return mIsScanning;
    }

    @Override
    public boolean isSeeking() {
        return mIsSeeking;
    }


    @Override
    public void release() {
        LogUtils.d("");
        if (radioServices != null) {
            radioServices.unregisterReceiver(mFmBroadcastReceiver);
        }
        setMuteStatus(true);
        AudioFocusManager.getInstance().releaseAudioFocus();
        exitFm();
        exitRenderThread();
    }

    private static final int DEFAULT_STATION = 1000;
    public int mCurrentStation = DEFAULT_STATION;
    private boolean mIsDeviceOpened = false;
    private boolean mIsNativeScanning = false;
    public boolean mIsScanning = false;
    private boolean mIsNativeSeeking = false;
    public boolean mIsSeeking = false;//搜索
    private boolean mIsStopScanCalled = false;//停止扫描

    boolean mIsRender = false;
    public final Object mRenderLock = new Object();
    public final Object mRenderingLock = new Object();

    private final BroadcastReceiver mFmBroadcastReceiver = new FmBroadcastReceiver(this);

    @SuppressLint("MissingPermission")
    public synchronized boolean initAudioRecordSink() {
        try {
            if (mAudioRecord != null) {
                if (mAudioRecord.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
                    mAudioRecord.stop();
                }
                mAudioRecord.release();
                mAudioRecord = null;
            }
            mAudioRecord = new AudioRecord(1998,
                    SAMPLE_RATE,
                    CHANNEL_CONFIG,
                    AUDIO_FORMAT,
                    BUFFER_SIZE_IN_BYTES);

            if (mAudioTrack != null) {
                if (mAudioTrack.getPlayState() == AudioTrack.PLAYSTATE_PLAYING) {
                    mAudioTrack.stop();
                }
                mAudioTrack.release();
                mAudioTrack = null;
            }
            mAudioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                    SAMPLE_RATE,
                    CHANNEL_CONFIG,
                    AUDIO_FORMAT,
                    BUFFER_SIZE_IN_BYTES,
                    AudioTrack.MODE_STREAM);
            //检查是否初始化成功
            if (mAudioRecord.getState() == AudioRecord.STATE_UNINITIALIZED
                    || mAudioTrack.getState() == AudioRecord.STATE_UNINITIALIZED) {
                LogUtils.d("init fail recordState：" + mAudioRecord.getState() + " audioState:" + mAudioTrack.getState());
                return false;
            }
            LogUtils.d("init success recordState：" + mAudioRecord.getState() + " audioState:" + mAudioTrack.getState());
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * 打开调频设备，应在通电前拨打
     *
     * @return true if FM device open, false FM device not open
     */
    private boolean openDevice() {
        if (!mIsDeviceOpened) {
            try {
                mIsDeviceOpened = FmNative.openDev();
            } catch (Exception e) {
                LogUtils.e(e.getMessage(), e);
                mIsDeviceOpened = false;
            }
            if (mIsDeviceOpened) LogUtils.d("success");
            else LogUtils.w("fail!");
        }
        return mIsDeviceOpened;
    }

    @SuppressLint("WrongConstant")
    public boolean powerUp(int frequency) {
        LogUtils.d("frequency:" + frequency);
        if (mPowerStatus == IRadioStatus.POWER_UP) {
            LogUtils.d("already done");
            return true;
        }
        // 检查音频模式是否为VOIP呼叫，不要启动调频
        if (mAudioManager.getMode() == AudioSystem.MODE_IN_COMMUNICATION) {
            LogUtils.d("Audio is in communication mode, dont start FM");
            return false;
        }
        boolean res = false;
        wakeLockAcquire();
        openDevice();
        if (FmNative.powerUp(UnitUtils.Channel.fm_i2F(frequency))) {
            notifyPowerStatusChanged(IRadioStatus.POWER_UP);
            if (!mIsRender) {
                startRender();
            }
            res = true;
        } else {
            notifyPowerStatusChanged(IRadioStatus.POWER_DOWN);
            wakeLockRelease();
            LogUtils.d("powerUp failed");
        }
        return res;
    }

    /**
     * 关闭设备
     */
    public boolean closeDevice() {
        LogUtils.d("");
        boolean isDeviceClose = false;
        if (mIsDeviceOpened) {
            isDeviceClose = FmNative.closeDev();
            mIsDeviceOpened = !isDeviceClose;
        }
        wakeLockRelease();
        mHandler.getLooper().quit();
        return isDeviceClose;
    }

    public void wakeLockRelease() {
//        if (!FmUtils.isFmSuspendSupport() && mWakeLock != null && mWakeLock.isHeld()) {
//            mWakeLock.release();
//            mWakeLock = null;
//        }
    }

    public void wakeLockAcquire() {
//        if (!FmUtils.isFmSuspendSupport() && mWakeLock != null && !mWakeLock.isHeld()) {
//            mWakeLock.acquire();
//        }
    }


    public void handlePowerUp(int freq) {
        if (powerUp(freq)) {
            playFrequency(UnitUtils.Channel.fm_i2F(freq));
        }
    }

    public boolean playFrequency(float frequency) {
        LogUtils.d("frequency:" + frequency);
        mCurrentStation = FmUtils.computeStation(frequency);
        wakeLockAcquire();
        enableFmAudio(true);
        setMuteStatus(false);
        return (mPowerStatus == IRadioStatus.POWER_UP);
    }

    /**
     * 打开或关闭调频收音机音频
     *
     * @param enable true, open FM audio; false, close FM audio;
     */
    private void enableFmAudio(boolean enable) {
        if (enable) {
            if ((mPowerStatus != IRadioStatus.POWER_UP)
                    || !AudioFocusManager.getInstance().isFocusGained()) {
                notifyPowerStatusChanged(IRadioStatus.POWER_DOWN);
                stopRender();
                stopAudioTrack();
                return;
            }
            startAudioTrack();
        } else {
            stopRender();
            stopAudioTrack();
        }
    }


    /**
     * 是否就绪
     *
     * @return
     */
    public boolean isRender() {
        return (mIsRender && (mPowerStatus == IRadioStatus.POWER_UP));
    }

    private synchronized void startRender() {
        //需要创建新的音频记录和音频回放轨道，
        //因为输入/输出设备可能被改变。
        if (mAudioRecord != null) {
            if (mAudioRecord.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
                mAudioRecord.stop();
            }
            mAudioRecord.release();
            mAudioRecord = null;
        }
        if (mAudioTrack != null) {
            stopAudioTrack();
        }
        if (initAudioRecordSink()) {
            mIsRender = true;
            synchronized (mRenderLock) {
                LogUtils.d("notifying for mRenderLock");
                mRenderLock.notify();
            }
        }
    }

    private synchronized void stopRender() {
        synchronized (mRenderingLock) {
            boolean localRender = isRender();
            mIsRender = false;
            if (localRender) {
                try {
                    long wait = 200;
                    mRenderingLock.wait(wait);
                } catch (InterruptedException e) {
                    LogUtils.w("stopRender, thread is interrupted" + e.getMessage());
                }
            }
        }
    }

    private void startAudioTrack() {
        LogUtils.d("");
        if (mAudioTrack == null) {
            return;
        }
        if (mAudioTrack.getPlayState() == AudioTrack.PLAYSTATE_STOPPED) {
            mAudioTrack.play();
        }
    }

    private void stopAudioTrack() {
        LogUtils.d("");
        if (mAudioTrack == null) {
            return;
        }
        try {
            if (mAudioTrack.getPlayState() == AudioTrack.PLAYSTATE_PLAYING) {
                mAudioTrack.stop();
            }
            mAudioTrack.release();
            mAudioTrack = null;
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
    }

    public int[] startScan() {
        LogUtils.d("");
        int[] stations = null;
        setMuteStatus(true);
        short[] stationsInShort;
        if (!mIsStopScanCalled) {
            LogUtils.d("搜索中。。。");
            mIsNativeScanning = true;
            stationsInShort = FmNative.autoScan();
            mIsNativeScanning = false;
        } else {
            //收到关闭调频功能的信息，或被手机中断呼叫。请勿退回任何加油站。stationsInShort=null；
            //如果取消扫描，返回无效站-100
            stationsInShort = new short[]{-100};
            mIsStopScanCalled = false;
        }

        if (null != stationsInShort) {
            int size = stationsInShort.length;
            stations = new int[size];
            for (int i = 0; i < size; i++) {
                stations[i] = stationsInShort[i];
            }
        }
        return stations;
    }

    /**
     * 退出FMRadio应用程序
     */
    private void exitFm() {
        LogUtils.d("");
        //如果调频录音机工作，请停止
        //当退出时，我们将音频路径设置回耳机。
        if (mIsNativeScanning || mIsNativeSeeking) {
            stopAutoScan();
        }
        mHandler.removeCallbacksAndMessages(null);
        mHandler.sendEmptyMessage(RadioHandler.MSG_WHAT_FM_EXIT);
    }

    /**
     * FM断电
     */
    public boolean powerDown() {
        LogUtils.d("");
        if (mPowerStatus == IRadioStatus.POWER_DOWN) {
            return true;
        }
        if (mIsScanning) {
            stopAutoScan();
        }
        setMuteStatus(true);
        enableFmAudio(false);
        wakeLockRelease();
        if (!FmNative.powerDown(0/*FMRadio*/)) {
            return false;
        }
        notifyPowerStatusChanged(IRadioStatus.POWER_DOWN);
        return true;
    }

    private RenderThread mRenderThread;

    private synchronized void startRenderThread() {
        if (mRenderThread == null) {
            LogUtils.d("");
            mRenderThread = new RenderThread(this);
            mRenderThread.start();
        }
    }

    private synchronized void exitRenderThread() {
        LogUtils.d("");
        stopRender();
        mRenderThread.interrupt();
        mRenderThread = null;
    }

    public boolean tuneStation(int frequency) {
        LogUtils.d("frequency:" + frequency);
        boolean res = false;
        if (mPowerStatus == IRadioStatus.POWER_UP) {
            res = FmNative.tune(UnitUtils.Channel.fm_i2F(frequency));
            if (res) {
                mCurrentStation = FmUtils.computeStation(UnitUtils.Channel.fm_i2F(frequency));
            }
        } else {
            if (powerUp(frequency)) {
                res = playFrequency(frequency);
            }
        }
        return res;
    }

    public float seekStation(float frequency, boolean isUp) {
        LogUtils.d("frequency:" + frequency + " isUp:" + isUp);
        if (mPowerStatus != IRadioStatus.POWER_UP) {
            LogUtils.d("power down");
            return -1;
        }
        mIsNativeSeeking = true;
        float result = FmNative.seek(frequency, isUp);
        mIsNativeSeeking = false;
        mIsStopScanCalled = false;
        LogUtils.d("result:" + result + " afterTrans:" + UnitUtils.Channel.fm_f2I(result));
        return result;
    }

    private void registerReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SHUTDOWN);
        if (radioServices != null) {
            radioServices.registerReceiver(mFmBroadcastReceiver, filter);
        }
    }

    private void notifyPowerStatusChanged(int status) {
        mPowerStatus = status;
        if (mStateCallback != null) {
            mStateCallback.onPowerStatusChanged(status);
        }
    }

    private void notifyTuneStatusChanged(int status) {
        if (mStateCallback != null) {
            mStateCallback.onTuneStatusChanged(status);
        }
    }

    public void notifySeekStatusChanged(int status, int station) {
        if (mStateCallback != null) {
            mStateCallback.onSeekStatusChanged(status, station);
        }
    }

    /**
     * 电台扫描结束
     * 包含 主动取消扫描 和 自然扫描结束 两种情况
     *
     * @param status
     * @param stations
     */
    public void notifySearchStatusChanged(int status, int[] stations) {
        if (mStateCallback != null) {
            mStateCallback.onScanStatusChanged(status, stations);
        }
    }
}
