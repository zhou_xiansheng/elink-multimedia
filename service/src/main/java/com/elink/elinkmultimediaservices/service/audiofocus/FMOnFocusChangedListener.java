package com.elink.elinkmultimediaservices.service.audiofocus;

import android.media.AudioManager;

import com.elink.elinkmultimediaservices.constants.common.IPlaybackStatus;
import com.elink.elinkmultimediaservices.constants.radio.IRadioStatus;
import com.elink.elinkmultimediaservices.service.manager.radio.RadioIPlayManager;
import com.elink.elinkmultimediaservices.utils.LogUtils;

public class FMOnFocusChangedListener implements AudioFocusManager.OnFocusChangedListener {
    private final RadioIPlayManager mManager;

    public FMOnFocusChangedListener(RadioIPlayManager manager) {
        this.mManager = manager;
    }

    @Override
    public void onAudioFocusChanged(int state) {
        switch (state) {
            case AudioManager.AUDIOFOCUS_GAIN:
                LogUtils.d("AUDIOFOCUS_GAIN");
                //继续播放之前的电台
                mManager.playSpecialChannel(mManager.mCurrentChannel);
                break;
            case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT:
                LogUtils.d("AUDIOFOCUS_GAIN_TRANSIENT");
                break;
            case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK:
                LogUtils.d("AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK");
                break;
            case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE:
                LogUtils.d("AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE");
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                LogUtils.d("AUDIOFOCUS_LOSS");
                // 失去音频焦点 回调客户端
                if (mManager.mRadioController.mPowerStatus == IRadioStatus.POWER_UP) {
                    if (mManager.mRadioController.isScanning() || mManager.mRadioController.isSeeking()) {
                        mManager.stopScanAndSeek();
                    } else {
                        mManager.mMediaServices.radioFMFocusLostCallBack();
                    }
                    mManager.mRadioController.setMuteStatus(true);
                    mManager.mRadioController.powerDownAsync();
                    mManager.mMediaServices.onFMPlaybackStatusChangedCallBack(IPlaybackStatus.STATUS_PAUSE, mManager.mCurrentChannel);
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                LogUtils.d("AUDIOFOCUS_LOSS_TRANSIENT");
                // 暂时失去音频焦点（短暂中断）
                if (mManager.mRadioController.mPowerStatus == IRadioStatus.POWER_UP) {
                    mManager.temporarilyLoseFocusStop();
                    mManager.mMediaServices.radioFMFocusLostCallBack();
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                LogUtils.d("AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK");
                // 暂时失去音频焦点（可以降低音量）
                if (mManager.mRadioController.mPowerStatus == IRadioStatus.POWER_UP) {
                    mManager.stop();
                }
                break;
        }
    }
}
