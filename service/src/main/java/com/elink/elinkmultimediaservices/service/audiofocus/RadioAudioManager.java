package com.elink.elinkmultimediaservices.service.audiofocus;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.telephony.PhoneStateListener;

import com.elink.elinkmultimediaservices.service.manager.radio.RadioIPlayManager;
import com.elink.elinkmultimediaservices.utils.MyApplication;

public class RadioAudioManager extends PhoneStateListener implements AudioManager.OnAudioFocusChangeListener {

    private RadioIPlayManager radioPlayerManager;
    private AudioFocusRequest audioFocusRequest;
    private AudioManager audioManager;

    public RadioAudioManager(RadioIPlayManager radioPlayerManager) {
        this.radioPlayerManager = radioPlayerManager;
        audioManager = (AudioManager) MyApplication.getContext().getSystemService(Context.AUDIO_SERVICE);
        AudioAttributes audioAttr = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build();
        audioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                .setAudioAttributes(audioAttr)
                .setAcceptsDelayedFocusGain(true)
                .setOnAudioFocusChangeListener(this, new Handler())
                .build();
    }

    public boolean isGRANTEDFocus() {
        return (requestAudioFocus(AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN) == AudioManager.AUDIOFOCUS_REQUEST_GRANTED);
    }

    public void requestAudioFocus() {
        audioManager.requestAudioFocus(audioFocusRequest);
    }

    public int requestAudioFocus(int streamType, int durationHint) {
        return audioManager.requestAudioFocus(this, streamType, durationHint);
    }

    public void abandonAudioFocus() {
        audioManager.abandonAudioFocus(this);
        // Android O及以上版本
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            audioManager.abandonAudioFocusRequest(audioFocusRequest);
        }
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
//        switch (focusChange) {
//            case AudioManager.AUDIOFOCUS_GAIN:
//            case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT:
//            case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK:
//            case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE:
//                if (radioFMPlayerImpl.mIsPowerDownByCall) {
//                    if (radioFMPlayerImpl.mPowerStatus == IRadioStatus.POWER_DOWN) {
//                        radioFMPlayerImpl.mRadioPlaybackController.powerUpAsync(radioFMPlayerImpl.mCurrentChannel.getChannelNum());
//                    }
//                    radioFMPlayerImpl.playSpecialChannel(radioFMPlayerImpl.mCurrentChannel);
//                    radioFMPlayerImpl.mIsPowerDownByCall = false;
//                }
//                break;
//            case AudioManager.AUDIOFOCUS_LOSS:
//                Log.d(TAG, "onAudioFocusChanged: 失去音频焦点");
//                // 失去音频焦点 回调客户端
//                radioFMPlayerImpl.mService.radioFocusLost();
//            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
//                Log.d(TAG, "onAudioFocusChanged: 暂时失去音频焦点（短暂中断）");
//                // 暂时失去音频焦点（短暂中断）
//            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
//                Log.d(TAG, "onAudioFocusChanged: 暂时失去音频焦点（可以降低音量）");
//                // 暂时失去音频焦点（可以降低音量）
//                if (radioFMPlayerImpl.mPowerStatus == IRadioStatus.POWER_UP) {
//                    radioFMPlayerImpl.stop();
//                    radioFMPlayerImpl.mIsPowerDownByCall = true;
//                }
//                break;
//        }
    }
}
