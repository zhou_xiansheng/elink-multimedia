package com.elink.elinkmultimediaservices.service.manager.listeners;

import android.os.Handler;
import android.os.Looper;

import com.elink.bluetoothapi.BtJar;
import com.elink.bluetoothapi.data.BtStatus;
import com.elink.bluetoothapi.data.BtType;
import com.elink.bluetoothapi.data.CombineBluetoothDevice;
import com.elink.bluetoothapi.listener.IBluetoothCommonStateListener;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.common.EBtBState;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;
import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.service.MediaServices;
import com.elink.elinkmultimediaservices.service.manager.music.BtPlayManager;
import com.elink.elinkmultimediaservices.utils.LogUtils;

import java.util.List;
import java.util.Objects;

/**
 * 蓝牙连接状态监听
 */
public class BtCommonStateListener implements IBluetoothCommonStateListener {
    private final BtPlayManager btMusicManager;
    private final MediaServices mediaServices;

    private final Handler handler = new Handler(Looper.getMainLooper());
    private int ddlTime = 20;
    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (isBtConnect()) {
                MusicFile music = BtMusicInfoListener.getMusicData();
                if (Objects.equals(music.btName, "")) {
                    LogUtils.d("蓝牙连接，获取列表信息中。。。。");
                    getMusicInfo();
                } else {
                    //当前没有其他在播放就播放，检查开机恢复播放
                    ddlTime = 20;
                    mediaServices.btConnectStatusCallback(EBtBState.STATE_CONNECTED.getValue(), music);
                    if (!PlayManage.isMediaPlay6()) {
                        LogUtils.d("蓝牙连接上自动播放");
                        new Handler(Looper.getMainLooper()).postDelayed(() -> {
                            //会出现链接上自动播放声音先在手机上的情况，可能是连接还未完全。进行自动播放延时处理。
                            if (!PlayManage.isMediaPlay6()) {
                                btMusicManager.play();
                            } else {
                                LogUtils.d("延迟自动播放被打断！");
                            }
                        }, 4000);
                    }else {
                        LogUtils.d("其他音频在播放，不进行自动播放。");
                    }
                }
            }
        }
    };

    public void getMusicInfo() {
        if (ddlTime > 0) {
            ddlTime--;
            handler.postDelayed(runnable, 500);
        } else {
            ddlTime = 20;
            LogUtils.d("获取超时！");
        }
    }

    public BtCommonStateListener(BtPlayManager btMusicManager, MediaServices mediaServices) {
        this.btMusicManager = btMusicManager;
        this.mediaServices = mediaServices;
    }


    public static int isBtConnected = BtStatus.BT_STATUS_DISCONNECTED;//蓝牙连接状态
    private static String address = null;//蓝牙地址
    public static String devicesName = "";//蓝牙设备名称

    @Override
    public void onBluetoothStateChange(String s, int i) {
        LogUtils.d("");
    }

    @Override
    public void onBluetoothScanStateChange(String s, boolean b) {
        LogUtils.d("");
    }

    @Override
    public void onBluetoothDeviceListChange(String s, List<CombineBluetoothDevice> list) {
        boolean isIgnore = true;
        for (int i = 0; i < list.size(); i++) {
            CombineBluetoothDevice device = list.get(i);
            //获取第一个连接上的
            if (isBtConnect(device)) {
                if (!isBtConnect()) {
                    LogUtils.d("蓝牙链接上:" + device);
                    doAfterBtConnect(device);
                    //连接上立马回调 更新界面
                    MusicFile musicData = BtMusicInfoListener.getMusicData();
                    musicData.btName = device.getName();
                    btMusicManager.musicNow = musicData;
                    mediaServices.btConnectStatusCallback(EBtBState.STATE_CONNECTING.getValue(), btMusicManager.musicNow);
                    //获取音乐信息
                    getMusicInfo();
                    return;
                }
            } else if (isBtDisConnect(device)) {
                if (isBtConnect()) {
                    if (address != null && address.equals(device.getAddress())) {
                        LogUtils.d("蓝牙断开回调");
                        doAfterBtDisConnect();
                        if (PlayManage.sMusicSources == IPlaySource.BT) {
                            btMusicManager.btDisconnectPause();
                            mediaServices.btConnectStatusCallback(EBtBState.STATE_DISCONNECTED.getValue(), btMusicManager.musicNow);
                        }
                    }
                }
            }
            //可能是被忽略的情况，列表中将没有该设备
            if (list.get(i).getAddress().equals(address)) {
                isIgnore = false;
            }
        }
        if (isIgnore) {
            if (isBtConnect()) {
                LogUtils.d("蓝牙忽略回调");
                doAfterBtDisConnect();
                if (PlayManage.sMusicSources == IPlaySource.BT) {
                    btMusicManager.btDisconnectPause();
                    mediaServices.btConnectStatusCallback(EBtBState.STATE_DISCONNECTED.getValue(), btMusicManager.musicNow);
                }
            }
        }
    }

    @Override
    public void onDropFromPhone(String s) {
        LogUtils.d("");
    }

    @Override
    public void onPairStateChanged(String s, String s1, int i) {
        LogUtils.d("");
    }

    /**
     * 检查蓝牙连接状态
     *
     * @return 已连接蓝牙名称
     */
    public static boolean checkBtConnect() {
        LogUtils.d("");
        List<CombineBluetoothDevice> devices = BtJar.get().getBluetoothDeviceList(BtType.BT_TYPE_GOC);
        for (CombineBluetoothDevice device : devices) {
            if (isBtConnect(device)) {
                address = device.getAddress();
                isBtConnected = BtStatus.BT_STATUS_CONNECTED;
                devicesName = device.getName();
                return true;
            }
        }
        isBtConnected = BtStatus.BT_STATUS_DISCONNECTED;
        return false;
    }

    /**
     * 指定设备是否连接
     *
     * @param device
     * @return
     */
    public static boolean isBtConnect(CombineBluetoothDevice device) {
        if (device != null)
            return device.getStatus() == BtStatus.BT_STATUS_CONNECTED
                    || device.getStatus() == BtStatus.BT_STATUS_CONNECTED_NO_HEADSET;
        return false;
    }

    /**
     * 指定设备是否断开
     *
     * @param device
     * @return
     */
    public static boolean isBtDisConnect(CombineBluetoothDevice device) {
        if (device != null)
            return device.getStatus() == BtStatus.BT_STATUS_DISCONNECTED ||
                    device.getStatus() == BtStatus.BT_STATUS_CONNECTED_NO_A2DP;
        return false;
    }

    public static void doAfterBtConnect(CombineBluetoothDevice device) {
        address = device.getAddress();
        isBtConnected = BtStatus.BT_STATUS_CONNECTED;
        devicesName = device.getName();
    }

    public void doAfterBtDisConnect() {
        address = null;
        isBtConnected = BtStatus.BT_STATUS_DISCONNECTED;
        devicesName = "";
    }

    public static boolean isBtConnect() {
        return (isBtConnected == BtStatus.BT_STATUS_CONNECTED
                || isBtConnected == BtStatus.BT_STATUS_CONNECTED_NO_HEADSET);
    }
}
