package com.elink.elinkmultimediaservices.service.manager.music;

import android.content.ContentValues;

import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.common.EAudioSource;
import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.constants.common.IPlaybackStatus;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;
import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.beans.MusicPlayer;
import com.elink.elinkmultimediaservices.service.MediaServices;
import com.elink.elinkmultimediaservices.service.interfaces.IPlayManager;
import com.elink.elinkmultimediaservices.utils.LogUtils;

import cn.kuwo.autosdk.api.KWAPI;
import cn.kuwo.autosdk.api.OnEnterListener;
import cn.kuwo.autosdk.api.OnExitListener;
import cn.kuwo.autosdk.api.PlayState;
import cn.kuwo.autosdk.api.PlayerStatus;
import cn.kuwo.base.bean.Music;


public class KWPlayManager implements IPlayManager {
    private final KWAPI mKwApi;
    public MusicPlayer musicPlayer;
    public MusicFile musicNow;
    private final MediaServices services;
    private boolean mNeedPlayAfterEnter = false;
    private boolean isKwRunning = false;

    public KWPlayManager(MediaServices services) {
        this.services = services;
        musicPlayer = new MusicPlayer();
        musicNow = new MusicFile();

        KWAPI.createKWAPI(services, "multimedia");
        mKwApi = KWAPI.getKWAPI();
        setPlayStatus(mKwApi.getPlayerStatus());
        mKwApi.registerEnterListener(new OnEnterListener() {
            @Override
            public void onEnter() {
                isKwRunning = true;
                if (mNeedPlayAfterEnter) play();
                mNeedPlayAfterEnter = false;
            }
        });

        mKwApi.registerExitListener(new OnExitListener() {
            @Override
            public void onExit() {
                isKwRunning = false;
            }
        });
        mKwApi.registerConnectedListener(b -> {
            LogUtils.d("");
            if (mKwApi.isKuwoRunning(services)) {
                // 如果 在播放就回调
                if (convertPlayStatus(mKwApi.getPlayerStatus()) == IPlaybackStatus.STATUS_PLAYING) {
                    services.setAudioSources(EAudioSource.AUDIO_SOURCE_KUWO);
                    musicNow = convertMusicFile(mKwApi.getNowPlayingMusic());
                    services.kwPlayCallBack(musicNow);
                }
            }
        });
        mKwApi.registerPlayerStatusListener(services, (playerStatus, music) -> {
            setPlayStatus(playerStatus);
            Music nowPlayingMusic = mKwApi.getNowPlayingMusic();
            if (nowPlayingMusic != null) {
                String name = (String) nowPlayingMusic.getMVMusicContentValues(1).get("name");
                if (!name.equals(musicNow.fileName)) {
                    initIsVip();
                }
            }
            musicNow = convertMusicFile(music);
            if (playerStatus == PlayerStatus.PLAYING) {
                if (PlayManage.isBtCallPlay()) {
                    mKwApi.setPlayState(PlayState.STATE_PAUSE);
                } else {
                    services.setAudioSources(EAudioSource.AUDIO_SOURCE_KUWO);
                    services.unMute();
                    services.kwPlayCallBack(musicNow);
                }
            } else {
                services.pauseCallBack(musicNow);
            }
        });
        mKwApi.bindAutoSdkService(services);
    }

    private void setPlayStatus(PlayerStatus playerStatus) {
        if (playerStatus == PlayerStatus.PLAYING)
            PlayManage.setKwPlayStatus(EPlayStatus.STATUS_PLAYING);
        else
            PlayManage.setKwPlayStatus(EPlayStatus.STATUS_PAUSE);
    }

    public boolean canKwPlay() {
        if (mKwApi != null
                && (mKwApi.isKuwoRunning() || isKwRunning)
                && !PlayManage.isBtCallPlay()
                && !services.isHibernate) {
            return true;
        } else if (mKwApi == null) {
            LogUtils.d("mKwApi null !");
        } else if (!mKwApi.isKuwoRunning()) {
            LogUtils.d("mKwApi 未在运行中，打开应用。。。");
            mKwApi.startAPP(true);
        } else if (PlayManage.isBtCallPlay()) {
            LogUtils.d("蓝牙电话通话中 ！");
        } else if (services.isHibernate) {
            LogUtils.d("休眠中 ！");
        }
        return false;
    }

    @Override
    public void play() {
        LogUtils.d("");
        mNeedPlayAfterEnter = true;
        if (canKwPlay()) {
            mNeedPlayAfterEnter = false;
            if (!PlayManage.isKwPlay()) {
                mKwApi.setPlayState(PlayState.STATE_PLAY);
            }
        }
    }

    @Override
    public void pause() {
        if (canKwPlay()) {
            if (PlayManage.isKwPlay()) {
                mKwApi.setPlayState(PlayState.STATE_PAUSE);
            }
        }
    }

    private boolean isNextVipMusic = false;
    private boolean isPreVipMusic = false;

    private void initIsVip() {
        isPreVipMusic = false;
        isNextVipMusic = false;
    }

    /**
     * 切换歌曲后，
     * 过一段时间检查一次
     * 如果当前歌曲未发生改变即下一首为vip
     */
    public void checkVip() {
        services.mMediaHandler.handler.postDelayed(() -> {
            Music nowPlayingMusic = mKwApi.getNowPlayingMusic();
            if (nowPlayingMusic != null) {
                ContentValues contentValues = nowPlayingMusic.getMVMusicContentValues(1);
                Object name = contentValues.get("name");
                if (name.equals(musicNow.fileName)) isNextVipMusic = true;
            } else {
                LogUtils.w("music null !");
            }
        }, 100);
    }

    @Override
    public void next() {
        LogUtils.d("");
        if (canKwPlay()) {
            if (isNextVipMusic) {
                mKwApi.openOrCloseToast(true);
                LogUtils.d("vip music!");
            } else {
                mKwApi.setPlayState(PlayState.STATE_NEXT);
                checkVip();
            }
        }
    }

    @Override
    public void prev() {
        LogUtils.d("");
        if (canKwPlay()) {
            if (isPreVipMusic) {
                LogUtils.d("vip music!");
            } else {
                mKwApi.setPlayState(PlayState.STATE_PRE);
                checkVip();
            }
        }
    }

    @Override
    public void stop() {
    }

    public void onRelease() {
        mKwApi.unRegisterPlayerStatusListener();
    }


    /**
     * 将三方音乐应用播放状态转换为多媒体播放状态
     *
     * @return
     */
    private int convertPlayStatus(PlayerStatus playerStatus) {
        if (playerStatus == PlayerStatus.PLAYING)
            return IPlaybackStatus.STATUS_PLAYING;
        return IPlaybackStatus.STATUS_PAUSE;
    }

    /**
     * 将三方音乐对象转换为MusicFile
     *
     * @param music
     * @return
     */
    private MusicFile convertMusicFile(Music music) {
        MusicFile musicFile = new MusicFile();
        if (music != null) {
            musicFile.fileName = music.name;
            musicFile.playSource = IPlaySource.KW;
            musicFile.artist = music.artist;
            return musicFile;
        } else {
            LogUtils.w("music null !");
        }
        return musicFile;
    }
}
