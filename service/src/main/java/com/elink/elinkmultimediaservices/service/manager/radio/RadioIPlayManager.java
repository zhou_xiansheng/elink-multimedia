package com.elink.elinkmultimediaservices.service.manager.radio;

import android.annotation.SuppressLint;

import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.beans.RadioChannel;
import com.elink.elinkmultimediaservices.constants.common.IPlaybackStatus;
import com.elink.elinkmultimediaservices.constants.radio.IRadioStatus;
import com.elink.elinkmultimediaservices.service.MediaServices;
import com.elink.elinkmultimediaservices.service.audiofocus.AudioFocusManager;
import com.elink.elinkmultimediaservices.service.audiofocus.FMOnFocusChangedListener;
import com.elink.elinkmultimediaservices.service.manager.radio.impl.RadioControllerImpl;
import com.elink.elinkmultimediaservices.service.manager.radio.interfaces.IRadioPlayer;
import com.elink.elinkmultimediaservices.utils.LogUtils;
import com.elink.elinkmultimediaservices.utils.SPUtils;

import java.util.List;

public class RadioIPlayManager implements IRadioPlayer {
    public final MediaServices mMediaServices;
    public RadioControllerImpl mRadioController;
    private final AudioFocusManager.OnFocusChangedListener mOnFocusChangedListener;
    public RadioChannel mCurrentChannel = RadioChannel.DEFAULT_50HZ_CHANNEL;//当前的FM
    public int mSeekStatus = IRadioStatus.SEEK_FINISHED;//搜索
    public int mScanStatus = IRadioStatus.SCAN_FINISHED;//扫描
//    public boolean mIsPowerDownByCall = false;//被打电话中断
//    public boolean mReplaySelectChannelIfNeed = false; //避免播放特定频道时受 SEEK_FINISHED 影响
    public boolean mIsRadioMuted = false;//是否静音

    @SuppressLint("WrongConstant")
    public RadioIPlayManager(MediaServices mediaServices) {
        this.mMediaServices = mediaServices;
        mRadioController = new RadioControllerImpl(this);
        mOnFocusChangedListener = new FMOnFocusChangedListener(this);
        AudioFocusManager.getInstance().registerAudioFocusChangedListener(mOnFocusChangedListener);
    }

    /**
     * mPowerStatus == IRadioStatus.POWER_DOWN
     *
     * @return
     */
    public boolean canFmPlay() {
        if (PlayManage.isBtCallPlay()
                || mMediaServices.isHibernate
                || mSeekStatus == IRadioStatus.SEEK_RUNNING
                || mRadioController.isScanning()
                || mRadioController.isSeeking()) {
            if (PlayManage.isBtCallPlay()) {
                LogUtils.d("蓝牙通话中");
            } else if (mMediaServices.isHibernate) {
                LogUtils.d("车机休眠中");
            } else if (mSeekStatus == IRadioStatus.SEEK_RUNNING) {
                LogUtils.d("搜索中。。。。");
            } else {
                LogUtils.d("其他原因");
            }
            return false;
        }
        return true;
    }

    private boolean isChannelUpdate = false;

    @Override
    public void play() {
        if (!canFmPlay()) return;
        if (AudioFocusManager.getInstance().requestAudioFocus()) {
            if (isChannelUpdate) {
                mRadioController.tuneChannelAsync(mCurrentChannel.getChannelNum());
                isChannelUpdate = false;
            } else {
                LogUtils.d("mCurrentChannel:" + mCurrentChannel);
                mRadioController.powerUpAsync(mCurrentChannel.getChannelNum());
            }
            mMediaServices.onFMPlaybackStatusChangedCallBack(IPlaybackStatus.STATUS_PLAYING, mCurrentChannel);
            mMediaServices.unMute();
        }
    }

    /**
     * 播放指定电台
     *
     * @param channel 频道
     */
    @Override
    public void playSpecialChannel(RadioChannel channel) {
        if (!canFmPlay() || channel == null) {
            return;
        }
        if (AudioFocusManager.getInstance().requestAudioFocus()) {
//            if (mSeekStatus != IRadioStatus.SEEK_FINISHED) {
//                mReplaySelectChannelIfNeed = true;
//            }
            mCurrentChannel = channel;
            SPUtils.put(SPUtils.LATEST_CHANNEL, channel.getChannelNum());
            isChannelUpdate = true;
            LogUtils.d("mCurrentChannel:" + mCurrentChannel);
            play();
        }
    }


    /**
     * 上一个可用电台
     */
    @Override
    public void prev() {
        if (!canFmPlay())
            return;
        if (mRadioController.mPowerStatus == IRadioStatus.POWER_UP
                && AudioFocusManager.getInstance().requestAudioFocus()) {
            LogUtils.d("");
            mMediaServices.onFMPlaybackStatusChangedCallBack(IPlaybackStatus.STATUS_PAUSE, mCurrentChannel);
            stopScanAndSeek();
            if (mCurrentChannel != null) {
                mRadioController.seekChannelAsync(mCurrentChannel.getChannelNum(), false);
            }
        }
    }

    /**
     * 下一个可用电台
     */
    @Override
    public void next() {
        if (!canFmPlay())
            return;
        if (mRadioController.mPowerStatus == IRadioStatus.POWER_UP
                && AudioFocusManager.getInstance().requestAudioFocus()) {
            LogUtils.d("");
            mMediaServices.onFMPlaybackStatusChangedCallBack(IPlaybackStatus.STATUS_PAUSE, mCurrentChannel);
            stopScanAndSeek();
            if (mCurrentChannel != null) {
                mRadioController.seekChannelAsync(mCurrentChannel.getChannelNum(), true);
            }
        }
    }

    @Override
    public void stop() {
        LogUtils.d("");
        mRadioController.setMuteStatus(true);
        stopScanAndSeek();
        mRadioController.powerDownAsync();
        AudioFocusManager.getInstance().releaseAudioFocus();
        mMediaServices.onFMPlaybackStatusChangedCallBack(IPlaybackStatus.STATUS_PAUSE, mCurrentChannel);
    }

    @Override
    public void pause() {
        LogUtils.d("");
        stopScanAndSeek();
        mRadioController.powerDownAsync();
        AudioFocusManager.getInstance().releaseAudioFocus();
        mMediaServices.onFMPlaybackStatusChangedCallBack(IPlaybackStatus.STATUS_PAUSE, mCurrentChannel);
    }

    public void temporarilyLoseFocusStop() {
        LogUtils.d("");
        stopScanAndSeek();
        mRadioController.powerDownAsync();
        mMediaServices.onFMPlaybackStatusChangedCallBack(IPlaybackStatus.STATUS_PAUSE, mCurrentChannel);
    }


    @Override
    public boolean isSearching() {
        return mRadioController.isScanning();
    }


    @Override
    public void stopScanAndSeek() {
        LogUtils.d("");
        if (isSearching() || mRadioController.isSeeking())
            mRadioController.stopScanAndSeek();
    }

    /**
     * 停止搜索并播放之前的电台
     */
    public void stopScan() {
        LogUtils.d("停止搜索并播放之前的电台");
        stopScanAndSeek();
    }

    @Override
    public void release() {
        AudioFocusManager.getInstance().unregisterAudioFocusChangedListener(mOnFocusChangedListener);
        mRadioController = null;
    }


    /**
     * 搜可用电台
     */
    @Override
    public void searchStableChannel() {
        mMediaServices.onFMPlaybackStatusChangedCallBack(IPlaybackStatus.STATUS_PAUSE, mCurrentChannel);
        stopScanAndSeek();
        mRadioController.startAutoScanAsync();
    }

    @Override
    public void setChannelCollection(RadioChannel radioChannel) {
        if (radioChannel != null) {
            RadioChannel temp = null;
            if (temp != null) {
                //说明已经存在了，更新收藏状态
                temp.setCollected(!temp.isCollected());
            } else {
                //说明尚未收藏,收藏，来源是收藏
                radioChannel.setCollected(true);
                radioChannel.setFrom(RadioChannel.FROM_TYPE_COLLECT);
            }
        } else {
            LogUtils.d("radioChannel null");
        }
    }

    @Override
    public List<RadioChannel> getCollectionList() {
        return null;
    }

    @Override
    public List<RadioChannel> getSearchedList() {
        return null;
    }

    @Override
    public int getPowerStatus() {
        return mRadioController.mPowerStatus;
    }
}
