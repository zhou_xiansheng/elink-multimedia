package com.elink.elinkmultimediaservices.service.manager.listeners;

import static com.elink.elinkmultimediaservices.service.manager.music.BtPlayManager.isBtTerminated;

import com.elink.bluetoothapi.BtJar;
import com.elink.bluetoothapi.data.BtCallState;
import com.elink.bluetoothapi.data.BtType;
import com.elink.bluetoothapi.data.HfpClientCall;
import com.elink.bluetoothapi.listener.IBluetoothCallListener;
import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.constants.wrapper.PlayStatusWrapper;
import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.service.MediaServices;
import com.elink.elinkmultimediaservices.utils.LogUtils;

import java.util.List;

public class BtCallListener implements IBluetoothCallListener {
    private final MediaServices mediaServices;
    private boolean mNeedPlayAfterCall = false;//挂断电话后是否需要继续播放

    public BtCallListener(MediaServices mediaServices) {
        this.mediaServices = mediaServices;
    }

    @Override
    public void onCallStateChange(String s, List<HfpClientCall> list) {
        LogUtils.d("size:" + list.size());
        if (isBtPhoning(list)) {
            if (list.size() > 0 && list.get(0).isWeChatCall()) {
                LogUtils.d("微信电话");
            }
            if (!PlayManage.isBtCallPlay()) {
                LogUtils.d("打电话中。。。。");
                PlayManage.setBtCallStatus(EPlayStatus.STATUS_PLAYING);
                mediaServices.btCallStatusChangeCallback(new PlayStatusWrapper(EPlayStatus.STATUS_PLAYING));
                if (PlayManage.isMediaPlay2()) {
                    LogUtils.d("before call up music is playing");
                    mNeedPlayAfterCall = true;
                }
            }
        } else if (isBtTerminated(list)) {
            LogUtils.d("电话挂断");
            PlayManage.setBtCallStatus(EPlayStatus.STATUS_PAUSE);
            mediaServices.btCallStatusChangeCallback(new PlayStatusWrapper(EPlayStatus.STATUS_PAUSE));
            if (mNeedPlayAfterCall) {
                LogUtils.d("电话结束 ，恢复之前的播放");
                mNeedPlayAfterCall = false;
                mediaServices.play();
            }
        }
    }

    @Override
    public void onCallAudioStateChange(String s, boolean b) {

    }

    /**
     * 通过遍历蓝牙设备的连接状态，可以找到具体的设备
     *
     * @param hfpClientCalls
     * @return
     */
    public static boolean isBtPhoning(List<HfpClientCall> hfpClientCalls) {
        for (int i = 0; i < hfpClientCalls.size(); i++) {
            HfpClientCall device = hfpClientCalls.get(i);
            if (device.getState() == BtCallState.CALL_STATE_INCOMING
                    || device.getState() == BtCallState.CALL_STATE_DIALING
                    || device.getState() == BtCallState.CALL_STATE_ALERTING
                    || device.getState() == BtCallState.CALL_STATE_ACTIVE)
                return true;
        }
        return false;
    }

    /**
     * 通过判断蓝牙通话中设备的数量，不能造出具体的设备
     *
     * @return 是否通话
     */
    public static boolean isBtPhoning() {
        return BtJar.get().getCallList(BtType.BT_TYPE_GOC).size() > 0;
    }
}
