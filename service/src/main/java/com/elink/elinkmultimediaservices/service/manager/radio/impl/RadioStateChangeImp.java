package com.elink.elinkmultimediaservices.service.manager.radio.impl;

import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.beans.RadioChannel;
import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.constants.common.IPlaybackStatus;
import com.elink.elinkmultimediaservices.constants.radio.ERadioStatus;
import com.elink.elinkmultimediaservices.constants.radio.IRadioStatus;
import com.elink.elinkmultimediaservices.constants.radio.IRadioType;
import com.elink.elinkmultimediaservices.service.audiofocus.AudioFocusManager;
import com.elink.elinkmultimediaservices.service.manager.radio.RadioIPlayManager;
import com.elink.elinkmultimediaservices.service.manager.radio.interfaces.IRadioStateChanged;
import com.elink.elinkmultimediaservices.utils.LogUtils;
import com.elink.elinkmultimediaservices.utils.SPUtils;

import java.util.ArrayList;
import java.util.Arrays;

public class RadioStateChangeImp implements IRadioStateChanged {
    private final RadioIPlayManager mManager;

    public RadioStateChangeImp(RadioIPlayManager playManager) {
        this.mManager = playManager;
    }

    @Override
    public void onSeekStatusChanged(int status, int station) {
        mManager.mSeekStatus = status;
        switch (status) {
            case IRadioStatus.SEEK_RUNNING:
                //回调客户端执行动画
                mManager.mMediaServices.onSeekStatusChanged(ERadioStatus.SEEK_RUNNING);
                break;
            case IRadioStatus.SEEK_FINISHED:
                mManager.mMediaServices.onSeekStatusChanged(ERadioStatus.SEEK_FINISHED);
//                if (mManager.mReplaySelectChannelIfNeed) {
//                    mManager.playSpecialChannel(mManager.mCurrentChannel);
//                    mManager.mReplaySelectChannelIfNeed = false;
//                    return;
//                }
                mManager.mCurrentChannel = new RadioChannel(station, IRadioType.FM);
                SPUtils.put(SPUtils.LATEST_CHANNEL, station);
                break;
        }
    }

    @Override
    public void onScanStatusChanged(int status, int[] stations) {
        switch (status) {
            case IRadioStatus.SCAN_RUNNING:
                break;
            case IRadioStatus.SCAN_FINISHED:
                LogUtils.d("扫描结束回调逻辑:" + Arrays.toString(stations));
                //扫描结束
                ArrayList<RadioChannel> radioChannels = new ArrayList<>();
                if (stations == null) {
                    mManager.mMediaServices.onSearchedListChanged(IPlaybackStatus.STATUS_PAUSE, radioChannels);
                    return;
                } else {
                    //封装搜索到的电台
                    RadioChannel channel;
                    for (int sta : stations) {
                        LogUtils.d("station:" + sta);
                        channel = new RadioChannel();
                        channel.setFrom(RadioChannel.FROM_TYPE_SEARCHED);
                        channel.setCollected(false);
                        channel.setChannelNum(sta);
                        radioChannels.add(channel);
                    }
                    mManager.mMediaServices.onSearchedListChanged(IPlaybackStatus.STATUS_PLAYING, radioChannels);
                    //继续播放之前的电台
                    mManager.stopScanAndSeek();
                    mManager.playSpecialChannel(mManager.mCurrentChannel);
                }
                break;
            default:
        }
    }

    @Override
    public void onPowerStatusChanged(int status) {

        switch (status) {
            case IRadioStatus.POWER_DOWN:
                PlayManage.setFmPlayStatus(EPlayStatus.STATUS_PAUSE);
                break;
            case IRadioStatus.POWERING_UP:
                PlayManage.setFmPlayStatus(EPlayStatus.STATUS_PLAY_PREPARING);
                break;
            case IRadioStatus.POWER_UP:
                PlayManage.setFmPlayStatus(EPlayStatus.STATUS_PLAYING);
                break;
        }
        mManager.mRadioController.mPowerStatus = status;
    }

    @Override
    public void onTuneStatusChanged(int status) {
        switch (status) {
            case IRadioStatus.TUNE_RUNNING:
                break;
            case IRadioStatus.TUNE_FINISHED:
                if (AudioFocusManager.getInstance().isFocusGained()) {
                    mManager.mRadioController.setMuteStatus(false);
                }
                break;
        }
    }
}
