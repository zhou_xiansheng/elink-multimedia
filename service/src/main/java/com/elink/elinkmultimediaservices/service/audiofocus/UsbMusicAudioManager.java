package com.elink.elinkmultimediaservices.service.audiofocus;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.service.manager.music.UsbPlayManager;
import com.elink.elinkmultimediaservices.utils.LogUtils;
import com.elink.elinkmultimediaservices.utils.MyApplication;

public class UsbMusicAudioManager extends PhoneStateListener implements AudioManager.OnAudioFocusChangeListener {

    private UsbPlayManager musicManager;


    private AudioFocusRequest audioFocusRequest;
    private TelephonyManager telephonyManager;
    private AudioManager audioManager;

    public UsbMusicAudioManager(UsbPlayManager musicManager) {
        this.musicManager = musicManager;
        audioManager = (AudioManager) MyApplication.getContext().getSystemService(Context.AUDIO_SERVICE);
        AudioAttributes audioAttr = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build();
        audioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                .setAudioAttributes(audioAttr)
                .setAcceptsDelayedFocusGain(true)
                .setOnAudioFocusChangeListener(this, new Handler())
                .build();


        telephonyManager = (TelephonyManager) MyApplication.getContext().getSystemService(Context.TELEPHONY_SERVICE);
    }

    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                LogUtils.d("重新获得焦点");
                // 重新获得焦点
                if (musicManager.player == null) {
                    musicManager.initMediaPlay(musicManager.musicNow);
                    musicManager.mediaPlayerSeekTo(musicManager.musicPlayer.getCurrentPosition());
                } else {
                    musicManager.play();
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                LogUtils.d("失去焦点");
                //暂停播放
                if (PlayManage.isUsbPlay()) {
                    musicManager.pause2();
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                LogUtils.d("暂时失去音频焦点，很快会重新获得");
                // 暂时失去音频焦点，很快会重新获得
                //暂停播放
                musicManager.temporarilyLoseFocusPause();
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                LogUtils.d("暂时失去焦点，但可以“低声播放");
                // 暂时失去焦点，但可以“低声播放”（duck）
                if (musicManager.player.isPlaying())
                    new Handler(Looper.getMainLooper()).post(() -> {
                        musicManager.player.setVolume(0.1f, 0.1f);
                    });
                break;
            case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE:
                LogUtils.d("AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE");
                break;
        }
    }

    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        super.onCallStateChanged(state, incomingNumber);
        switch (state) {
            case TelephonyManager.CALL_STATE_RINGING:
                LogUtils.d("CALL_STATE_RINGING");
                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:
                LogUtils.d("CALL_STATE_OFFHOOK");
                // 电话响铃或电话正在进行中，暂停音乐
//                    pausePlayback();
                break;
            case TelephonyManager.CALL_STATE_IDLE:
                LogUtils.d("CALL_STATE_IDLE");
                // 电话结束，检查是否持有音频焦点，如果是则恢复播放
//                    if (mAudioManager.isMusicActive()) {
//                        resumePlayback();
//                    }
                break;
        }
    }


    public boolean requestAudioFocus() {
        int result;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // 使用AudioFocusRequest的方法请求音频焦点（API 26及以上）
            result = audioManager.requestAudioFocus(audioFocusRequest);
        } else {
            // 使用旧版本的方法请求音频焦点（API 25及以下）
            result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        }
        return result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED;
    }


    public void abandonAudioFocus() {
        audioManager.abandonAudioFocus(this);
        // Android O及以上版本
        audioManager.abandonAudioFocusRequest(audioFocusRequest);
    }
}
