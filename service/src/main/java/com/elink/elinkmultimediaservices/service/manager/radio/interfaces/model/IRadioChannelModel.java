package com.elink.elinkmultimediaservices.service.manager.radio.interfaces.model;

import com.elink.elinkmultimediaservices.beans.RadioChannel;

import java.util.List;

public interface IRadioChannelModel{
    /**
     * 插入一条记录
     *
     * @param channel 频道号
     * @return 操作成功与否
     */
    boolean insert(RadioChannel channel);

    /**
     * 更新一条记录
     *
     * @param channel 频道号
     * @return 操作成功与否
     */
    boolean update(RadioChannel channel);

    /**
     * 删除一条记录
     *
     * @param channel 频道号
     * @return 操作成功与否
     */
    boolean delete(RadioChannel channel);

    /**
     * 查询收藏的频道
     *
     * @return 已收藏的频道
     */
    List<RadioChannel> queryCollectedChannels();

    /**
     * 查询搜索到的电台频道
     *
     * @return 搜索到的电台频道
     */
    List<RadioChannel> querySearchedList();

    /**
     * 根据频道号查询频道记录
     *
     * @param channelNum 频道号
     * @return 搜索到的电台频道
     */
    RadioChannel queryChannelByNum(int channelNum);

    /**
     * 清空搜索到的频道
     */
    void deleteAllSearched();
}
