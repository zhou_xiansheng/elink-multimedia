package com.elink.elinkmultimediaservices.service.manager.listeners;

import static com.elink.elinkmultimediaservices.service.manager.music.BtPlayManager.CHANGE_MUSIC_MESSAGE;
import static com.elink.elinkmultimediaservices.service.manager.music.BtPlayManager.HANDLE_MESSAGE;

import android.os.Message;

import com.elink.bluetoothapi.BtJar;
import com.elink.bluetoothapi.data.BtType;
import com.elink.bluetoothapi.data.MusicData;
import com.elink.bluetoothapi.listener.IBluetoothMusicInfoListener;
import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;
import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.service.manager.cache.BtMusicDataCache;
import com.elink.elinkmultimediaservices.service.manager.music.BtPlayManager;
import com.elink.elinkmultimediaservices.utils.LogUtils;

import java.util.Objects;

public class BtMusicInfoListener implements IBluetoothMusicInfoListener {
    private final BtPlayManager btMusicManager;
    public final BtMusicDataCache<Integer, MusicData> btMusicDataCache;
    public MusicData lastMusicData;

    private int infoNum = 0;
    /**
     * 将静音后600毫秒内的回调丢弃。
     * 当静音后，苹果手机可能还会发一个播放的回调，静音前发送，但是在暂停后才收到。（将歌词当作title传过来为一个触发条件）
     */
    public boolean discard = false;
    /**
     * 暂停后的第一次回调不考虑 播放状态，
     */
    public boolean firstCallbackAfterPause = false;

    public BtMusicInfoListener(BtPlayManager btMusicManager) {
        btMusicDataCache = new BtMusicDataCache<>(10);
        lastMusicData = BtJar.get().getMusicData(BtType.BT_TYPE_GOC);
        this.btMusicManager = btMusicManager;
    }

    @Override
    public void onMusicInfoChange(String s, MusicData musicData) {
        infoNum++;
        btMusicDataCache.put(infoNum, musicData);
//        LogUtils.d("infoNum:" + infoNum
//                + " title:" + musicData.getTitle()
//                + " artist:" + musicData.getArtist()
//                + " isPlaying:" + musicData.isPlaying());
        if (BtCallListener.isBtPhoning() || PlayManage.isBtCallPlay()) {
            LogUtils.d("蓝牙通话中");
            if (!"".equals(musicData.getTitle())) {
                lastMusicData = musicData;
            }
            return;
        }
        try {
            EPlayStatus status = musicData.isPlaying() ? EPlayStatus.STATUS_PLAYING : EPlayStatus.STATUS_PAUSE;
            if (!PlayManage.isMediaPlay6()) {
                MusicFile musicFile = getMusicData(musicData);
                if (musicData.isPlaying() != lastMusicData.isPlaying()) {
                    //处理播放暂停操作
                    PlayManage.setBtPlayStatus(status);
                    btMusicManager.musicNow = musicFile;
                    btMusicManager.musicInfoChangeHandler.removeCallbacksAndMessages(null);
                    Message message = btMusicManager.musicInfoChangeHandler.obtainMessage(HANDLE_MESSAGE);
                    message.arg2 = infoNum;
                    btMusicManager.musicInfoChangeHandler.sendMessageDelayed(message, 2900);
                } else if (status == EPlayStatus.STATUS_PLAYING) {
                    //过滤，将相同的冲掉
                    Message message = btMusicManager.musicInfoChangeHandler.obtainMessage(CHANGE_MUSIC_MESSAGE);
                    message.arg2 = infoNum;
                    btMusicManager.musicInfoChangeHandler.sendMessageDelayed(message, 500);
                }
            } else if (musicData.isPlaying()) {
                // 将 蓝牙音乐静音 再暂停
                LogUtils.d("当前其他音源在播放，手机禁止播放蓝牙音乐！");
                PlayManage.showStatus();
                btMusicManager.musicInfoChangeHandler.removeMessages(CHANGE_MUSIC_MESSAGE);
                BtJar.get().muteMusic(BtType.BT_TYPE_GOC, true);
                BtJar.get().pauseMusic(BtType.BT_TYPE_GOC);
                PlayManage.setBtPlayStatus(EPlayStatus.STATUS_PAUSE);
            }
            if (!"".equals(musicData.getTitle())) {
                lastMusicData = musicData;
            }
        } catch (Exception e) {
            LogUtils.e(e.getMessage(), e);
        }
    }

    @Override
    public void onIsPlayingChange(String s, Boolean aBoolean) {

    }

    /**
     * 封装蓝牙信息
     *
     * @return
     */
    public static MusicFile getMusicData() {
        MusicFile musicFile = new MusicFile();
        musicFile.playSource = IPlaySource.BT;
        if (BtCommonStateListener.isBtConnect()) {
            MusicData musicData = BtJar.get().getMusicData(BtType.BT_TYPE_GOC);
            if (!Objects.equals(musicData.getTitle(), ""))
                musicFile.fileName = musicData.getTitle();
            if (!Objects.equals(musicData.getArtist(), ""))
                musicFile.artist = musicData.getArtist();
            musicFile.duration = musicData.getSongLen();
            musicFile.lastPlayProgress = musicData.getSongPos();
            musicFile.isPlaying = musicData.isPlaying();
            musicFile.btName = musicData.getConnectedBtName();//用这个字段存储连接的蓝牙名称
        }
        return musicFile;
    }

    public static MusicFile getMusicData(MusicData musicData) {
        MusicFile musicFile = new MusicFile();
        musicFile.playSource = IPlaySource.BT;
        if (!Objects.equals(musicData.getTitle(), "")) {
            musicFile.fileName = musicData.getTitle();
        }
        if (!Objects.equals(musicData.getArtist(), "")) {
            musicFile.artist = musicData.getArtist();
        }
        musicFile.duration = musicData.getSongLen();
        musicFile.lastPlayProgress = musicData.getSongPos();
        musicFile.isPlaying = musicData.isPlaying();
        musicFile.albumCover = musicData.getConnectedBtName();//用这个字段存储连接的蓝牙名称
        return musicFile;
    }
}
