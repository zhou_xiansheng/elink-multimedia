package com.elink.elinkmultimediaservices.service.manager.radio.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.elink.elinkmultimediaservices.service.manager.radio.impl.RadioControllerImpl;

public class FmBroadcastReceiver extends BroadcastReceiver {

    private RadioControllerImpl mRadioController;

    public FmBroadcastReceiver(RadioControllerImpl mRadioController) {
        this.mRadioController = mRadioController;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_SHUTDOWN.equals(intent.getAction())) {
            mRadioController.release();
        }
    }
}
