package com.elink.elinkmultimediaservices.beans;

import android.os.Parcel;
import android.os.Parcelable;
import com.elink.elinkmultimediaservices.constants.radio.IRadioType;

/**
 * 广播频道
 */
public class RadioChannel implements Parcelable {
    public static final int FROM_TYPE_UNKNOWN= 0;
    public static final int FROM_TYPE_SEARCHED = 1;
    public static final int FROM_TYPE_COLLECT = 2;
    int id;
    int channelNum;//频道号
    int type = IRadioType.FM;//频道类型
    int from = FROM_TYPE_UNKNOWN;//来源:0:Unknown    1:Searched    2:Collect
    boolean isCollected =false;//是否收藏

    public static final RadioChannel DEFAULT_50HZ_CHANNEL;

    static {
        DEFAULT_50HZ_CHANNEL = new RadioChannel();
        DEFAULT_50HZ_CHANNEL.setChannelNum(875);
        DEFAULT_50HZ_CHANNEL.setType(IRadioType.FM);
    }

    public RadioChannel(int channelNum, int type){
        this.channelNum = channelNum;
        this.type = type;
    }

    public RadioChannel(){
    }
    protected RadioChannel(Parcel in) {
        id = in.readInt();
        channelNum = in.readInt();
        type = in.readInt();
        from = in.readInt();
        isCollected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(channelNum);
        dest.writeInt(type);
        dest.writeInt(from);
        dest.writeByte((byte) (isCollected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RadioChannel> CREATOR = new Creator<RadioChannel>() {
        @Override
        public RadioChannel createFromParcel(Parcel in) {
            return new RadioChannel(in);
        }

        @Override
        public RadioChannel[] newArray(int size) {
            return new RadioChannel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getChannelNum() {
        return channelNum;
    }

    public void setChannelNum(int channelNum) {
        this.channelNum = channelNum;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public boolean isCollected() {
        return isCollected;
    }

    public void setCollected(boolean collected) {
        isCollected = collected;
    }

    @Override
    public String toString() {
        return "RadioChannel{" +
                "id=" + id +
                ", channelNum=" + channelNum +
                ", type=" + type +
                ", from=" + from +
                ", isCollected=" + isCollected +
                '}';
    }
}
