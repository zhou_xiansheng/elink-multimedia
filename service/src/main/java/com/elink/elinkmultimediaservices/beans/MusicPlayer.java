package com.elink.elinkmultimediaservices.beans;


import com.elink.elinkmultimediaservices.utils.LogUtils;
import com.elink.elinkmultimediaservices.utils.SPUtils;

/**
 * 音乐播放配置类，包含播放状态，播放列表，播放模式等信息。
 */
public class MusicPlayer {
    public static final String FIELD_MUSIC_PLAYER = "field_usb_music_player";
    public int mPlayMode = 0; // 当前的播放模式，默认为列表循环
    public int mPlayIndex;//播放下标
    public int mDuration;//当前歌曲时长
    public int mCurrentPosition;//当前播放的位置

    public void setPlayMode(int playMode) {
        this.mPlayMode = playMode;
        SPUtils.put(FIELD_MUSIC_PLAYER, this);
    }

    public void setPlayIndex(int playIndex) {
        if (mPlayIndex != playIndex) {
            LogUtils.d(mPlayIndex + "------->" + playIndex);
            mPlayIndex = playIndex;
            SPUtils.put(FIELD_MUSIC_PLAYER, this);
        }
    }

    public void setDuration(int duration) {
        this.mDuration = duration;
    }

    public void setCurrentPosition(int currentPosition) {
        this.mCurrentPosition = currentPosition;
    }

    public int getPlayMode() {
        return mPlayMode;
    }

    public int getPlayIndex() {
        return mPlayIndex;
    }

    public int getDuration() {
        return mDuration;
    }

    public int getCurrentPosition() {
        return mCurrentPosition;
    }

    @Override
    public String toString() {
        return "MusicPlayer{" +
                "playMode=" + mPlayMode +
                ", index=" + mPlayIndex +
                ", duration=" + mDuration +
                ", currentPosition=" + mCurrentPosition +
                '}';
    }
}



