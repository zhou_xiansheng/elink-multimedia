package com.elink.elinkmultimediaservices.beans;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import com.elink.elinkmultimediaservices.constants.music.IPlaySource;

import org.litepal.crud.DataSupport;

import java.util.Objects;

public class MusicFile extends DataSupport implements Parcelable {
    public int id;//数据库id
    public String path = "";//绝对路径
    public String fileName = "未知曲目";//歌名 暂无曲目

    public String artist = "未知歌手";  //专辑艺术家敬请期待
    public String albumCover = ""; //专辑封面路径
    public String lyricPath = null; //歌词 null:没有检查过 “”：无歌词 “path”:有歌词
    public int playSource = IPlaySource.UNKNOWN;//文件来源
    public int duration = 0; //歌曲总时长
    public long playProgress = 0; //播放进度
    public long lastPlayProgress = 0;  //最后一次播放时间
    public boolean isSelected = false; //是否被选中
    public boolean isDir;//是否是目录
    public boolean isValid = true;//是否可用，可播放

    public boolean isPlaying = false;//是否当前播放，高亮有用
    public String btName = "";


    public MusicFile() {
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        MusicFile musicFile = (MusicFile) obj;
        // 根据具体的业务逻辑比较对象的内容
        return Objects.equals(path, musicFile.path);
    }

    protected MusicFile(Parcel in) {
        id = in.readInt();
        artist = in.readString();
        albumCover = in.readString();
        duration = in.readInt();
        playProgress = in.readLong();
        lyricPath = in.readString();
        lastPlayProgress = in.readLong();
        isSelected = in.readByte() != 0;
        fileName = in.readString();
        path = in.readString();
        isDir = in.readByte() != 0;
        isValid = in.readByte() != 0;
        playSource = in.readInt();
        btName = in.readString();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            isPlaying = in.readBoolean();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(artist);
        dest.writeString(albumCover);
        dest.writeInt(duration);
        dest.writeLong(playProgress);
        dest.writeString(lyricPath);
        dest.writeLong(lastPlayProgress);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeString(fileName);
        dest.writeString(path);
        dest.writeByte((byte) (isDir ? 1 : 0));
        dest.writeByte((byte) (isValid ? 1 : 0));
        dest.writeInt(playSource);
        dest.writeString(btName);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            dest.writeBoolean(isPlaying);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MusicFile> CREATOR = new Creator<MusicFile>() {
        @Override
        public MusicFile createFromParcel(Parcel in) {
            return new MusicFile(in);
        }

        @Override
        public MusicFile[] newArray(int size) {
            return new MusicFile[size];
        }
    };

    @Override
    public String toString() {
        return "MusicFile{" +
                "id=" + id +
                ", artist='" + artist + '\'' +
                ", albumCover='" + albumCover + '\'' +
                ", duration=" + duration +
                ", playbackProgress=" + playProgress +
                ", lyricPath='" + lyricPath + '\'' +
                ", lastPlayTime=" + lastPlayProgress +
                ", isSelected=" + isSelected +
                ", fileName='" + fileName + '\'' +
                ", path='" + path + '\'' +
                ", isDir=" + isDir +
                ", isValid=" + isValid +
                ", playSource=" + playSource +
                ", isPlaying=" + isPlaying +
                ", btName==" + btName +
                '}';
    }

    @Override
    public int hashCode() {
        return path.hashCode();
    }
}
