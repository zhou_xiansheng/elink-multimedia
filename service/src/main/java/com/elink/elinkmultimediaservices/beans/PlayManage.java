package com.elink.elinkmultimediaservices.beans;

import com.elink.elinkmultimediaservices.constants.common.EAudioSource;
import com.elink.elinkmultimediaservices.constants.common.IPlayMode;
import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.constants.common.EUsbStatus;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;
import com.elink.elinkmultimediaservices.utils.LogUtils;
import com.elink.elinkmultimediaservices.utils.SPUtils;

/**
 *
 */
public class PlayManage {
    //SP中存储的字段
    public static String IS_CLIENT_OPEN = "IS_CLIENT_OPEN";
    public static String AUDIO_SOURCE = "AUDIO_SOURCE";
    public static String PLAY_MODE = "PLAY_MODE";
    public static String MUSIC_SOURCE = "MUSIC_SOURCE";
    public static String USB_STATUS = "USB_STATUS";
    public static String BT_STATUS = "BT_STATUS";
    public static String FM_STATUS = "FM_STATUS";
    public static String KW_STATUS = "KW_STATUS";
    //关机前客户端是否在运行
    public static boolean sIsClientOpen = false;
    public static EUsbStatus mUsbStatus = EUsbStatus.UNKNOWN;

    // 播放状态
    public static EPlayStatus sUsbStatus = EPlayStatus.STATUS_PAUSE;
    public static EPlayStatus sBtStatus = EPlayStatus.STATUS_PAUSE;
    public static EPlayStatus sBtCallStatus = EPlayStatus.STATUS_PAUSE;
    public static EPlayStatus sFmStatus = EPlayStatus.STATUS_PAUSE;
    public static EPlayStatus sKwStatus = EPlayStatus.STATUS_PAUSE;
    public static EPlayStatus sVideoStatus = EPlayStatus.STATUS_PAUSE;
    //播放源
    public static EAudioSource sAudioSource = EAudioSource.AUDIO_SOURCE_UNKNOWN;
    public static EAudioSource sResumeSource = EAudioSource.AUDIO_SOURCE_UNKNOWN;
    public static Integer sPlayMode = IPlayMode.PLAY_MODE_UNKNOWN;
    public static Integer sMusicSources = IPlaySource.UNKNOWN;


    public static void setSpData(boolean clientState, EAudioSource audioSource,
                                 int playMode, int musicSources,
                                 EPlayStatus usbStatus, EPlayStatus btStatus, EPlayStatus fmStatus) {
        sIsClientOpen = clientState;
        sAudioSource = audioSource;
        sPlayMode = playMode;
        sMusicSources = musicSources;
        sUsbStatus = usbStatus;
        sBtStatus = btStatus;
        sFmStatus = fmStatus;
        if (usbStatus == EPlayStatus.STATUS_PLAYING) {
            sResumeSource = EAudioSource.AUDIO_SOURCE_USB;
            sUsbStatus = EPlayStatus.STATUS_PAUSE;
        } else if (btStatus == EPlayStatus.STATUS_PLAYING) {
            sResumeSource = EAudioSource.AUDIO_SOURCE_BT;
            sBtStatus = EPlayStatus.STATUS_PAUSE;
        } else if (fmStatus == EPlayStatus.STATUS_PLAYING) {
            sResumeSource = EAudioSource.AUDIO_SOURCE_FM;
            sFmStatus = EPlayStatus.STATUS_PAUSE;
        }
        LogUtils.d("sResumeSource:" + sResumeSource);
    }

    //
    public static void setClientStatus(boolean status) {
        LogUtils.d("status:" + status);
        sIsClientOpen = status;
        SPUtils.PlayManagePut();
    }

    public static void setUsbPlayStatus(EPlayStatus status) {
        LogUtils.d("status:" + status);
        sUsbStatus = status;
        SPUtils.PlayManagePut();
    }

    public static void setBtPlayStatus(EPlayStatus status) {
        LogUtils.d("status:" + status);
        sBtStatus = status;
        SPUtils.PlayManagePut();
    }

    public static void setBtCallStatus(EPlayStatus status) {
        LogUtils.d("status:" + status);
        sBtCallStatus = status;
    }

    public static void setKwPlayStatus(EPlayStatus status) {
        LogUtils.d("status:" + status);
        sKwStatus = status;
    }

    public static void setFmPlayStatus(EPlayStatus status) {
        LogUtils.d("status:" + status);
        sFmStatus = status;
        SPUtils.PlayManagePut();
    }

    public static void setVideoPlayStatus(EPlayStatus status) {
        LogUtils.d("status:" + status);
        sVideoStatus = status;
    }

    public static void setAudioSource(EAudioSource source) {
        sAudioSource = source;
        SPUtils.PlayManagePut();
    }

    public static void setPlayMode(int mode) {
        sPlayMode = mode;
        SPUtils.PlayManagePut();
    }

    public static void setMusicSources(int sources) {
        sMusicSources = sources;
        SPUtils.PlayManagePut();
    }

    public static void setUsbStatus(EUsbStatus usbStatus) {
        if (mUsbStatus != usbStatus) {
            LogUtils.d("status:" + usbStatus);
            mUsbStatus = usbStatus;
        }
    }


    public static boolean isClientOpen() {
        return sIsClientOpen = true;
    }


    public static boolean needMediaResumePlay() {
        return sResumeSource == EAudioSource.AUDIO_SOURCE_USB
                || sResumeSource == EAudioSource.AUDIO_SOURCE_BT
                || sResumeSource == EAudioSource.AUDIO_SOURCE_FM;
    }


    public static boolean isUsbPlay() {
        return sUsbStatus == EPlayStatus.STATUS_PLAYING || sUsbStatus == EPlayStatus.STATUS_PLAY_PREPARING;
    }

    public static boolean isUsbPlaying() {
        return sUsbStatus == EPlayStatus.STATUS_PLAYING;
    }

    public static boolean isBtPlay() {
        return sBtStatus == EPlayStatus.STATUS_PLAYING;
    }

    public static boolean isBtCallPlay() {
        return sBtCallStatus == EPlayStatus.STATUS_PLAYING;
    }

    public static boolean isKwPlay() {
        return sKwStatus == EPlayStatus.STATUS_PLAYING;
    }

    public static boolean isFmPlay() {
        return sFmStatus == EPlayStatus.STATUS_PLAYING;
    }

    public static boolean isVideoPlay() {
        return sVideoStatus == EPlayStatus.STATUS_PLAYING;
    }

    //

    public static int getMusicStatus() {
        return PlayManage.isMediaPlay1() ? EPlayStatus.STATUS_PLAYING.getValue() : EPlayStatus.STATUS_PAUSE.getValue();
    }

    public static int getMediaStatus() {
        return PlayManage.isMediaPlay4() ? EPlayStatus.STATUS_PLAYING.getValue() : EPlayStatus.STATUS_PAUSE.getValue();
    }

    public static int getMusicSources() {
        return sMusicSources;
    }

    public static EAudioSource getAudioSource() {
        return sAudioSource;
    }

    public static int getPlayMode() {
        return sPlayMode;
    }

    public static EUsbStatus getUsbStatus() {
        return mUsbStatus;
    }

    //
    public static boolean checkAudioSource(EAudioSource source) {
        return sAudioSource == source;
    }

    public static boolean checkPlayMode(int mode) {
        return mode == sPlayMode;
    }

    public static boolean checkMusicSources(int sources) {
        return sources == sMusicSources;
    }

    public static boolean checkUsbUseful() {
        return mUsbStatus == EUsbStatus.INSERT || mUsbStatus == EUsbStatus.SCANNED;
    }

    public static boolean checkUsbUseful(EUsbStatus usbStatus) {
        return usbStatus == EUsbStatus.INSERT || usbStatus == EUsbStatus.SCANNED;
    }

    public static boolean checkUsbInsert() {
        return mUsbStatus == EUsbStatus.INSERT;
    }

    public static boolean checkUsbScanned() {
        return mUsbStatus == EUsbStatus.SCANNED;
    }

    /**
     * usb、bt
     *
     * @return
     */
    public static boolean isMediaPlay1() {
        return isUsbPlay() || isBtPlay();
    }

    /**
     * usb、bt、kw
     *
     * @return
     */
    public static boolean isMediaPlay2() {
        return isUsbPlay() || isBtPlay() || isKwPlay();
    }


    /**
     * usb、bt、kw、fm
     *
     * @return
     */
    public static boolean isMediaPlay4() {
        return isUsbPlay() || isBtPlay() || isKwPlay() || isFmPlay();
    }

    /**
     * usb、kw、fm video
     *
     * @return
     */
    public static boolean isMediaPlay6() {
        return isUsbPlay() || isKwPlay() || isFmPlay() || isVideoPlay();
    }


    public static void showStatus() {
        LogUtils.d("usb:" + sUsbStatus + " bt:" + sBtStatus + " fm:" + sFmStatus + " video:" + sVideoStatus + " kw:" + sKwStatus);
    }

}
