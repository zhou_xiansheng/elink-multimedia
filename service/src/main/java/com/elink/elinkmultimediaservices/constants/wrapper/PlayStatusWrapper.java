package com.elink.elinkmultimediaservices.constants.wrapper;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * PlayStatus 的包装类，用于在aidl中传递
 */
import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;

public class PlayStatusWrapper implements Parcelable {
    private final EPlayStatus status;

    public PlayStatusWrapper(EPlayStatus status) {
        this.status = status;
    }

    protected PlayStatusWrapper(Parcel in) {
        int statusValue = in.readInt();
        this.status = EPlayStatus.fromValue(statusValue);
    }

    public EPlayStatus getStatus() {
        return status;
    }

    public static final Creator<PlayStatusWrapper> CREATOR = new Creator<PlayStatusWrapper>() {
        @Override
        public PlayStatusWrapper createFromParcel(Parcel in) {
            return new PlayStatusWrapper(in);
        }

        @Override
        public PlayStatusWrapper[] newArray(int size) {
            return new PlayStatusWrapper[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status.getValue());
    }
}

