package com.elink.elinkmultimediaservices.constants.common;


/**
 * USB状态
 */
public enum EUSBState {
    STATE_NONE(0),
    STATE_MOUNTED(1),
    STATE_UNMOUNTED(2),
    STATE_SCANNER_STARTED(3),
    STATE_SCANNER_FINISHED(4),
    STATE_TATE_EJECT(5);

    private final int value;

    EUSBState(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public EUSBState fromValue(int value) {
        for (EUSBState usbState : values()) {
            if (usbState.getValue() == value) {
                return usbState;
            }
        }
        return STATE_NONE;
    }
}
