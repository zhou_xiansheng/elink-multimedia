package com.elink.elinkmultimediaservices.constants.radio;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 播放模式
 */
@IntDef()
@Retention(RetentionPolicy.SOURCE)
public @interface IRadioStatus {
    int STATUS_TYPE_SEEK = 0x00;
    int STATUS_TYPE_SCAN = 0x10;
    int STATUS_TYPE_POWER = 0x20;
    int STATUS_TYPE_TUNE = 0x30;
    //
    int SEEK_RUNNING = STATUS_TYPE_SEEK | 0x01;
    int SEEK_FINISHED = STATUS_TYPE_SEEK | 0x02;
    //
    int SCAN_RUNNING = STATUS_TYPE_SCAN | 0x01;
    int SCAN_FINISHED = STATUS_TYPE_SCAN | 0x02;
    //
    int POWER_UP = STATUS_TYPE_POWER | 0x02;
    int POWERING_UP = STATUS_TYPE_POWER | 0x03;
    int POWER_DOWN = STATUS_TYPE_POWER | 0x04;
    //
    int TUNE_RUNNING = STATUS_TYPE_TUNE | 0x01;
    int TUNE_FINISHED = STATUS_TYPE_TUNE | 0x02;
}
