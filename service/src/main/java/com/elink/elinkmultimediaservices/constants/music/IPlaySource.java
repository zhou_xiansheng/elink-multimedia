package com.elink.elinkmultimediaservices.constants.music;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

//播放源
@IntDef()
@Retention(RetentionPolicy.SOURCE)
public @interface IPlaySource {
   int UNKNOWN = 0;//未知
   int USB = 1;//USB
   int BT = 2;//蓝牙
   int KW = 8; //Kuwo
}
