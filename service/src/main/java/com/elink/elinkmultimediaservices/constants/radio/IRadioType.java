package com.elink.elinkmultimediaservices.constants.radio;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 播放模式
 */
@IntDef()
@Retention(RetentionPolicy.SOURCE)
public @interface IRadioType {
    int FM = 0x00;
    int AM = 0x01;
}
