package com.elink.elinkmultimediaservices.constants.common;


/**
 * USB状态
 */
public enum EBtBState {
    STATE_UNKNOWN(0),
    STATE_CONNECTING(1),
    STATE_CONNECTED(2),
    STATE_DISCONNECTED(3);

    private final int value;

    EBtBState(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static EBtBState fromValue(int value) {
        for (EBtBState btState : values()) {
            if (btState.getValue() == value) {
                return btState;
            }
        }
        return STATE_UNKNOWN;
    }
}
