package com.elink.elinkmultimediaservices.constants.radio;

public enum ERadioStatus {
    STATE_UNKNOWN(0x99),
    SEEK_RUNNING(0x01),
    SEEK_FINISHED(0x02),
    SCAN_RUNNING(0x10 | 0x01),
    SCAN_FINISHED(0x10 | 0x02),
    TUNE_RUNNING(0x30 | 0x01),
    TUNE_FINISHED(0x30 | 0x02);
    private final int value;

    ERadioStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static ERadioStatus fromValue(int value) {
        for (ERadioStatus radioStatus : values()) {
            if (radioStatus.getValue() == value) {
                return radioStatus;
            }
        }
        return STATE_UNKNOWN;
    }
}
