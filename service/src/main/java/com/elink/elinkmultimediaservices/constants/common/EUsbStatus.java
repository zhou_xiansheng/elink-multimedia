package com.elink.elinkmultimediaservices.constants.common;

public enum EUsbStatus {
    UNKNOWN(-1),
    DISCONNECT(0),
    INSERT(1),
    SCANNED(2);
    public final int value;

    EUsbStatus(int value) {
        this.value = value;
    }


    public int getValue() {
        return value;
    }

    public static EUsbStatus fromValue(int value) {
        for (EUsbStatus status : EUsbStatus.values())
            if (status.value == value)
                return status;
        return UNKNOWN;
    }

    public static boolean isStateUseFull(EUsbStatus state) {
        return state == INSERT || state == SCANNED;
    }
}
