package com.elink.elinkmultimediaservices.constants.common;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


/**
 * 播放状态 通用
 */
@IntDef()
@Retention(RetentionPolicy.SOURCE)
public @interface IPlaybackStatus {
    /**
     * 播放停止
     */
    int STATUS_STOPPED = 0x00;

    /**
     * 正在播放
     */
    int STATUS_PLAYING = 0x01;

    /**
     * 播放暂停
     */
    int STATUS_PAUSE = 0x02;
    /**
     * 播放准备中
     */
    int STATUS_PLAY_PREPARING = 0x03;
    /**
     * 暂停中
     */
    int STATUS_PAUSING = 0x04;

    /**
     * 未知播放状态
     */
    int STATUS_UNKNOWN = 0X99;
}
