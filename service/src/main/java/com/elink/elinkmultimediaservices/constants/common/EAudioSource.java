package com.elink.elinkmultimediaservices.constants.common;

/**
 * 具体播放源
 */

public enum EAudioSource {
    AUDIO_SOURCE_UNKNOWN(0x00),//无播放源
    AUDIO_SOURCE_USB(0x01),//USB Music
    AUDIO_SOURCE_BT(0x02),//BT Music
    AUDIO_SOURCE_FM(0x04),//FM
    AUDIO_SOURCE_KUWO(0x08),//KUWO
    AUDIO_SOURCE_VIDEO(0x016);//视频
    private final int value;

    EAudioSource(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    // Method to get the next enum value
    public EAudioSource getNext() {
        EAudioSource[] values = EAudioSource.values();
        int ordinal = this.ordinal();
        int nextOrdinal = (ordinal + 1) % values.length;
        return values[nextOrdinal];
    }
    public static EAudioSource fromValue(int value) {
        for (EAudioSource status : EAudioSource.values())
            if (status.value == value)
                return status;
        return AUDIO_SOURCE_UNKNOWN;
    }

    public boolean check(EAudioSource source) {
        return this==source;
    }
}
