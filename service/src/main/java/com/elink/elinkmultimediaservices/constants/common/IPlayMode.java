package com.elink.elinkmultimediaservices.constants.common;


import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 播放模式
 */
@IntDef()
@Retention(RetentionPolicy.SOURCE)
public @interface IPlayMode {

    /**
     * 未知
     */
    int PLAY_MODE_UNKNOWN = Integer.MAX_VALUE;
    /**
     * 音乐
     */
    int PLAY_MODE_MUSIC = 0x00;

    /**
     * FM
     */
    int PLAY_MODE_RADIO = 0x01;

    /**
     * 酷我音乐
     */
    int PLAY_MODE_KW = 0x02;
}
