package com.elink.elinkmultimediaservices.constants.music;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 播放模式
 */
@IntDef()
@Retention(RetentionPolicy.SOURCE)
public @interface IRepeatMode {
    /**
     * 顺序播放
     */
    int REPEAT_MODE_ORDER = 0x00;

    /**
     * 随机播放
     */
    int REPEAT_MODE_RANDOM = 0x01;

    /**
     * 单曲循环
     */
    int REPEAT_MODE_SINGLE = 0x02;

    /**
     * 未知播放模式
     */
    int REPEAT_MODE_UNKNOWN = 0x03;
}
