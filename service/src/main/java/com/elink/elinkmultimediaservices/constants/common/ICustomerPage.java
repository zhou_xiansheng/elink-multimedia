package com.elink.elinkmultimediaservices.constants.common;


import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 开机需要恢复客户端的页面
 */
@IntDef()
@Retention(RetentionPolicy.SOURCE)
public @interface ICustomerPage {
    /**
     * 无需恢复
     */
    String PAGE_NO = "page_no";

    /**
     * USB Music
     */
    String PAGE_USB = "page_usb";

    /**
     * BT Music
     */
    String PAGE_BT = "page_bt";

    /**
     * FM
     */
    String PAGE_FM = "page_fm";

    /**
     * KU WO
     */
    String PAGE_KW = "page_kw";
}
