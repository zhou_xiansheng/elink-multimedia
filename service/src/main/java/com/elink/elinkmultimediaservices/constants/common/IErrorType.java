package com.elink.elinkmultimediaservices.constants.common;


import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 异常类型
 * 回调客户端
 */
@IntDef()
@Retention(RetentionPolicy.SOURCE)
public @interface IErrorType {
    int USB_PLAY = 0x00;//usb 音乐播放错误
    int BT_PHONE = 0x01;//蓝牙通话中
    int USB_DISCONNECT = 0x02;//usb 未插入
}
