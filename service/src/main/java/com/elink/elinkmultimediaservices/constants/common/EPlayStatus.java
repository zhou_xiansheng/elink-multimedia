package com.elink.elinkmultimediaservices.constants.common;


/**
 * 状态枚举 通用
 */
public enum EPlayStatus {
    STATUS_STOPPED(0x00),//播放停止
    STATUS_PLAYING(0x01),//正在播放
    STATUS_PAUSE(0x02),//播放暂停
    STATUS_PLAY_PREPARING(0x03),//播放准备中
    STATUS_PAUSING(0x04),//暂停中
    STATUS_UNKNOWN(0X99);//未知播放状态
    private final int value;// 枚举常量关联的整数值

    // 枚举的构造函数
    EPlayStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public boolean isPlaying() {
        return this == STATUS_PLAYING;
    }

    public boolean isPause() {
        return this == STATUS_PAUSE;
    }

    // 添加 fromValue 方法来将 int 转换为 IPlayStatus
    public static EPlayStatus fromValue(int value) {
        for (EPlayStatus status : EPlayStatus.values())
            if (status.value == value)
                return status;
        return STATUS_UNKNOWN;
    }
}
