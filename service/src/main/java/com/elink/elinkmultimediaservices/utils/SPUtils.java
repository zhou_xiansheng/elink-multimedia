package com.elink.elinkmultimediaservices.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.beans.MusicPlayer;
import com.elink.elinkmultimediaservices.constants.common.EAudioSource;
import com.elink.elinkmultimediaservices.constants.common.EPlayStatus;
import com.elink.elinkmultimediaservices.constants.common.IPlayMode;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;
import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.google.gson.Gson;

/**
 * SharedPreferences 工具类
 */
public class SPUtils {
    private static final String SP_FILENAME = "sp_config";
    public static final String LATEST_CHANNEL = "latest_channel";//当前FM频率


    public static void put(String key, Object value) {
        SharedPreferences sp = MyApplication.getContext().getSharedPreferences(SP_FILENAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        if (value instanceof Boolean) {
            editor.putBoolean(key, (boolean) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (int) value);
        } else if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        } else if (value instanceof MusicFile) {
            Gson gson = new Gson();
            String userJson = gson.toJson(value);
            editor.putString(key, userJson);
        } else if (value instanceof MusicPlayer) {
            Gson gson = new Gson();
            String userJson = gson.toJson(value);
            editor.putString(key, userJson);
        } else {
            LogUtils.w("key:" + key + " value:" + value);
        }
        editor.apply();
    }

    public static void PlayManagePut() {
        put(PlayManage.IS_CLIENT_OPEN, PlayManage.sIsClientOpen);
        put(PlayManage.AUDIO_SOURCE, PlayManage.sAudioSource.getValue());
        put(PlayManage.PLAY_MODE, PlayManage.sPlayMode);
        put(PlayManage.MUSIC_SOURCE, PlayManage.sMusicSources);
        put(PlayManage.USB_STATUS, PlayManage.sUsbStatus.getValue());
        put(PlayManage.BT_STATUS, PlayManage.sBtStatus.getValue());
        put(PlayManage.FM_STATUS, PlayManage.sFmStatus.getValue());
        put(PlayManage.KW_STATUS, PlayManage.sKwStatus.getValue());
    }

    public static void PlayManageGet() {
        LogUtils.d("");
        PlayManage.setSpData(getBoolean(PlayManage.IS_CLIENT_OPEN, false)
                , EAudioSource.fromValue(getInt(PlayManage.AUDIO_SOURCE, EAudioSource.AUDIO_SOURCE_UNKNOWN.getValue()))
                , getInt(PlayManage.PLAY_MODE, IPlayMode.PLAY_MODE_UNKNOWN)
                , getInt(PlayManage.MUSIC_SOURCE, IPlaySource.UNKNOWN)
                , EPlayStatus.fromValue(getInt(PlayManage.USB_STATUS, EAudioSource.AUDIO_SOURCE_UNKNOWN.getValue()))
                , EPlayStatus.fromValue(getInt(PlayManage.BT_STATUS, EAudioSource.AUDIO_SOURCE_UNKNOWN.getValue()))
                , EPlayStatus.fromValue(getInt(PlayManage.FM_STATUS, EAudioSource.AUDIO_SOURCE_UNKNOWN.getValue())));
    }


    public static String getString(String key, String defValue) {
        SharedPreferences sp = MyApplication.getContext().getSharedPreferences(SP_FILENAME, Context.MODE_PRIVATE);
        return sp.getString(key, defValue);
    }

    public static String getString(String key) {
        return getString(key, null);
    }

    public static int getInt(String key, int defValue) {
        SharedPreferences sp = MyApplication.getContext().getSharedPreferences(SP_FILENAME, Context.MODE_PRIVATE);
        return sp.getInt(key, defValue);
    }

    private static int getInt(String key) {
        return getInt(key, Integer.MAX_VALUE);
    }

    public static boolean getBoolean(String key, boolean defValue) {
        SharedPreferences sp = MyApplication.getContext().getSharedPreferences(SP_FILENAME, Context.MODE_PRIVATE);
        return sp.getBoolean(key, defValue);
    }

    public static boolean getBoolean(String key) {
        return getBoolean(key, false);
    }

    public static float getFloat(String key, Float defValue) {
        SharedPreferences sp = MyApplication.getContext().getSharedPreferences(SP_FILENAME, Context.MODE_PRIVATE);
        return sp.getFloat(key, defValue);
    }

    public static float getFloat(String key) {
        return getFloat(key, 0.0f);
    }

    public static MusicFile getMusic(String key, String defValue) {
        SharedPreferences sp = MyApplication.getContext().getSharedPreferences(SP_FILENAME, Context.MODE_PRIVATE);
        String value = sp.getString(key, defValue);
        Gson gson = new Gson();
        return gson.fromJson(value, MusicFile.class);
    }

    public static MusicFile getMusic(String key) {
        Gson gson = new Gson();
        String gsonStr = gson.toJson(new MusicFile());
        return getMusic(key, gsonStr);
    }

    public static MusicPlayer getMusicPlayer(String key, String defValue) {
        SharedPreferences sp = MyApplication.getContext().getSharedPreferences(SP_FILENAME, Context.MODE_PRIVATE);
        String value = sp.getString(key, defValue);
        Gson gson = new Gson();
        return gson.fromJson(value, MusicPlayer.class);
    }

    public static MusicPlayer getMusicPlayer(String key) {
        Gson gson = new Gson();
        String gsonStr = gson.toJson(new MusicPlayer());
        return getMusicPlayer(key, gsonStr);
    }
}
