package com.elink.elinkmultimediaservices.utils;

import static com.elink.elinkmultimediaservices.utils.Constants.EJECTING;
import static com.elink.elinkmultimediaservices.utils.Constants.MOUNTED;
import static com.elink.elinkmultimediaservices.utils.Constants.SCAN_FINISH;

import android.os.SystemProperties;

import com.elink.elinkmultimediaservices.beans.PlayManage;
import com.elink.elinkmultimediaservices.constants.common.EUsbStatus;

public class SystemPropertiesUtils {
    private static final String OTG_STATE = "sys.bd.otg_state1";
    private static final String MEDIA_PAGE_RECOVERY = "ro.elink.multimedia_page_recovery";

    public static EUsbStatus checkUsbState() {
        int status = -1;
        try {
            status = Integer.parseInt(SystemProperties.get(OTG_STATE, String.valueOf(EJECTING)));
        } catch (Exception e) {
            LogUtils.w(e.getMessage());
        }
        if (status == SCAN_FINISH) {
            PlayManage.setUsbStatus(EUsbStatus.SCANNED);
        } else if (status == MOUNTED) {
            PlayManage.setUsbStatus(EUsbStatus.INSERT);
        } else {
            PlayManage.setUsbStatus(EUsbStatus.DISCONNECT);
        }
        return PlayManage.getUsbStatus();
    }

    public static boolean needCustomerResume() {
        String status = SystemProperties.get(MEDIA_PAGE_RECOVERY, "0");
        return "1".equals(status);
    }
}