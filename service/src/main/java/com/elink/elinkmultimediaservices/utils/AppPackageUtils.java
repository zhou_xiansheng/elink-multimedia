package com.elink.elinkmultimediaservices.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

public class AppPackageUtils {
    public static boolean isContainsApp(String packageName, String className, Context context) {
        Intent intent = new Intent();
        intent.setClassName(packageName, className);
        PackageManager packageManager = context.getPackageManager();
        return packageManager.resolveActivity(intent, 0) != null;
    }
    public static boolean isContainsApp(Intent intent, Context context) {
        PackageManager packageManager = context.getPackageManager();
        return packageManager.resolveActivity(intent, 0) != null;
    }
}
