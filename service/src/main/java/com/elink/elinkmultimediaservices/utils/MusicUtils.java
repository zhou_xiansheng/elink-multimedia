package com.elink.elinkmultimediaservices.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.provider.MediaStore;

import com.elink.elinkmultimediaservices.beans.MusicFile;
import com.elink.elinkmultimediaservices.constants.music.IPlaySource;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MusicUtils {

    private static final String COLUMN_ID = "id";
    private static final String COLUMN_FILE_NAME = "fileName";
    private static final String COLUMN_FILE_PATH = "filePath";
    private static final String COLUMN_FILE_TYPE = "fileType";
    private static final String COLUMN_CLASSIFY = "classify";
    private static final String COLUMN_ARTIST = "artist";
    private static final String COLUMN_ALBUM_COVER = "albumCover";
    private static final String COLUMN_DURATION = "duration";
    private static final String COLUMN_LAST_PLAYBACK_TIMESTAMP = "lastPlaybackTimestamp";
    @SuppressLint("StaticFieldLeak")
    public static Context context = MyApplication.getContext();
    private static final ExecutorService executorService = Executors.newFixedThreadPool(4);

    @SuppressLint("Range")
    public static List<MusicFile> musicScanner() {
        ArrayList<MusicFile> musics = new ArrayList<>();
        musics.addAll(getUsbMusicFromDB());
        return musics;
    }

    @SuppressLint("Range")
    private static List<MusicFile> getLocalMusic() {

        Cursor cursor;
        ArrayList<MusicFile> musics = new ArrayList<>();
        MusicFile music;
        //扫描本地音乐
        try {
            cursor = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    , null, null, null, MediaStore.Audio.AudioColumns.IS_MUSIC);
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                music = new MusicFile();
                music.fileName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME));//音乐名称，包含后缀
                music.artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                music.duration = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION));//音乐总时长，单位毫秒
                music.lastPlayProgress = cursor.getPosition();//当前播放位置
                music.path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));//音乐的绝对路径
                LogUtils.d("path:" + music.path);
                music.lyricPath = getLyricPath(music.path);
                music.playSource = IPlaySource.USB;
                music.albumCover = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM_ID)) + "";
                if (music.artist.trim().equals("<unknown>")) {
                    music.artist = "未知歌手";
                }
                musics.add(music);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return musics;
    }

    @SuppressLint("Range")
    private static List<MusicFile> getUsbMusicFromDB() {
        Cursor cursor = null;
        ArrayList<MusicFile> musics = new ArrayList<>();
        MusicFile music;
        try {
            Uri uri = Uri.parse("content://com.example.udiskprovider/music/1");
            String[] projection = {"DISTINCT " + COLUMN_FILE_PATH}; // 列名数组，包含 DISTINCT 关键字和 COLUMN_FILE_PATH 列
            cursor = context.getContentResolver().query(uri, projection, null, null,
                    COLUMN_FILE_PATH + " ASC");
            if (cursor != null)
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    music = new MusicFile();
                    music.fileName = cursor.getString(cursor.getColumnIndex(COLUMN_FILE_NAME));
                    music.path = cursor.getString(cursor.getColumnIndex(COLUMN_FILE_PATH));
                    music.artist = cursor.getString(cursor.getColumnIndex(COLUMN_ARTIST));
                    music.duration = cursor.getInt(cursor.getColumnIndex(COLUMN_DURATION));
                    // music.lyricPath = getLyricPath(music.path);
                    music.playSource = IPlaySource.USB;
                    if (music.artist == null || music.artist.trim().equals("<unknown>") || music.artist.trim().equals("UNKNOWN")) {
                        music.artist = "未知歌手";
                    }
                    musics.add(music);
                }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        LogUtils.d("size:" + musics.size());
        return musics;
    }

    @SuppressLint({"Range", "Recycle"})
    public static ArrayList<MusicFile> getMusicUnderTheFirstDirectory(String directoryPath) {
        LogUtils.d("directoryPath:" + directoryPath);
        LinkedHashSet<MusicFile> directories = new LinkedHashSet<>();
        ArrayList<MusicFile> musics = new ArrayList<>();
        Uri uri = Uri.parse("content://com.example.udiskprovider/music/1");
        try (Cursor cursor = MyApplication.getContext().getContentResolver().query(uri,
                null, null, new String[]{directoryPath}, null);) {
            while (cursor != null && cursor.moveToNext()) {
                if (musics.size() >= 10) {
                    //只播放 10 以内
                    break;
                }
                MusicFile music = new MusicFile();
                //时长不为 -1 ,且后缀进行过滤
                music.path = cursor.getString(cursor.getColumnIndex(COLUMN_FILE_PATH));
                String relativePath = music.path.substring(directoryPath.length() + 1);
                if (relativePath.contains("/")) {
                    String name = relativePath.split("/")[0];
                    music.isDir = true;
                    music.fileName = name;
                    music.path = directoryPath + "/" + name;
                    directories.add(music);
                } else {
                    music.isDir = false;
                    music.fileName = cursor.getString(cursor.getColumnIndex(COLUMN_FILE_NAME));
                    music.playSource = IPlaySource.USB;
                    try {
                        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                        retriever.setDataSource(music.path);
                        String duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION); // 获取时长（以毫秒为单位）
                        if (duration != null) {
                            music.duration = Integer.parseInt(duration);
                        }
                        music.artist = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST); // 获取艺术家
                        if (music.artist == null
                                || music.artist.trim().equals("<unknown>")
                                || music.artist.trim().equals("UNKNOWN")
                                || music.artist.trim().equals("")) {
                            music.artist = "未知歌手";
                        }
                        retriever.release();
                    } catch (Exception e) {
                        LogUtils.d("set retriever error " + music.path);
                    }
                    musics.add(music);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 遍历完当前目录，如果该目录下有 music 直接返回，没有就进入第一个子目录继续找
        if (musics.size() > 0) {
            return musics;
        } else {
            if (directories.size() > 0) {
                return getMusicUnderTheFirstDirectory(directories.iterator().next().path);
            }
        }
        return new ArrayList<>();
    }


    @SuppressLint({"Range", "Recycle"})
    public static List<MusicFile> getMusicByPath(String directoryPath) {
        ArrayList<MusicFile> musics = new ArrayList<>();
        Uri uri = Uri.parse("content://com.example.udiskprovider/music/1");
        try (Cursor cursor = MyApplication.getContext().getContentResolver().query(uri,
                null, null, new String[]{directoryPath}, null);) {
            while (cursor != null && cursor.moveToNext()) {
                MusicFile music = new MusicFile();
                music.isDir = false;
                music.fileName = cursor.getString(cursor.getColumnIndex(COLUMN_FILE_NAME));
                music.path = cursor.getString(cursor.getColumnIndex(COLUMN_FILE_PATH));
                music.artist = cursor.getString(cursor.getColumnIndex(COLUMN_ARTIST));
                music.duration = cursor.getInt(cursor.getColumnIndex(COLUMN_DURATION));
                music.lyricPath = getLyricPath(music.path);
                music.playSource = 1;
                if (music.artist == null || music.artist.trim().equals("<unknown>") || music.artist.trim().equals("UNKNOWN")) {
                    music.artist = "未知歌手";
                }
                //时长不为 -1 ,且后缀进行过滤
                String[] split = music.path.split("\\.");
                String ext = split[split.length - 1];
                if (music.duration > 0 && typeAdaptation(ext)) {
                    musics.add(music);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return musics;
    }

    /**
     * 得到音乐目录
     *
     * @param musicFiles
     * @return
     */
    public static List<MusicFile> getRootDirectory(List<MusicFile> musicFiles) {
        LogUtils.d("");
        ArrayList<MusicFile> musics = new ArrayList<>();
        HashSet<String> strings = new HashSet<>();
        for (MusicFile music : musicFiles
        ) {
            String path = music.path;
            String substring = path.substring(0, path.lastIndexOf("/"));
            strings.add(substring);
        }
        for (String s : strings
        ) {
            MusicFile musicFile = new MusicFile();
            musicFile.fileName = s.substring(s.lastIndexOf("/") + 1);
            musicFile.path = s;
            musicFile.isDir = true;
            musics.add(musicFile);
        }
        //目录名排序
        musics.sort((o1, o2) -> {
            int n = 0;
            while (o1.fileName.length() > n && o2.fileName.length() > n) {
                char c1 = o1.fileName.charAt(n);
                char c2 = o2.fileName.charAt(n);
                if (c1 != c2)
                    return c1 - c2;
                n++;
            }
            if (o1.fileName.length() >= n) {
                return -1;
            } else {
                return 1;
            }
        });
        return musics;
    }


    public static String getRelativePath(String path) {
        String pre = "/mnt/media_rw/";
        String res = null;
        if (path != null && path.startsWith(pre)) {
            int lastIndex = path.lastIndexOf(pre) + pre.length();
            int nextSlashIndex = path.indexOf("/", lastIndex);
            res = path.substring(nextSlashIndex);
        }
        return res;
    }

    /**
     * 根据音乐文件路径查找相应的歌词文件路径。
     *
     * @param musicPath 音乐文件的路径
     * @return 歌词文件的路径，如果找不到返回null
     */
    private static String getLyricPath(String musicPath) {
        // 从音乐文件路径中获取文件名，不包含扩展名
        String fileNameWithoutExtension = new File(musicPath).getName().replaceFirst("[.][^.]+$", "");
        // 获取文件所在的目录
        File musicDirectory = new File(musicPath).getParentFile();
        if (musicDirectory != null && musicDirectory.exists()) {
            File[] filesInDir = musicDirectory.listFiles();
            if (filesInDir != null) {
                for (File file : filesInDir) {
                    // 如果文件是一个歌词文件，且包含音乐名，可以不相同包含就行
                    if (file.getName().contains(fileNameWithoutExtension) && file.getName().endsWith(".lrc")) {
                        return file.getAbsolutePath();
                    }
                }
            }
        }
        return null;
    }


    public class MusicItem {
        public boolean isDirectory;
        public String path;
        public List<MusicItem> children;

        public MusicItem(String path, boolean isDirectory) {
            this.path = path;
            this.isDirectory = isDirectory;
            if (isDirectory) {
                this.children = new ArrayList<>();
            }
        }
    }


    /**
     * 为音乐建立数据结构
     *
     * @return
     */
    @SuppressLint("Range")
    public Map<String, MusicItem> getOrganizedMusicList() {
        Uri uri = Uri.parse("content://com.example.udiskprovider/music");
        String selection = MediaStore.Files.FileColumns.MEDIA_TYPE + "=" + MediaStore.Files.FileColumns.MEDIA_TYPE_AUDIO;
        String sortOrder = MediaStore.Files.FileColumns.DATA + " ASC";  // 按路径排序
        Cursor cursor = context.getContentResolver().query(uri, null, selection, null, sortOrder);
        Map<String, MusicItem> itemMap = new HashMap<>();
        while (cursor.moveToNext()) {
            String path = cursor.getString(cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA));
            MusicItem musicFileItem = new MusicItem(path, false);
            File parentFile = new File(path).getParentFile();
            while (parentFile != null && !itemMap.containsKey(parentFile.getAbsolutePath())) {
                MusicItem parentDirectoryItem = new MusicItem(parentFile.getAbsolutePath(), true);
                itemMap.put(parentFile.getAbsolutePath(), parentDirectoryItem);
                parentFile = parentFile.getParentFile();
            }
            if (parentFile != null) {
                String parentPath = parentFile.getAbsolutePath();
                MusicItem parentItem = itemMap.get(parentPath);
                if (parentItem != null && !parentItem.children.contains(musicFileItem)) {
                    parentItem.children.add(musicFileItem);
                }
            } else {
                itemMap.put(path, musicFileItem);
            }
        }
        cursor.close();
        // 组织目录层次结构
        Iterator<Map.Entry<String, MusicItem>> iterator = itemMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, MusicItem> entry = iterator.next();
            String path = entry.getKey();
            MusicItem item = entry.getValue();
            File parentFile = new File(path).getParentFile();
            if (parentFile != null) {
                String parentPath = parentFile.getAbsolutePath();
                MusicItem parentItem = itemMap.get(parentPath);
                if (parentItem != null && !parentItem.children.contains(item)) {
                    parentItem.children.add(item);
                    iterator.remove();
                }
            }
        }
        // 优化层级结构，裁剪掉不需要的
        iterator = itemMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, MusicItem> entry = iterator.next();
            MusicItem item = entry.getValue();
            if (item != null && item.children != null) {
                while (item.children.size() == 1 && item.children.get(0).isDirectory) {
                    item = item.children.get(0);
                }
                if (!item.path.equals(entry.getKey())) { // 如果顶级项与当前项不同，则更新映射
                    iterator.remove();
                    itemMap.put(item.path, item);
                }
            }
        }
        return itemMap;
    }

    // 查找指定目录下的文件
    public List<MusicItem> findFilesUnderDirectory(String directoryName) {
        Map<String, MusicItem> organizedMusicList = getOrganizedMusicList();
        for (MusicItem item : organizedMusicList.values()) {
            List<MusicItem> result = searchDirectory(directoryName, item);
            if (result != null) {
                return result;
            }
        }
        return new ArrayList<>();  // 如果没有找到匹配的目录，则返回空列表
    }

    private List<MusicItem> searchDirectory(String directoryName, MusicItem currentItem) {
        if (currentItem.path.equals(directoryName)) {
            return currentItem.children; // 找到了匹配的目录，返回其子文件和子目录
        }
        if (currentItem.isDirectory && currentItem.children != null) {
            for (MusicItem child : currentItem.children) {
                List<MusicItem> result = searchDirectory(directoryName, child);
                if (result != null) {
                    return result;
                }
            }
        }
        return null;  // 对于当前的目录项，没有找到匹配的子目录
    }

    /**
     * 查找当前文件夹和子文件夹下的所有音乐文件
     *
     * @param directoryName
     * @return
     */
    public List<MusicItem> findAllMusicFilesUnderDirectory(String directoryName) {
        List<MusicItem> immediateChildren = findFilesUnderDirectory(directoryName);
        List<MusicItem> allMusicFiles = new ArrayList<>();
        for (MusicItem item : immediateChildren) {
            collectMusicFiles(item, allMusicFiles);
        }
        return allMusicFiles;
    }

    private List<MusicItem> collectMusicFiles(MusicItem root, List<MusicItem> accumulator) {
        if (!root.isDirectory) {
            accumulator.add(root);  // 如果它是音乐文件，直接添加到结果列表
        } else if (root.children != null) {
            for (MusicItem child : root.children) {
                collectMusicFiles(child, accumulator);  // 如果它是目录，则递归地处理它的子文件和子目录
            }
        }
        return accumulator;
    }


    /**
     * 输出音乐文件的层级结构
     */
    public void myPrint() {
        getOrganizedMusicList().forEach((s, musicItem) -> printMusicItem(musicItem, ""));
    }

    private void printMusicItem(MusicItem item, String prefix) {
        System.out.println(prefix + (item.isDirectory ? "[Dir] " : "[File] ") + new File(item.path).getName());
        if (item.isDirectory && item.children != null) {
            for (MusicItem child : item.children) {
                printMusicItem(child, prefix + "  ");
            }
        }
    }

    private static final ArrayList<String> types = new ArrayList<>();

    static {
        //默认支持
        types.add("mp3");
        types.add("aac");
        types.add("amr");
        types.add("flac");
        types.add("midi");
        types.add("vorbis");
        types.add("opus");
        types.add("wav");
        types.add("alac");

        //扩展
        types.add("ogg");
    }

    public static boolean typeAdaptation(String type) {
        return types.contains(type.trim().toLowerCase());
    }


}
