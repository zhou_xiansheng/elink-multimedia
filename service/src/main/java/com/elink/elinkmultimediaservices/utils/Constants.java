package com.elink.elinkmultimediaservices.utils;

public class Constants {
    /**
     * usb 相关
     */
    //表相关
    public static final int tableCount = 3;//表数量
    public static final int table1 = 1;
    public static final int table2 = 2;
    public static final int table3 = 3;
    // 广播 类型
    public static final int SCAN_FF = -3;
    public static final int SCAN_F = -1;
    public static final int DETACHED = -4;
    public static final int ATTACHED = 4;
    public static final int SCAN_FINISH = 3;
    // usb 挂载
    public static final int UNMOUNTED = 0;
    public static final int MOUNTED = 2;
    public static final int EJECTING = 5;
    /**
     * 广播相关
     */
    public static final String STREAM_MUTE_CHANGED_ACTION = "android.media.STREAM_MUTE_CHANGED_ACTION";
    //电源管理相关
    public static final String ACTION_SHUTDOWN_HU = "android.intent.action.ACTION_SHUTDOWN_HU";
    public static final String ACTION_BOOT_HU = "android.intent.action.ACTION_BOOT_HU";
    //usb 扫描
    public static final String UDISK_PROVIDER = "action.elink.udisk.provider";
    public static final String ECGALLERY_VIDEO_STATE = "com.elink.ecgallery.videostate";
    //mode 键切换
    public static final String MEDIA_MODE_NEXT = "elink.action.ACTION_PLAYSOURCE_CHANGE";
}
